<?php

namespace App\Controller\Gestion\Evenement;

use App\Entity\OptionPurchase;
use App\Form\OptionPurchaseType;

use App\SearchData\OptionPurchaseData;
use App\SearchForm\OptionPurchaseForm;
use App\Repository\EvenementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\OptionPurchaseRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("gestion/evenement/option/purchase", name="gestion_evenement_option_purchase")
 */
class OptionPurchaseController extends AbstractController
{
    /**
     * @Route("/index/{id_evenement}", name="_index", methods={"GET"})
     */
    public function index($id_evenement, Request $request, PaginatorInterface $paginator, EvenementRepository $evenementRepository, OptionPurchaseRepository $optionPurchaseRepository): Response
    {
        $evenement=$evenementRepository->find($id_evenement);
        $data=new OptionPurchaseData();
        $form = $this->createForm(OptionPurchaseForm::class, $data);
        $form->handleRequest($request);
        $option_purchases=$paginator->paginate(
            $optionPurchaseRepository->findByEvenement($evenement, $data),
            $request->query->getInt('page', 1),
            $data->nb
        );
        return $this->render('gestion/evenement/option_purchase/index.html.twig', [
            'option_purchases' => $option_purchases,
            'evenement'=>$evenement,
            'form'=>$form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="option_purchase_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $optionPurchase = new OptionPurchase();
        $form = $this->createForm(OptionPurchaseType::class, $optionPurchase);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($optionPurchase);
            $entityManager->flush();

            return $this->redirectToRoute('option_purchase_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('option_purchase/new.html.twig', [
            'option_purchase' => $optionPurchase,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="option_purchase_show", methods={"GET"})
     */
    public function show(OptionPurchase $optionPurchase): Response
    {
        return $this->render('option_purchase/show.html.twig', [
            'option_purchase' => $optionPurchase,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="option_purchase_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, OptionPurchase $optionPurchase, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(OptionPurchaseType::class, $optionPurchase);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('option_purchase_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('option_purchase/edit.html.twig', [
            'option_purchase' => $optionPurchase,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="option_purchase_delete", methods={"POST"})
     */
    public function delete(Request $request, OptionPurchase $optionPurchase, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$optionPurchase->getId(), $request->request->get('_token'))) {
            $entityManager->remove($optionPurchase);
            $entityManager->flush();
        }

        return $this->redirectToRoute('option_purchase_index', [], Response::HTTP_SEE_OTHER);
    }
    /**
     * @Route("/livraisonGroupe/{ids}", name="_livraisonGroupe", methods={"GET","POST"})
     */
    public function livraisonGroupe($ids, OptionPurchaseRepository $optionPurchaseRepository, EntityManagerInterface $em): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $optionPurchase = $optionPurchaseRepository->find($id);
            if ($optionPurchase) {
                $optionPurchase->setDelivery(($optionPurchase->getDelivery() ? '0' : '1'));
            }
        }

        $em->flush();

        return new Response('L\'état de livraison des option a changé', Response::HTTP_OK);
    }
}
