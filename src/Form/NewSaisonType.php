<?php

namespace App\Form;

use App\Entity\Saison;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewSaisonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('saison')
            ->add('debut', DateType::class, [
                 'widget' => 'single_text',
                 'input' => 'datetime_immutable',
            ])
            ->add('fin', DateType::class, [
                 'widget' => 'single_text',
                 'input' => 'datetime_immutable',
            ])
            ->add('dupliquer', ChoiceType::class, [
                'label'=>'Dupliquer la dernière saison',
                'mapped'=>false,
                'choices' => [
                    'OUI' => true,
                    'NON' => false,
                ],
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Saison::class,
        ]);
    }
}
