<?php

namespace App\Form;

use App\Entity\Evenement;
use App\Entity\CategorieSection;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Eckinox\TinymceBundle\Form\Type\TinymceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class EvenementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => true,
            ])
            ->add('section', EntityType::class, [
                'label' => 'Ouvert à la section (sinon tous les utilisateurs):',
                'class' => CategorieSection::class,
                'placeholder' => 'selectionner une section',
                'required' => false,
            ])
            ->add('localisation', TextType::class, [
                'label' => 'Latitude , Longitude où se déroule l\'événement',
            ])
            ->add('description', TinymceType::class, [
                'label' => 'Information concernant l\'évenement',
            ])

            ->add('quota', IntegerType::class, [
                'label' => 'Nb maxi de reservation possible',
            ])
            ->add('reduction', IntegerType::class, [
                'label' => 'Reduction si reservation dans plusieurs conteneur',
            ])
            ->add('actif', CheckboxType::class, [
                'label' => 'Evénement visible aux utilisateurs',
                'required' => false,
            ])
            ->add('payant', CheckboxType::class, [
                'label' => 'Evénement payant',
                'required' => false,
            ])

            ->add('plan', CheckboxType::class, [
                'label' => 'Evenement avec un plan de salle',
                'required' => false,
            ])

            ->add('imageFile', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('largeur', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('hauteur', HiddenType::class, [
                'mapped' => false,
            ])


            ->add('envoyer', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);

        $ajout = function (FormInterface $form, $plan = null) {
            if ($plan == 1) {
                $form->add('imageFile', VichFileType::class, [
                    'label' => 'Choisir une image de fond pour le plan de salle',
                    'required' => false,
                    'allow_delete' => false,
                    'download_uri' => false,
                ])
                    ->add('largeur', IntegerType::class, [
                        'label' => 'Largeur du lieu en px',
                        'required' => false,
                    ])
                    ->add('hauteur', IntegerType::class, [
                        'label' => 'Hauteur du lieu en px',
                        'required' => false,
                    ]);
            } else {
                $form->add('imageFile', HiddenType::class, [
                    'mapped' => false,
                ])
                    ->add('largeur', HiddenType::class, [
                        'mapped' => false,
                    ])
                    ->add('hauteur', HiddenType::class, [
                        'mapped' => false,
                    ]);
            }
        };
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($ajout) {
            $evenement = $event->getData();
            $form = $event->getForm();
            if (!($evenement->getId() === null)) {
                $ajout($form, $evenement->getPlan());
            }
        });

        $builder->get('plan')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($ajout) {
                $plan = $event->getForm()->getData();
                $form = $event->getForm();
                $essai = $form->getParent()->getData();
                $ajout($form->getParent(), $plan);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evenement::class,
        ]);
    }
}
