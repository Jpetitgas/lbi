<?php

namespace App\Entity;

use App\Repository\ContainerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContainerRepository::class)
 */
class Container
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity=Evenement::class, inversedBy="containers",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $evenement;

    /**
     * @ORM\OneToMany(targetEntity=Place::class, mappedBy="container",cascade={"persist"}, orphanRemoval=true,fetch="EAGER")
     */
    private $places;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $x;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $y;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $angle;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $largeur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hauteur;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbLigne;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $z;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pricePlace;

    public function __construct()
    {
        $this->places = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEvenement(): ?Evenement
    {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): self
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * @return Collection|Place[]
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places[] = $place;
            $place->setContainer($this);
        }

        return $this;
    }

    public function removePlace(Place $place): self
    {
        if ($this->places->removeElement($place)) {
            // set the owning side to null (unless already changed)
            if ($place->getContainer() === $this) {
                $place->setContainer(null);
            }
        }

        return $this;
    }

    public function getX(): ?int
    {
        return $this->x;
    }

    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    public function getY(): ?int
    {
        return $this->y;
    }

    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    public function getAngle(): ?int
    {
        return $this->angle;
    }

    public function setAngle(?int $angle): self
    {
        $this->angle = $angle;

        return $this;
    }

    public function getLargeur(): ?int
    {
        return $this->largeur;
    }

    public function setLargeur(?int $largeur): self
    {
        $this->largeur = $largeur;

        return $this;
    }

    public function getHauteur(): ?int
    {
        return $this->hauteur;
    }

    public function setHauteur(?int $hauteur): self
    {
        $this->hauteur = $hauteur;

        return $this;
    }

    public function getNbLigne(): ?int
    {
        return $this->nbLigne;
    }

    public function setNbLigne(int $nbLigne): self
    {
        $this->nbLigne = $nbLigne;

        return $this;
    }

    public function getZ(): ?int
    {
        return $this->z;
    }

    public function setZ(?int $z): self
    {
        $this->z = $z;

        return $this;
    }

    public function getPricePlace(): ?float
    {
        return $this->pricePlace;
    }

    public function setPricePlace(?float $pricePlace): self
    {
        $this->pricePlace = $pricePlace;

        return $this;
    }

    public function __clone()
    {
        $this->setY($this->getY()+40)
            ->setNom($this->getNom());
        $places = new ArrayCollection();
        foreach ($this->places as $place) {
            /* @var Address $address */
            $place = clone $place;
            $place->setUtilisateur(null)
                ->setVerrouiller(false)
                ->setContainer($this);
            $places->add($place);
        }
        $this->places = $places;
    }
}
