<?php

namespace App\EventSubscriber;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Common\EventSubscriber;
use App\Entity\Inscription;
use App\Event\InscriptionUpdatedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class InscriptionUpdateSubscriber implements EventSubscriber
{
    private $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    // Cette méthode est requise par l'interface EventSubscriber de Doctrine
    public function getSubscribedEvents()
    {
        return [
            'preUpdate' => 'preUpdate',
        ];
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Inscription) {
            return;
        }

        if ($args->hasChangedField('cours')) {
            $event = new InscriptionUpdatedEvent($entity);
            $this->dispatcher->dispatch($event, InscriptionUpdatedEvent::NAME);
        }
    }
}
