<?php

namespace App\Controller\Utilisateur;

use App\Entity\Chat;
use App\Entity\ConsultationChat;
use App\Event\NouveauMessageChatEvent;
use App\Form\ChatType;
use App\Repository\ChatRepository;
use App\Repository\ConsultationChatRepository;
use App\Repository\CoursRepository;
use App\Repository\InscriptionRepository;
use App\Repository\UserChatNotificationRepository;
use App\Service\ChatService;
use App\Service\EnCours;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @Route("/utilisateur/chat", name="utilisateur_chat")
 */
class ChatController extends AbstractController
{
    private $entityManager;
    private $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    /**
     * @Route("/{id_cours}", name="_show", methods={"GET","POST"})
     */
    public function show(
        $id_cours,
        Request $request,
        ChatRepository $chatRepository,
        ChatService $chatService,
        InscriptionRepository $inscriptionRepository,
        EnCours $enCours,
        EventDispatcherInterface $dispatcher,
        ConsultationChatRepository $consultationChatRepository,
        CoursRepository $coursRepository,
        UserChatNotificationRepository $userChatNotificationRepository
    ): Response {
        $cours = $coursRepository->find($id_cours);
        $autorisation = $chatService->autorisation($cours);

        if (!$autorisation) {
            return $this->render('security/accesRefuse.html.twig');
        }

        $chatService->consultation($cours);
        $chats = $chatRepository->findBy(['cours' => $id_cours], ['created_at' => 'ASC']);

        $chat = new Chat();
        $formChat = $this->createForm(ChatType::class, $chat);
        $formChat->handleRequest($request);

        if ($formChat->isSubmitted() && $formChat->isValid()) {
            $chat->setExpediteur($this->getUser())
                ->setCours($cours);

            $this->entityManager->persist($chat);
            $this->entityManager->flush();

            $chatEvent = new NouveauMessageChatEvent($chat);
            $dispatcher->dispatch($chatEvent, 'ajout.message.chat');

            return $this->redirectToRoute('utilisateur_chat_show', ['id_cours' => $id_cours]);
        }

        // Marquer les messages comme lus pour ce cours
        $user = $this->security->getUser();
        $notification = $userChatNotificationRepository->findOneBy(['user' => $user]);
        if ($notification) {
            $notification->removeUnreadCourse($id_cours);
            if (empty($notification->getUnreadCourses())) {
                $notification->setHasUnreadMessages(false);
            }
            $this->entityManager->flush();
        }

        return $this->render('utilisateur/chat/show.html.twig', [
            'cours' => $cours,
            'chats' => $chats,
            'formChat' => $formChat->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{id_cours}", name="_delete", methods={"POST"})
     */
    public function delete($id_cours, Request $request, Chat $chat): Response
    {
        if ($this->isCsrfTokenValid('delete' . $chat->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($chat);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('utilisateur_chat_show', ['id_cours' => $id_cours]);
    }
}
