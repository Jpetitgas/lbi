<?php

namespace App\DataFixtures;

use App\Entity\Document;
use App\Entity\Dossier;
use App\Entity\Inscription;
use App\Entity\Paiement;
use App\Service\InfosDossierEncours;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class Dossiers extends Fixture implements DependentFixtureInterface
{
    private $faker;
    private $infosDossier;

    public function __construct(InfosDossierEncours $infosDossier)
    {
        $this->faker = Factory::create('fr_FR');
        $this->infosDossier = $infosDossier;
    }

    public function load(ObjectManager $manager)
    {
        $this->deleteFiles();

        for ($d = 1; $d < 30; ++$d) {
            $dossier = new Dossier();
            $dossier->setAdherent($this->getReference('utilisateur_'.$d))
                ->setSaison($this->getReference('saison'))
                ->setBulletinValide($this->faker->boolean())
                ->setDossierVerrouille(false)
                ->setLocked(false);
            if ($dossier->getBulletinValide() == true) {
                $nomBulletin = $this->bulletin();
                $dossier->setBulletinAdhesion($nomBulletin)
                ->setBulletinValide($this->faker->boolean())
                ->setLocked(false);
            }

            $manager->persist($dossier);
            for ($p = 1; $p <= 3; ++$p) {
                $inscription = new Inscription();
                $a = rand(1, 7);
                $inscription
                    ->setDossier($dossier)
                    ->setPratiquant($this->getReference('utilisateur_'.$d.'_pratiquant_'.$p))
                    ->setActivite($this->getReference('activite'.$a))
                    ->setCertificatObligatoire($this->getReference('activite'.$a)->getCertificatObligatoire())
                    ->setCours($this->getReference('cours_'.$a.'_'.rand(1, 2)))
                    ->setStatut('LISTE PRINCIPALE')
                    ->setDateChangementStatut(new DateTime())
                    ->setCertificatValide(false)
                    ->setTransmisAssureur((false))
                    ->setCanceled(false);
                if ($inscription->getCertificatObligatoire() == true) {
                    if ($this->faker->boolean() == true) {
                        $nomCertificat = $this->certificat();
                        $document = new Document();
                        $document->setType($this->getReference('doc'))
                            ->setDesignation('certif')
                            ->setDateCertificat($this->faker->dateTimeThisMonth())
                            ->setNom($nomCertificat)
                            ->setPratiquant($inscription->getPratiquant());
                        $manager->persist($document);
                        $inscription->setDocument($document)
                            ->setCertificatValide($this->faker->boolean());
                    }
                }
                $manager->persist($inscription);
            }
            $manager->flush();
            if ($this->faker->boolean()) {
                $cotisation = $this->infosDossier->cotisation($dossier);
                $paiement=new Paiement();
                $paiement->setBanque($this->getReference('stripe'))
                    ->setDossier($dossier)
                    ->setEncaisse(true)
                    ->setMontant($cotisation['total'])
                    ->setTypeDePaiement($this->getReference('cb'))
                    ->setSaison($this->getReference('saison'));
                $manager->persist($paiement);
                if ($this->infosDossier->bulletinPresent($dossier)) {
                    $dossier->setBulletinValide(true)
                    ->setDossierVerrouille(true);
                }


                $manager->flush();
            }
        }
    }

    public function getDependencies()
    {
        return [
            Parametres::class,
            Utilisateurs::class,
            Yoga::class,
            Danse::class,
            Theatre::class,
        ];
    }

    private function bulletin()
    {
        $origine = './public/assets/sauvegarde/doc/bulletin.pdf';
        $destination = './public/assets/uploads/';
        $file = $this->faker->md5();
        copy($origine, $destination.$file.'.pdf');

        return $file.'.pdf';
    }

    private function certificat()
    {
        $origine = './public/assets/sauvegarde/doc/certificat.pdf';
        $destination = './public/assets/uploads/';
        $file = $this->faker->md5();
        copy($origine, $destination.$file.'.pdf');

        return $file.'.pdf';
    }

    private function deleteFiles()
    {
        $folder = './public/assets/uploads/';

        //Get a list of all of the file names in the folder.
        $files = glob($folder.'/*');

        //Loop through the file list.
        foreach ($files as $file) {
            //Make sure that this is a file and not a directory.
            if (is_file($file)) {
                //Use the unlink function to delete the file.
                unlink($file);
            }
        }
    }
}
