<?php

namespace App\Repository;

use App\Entity\Saison;
use App\Entity\Presence;
use App\SearchData\PresenceTotalData;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Presence|null find($id, $lockMode = null, $lockVersion = null)
 * @method Presence|null findOneBy(array $criteria, array $orderBy = null)
 * @method Presence[]    findAll()
 * @method Presence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PresenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Presence::class);
    }

    public function presenceParCours($cours, $saison)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.cours = :cours')
            ->setParameter('cours', $cours)
            ->andWhere('p.saison = :saison')
            ->setParameter('saison', $saison)
            ->join('p.pratiquant', 'pr')
            ->select('COUNT(pr.id) as nb, pr.nom, pr.prenom')
            ->groupBy('pr.id')
            ->orderBy('pr.nom', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function presences(Saison $saison, PresenceTotalData $data)
    {
        $query = $this
            ->createQueryBuilder('p')
            ->select('p', 'pra', 'c')
            ->join('p.cours', 'c')
            ->join('p.pratiquant', 'pra')
            ->where('p.saison = :saison')
            ->setParameter('saison', $saison);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('pra.nom LIKE :q or pra.prenom LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }

        return $query->getQuery();
    }
    public function countByInscription($inscription, $saison)
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.pratiquant = :pratiquant')
            ->andWhere('p.cours = :cours')
            ->andWhere('p.saison = :saison')
            ->setParameter('pratiquant', $inscription->getPratiquant())
            ->setParameter('cours', $inscription->getCours())
            ->setParameter('saison', $saison)
            ->getQuery()
            ->getSingleScalarResult();
    }
    // /**
    //  * @return Presence[] Returns an array of Presence objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Presence
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
