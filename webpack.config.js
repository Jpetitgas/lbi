const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('base', './assets/base.js')
    /*.addEntry('tableau', './assets/tableau.js')*/
    .addEntry('tableauBis', './assets/tableauBis.js')
    
    //form
    .addEntry('formInscription', './assets/js/form/formInscription.js')
    .addEntry('formUtilisateur', './assets/js/form/formUtilisateur.js')
    .addEntry('formRegleTarifaire', './assets/js/form/formRegleTarifaire.js')
    .addEntry('formEvenement', './assets/js/form/formEvenement.js')
    // site 
    .addEntry('site', './assets/site/accueil.js')
    .addEntry('article', './assets/site/article.js')
    .addEntry('menu', './assets/site/menu.js')
    // Gestion
    .addEntry('gestion', './assets/gestion/gestion.js')
    .addEntry('accueil', './assets/gestion/accueil.js')
            //Dossier
    .addEntry('indexDossierGestion', './assets/gestion/dossier/index.js')
    .addEntry('editDossierGestion', './assets/gestion/dossier/edit.js')
            //Inscription    
    .addEntry('indexInscriptionGestion', './assets/gestion/inscription/index.js')
            //Boutique       
    .addEntry('indexPurchasePaiement', './assets/gestion/boutique/paiement/purchase/index.js')
    .addEntry('boutiqueGestion', './assets/gestion/boutique/boutique.js')
            //evenement
    .addEntry('editEvenementGestion', './assets/gestion/evenement/edit.js')
    .addEntry('editPlan', './assets/gestion/evenement/plan/edit.js')
    .addEntry('indexEvenement', './assets/gestion/evenement/index.js')
    .addEntry('indexEvenementPaiement', './assets/gestion/evenement/paiement/evenement/index.js')
            //utilisateur
    
    //intervenant
        //presence
    .addEntry('indexPresence', './assets/intervenant/presence/index.js')

    //utilisateur
    .addEntry('utilisateur', './assets/utilisateur/utilisateur.js')
    .addEntry('boutique', './assets/utilisateur/boutique.js')    
        //chat
    .addEntry('showChat', './assets/utilisateur/chat/show.js')
        //gestion
    .addEntry('editDossier', './assets/utilisateur/dossier/edit.js')
        //boutique
    .addEntry('paiementBoutique', './assets/utilisateur/boutique/purchase/paiement.js')
        //evenement
    .addEntry('editEvenement', './assets/utilisateur/evenement/edit.js')
        //message
    .addEntry('newMessage', './assets/utilisateur/message/new.js')
    

//tableau
    .addEntry('tableauFree', './assets/gestion/tableauFree.js')
   /*.addEntry('indexUtilisateur', './assets/js/gestion/tableau/indexUtilisateur.js')
    .addEntry('indexDossier', './assets/js/gestion/tableau/indexDossier.js')
    .addEntry('indexInscription', './assets/js/gestion/tableau/indexInscription.js')
    .addEntry('indexPaiement', './assets/js/gestion/tableau/indexPaiement.js')
    .addEntry('indexPaiementBoutique', './assets/js/gestion/tableau/indexPaiementBoutique.js')
    .addEntry('indexPaiementEvenement', './assets/js/gestion/tableau/indexPaiementEvenement.js')
    .addEntry('indexActivite', './assets/js/gestion/tableau/indexActivite.js')
    .addEntry('indexEnvoi', './assets/js/utilisateur/tableau/indexEnvoi.js')
    .addEntry('indexPurchase', './assets/js/gestion/tableau/indexPurchase.js')
    .addEntry('indexPresent', './assets/js/intervenant/tableau/indexPresent.js')*/

    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    .enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    //.splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // enables Sass/SCSS support
    .enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    //.enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()
    //.addEntry('inscriptionGestion', './assets/js/inscriptionGestionType.js')
    //.addEntry('inscriptionAdherent', './assets/js/inscriptionType.js')

    .copyFiles([
        {from: './node_modules/ckeditor/', to: 'ckeditor/[path][name].[ext]', pattern: /\.(js|css)$/, includeSubdirectories: false},
        {from: './node_modules/ckeditor/adapters', to: 'ckeditor/adapters/[path][name].[ext]'},
        {from: './node_modules/ckeditor/lang', to: 'ckeditor/lang/[path][name].[ext]'},
        {from: './node_modules/ckeditor/plugins', to: 'ckeditor/plugins/[path][name].[ext]'},
        {from: './node_modules/ckeditor/skins', to: 'ckeditor/skins/[path][name].[ext]'}
    ])
;

module.exports = Encore.getWebpackConfig();
