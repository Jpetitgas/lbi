<?php

namespace App\Controller\Gestion;

use App\Entity\Saison;
use App\Form\NewSaisonType;
use App\Form\SaisonType;
use App\Repository\ActiviteRepository;
use App\Repository\CoursRepository;
use App\Repository\SaisonRepository;
use App\Service\EnCours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/saison", name="gestion_saison")
 */
class SaisonController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(SaisonRepository $saisonRepository): Response
    {
        return $this->render('gestion/saison/index.html.twig', [
            'saisons' => $saisonRepository->findBy([], ['saison' => 'DESC']),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request, EnCours $enCours, ActiviteRepository $activiteRepository, CoursRepository $coursRepository): Response
    {
        $avantDerniereSaison = $enCours->avantDerniereSaison();
        $saison = new Saison();
        $form = $this->createForm(NewSaisonType::class, $saison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($saison);
            $entityManager->flush();

            if ($form->get('dupliquer')->getData()) {
                return $this->redirectToRoute('gestion_saison_dupliquer', ['id_saison' => $avantDerniereSaison->getId()]);
            }

            return $this->redirectToRoute('gestion_saison_index');
        }

        return $this->render('gestion/saison/new.html.twig', [
            'saison' => $saison,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Saison $saison, EnCours $enCours): Response
    {
        if ($enCours->saison() !== $saison) {
            $this->addFlash('danger', 'Vous ne pouvez modifier cette saison!');
            return $this->redirectToRoute('gestion_saison_index');
        }
        $form = $this->createForm(SaisonType::class, $saison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_saison_index');
        }

        return $this->render('gestion/saison/edit.html.twig', [
            'saison' => $saison,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/dupliquer/{id_saison}", name="_dupliquer", methods={"GET", "POST"})
     */
    public function dupliquer($id_saison, SaisonRepository $saisonRepository)
    {
        $saison = $saisonRepository->find($id_saison);
        $em = $this->getDoctrine()->getManager();
        foreach ($saison->getRegleTarifaires() as $regle) {
            $nouvelleRegle = clone $regle;
            $em->persist($nouvelleRegle);
        }
        foreach ($saison->getActivites() as $activite) {
            $nouvelleactivite = clone $activite;
            $em->persist($nouvelleactivite);
            foreach ($activite->getCours() as $cour) {
                $nouceauCour = clone $cour;
                $nouceauCour->setActivite($nouvelleactivite);
                $em->persist($nouceauCour);
            }
        }
        $em->flush();

        return $this->redirectToRoute('gestion_saison_index');
    }
}
