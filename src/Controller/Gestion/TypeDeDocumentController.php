<?php

namespace App\Controller\Gestion;

use App\Entity\TypeDeDocument;
use App\Form\TypeDeDocumentType;
use App\Repository\TypeDeDocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/type_document", name="gestion_type_document")
 */
class TypeDeDocumentController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(TypeDeDocumentRepository $typeDeDocumentRepository): Response
    {
        return $this->render('gestion/type_document/index.html.twig', [
            'type_de_documents' => $typeDeDocumentRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeDeDocument = new TypeDeDocument();
        $formTypeDeDocument = $this->createForm(TypeDeDocumentType::class, $typeDeDocument);
        $formTypeDeDocument->handleRequest($request);

        if ($formTypeDeDocument->isSubmitted() && $formTypeDeDocument->isValid()) {
            $typeDeDocument->getVerrouille(false);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeDeDocument);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_type_document_index');
        }

        return $this->render('gestion/type_document/new.html.twig', [
            'type_de_document' => $typeDeDocument,
            'formTypeDeDocument' => $formTypeDeDocument->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeDeDocument $typeDeDocument): Response
    {
        $formTypeDeDocument = $this->createForm(TypeDeDocumentType::class, $typeDeDocument);
        $formTypeDeDocument->handleRequest($request);

        if ($formTypeDeDocument->isSubmitted() && $formTypeDeDocument->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_type_document_index');
        }

        return $this->render('gestion/type_document/edit.html.twig', [
            'type_de_document' => $typeDeDocument,
            'formTypeDeDocument' => $formTypeDeDocument->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, TypeDeDocument $typeDeDocument): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeDeDocument->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeDeDocument);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_type_document_index');
    }
}
