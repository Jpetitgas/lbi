<?php

namespace App\Service;

use App\Entity\Utilisateur;
use App\Repository\UserChatNotificationRepository;
use Symfony\Component\Security\Core\Security;

class InAppNotificationService
{
    private $security;
    private $userChatNotificationRepository;

    public function __construct(Security $security, UserChatNotificationRepository $userChatNotificationRepository)
    {
        $this->security = $security;
        $this->userChatNotificationRepository = $userChatNotificationRepository;
    }

    public function getUnreadNotificationsCount(): int
    {
        $user = $this->security->getUser();
        if (!$user instanceof Utilisateur) {
            return 0;
        }

        $notification = $this->userChatNotificationRepository->findOneBy(['user' => $user]);
        if (!$notification || !$notification->getHasUnreadMessages()) {
            return 0;
        }

        return count($notification->getUnreadCourses());
    }
}