<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DeleteAndRecreateDatabaseWithStructureAndDataCommand extends Command
{
    protected static $defaultName = 'app:clean-db';
    protected static $defaultDescription = 'supprime et recrée la bd et relance une fixture';

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);


        $io->section("Suppression de la bd puis creation d'une nouvelle avec structure et données");

        $this->runSymfonyCommand($input, $output, 'doctrine:database:drop', true);
        $this->runSymfonyCommand($input, $output, 'doctrine:database:create');

        $this->runSymfonyCommand($input, $output, 'doctrine:migration:migrate');
        $this->runSymfonyCommand($input, $output, 'doctrine:fixture:load');

        $io->success('RAS =>base de donnée toute propre avec ses données');

        return Command::SUCCESS;
    }

    private function runSymfonyCommand(InputInterface $input, OutputInterface $output, string $command, bool $forceOption= false): void
    {
        $application=$this->getApplication();
        if (!$application) {
            throw new \LogicException("pas d'application:(");
        }
        $command=$application->find($command);
        if ($forceOption) {
            $input=new ArrayInput(['--force'=>true]);
        }
        $input->setInteractive(false);
        $command->run($input, $output);
    }
}
