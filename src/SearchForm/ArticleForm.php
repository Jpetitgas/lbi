<?php

namespace App\SearchForm;

use App\Entity\Menu;
use App\Entity\Cours;
use App\Entity\Activite;
use App\Entity\Utilisateur;
use App\Entity\CategoryArticle;
use App\SearchData\ArticleData;
use Doctrine\ORM\EntityRepository;
use App\SearchData\InscriptionData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ArticleForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('q', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Rechercher',
                ],
            ])

            ->add('categorie', EntityType::class, [
               'required' => false,
               'class' => CategoryArticle::class,
               'multiple' => false,
            ])
            ->add('menu', EntityType::class, [
               'required' => false,
               'class' => Menu::class,
               'multiple' => false,
            ])

            ->add('nb', IntegerType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArticleData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
