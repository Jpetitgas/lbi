<?php

namespace App\Form;

use App\Entity\Inscription;
use App\Entity\TypeDeDocument;
use App\Repository\InscriptionRepository;
use App\Service\EnCours;
use Symfony\Component\Form\AbstractType;
use Symfony\UX\Dropzone\Form\DropzoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class DocumentType extends AbstractType
{
    protected $inscriptionRepository;

    public function __construct(InscriptionRepository $inscriptionRepository)
    {
        $this->inscriptionRepository=$inscriptionRepository;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $pratiquant=$options['pratiquant'];
        $dossier=$options['dossier'];
        $inscriptions=$this->inscriptionRepository->findby(['dossier'=>$dossier,'pratiquant'=>$pratiquant,'CertificatObligatoire'=>true]);
        $builder
            ->add('type', EntityType::class, [
                'class' => TypeDeDocument::class,
                'placeholder' => 'selectionner un type de document',
                ])
             ->add('designation', TextType::class, [
                'required' => true,
            ])
            ->add('dateCertificat', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('image', DropzoneType::class, [
                'label' => false,
                'mapped' => false,
                'required' => false,
                'attr' => ['class' => 'form-control-file'],
                'constraints' => [
                            new File([
                                'maxSize' => '5M',
                                'mimeTypes' => [
                                    'application/pdf',
                                ],
                                'mimeTypesMessage' => 'Merci de télécharger un fichier pdf',
                            ]),
                        ],
            ])
             ->add('inscription', EntityType::class, [
                'class' => Inscription::class,
                'mapped'=>false,
                'required' => false,
                'label' => 'Sélectionner l\'activité qui doit être attachée à ce document',
                'choices' => $inscriptions,
                ])
            ->add('ajouter', SubmitType::class, [
                'label' => 'Ajouter une nouvelle activité',
                'attr' => [
                    'class' => 'btn btn-secondary',
                ],
            ])
            ->add('envoyer', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'pratiquant'=>false,
            'dossier'=>false
        ]);
    }
}
