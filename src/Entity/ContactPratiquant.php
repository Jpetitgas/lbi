<?php

namespace App\Entity;

use App\Repository\ContactPratiquantRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContactPratiquantRepository::class)
 */
class ContactPratiquant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $portable;

    /**
     * @ORM\ManyToOne(targetEntity=Pratiquant::class, inversedBy="contactPratiquants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pratiquant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPortable(): ?string
    {
        return $this->portable;
    }

    public function setPortable(string $portable): self
    {
        $this->portable = $portable;

        return $this;
    }

    public function getPratiquant(): ?Pratiquant
    {
        return $this->pratiquant;
    }

    public function setPratiquant(?Pratiquant $pratiquant): self
    {
        $this->pratiquant = $pratiquant;

        return $this;
    }
    public function __toString(): string
    {
        return $this->getNom();
    }
}
