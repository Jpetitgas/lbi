<?php

namespace App\EventListener;

use App\Entity\Activite;
use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class ActiviteListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison = $encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $activite = $event->getObject();

        if ($activite instanceof Activite) {
            if ($this->saison) {
                $activite->setSaison($this->saison);
            }
        }
    }
}
