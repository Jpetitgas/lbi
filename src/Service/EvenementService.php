<?php

namespace App\Service;

use App\Entity\CategorieSection;
use App\Entity\Evenement;
use App\Entity\Inscription;
use App\Entity\OptionPurchase;
use App\Entity\Utilisateur;
use App\Repository\ContainerRepository;
use App\Repository\OptionPurchaseRepository;
use App\Repository\PaiementEvenementRepository;
use App\Repository\PlaceRepository;
use Doctrine\Persistence\ManagerRegistry;

class EvenementService
{
    protected $containerRepository;
    protected $placeRepository;
    protected $paiementEvenementRepository;
    protected $encours;
    protected $optionPurchaseRepository;
    protected $doctrine;

    public function __construct(EnCours $encours, ManagerRegistry $doctrine, OptionPurchaseRepository $optionPurchaseRepository, ContainerRepository $containerRepository, PlaceRepository $placeRepository, PaiementEvenementRepository $paiementEvenementRepository)
    {
        $this->containerRepository = $containerRepository;
        $this->placeRepository = $placeRepository;
        $this->paiementEvenementRepository = $paiementEvenementRepository;
        $this->encours = $encours;
        $this->optionPurchaseRepository = $optionPurchaseRepository;
        $this->doctrine = $doctrine;
    }

    public function placesEvenement(Evenement $evenement)
    {
        $containers = $evenement->getContainers();
        $totalPlace = 0;
        $placeLibre = 0;
        if ($containers) {
            foreach ($containers as $container) {
                $places = $container->getPlaces();
                foreach ($places as $place) {
                    if ($place->getVerrouiller() == 0) {
                        if ($place) {
                            ++$totalPlace;
                            if ($place->getUtilisateur() == null) {
                                ++$placeLibre;
                            }
                        }
                    }
                }
            }
        }
        $statut = ['Nb place' => $totalPlace, 'Nb place libre' => $placeLibre];

        return $statut;
    }

    public function reservation(Utilisateur $utilisateur, Evenement $evenement)
    {
        $reservations = [];
        foreach ($evenement->getContainers() as $container) {
            foreach ($container->getPlaces() as $place) {
                if ($place->getUtilisateur() === $utilisateur) {
                    array_push($reservations, $place);
                }
            }
        }

        return $reservations;
    }

    public function montantTotalReservation(Utilisateur $utilisateur, Evenement $evenement)
    {
        $reservations = $this->reservation($utilisateur, $evenement);
        $reservationsMemoire = $reservations;
        $totalReservation = 0;
        $containers = [];

        /** @var $place Place */
        foreach ($reservations as $place) {
            $container = $place->getContainer();
            $containerName = $container->getNom();

            // Ajouter le conteneur à la liste des conteneurs utilisés
            if (!isset($containers[$containerName])) {
                $containers[$containerName] = [];
            }
            // Ajouter un 1 pour chaque place réservée
            $containers[$containerName][] = 1;
        }

        // Calculer la somme des colonnes
        $maxColumns = 0;
        foreach ($containers as $containerName => $reservations) {
            $maxColumns = max($maxColumns, count($reservations));
        }

        $sumLine = array_fill(0, $maxColumns, 0);
        $reductionLine = array_fill(0, $maxColumns, 0);

        foreach ($containers as $containerName => $reservations) {
            for ($i = 0; $i < count($reservations); $i++) {
                $sumLine[$i] += $reservations[$i];
            }
        }

        // Calculer le nombre de réductions pour chaque colonne
        foreach ($sumLine as $i => $sum) {
            $reductionLine[$i] = max(0, $sum - 1);
        }

        // Calculer la somme des réductions
        $nbReduction = array_sum($reductionLine);

        $reduction = $evenement->getReduction();
        $reductionAmount = $nbReduction * $reduction;

        // Calculer le montant total des réservations
        foreach ($reservationsMemoire as $place) {
            $totalReservation += $place->getContainer()->getPricePlace();
        }

        // Appliquer les réductions au total
        $totalReservation -= $reductionAmount;

        return [
            'totalReservation' => $totalReservation,
            'reductionAmount' => $reductionAmount,
        ];
    }


    public function montantTotalPaiement(Utilisateur $utilisateur, Evenement $evenement)
    {
        $paiements = $this->paiementEvenementRepository->findBy(['utilisateur' => $utilisateur, 'evenement' => $evenement]);
        $totalPaiement = 0;
        foreach ($paiements as $paiement) {
            $totalPaiement += $paiement->getMontant();
        }

        return $totalPaiement;
    }

    public function autorisationEvenement(Utilisateur $utilisateur, CategorieSection $section = null)
    {
        if ($section == null) {
            return true;
        }
        /** @var Dossier $dossier */
        $dossier = $this->encours->dossierAdherent($utilisateur);

        if ($dossier) {
            $inscriptions = $dossier->getInscriptions();
            foreach ($inscriptions as $inscription) {
                /** @var Inscription $inscription */
                $activite = $inscription->getActivite();
                $sec = $activite->getSection();
                $cat = $sec->getCategorieSection();
                if ($cat === $section) {
                    return true;
                }
            }
        }

        return false;
    }

    public function montantOptions(Utilisateur $utilisateur, Evenement $evenement)
    {
        $optionPurchases = $this->optionPurchaseRepository->findAllOptionByEvenementAndUtilisateur($evenement, $utilisateur);
        $montantOptions = 0;
        if ($optionPurchases) {
            /** @var OptionPurchase $option */
            foreach ($optionPurchases as $option) {
                $montantOptions += $option->getOptionEvenement()->getPrice();
            }
        }

        return $montantOptions;
    }

    public function deletedOptions(Evenement $evenement, Utilisateur $utilisateur)
    {
        $optionPurchases = $this->optionPurchaseRepository->findAllOptionByEvenementAndUtilisateur($evenement, $utilisateur);
        $entityManager = $this->doctrine->getManager();
        if ($optionPurchases) {
            foreach ($optionPurchases as $option) {
                $entityManager->remove($option);
                $entityManager->flush();
            }
        }
    }

    public function solded(Utilisateur $utilisateur, Evenement $evenement)
    {
        $montantOptions = $this->montantOptions($utilisateur, $evenement);
        $montantReservation = $this->montantTotalReservation($utilisateur, $evenement)['totalReservation'];
        $montantPaiement = $this->montantTotalPaiement($utilisateur, $evenement);
        $solded = ($montantPaiement - ($montantOptions + $montantReservation)) == 0 ? 'Oui' : 'Non';

        return $solded;
    }

    public function utilisateurSansReservation(Evenement $evenement)
    {
        $section = $evenement->getSection();
        $utilisateurs = $this->encours->UtilisateursParSection($section);
        if (!empty($utilisateurs)) {
            $containers = $evenement->getContainers();

            foreach ($containers as $container) {
                $places = $container->getPlaces();
                foreach ($places as $place) {
                    foreach ($utilisateurs as $utilisateur) {
                        if ($utilisateur == $place->getUtilisateur()) {
                            unset($utilisateurs[array_search($utilisateur, $utilisateurs)]);
                            break;
                        }
                    }
                }
            }
            $ids = '';
            foreach ($utilisateurs as $utilisateur) {
                $ids .= $utilisateur->getId() . ',';
            }
            $ids = rtrim($ids, ',');

            return $ids;
        }

        return '';
    }
}
