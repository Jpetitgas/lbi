<?php

namespace App\Controller\Gestion;

use App\Entity\Sujet;
use App\Form\SujetType;
use App\Repository\SujetRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PropertyAccess\PropertyAccess;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/sujet", name="gestion_sujet")
 */
class SujetController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(SujetRepository $sujetRepository): Response
    {
        return $this->render('gestion/sujet/index.html.twig', [
            'sujets' => $sujetRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sujet = new Sujet();
        $formSujet = $this->createForm(SujetType::class, $sujet);
        $formSujet->handleRequest($request);

        if ($formSujet->isSubmitted() && $formSujet->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sujet);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_sujet_index');
        }

        return $this->render('gestion/sujet/new.html.twig', [
            'sujet' => $sujet,
            'formSujet' => $formSujet->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sujet $sujet, NormalizerInterface $normalizer): Response
    {
        $verrouille = $sujet->getVerrouille();
        $sujetSauvegarde = $sujet->getSujet();
        $categorieSauvegarde = $sujet->getCategorie();

        $formSujet = $this->createForm(SujetType::class, $sujet);
        $formSujet->handleRequest($request);

        if ($formSujet->isSubmitted() && $formSujet->isValid()) {
            if ($verrouille) {
                $sujet->setSujet($sujetSauvegarde)
                    ->setCategorie($categorieSauvegarde);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_sujet_index');
        }

        return $this->render('gestion/sujet/edit.html.twig', [
            'sujet' => $sujet,
            'formSujet' => $formSujet->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Sujet $sujet): Response
    {
        if ($this->isCsrfTokenValid('delete' . $sujet->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sujet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_sujet_index');
    }
}
