<?php

namespace App\Controller\Gestion\Site;

use App\Entity\Site;
use App\Form\SiteType;
use App\Repository\SiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("gestion/site", name="gestion_site")
 */
class SiteController extends AbstractController
{
    /**
     * @Route("/edit", name="_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, EntityManagerInterface $entityManager, SiteRepository $siteRepository): Response
    {
        $site=$siteRepository->findOneBy([]);
        $form = $this->createForm(SiteType::class, $site);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('gestion_site_edit', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gestion/site/edit.html.twig', [
            'site' => $site,
            'form' => $form,
        ]);
    }
}
