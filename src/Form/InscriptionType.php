<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Activite;
use App\Entity\Document;
use App\Service\EnCours;
use App\Entity\Inscription;
use App\Entity\CategorieActivite;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use App\Repository\ActiviteRepository;
use App\Repository\DocumentRepository;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class InscriptionType extends AbstractType
{
    public $security;
    public $activiteRepository;
    public $documentRepository;
    public $enCours;

    public function __construct(Security $security, ActiviteRepository $activiteRepository, EnCours $enCours, DocumentRepository $documentRepository)
    {
        $this->security = $security;
        $this->documentRepository = $documentRepository;
        $this->activiteRepository = $activiteRepository;
        $this->enCours = $enCours;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('categorie', EntityType::class, [
                'class' => CategorieActivite::class,
                'placeholder' => 'selectionner une catégorie',
                'choice_label' => 'designation',
                'query_builder' => function (EntityRepository $er) {
                                return $er->createQueryBuilder('c')
                                    ->orderBy('c.designation', 'ASC');
                            },
                'mapped' => false,
                'required' => false,
            ])
            ->add('activite', HiddenType::class)
            ->add('document', HiddenType::class)
            ->add('demandeParticuliere', TextType::class, [
                'required' => false,
                        'label' => 'Demande Particulière',
                    ])
            ->add('envoyer', SubmitType::class, [
                'label' => 'Sauvegarder',

                        'attr' => [
                            'class' => 'btn btn-primary',
                        ],
            ]);
        if ($this->security->isGranted('ROLE_GESTION')) {
            $builder->add('cours', HiddenType::class);
        }

        $ajoutChampDocument = function (FormInterface $form, $activite_id, $inscription) {
            $activite = $this->activiteRepository->find($activite_id);
            /** @Var Inscription $inscription */
            $pratiquant = $inscription->getPratiquant();
            $documents = $this->documentRepository->findBy(['pratiquant' => $pratiquant]);
            if ($activite && $activite->getCertificatObligatoire() === true) {
                $form->add('document', EntityType::class, [
                'class' => Document::class,
                'label' => 'Document',
                'placeholder' => 'selectionner un document',
                'choices' => $documents,
                'required' => false,
                ])
                ->add('ajouter', SubmitType::class, [
                    'label' => 'Ajouter un nouveau document',

                    'attr' => [
                            'class' => 'btn btn-secondary',
                        ], ]);
            } else {
                $form->add('document', HiddenType::class);
            }
        };
        $ajoutChampCours = function (FormInterface $form, $activite_id) {
            $activite = $this->activiteRepository->find($activite_id);

            if ($activite) {
                /** @Var Inscription $inscription */
                $cours = $activite->getCours();
                $form->add('cours', EntityType::class, [
                'class' => Cours::class,
                'label' => 'Cours',
                'placeholder' => 'selectionner un cours',
                'choices' => $cours,
                'required' => false,
                ]);
            } else {
                $form->add('cours', HiddenType::class);
            }
        };
        $ajoutChampActivite = function (FormInterface $form, $categorie, $inscription) use ($ajoutChampDocument, $ajoutChampCours) {
            if ($this->security->isGranted('ROLE_GESTION')) {
                $activites = (null === $categorie) ? [] : $this->activiteRepository->activiteCategorie($categorie, $this->enCours->saison());;
            } else {
                $activites = $this->activiteRepository->activiteSansConditionParCategorie($categorie, $this->enCours->saison());
            }
            $builder = $form->getConfig()->getFormFactory()->createNamedBuilder('activite', EntityType::class, null, [
                'class' => Activite::class,
                'label' => 'Activité',
                'placeholder' => $categorie ? 'Selectionner une activité' : 'Selectionner d\'abord une catégorie',
                'choices' => $activites,
                'required' => false,
                'auto_initialize' => false,
            ]);
            $builder->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($ajoutChampDocument, $ajoutChampCours, $inscription) {
                    $activite_id = $event->getData();
                    $form = $event->getForm();
                    if ($this->security->isGranted('ROLE_GESTION')) {
                        $ajoutChampCours($form->getParent(), $activite_id);
                    }
                    $ajoutChampDocument($form->getParent(), $activite_id, $inscription);
                }
            );
            $form->add($builder->getForm());
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($ajoutChampActivite, $ajoutChampCours, $ajoutChampDocument) {
            $inscription = $event->getData();
            $form = $event->getForm();
            if (!($inscription->getId() === null)) {
                $categorie = $inscription->getActivite()->getCategorie();
                $builder = $form->getConfig()->getFormFactory()->createNamedBuilder('categorie', EntityType::class, null, [
                        'class' => CategorieActivite::class,
                        'placeholder' => 'selectionner une catégorie',
                        'choice_label' => 'designation',
                        'mapped' => false,
                        'required' => false,
                        'data' => $categorie,
                        'auto_initialize' => false,
                    ]);
                $builder->addEventListener(
                    FormEvents::POST_SUBMIT,
                    function (FormEvent $event) use ($ajoutChampActivite, $inscription) {
                        $categorie = $event->getForm()->getData();
                        $form = $event->getForm();
                        $ajoutChampActivite($form->getParent(), $categorie, $inscription);
                    }
                );
                $form->add($builder->getForm());
                $ajoutChampActivite($form, $categorie, $inscription);
                $activite = $inscription->getActivite();
                if ($this->security->isGranted('ROLE_GESTION')) {
                    $ajoutChampCours($form, $activite->getId());
                }
                $ajoutChampDocument($form, $activite->getId(), $inscription);
            }
        });

        $builder->get('categorie')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($ajoutChampActivite) {
                $categorie = $event->getForm()->getData();
                $form = $event->getForm();
                $inscription = $form->getParent()->getData();
                $ajoutChampActivite($form->getParent(), $categorie, $inscription);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Inscription::class,
        ]);
    }
}
