<?php

namespace App\Entity;

use App\Repository\DossierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DossierRepository::class)
 */
class Dossier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class, inversedBy="dossiers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $saison;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="dossiers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adherent;

    /**
     * @ORM\OneToMany(targetEntity=Inscription::class, mappedBy="dossier",fetch="EAGER")
     */
    private $inscriptions;

    /**
     * @ORM\OneToMany(targetEntity=Paiement::class, mappedBy="dossier",fetch="EAGER")
     */
    private $paiements;

    /**
     * @ORM\OneToMany(targetEntity=Reduction::class, mappedBy="dossier")
     */
    private $reductions;

    /**
     * @ORM\Column(type="boolean")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bulletinValide;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bulletinAdhesion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $dossierVerrouille;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $remarque;

    private $bulletinPresent;
    private $nbCertificatManquant;
    private $cotisationAJour;
    private $nbCertificatAValider;
    private $nbTransmissionAssureur;
    private $solde;

    /**
     * @ORM\Column(type="boolean")
     */
    private $locked;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $noteLock;

    public function getSolde()
    {
        return $this->solde;
    }

    public function setSolde($solde): self
    {
        $this->solde = $solde;

        return $this;
    }

    public function getNbCertificatAValider()
    {
        return $this->nbCertificatAValider;
    }

    public function setNbCertificatAValider($nbCertificatAValider): self
    {
        $this->nbCertificatAValider = $nbCertificatAValider;

        return $this;
    }

    public function getNbTransmissionAssureur()
    {
        return $this->nbTransmissionAssureur;
    }

    public function setNbTransmissionAssureur($nbTransmissionAssureur): self
    {
        $this->nbTransmissionAssureur = $nbTransmissionAssureur;

        return $this;
    }

    public function getBulletinPresent()
    {
        return $this->bulletinPresent;
    }

    public function setBulletinPresent($bulletinPresent): self
    {
        $this->bulletinPresent = $bulletinPresent;

        return $this;
    }

    public function getNbCertificatManquant()
    {
        return $this->nbCertificatManquant;
    }

    public function setNbCertificatManquant($nbCertificatManquant): self
    {
        $this->nbCertificatManquant = $nbCertificatManquant;

        return $this;
    }

    public function getCotisationAJour()
    {
        return $this->cotisationAJour;
    }

    public function setCotisationAJour($cotisationAJour): self
    {
        $this->cotisationAJour = $cotisationAJour;

        return $this;
    }

    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
        $this->paiements = new ArrayCollection();
        $this->reductions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    public function getAdherent(): ?Utilisateur
    {
        return $this->adherent;
    }

    public function setAdherent(?Utilisateur $adherent): self
    {
        $this->adherent = $adherent;

        return $this;
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->setDossier($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->removeElement($inscription)) {
            // set the owning side to null (unless already changed)
            if ($inscription->getDossier() === $this) {
                $inscription->setDossier(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getAdherent();
    }

    /**
     * @return Collection|Paiement[]
     */
    public function getPaiements(): Collection
    {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): self
    {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements[] = $paiement;
            $paiement->setDossier($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): self
    {
        if ($this->paiements->removeElement($paiement)) {
            // set the owning side to null (unless already changed)
            if ($paiement->getDossier() === $this) {
                $paiement->setDossier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reduction[]
     */
    public function getReductions(): Collection
    {
        return $this->reductions;
    }

    public function addReduction(Reduction $reduction): self
    {
        if (!$this->reductions->contains($reduction)) {
            $this->reductions[] = $reduction;
            $reduction->setDossier($this);
        }

        return $this;
    }

    public function removeReduction(Reduction $reduction): self
    {
        if ($this->reductions->removeElement($reduction)) {
            // set the owning side to null (unless already changed)
            if ($reduction->getDossier() === $this) {
                $reduction->setDossier(null);
            }
        }

        return $this;
    }

    public function getBulletinValide(): ?bool
    {
        return $this->bulletinValide;
    }

    public function setBulletinValide(bool $bulletinValide): self
    {
        $this->bulletinValide = $bulletinValide;

        return $this;
    }

    public function getBulletinAdhesion(): ?string
    {
        return $this->bulletinAdhesion;
    }

    public function setBulletinAdhesion(?string $bulletinAdhesion): self
    {
        $this->bulletinAdhesion = $bulletinAdhesion;

        return $this;
    }

    public function getDossierVerrouille(): ?bool
    {
        return $this->dossierVerrouille;
    }

    public function setDossierVerrouille(?bool $dossierVerrouille): self
    {
        $this->dossierVerrouille = $dossierVerrouille;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function isLocked(): ?bool
    {
        return $this->locked;
    }
    public function getLocked(): ?string
    {
        return $this->locked;
    }
    public function setLocked(?bool $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    public function getNoteLock(): ?string
    {
        return $this->noteLock;
    }

    public function setNoteLock(?string $noteLock): self
    {
        $this->noteLock = $noteLock;

        return $this;
    }
    public function getPratiquants(): array
    {
        $pratiquants = [];
        foreach ($this->inscriptions as $inscription) {
            $pratiquant = $inscription->getPratiquant();
            if (!in_array($pratiquant, $pratiquants)) {
                $pratiquants[] = $pratiquant;
            }
        }
        return $pratiquants;
    }
}
