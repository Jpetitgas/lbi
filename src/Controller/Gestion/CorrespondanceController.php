<?php

namespace App\Controller\Gestion;

use App\Entity\Messages;
use App\Form\EmailUtilisateurType;
use App\Form\MessagesInscriptionType;
use App\Form\MessagesUtilisateurType;
use App\Message\SendEmailMessage;
use App\Repository\ContactPratiquantRepository;
use App\Repository\InscriptionRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mime\Message;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/correspondance", name="gestion_correspondance")
 */
class CorrespondanceController extends AbstractController
{
    /**
     * @Route("/message/{ids}", name="_message", methods={"GET", "POST"})
     */
    public function messageUtilisateur($ids, MessageBusInterface $messageBus, Request $request, UtilisateurRepository $utilisateurRepository): Response
    {
        $referer = $request->headers->get('referer');
        $utilisateurs = explode(',', $ids);
        $destinaires = [];
        foreach ($utilisateurs as $utilisateur) {
            array_push($destinaires, $utilisateurRepository->find($utilisateur));
        }

        $message = new Messages();
        $formMessage = $this->createForm(MessagesUtilisateurType::class, $message);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $contact = [];
            $sms = $this->traitementForm($destinaires, $contact, $message, $formMessage, $messageBus);
            if (in_array('SMS', $copie = $formMessage->get('copie')->getData())) {
                $contenu = strip_tags($message->getMessage());

                return $this->redirectToRoute('gestion_sms_envoi', [
                    'sms' => implode(',', $sms),
                    'message' => $contenu,
                ]);
            }

            return $this->redirectToRoute('gestion');
        }

        return $this->render('gestion/message/newUtilisateur.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }

    /**
     * @Route("/email/{ids}", name="_email", methods={"GET", "POST"})
     */
    public function emailUtilisateur($ids, MessageBusInterface $messageBus, Request $request, UtilisateurRepository $utilisateurRepository): Response
    {
        $referer = $request->headers->get('referer');
        $utilisateurs = array_unique(explode(',', $ids));
        $destinaires = [];
        foreach ($utilisateurs as $utilisateur) {
            $destinaire = $utilisateurRepository->find($utilisateur);
            if ($destinaire) { // Vérifier si l'utilisateur existe avant de l'ajouter
                array_push($destinaires, $destinaire);
            }
        }
        $message = new Messages();
        $formMessage = $this->createForm(EmailUtilisateurType::class, $message);
        $formMessage->handleRequest($request);
        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            foreach ($destinaires as $contact) {
                // $envoi = new Messages();
                // $envoi->setExpediteur($this->getUser())
                //     ->setDestinataire($contact)
                //     ->setTitre($message->getTitre())
                //     ->setMessage($message->getMessage());
                // $entityManager = $this->getDoctrine()->getManager();
                // $entityManager->persist($envoi);
                // $entityManager->flush();

                if ($contact->getEmail()) {
                    $messageBus->dispatch(new SendEmailMessage(
                        $contact->getEmail(),
                        $message->getTitre(),
                        'email/email.html.twig',
                        $contact,
                        $message->getMessage()
                    ));
                }
            }
            $this->addFlash('success', 'Vos messages ont été envoyé');

            return $this->redirectToRoute('gestion_utilisateur_index');
        }

        return $this->render('gestion/message/newEmailUtilisateur.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }

    /**
     * @Route("/message/pratiquant/{ids}", name="_message_pratiquant", methods={"GET", "POST"})
     */
    public function messagePratiquant($ids, MessageBusInterface $messageBus, Request $request, InscriptionRepository $inscriptionRepository, ContactPratiquantRepository $contactPratiquantRepository): Response
    {
        $referer = $request->headers->get('referer');
        $ids = explode(',', $ids);
        $destinaires = [];
        $contacts = [];
        foreach ($ids as $id) {
            $inscription = $inscriptionRepository->find($id);
            if (!in_array($inscription->getDossier()->getAdherent(), $destinaires)) {
                array_push($destinaires, $inscription->getDossier()->getAdherent());
            }

            foreach ($inscription->getPratiquant()->getContactPratiquants() as $contact) {
                if (!in_array($contact, $contacts)) {
                    array_push($contacts, $contact);
                }
            }
        }
        $message = new Messages();
        $formMessage = $this->createForm(MessagesInscriptionType::class, $message);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $sms = $this->traitementForm($destinaires, $contacts, $message, $formMessage, $messageBus);
            if (in_array('SMS', $copie = $formMessage->get('copie')->getData())) {
                $contenu = strip_tags($message->getMessage());

                return $this->redirectToRoute('gestion_sms_envoi', [
                    'sms' => implode(',', $sms),
                    'message' => $contenu,
                ]);
            }

            return $this->redirectToRoute('gestion');
        }

        return $this->render('gestion/message/newUtilisateur.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }

    public function traitementForm($destinaires, $contacts, $message, $formMessage, $messageBus)
    {
        if (in_array('Messagerie', $copie = $formMessage->get('copie')->getData())) {
            foreach ($destinaires as $destinaire) {
                $envoi = new Messages();
                $envoi->setExpediteur($this->getUser())
                    ->setDestinataire($destinaire)
                    ->setTitre($message->getTitre())
                    ->setMessage($message->getMessage());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($envoi);
            }
            $entityManager->flush();
            foreach ($destinaires as $destinaire) {
                $messageBus->dispatch(new SendEmailMessage(
                    $destinaire->getEmail(),
                    'Vous avez reçu un nouveau message ',
                    'email/infoMessage.html.twig',
                    $destinaire,
                    $message->getTitre()
                ));
            }
            $this->addFlash('success', 'Vos messages ont été envoyé aux utilisateurs sélectionnés');

            if (in_array('contact', $copie = $formMessage->get('copie')->getData())) {
                foreach ($contacts as $contact) {
                    $messageBus->dispatch(new SendEmailMessage(
                        $contact->getEmail(),
                        $message->getTitre(),
                        'email/infoMessageContact.html.twig',
                        $contact,
                        $message->getMessage()
                    ));
                }
                $this->addFlash('success', 'Vos messages ont été envoyé aux contacts des pratiquants sélectionnés');
            }
        }

        if (in_array('SMS', $copie = $formMessage->get('copie')->getData())) {
            $sms = [];
            foreach ($destinaires as $destinaire) {
                if (!in_array($destinaire->getPortable(), $sms)) {
                    array_push($sms, $destinaire->getPortable());
                }
            }

            if (in_array('contact', $copie = $formMessage->get('copie')->getData())) {
                foreach ($contacts as $contact) {
                    if (!in_array($contact->getPortable(), $sms)) {
                        array_push($sms, $contact->getPortable());
                    }
                }
            }

            return $sms;
        }

        return;
    }
}
