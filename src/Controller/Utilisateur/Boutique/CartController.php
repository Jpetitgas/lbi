<?php

namespace App\Controller\Utilisateur\Boutique;

use App\Service\Cart;
use App\Repository\TailleRepository;
use App\Repository\InformationRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartController extends AbstractController
{
    /**
     * @Route("/utilisateur/boutique/panier", name="utilisateur_boutique_cart_show")
     */
    public function show(Cart $cart, InformationRepository $informationRepository): Response
    {
        $detailCart = $cart->getDetailedCartItems();
        $total = $cart->getTotal();

        return $this->render('utilisateur/boutique/cart/index.html.twig', [
            'items' => $detailCart,
            'total' => $total,
            'information' => $informationRepository->findOneBy([])

        ]);
    }
    /**
     * @Route("/utilisateur/boutique/panier/ajout/{id}", name="utilisateur_boutique_cart_add", requirements={"id": "\d+"})
     */
    public function add($id, TailleRepository $tailleRepository, Cart $cart): Response
    {
        $taille = $tailleRepository->find($id);
        if (!$taille || $taille->getProduct()->getActif() == false) {
            $this->addFlash('danger', 'Le produit demandé n\'existe pas!');
            return $this->redirectToRoute('utilisateur_boutique');
        }

        $cart->add($id);
        $this->addFlash('success', 'Le produit ' . $taille->getProduct()->getName() . ' en taille ' . $taille->getName() . ' a été ajouté au panier');
        return $this->redirectToRoute("utilisateur_boutique_cart_show");
    }

    /**
     * @Route("/utilisateur/boutique/panier/supprimer/{id}", name="utilisateur_boutique_cart_delete", requirements={"id": "\d+"})
     */
    public function delete($id, TailleRepository $tailleRepository, Cart $cart): Response
    {
        $taille = $tailleRepository->find($id);
        if (!$taille) {
            throw new NotFoundHttpException("Le produit demandé n'existe pas!");
        }

        $cart->remove($id);
        $this->addFlash('success', 'Le produit ' . $taille->getProduct()->getName() . ' en taille ' . $taille->getName() . ' a été supprimé du panier');
        return $this->redirectToRoute("utilisateur_boutique_cart_show");
    }
    /**
     * @Route("/utilisateur/boutique/panier/decrement/{id}", name="utilisateur_boutique_cart_decrement", requirements={"id": "\d+"})
     */
    public function decrement($id, TailleRepository $tailleRepository, Cart $cart): Response
    {
        $taille = $tailleRepository->find($id);
        if (!$taille) {
            throw new NotFoundHttpException("Le produit demandé n'existe pas!");
        }

        $cart->decrement($id);
        $this->addFlash('success', 'La quantité du produit ' . $taille->getProduct()->getName() . ' en taille ' . $taille->getName() . ' a été décrementé du panier');
        return $this->redirectToRoute("utilisateur_boutique_cart_show");
    }
}
