<?php

namespace App\Entity;

use App\Repository\MensurationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MensurationRepository::class)
 */
class Mensuration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $taille;

    /**
     * @ORM\OneToMany(targetEntity=Pratiquant::class, mappedBy="tailleHaut")
     */
    private $pratiquants;

    public function __construct()
    {
        $this->pratiquants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * @return Collection|Pratiquant[]
     */
    public function getPratiquants(): Collection
    {
        return $this->pratiquants;
    }

    public function addPratiquant(Pratiquant $pratiquant): self
    {
        if (!$this->pratiquants->contains($pratiquant)) {
            $this->pratiquants[] = $pratiquant;
            $pratiquant->setTailleHaut($this);
        }

        return $this;
    }

    public function removePratiquant(Pratiquant $pratiquant): self
    {
        if ($this->pratiquants->removeElement($pratiquant)) {
            // set the owning side to null (unless already changed)
            if ($pratiquant->getTailleHaut() === $this) {
                $pratiquant->setTailleHaut(null);
            }
        }

        return $this;
    }
}
