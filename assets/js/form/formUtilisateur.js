import 'jquery'
const $ = require('jquery');
global.$ = $;

import 'select2/dist/css/select2.css'
import 'select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.rtl.min.css'
import 'select2/dist/js/select2'

$(document).ready(function () {

    $('#utilisateur_CP').select2({
        theme: "bootstrap-5",
        width:'resolve',
    });
    
    $('#utilisateur_CP').on('select2:select', function () {
        
        let form = this.closest("form");
        let data = this.name + "=" + this.value

        fetch(form.action, {
            method: form.getAttribute("method"),
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset:utf-8"
            }
        })
            .then(response => response.text())
            .then(html => {
                let content = document.createElement("html");
                content.innerHTML = html;
                let nouvelleTown = content.querySelector("#utilisateur_town");
                document.querySelector('#utilisateur_town').replaceWith(nouvelleTown);
            })
    })

});

