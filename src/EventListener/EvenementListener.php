<?php

namespace App\EventListener;

use App\Entity\Evenement;

use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class EvenementListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison = $encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $evenement = $event->getObject();

        if ($evenement instanceof Evenement) {
            $evenement->setSaison($this->saison);
        }
    }
}
