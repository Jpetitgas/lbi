<?php

namespace App\Controller\Intervenant;

use DateTime;
use App\Entity\Cours;
use App\Entity\Messages;
use App\Entity\Presence;
use App\Service\EnCours;
use App\Entity\Pratiquant;
use App\Form\PresenceType;
use App\Service\SendEmail;
use Psr\Log\LoggerInterface;
use App\SearchData\PresenceData;
use App\SearchForm\PresenceForm;
use App\Repository\CoursRepository;
use App\Repository\SujetRepository;
use App\Repository\PresenceRepository;
use App\Repository\PratiquantRepository;
use App\Repository\InscriptionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/intervenant/presence", name="intervenant_presence")
 * @IsGranted("ROLE_INTERVENANT")
 */
class PresenceController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET","POST"})
     */
    public function index(InscriptionRepository $inscriptionRepository, PaginatorInterface $paginator, CoursRepository $coursRepository, PresenceRepository $presenceRepository, Request $request, EnCours $enCours, LoggerInterface $logger): Response
    {
        $intervenant = $this->getUser();
        $saison = $enCours->saison();
        $data = new PresenceData();
        $form = $this->createForm(PresenceForm::class, $data);
        $form->handleRequest($request);

        $cours = $coursRepository->coursParSaisonParIntervenant($saison, $intervenant);

        $query = $inscriptionRepository->presenceIntervenant($intervenant, $saison, $data);
        $inscriptions = $query->getResult();

        $today = new DateTime('today');
        $tomorrow = clone $today;
        $tomorrow->modify('+1 day');

        $logger->info('Recherche des présences pour la date : ' . $today->format('Y-m-d'));

        $presencesDuJour = $presenceRepository->createQueryBuilder('p')
            ->join('p.cours', 'c')
            ->where('p.saison = :saison')
            ->andWhere('p.create_at >= :today')
            ->andWhere('p.create_at < :tomorrow')
            ->andWhere('c.intervenant = :intervenant')
            ->setParameter('saison', $saison)
            ->setParameter('today', $today)
            ->setParameter('tomorrow', $tomorrow)
            ->setParameter('intervenant', $intervenant)
            ->getQuery()
            ->getResult();

        $logger->info('Nombre de présences trouvées : ' . count($presencesDuJour));

        $presenceMap = [];
        foreach ($presencesDuJour as $presence) {
            $key = $presence->getPratiquant()->getId() . '-' . $presence->getCours()->getId();
            $presenceMap[$key] = true;
            $logger->info('Présence trouvée pour : ' . $key);
        }

        return $this->render('intervenant/presence/index.html.twig', [
            'inscriptions' => $inscriptions,
            'cours' => $cours,
            'form' => $form->createView(),
            'presenceMap' => $presenceMap,
        ]);
    }

    /**
     * @Route("/toggle-presence", name="_toggle_presence", methods={"POST"})
     */
    public function togglePresence(Request $request, EnCours $enCours, PresenceRepository $presenceRepository, LoggerInterface $logger, ValidatorInterface $validator): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $pratiquantId = $data['pratiquant'] ?? null;
        $coursId = $data['cours'] ?? null;

        if (!$pratiquantId || !$coursId) {
            return new JsonResponse(['success' => false, 'message' => 'Données invalides'], 400);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $pratiquantRepository = $entityManager->getRepository(Pratiquant::class);
        $coursRepository = $entityManager->getRepository(Cours::class);

        $pratiquant = $pratiquantRepository->find($pratiquantId);
        $cours = $coursRepository->find($coursId);

        if (!$pratiquant || !$cours) {
            return new JsonResponse(['success' => false, 'message' => 'Pratiquant ou cours non trouvé'], 404);
        }

        // Vérifier que l'intervenant est bien associé à ce cours
        if ($cours->getIntervenant() !== $this->getUser()) {
            return new JsonResponse(['success' => false, 'message' => 'Vous n\'êtes pas autorisé à modifier les présences pour ce cours'], 403);
        }

        $today = new DateTime('today');
        $tomorrow = clone $today;
        $tomorrow->modify('+1 day');

        $presence = $presenceRepository->createQueryBuilder('p')
            ->where('p.pratiquant = :pratiquant')
            ->andWhere('p.cours = :cours')
            ->andWhere('p.saison = :saison')
            ->andWhere('p.create_at >= :today')
            ->andWhere('p.create_at < :tomorrow')
            ->setParameter('pratiquant', $pratiquant)
            ->setParameter('cours', $cours)
            ->setParameter('saison', $enCours->saison())
            ->setParameter('today', $today)
            ->setParameter('tomorrow', $tomorrow)
            ->getQuery()
            ->getOneOrNullResult();

        if ($presence) {
            $entityManager->remove($presence);
            $present = false;
            $logger->info('Présence supprimée pour le pratiquant ' . $pratiquantId . ' et le cours ' . $coursId);
        } else {
            $presence = new Presence();
            $presence->setPratiquant($pratiquant)
                ->setCours($cours)
                ->setSaison($enCours->saison())
                ->setCreateAt(new DateTime());

            $errors = $validator->validate($presence);
            if (count($errors) > 0) {
                return new JsonResponse(['success' => false, 'message' => (string) $errors], 400);
            }

            $entityManager->persist($presence);
            $present = true;
            $logger->info('Présence ajoutée pour le pratiquant ' . $pratiquantId . ' et le cours ' . $coursId);
        }

        $entityManager->flush();

        return new JsonResponse(['success' => true, 'present' => $present]);
    }

    /**
     * @Route("/message-administrateur", name="_message_admin", methods={"POST"})
     */
    public function messageAdministrateur(Request $request, SujetRepository $sujetRepository, CoursRepository $coursRepository, SendEmail $sendEmail, ValidatorInterface $validator, LoggerInterface $logger): JsonResponse
    {
        try {
            $token = $request->request->get('_token');
            if (!$this->isCsrfTokenValid('message_admin', $token)) {
                return new JsonResponse(['success' => false, 'message' => 'Token CSRF invalide'], 403);
            }

            $content = $request->request->get('content');
            $coursId = $request->request->get('cours_id');

            if (empty(trim($content))) {
                return new JsonResponse(['success' => false, 'message' => 'Le contenu du message ne peut pas être vide'], 400);
            }

            if (empty($coursId)) {
                return new JsonResponse(['success' => false, 'message' => 'Aucun cours sélectionné'], 400);
            }

            $cours = $coursRepository->find($coursId);
            if (!$cours) {
                return new JsonResponse(['success' => false, 'message' => 'Cours non trouvé'], 404);
            }

            // Vérifier que l'intervenant est bien associé à ce cours
            if ($cours->getIntervenant() !== $this->getUser()) {
                return new JsonResponse(['success' => false, 'message' => 'Vous n\'êtes pas autorisé à envoyer un message pour ce cours'], 403);
            }

            $sujet = $sujetRepository->findOneBy(['sujet' => 'Message à un administrateur']);

            if (!$sujet) {
                return new JsonResponse(['success' => false, 'message' => 'Sujet non trouvé'], 404);
            }

            $destinataire = $sujet->getDestinataire();

            $messageContent = sprintf(
                "Dans le cours \"%s\", l'intervenant signale que : %s",
                $cours->getNom(),
                $content
            );

            $message = new Messages();
            $message->setExpediteur($this->getUser())
                ->setDestinataire($destinataire)
                ->setTitre('Signalement pour le cours : ' . $cours->getNom())
                ->setMessage($messageContent)
                ->setCreatedAt(new \DateTime())
                ->setIsRead(false);

            $errors = $validator->validate($message);
            if (count($errors) > 0) {
                return new JsonResponse(['success' => false, 'message' => (string) $errors], 400);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();

            $sendEmail->send([
                'recipient_email' => $destinataire->getEmail(),
                'sujet' => 'Nouveau signalement pour le cours : ' . $cours->getNom(),
                'htmlTemplate' => 'email/infoMessage.html.twig',
                'destinataire' => $destinataire,
                'contenu' => $messageContent,
            ]);

            return new JsonResponse(['success' => true, 'message' => 'Message envoyé avec succès']);
        } catch (\Exception $e) {
            $logger->error('Erreur lors de l\'envoi du message:', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            return new JsonResponse(['success' => false, 'message' => 'Une erreur interne est survenue. Veuillez réessayer plus tard.'], 500);
        }
    }
}
