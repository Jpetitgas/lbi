<?php

namespace  App\Service;

use DateTime;
use App\Entity\Activite;
use App\Entity\Messages;
use App\Entity\Inscription;
use App\Message\SendEmailMessage;
use App\Repository\SujetRepository;
use App\Repository\ActiviteRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\InscriptionRepository;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * [permet de gerer le statut d'une inscription].
 */
class StatutInscription
{
    protected $enCours;
    protected $activiteRepository;
    protected $inscriptionRepository;
    protected $messageBus;
    protected $sujetRepository;
    protected $entityManagerInterface;
    private SendEmail $sendEmail;

    public function __construct(EntityManagerInterface $entityManagerInterface, SendEmail $sendEmail, EnCours $enCours, SujetRepository $sujetRepository, MessageBusInterface $messageBus, ActiviteRepository $activiteRepository, InscriptionRepository $inscriptionRepository)
    {
        $this->entityManagerInterface = $entityManagerInterface;
        $this->sujetRepository = $sujetRepository;
        $this->messageBus = $messageBus;
        $this->enCours = $enCours;
        $this->activiteRepository = $activiteRepository;
        $this->inscriptionRepository = $inscriptionRepository;
        $this->sendEmail = $sendEmail;
    }

    public function statut(Activite $activite)
    {
        if ($activite->getNbDePlace() > $this->nbInscrit($activite)) {
            return 'LISTE PRINCIPALE';
        }

        return "LISTE D'ATTENTE";
    }

    public function nbInscrit(Activite $activite)
    {
        $nbInscrit = count($this->inscriptionRepository->InscriptionParSaisonPourActivite($activite));

        return $nbInscrit;
    }

    /**
     * Verification du statut et si LISTE D'ATTENTE alors EN ATTENTE et envoie mail.
     *
     * @return [type]
     */
    public function deleteInscription(Inscription $inscription)
    {
        if (!($inscription->getStatut() == $inscription::LISTE_PRINCIPALE)) {
            /* @var $first Inscription */
            $first = $this->inscriptionRepository->premierListeAttente($inscription->getActivite());
            if ($first) {
                $first[0]->setStatut($inscription::LISTE_EN_ATTENTE)
                    ->setDateChangementStatut(new DateTime());

                $sujet = $this->sujetRepository->findOneBy(['sujet' => $inscription::LISTE_ATTENTE]);
                if ($sujet) {
                    $envoi = new Messages();
                    $message = $first[0]->getPratiquant()->getPrenom() . ' ' . $first[0]->getPratiquant()->getNom() . ' était inscrit sur liste d\'attente. Une place vient de se libérer. Merci de nous indiquer si vous souhaitez la prendre.';
                    $envoi->setExpediteur($sujet->getDestinataire())
                        ->setDestinataire($first[0]->getPratiquant()->getUtilisateur())
                        ->setTitre($sujet->getSujet())
                        ->setMessage($message);

                    $this->entityManagerInterface->persist($envoi);
                    $this->entityManagerInterface->flush();
                }
                // $this->messageBus->dispatch(new SendEmailMessage(
                //     $first[0]->getPratiquant()->getUtilisateur()->getEmail(),
                //     'Vous aves reçu un nouveau message',
                //     'email/infoMessage.html.twig',
                //     []
                // ));
                $this->sendEmail->send([
                    'recipient_email' => $first[0]->getPratiquant()->getUtilisateur()->getEmail(),
                    'sujet' => 'Vous aves reçu un nouveau message',
                    'htmlTemplate' => 'email/infoMessage.html.twig',
                    $inscription->first[0]->getPratiquant()->getUtilisateur(),
                    'context' => [],
                ]);

                $message = $first[0]->getPratiquant()->getPrenom() . ' ' . $first[0]->getPratiquant()->getNom() . ' est en attente de validation de la place en liste principale';
                // $this->messageBus->dispatch(new SendEmailMessage(
                //     $sujet->getDestinataire()->getEmail(),
                //     $sujet->getSujet(),
                //     'email/email.html.twig',
                //     ['contenu' => '$message']
                // ));
                $this->sendEmail->send([
                    'recipient_email' => $sujet->getDestinataire()->getEmail(),
                    'sujet' => $sujet->getSujet(),
                    'htmlTemplate' => 'email/email.html.twig',

                    'context' => ['contenu' => '$message'],
                ]);
            }
        }

        return;
    }
}
