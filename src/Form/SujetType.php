<?php

namespace App\Form;

use App\Entity\Sujet;
use App\Entity\Utilisateur;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SujetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sujet')
            ->add('destinataire', EntityType::class, [
                'class' => Utilisateur::class,
                'placeholder' => 'Selectionner un gestionnaire',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                                    ->where('u.roles LIKE :role')
                                    ->setParameter('role', '%'.'ROLE_GESTION'.'%');
                },
            ])
            ->add('categorie', ChoiceType::class, [
                'choices' => [
                    'UTILISATEUR' => 'UTILISATEUR',
                    'INTERVENANT' => 'INTERVENANT',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sujet::class,
        ]);
    }
}
