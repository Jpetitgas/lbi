$(document).ready(function () {
   

    var table = $("#paiementTable").DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
           this.api().columns([4,5,6,9]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
        'dom': "Bfrtip",
        'scrollY': "700px",
        'scrollX': true,
        'scrollCollapse': true,
        'paging': false,
        'buttons': [
            {
                'extend': 'csvHtml5',
                'exportOptions': {
                    columns: [1, 2, 3, 4, 5, 6, 8]
                }
            }
        ],
        'columnDefs': [

            {
                'orderable': false,
                'className': 'select-checkbox',
                'targets': 0,
            }],
        'select': {
            'style': 'multi',
            'selector': 'td:first-child'
        },
        'order': [[1, 'asc']],
        "responsive": true,
        "language": {
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            }
        },
    });
    $(".selectAll").on("click", function (e) {
        if ($(this).is(":checked")) {
            table.rows().select();
        } else {
            table.rows().deselect();
        }
    });
    

});