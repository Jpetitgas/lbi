<?php

namespace App\Controller\Utilisateur\Evenement;

use App\Service\SendEmail;
use App\Message\SendEmailMessage;
use App\Service\EvenementService;
use App\Repository\PlaceRepository;
use App\Repository\ContainerRepository;
use App\Repository\EvenementRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 *  @Route("/utilisateur/place", name="utilisateur_place")
 */
class PlaceController extends AbstractController
{
    /**
     * @Route("/reserver/{id_place}", name="_reserver", methods={"GET", "POST"})
     */
    public function reserver($id_place, Security $security, SendEmail $sendEmail, MessageBusInterface $messageBus, EvenementRepository $evenementRepository, ContainerRepository $containerRepository, PlaceRepository $placeRepository, UtilisateurRepository $utilisateurRepository): Response
    {
        /** @Var Utilisateur $utilisateur  */
        $utilisateur = $security->getUser();
        $place = $placeRepository->find($id_place);
        if ($utilisateur && $place) {
            $evenement = $evenementRepository->find($place->getContainer()->getEvenement()->getId());
            $nbReservation = 0;
            foreach ($evenement->getContainers() as $c) {
                foreach ($c->getPlaces() as $p) {
                    if ($p->getUtilisateur() == $utilisateur) {
                        ++$nbReservation;
                    }
                }
            }
            if ($nbReservation >= $evenement->getQuota()) {
                $this->addFlash('danger', 'Vous avez déjà atteint le nombre max de reservation. Vous devez en annuler une avant!');

                return $this->redirectToRoute('utilisateur_evenement_edit', ['id' => $evenement->getId()]);
            }

            $place->setUtilisateur($utilisateur);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('success', 'Votre reservation est enregistrée!');
        }

        return $this->redirectToRoute('utilisateur_evenement_edit', ['id' => $evenement->getId()]);
    }

    /**
     * @Route("/reserverPlan/{id_place}", name="_reserver_plan", methods={"GET", "POST"})
     */
    public function reserverPlan($id_place, EvenementService $evenementService, SendEmail $sendEmail, Security $security, MessageBusInterface $messageBus, EvenementRepository $evenementRepository, PlaceRepository $placeRepository): Response
    {
        /** @Var Utilisateur $utilisateur  */
        $utilisateur = $security->getUser();
        $place = $placeRepository->find($id_place);
        if ($place) {
            if ($place->getVerrouiller() == 1) {
                $resultat = ['etat' => 'reserved', 'idContainer' => $place->getContainer()->getId(), 'nomContainer' => $place->getContainer()->getNom(), 'idPlace' => $place->getId(), 'nomPlace' => $place->getNom(), 'pricePlace' => $place->getContainer()->getPricePlace(), 'reponse' => 'Cette place n\'est pas disponible!'];

                return new JsonResponse($resultat);
            }
            if ($place->getUtilisateur()) {
                if ($place->getUtilisateur() != $utilisateur) {
                    $resultat = ['etat' => 'reserved', 'idContainer' => $place->getContainer()->getId(), 'nomContainer' => $place->getContainer()->getNom(), 'idPlace' => $place->getId(), 'nomPlace' => $place->getNom(), 'pricePlace' => $place->getContainer()->getPricePlace(), 'reponse' => 'Cette place n\'est pas disponible!'];

                    return new JsonResponse($resultat);
                }
                $place->setUtilisateur(null);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();

                /** Verification qu'il a des reservations sinon on supprime les options */
                $evenement = $place->getContainer()->getEvenement();
                $reservation = $evenementService->reservation($utilisateur, $evenement) ? true : false;
                if (!$reservation) {
                    if ($evenement->getOptions()) {
                        $evenementService->deletedOptions($evenement, $utilisateur);
                    }
                }
                $resultat = ['etat' => 'cancel', 'idContainer' => $place->getContainer()->getId(), 'nomContainer' => $place->getContainer()->getNom(), 'idPlace' => $place->getId(), 'nomPlace' => $place->getNom(), 'pricePlace' => $place->getContainer()->getPricePlace(), 'reponse' => 'Votre reservation est annulée!'];

                return new JsonResponse($resultat);
            }
            if ($utilisateur && $place) {
                $evenement = $evenementRepository->find($place->getContainer()->getEvenement()->getId());
                $nbReservation = 0;
                foreach ($evenement->getContainers() as $c) {
                    foreach ($c->getPlaces() as $p) {
                        if ($p->getUtilisateur() == $utilisateur) {
                            ++$nbReservation;
                        }
                    }
                }
                if ($nbReservation >= $evenement->getQuota()) {
                    $resultat = ['etat' => 'fullQuota', 'idContainer' => $place->getContainer()->getId(), 'nomContainer' => $place->getContainer()->getNom(), 'idPlace' => $place->getId(), 'nomPlace' => $place->getNom(), 'pricePlace' => $place->getContainer()->getPricePlace(), 'reponse' => 'Vous avez déjà atteint le nombre max de reservation. Vous devez en annuler une avant!'];

                    return new JsonResponse($resultat);
                }

                $place->setUtilisateur($utilisateur);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();
                $this->addFlash('success', 'Votre reservation est enregistrée!');
            }
            $resultat = ['etat' => 'ok', 'idContainer' => $place->getContainer()->getId(), 'nomContainer' => $place->getContainer()->getNom(), 'idPlace' => $place->getId(), 'nomPlace' => $place->getNom(), 'pricePlace' => $place->getContainer()->getPricePlace(), 'reponse' => 'Cette place vous est attribuée.'];

            return new JsonResponse($resultat);
        }
        $resultat = ['etat' => 'error', 'reponse' => 'Cette place n\'existe pas!'];

        return new JsonResponse($resultat);
    }

    /**
     * @Route("/annuler/{id_place}", name="_annuler", methods={"GET", "POST"})
     */
    public function annuler($id_place, EvenementService $evenementService, SendEmail $sendEmail, MessageBusInterface $messageBus, PlaceRepository $placeRepository, UtilisateurRepository $utilisateurRepository): Response
    {
        $place = $placeRepository->find($id_place);

        $utilisateur = $place->getUtilisateur();
        if ($place) {
            $evenement = $place->getContainer()->getEvenement();
            $place->setUtilisateur(null);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('success', 'Votre reservation est annulée!');

            /** Verification qu'il a des reservations sinon on supprime les options */
            $reservation = $evenementService->reservation($utilisateur, $evenement) ? true : false;
            if (!$reservation) {
                if ($evenement->getOptions()) {
                    $evenementService->deletedOptions($evenement, $utilisateur);
                }
            }
        }

        return $this->redirectToRoute('utilisateur_evenement_edit', ['id' => $place->getContainer()->getEvenement()->getId()]);
    }
}
