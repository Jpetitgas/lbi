<?php

namespace App\Controller\Gestion;

use App\Entity\PurchaseItem;
use App\Form\PurchaseItemType;
use App\Repository\PurchaseItemRepository;
use App\Repository\PurchaseRepository;
use App\Service\PurchaseService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/boutique/purchase/item", name="gestion_boutique_purchase_item")
 */
class PurchaseItemController extends AbstractController
{
    /**
     * @Route("/{id_purchase}", name="_index", methods={"GET"})
     */
    public function index($id_purchase, PurchaseRepository $purchaseRepository): Response
    {
        $purchase = $purchaseRepository->find($id_purchase);

        return $this->render('gestion/boutique/purchase_item/index.html.twig', [
            'purchase' => $purchase,
        ]);
    }

    /**
     * @Route("/new/{id_purchase}", name="_new", methods={"GET","POST"})
     */
    public function new($id_purchase, Request $request, PurchaseRepository $purchaseRepository): Response
    {
        $purchase = $purchaseRepository->find($id_purchase);
        $purchaseItem = new PurchaseItem();
        $form = $this->createForm(PurchaseItemType::class, $purchaseItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseItem->setPurchase($purchase);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($purchaseItem);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_boutique_purchase_item_index', ['id_purchase' => $purchase->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('gestion/boutique/purchase_item/new.html.twig', [
            'purchase_item' => $purchaseItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PurchaseItem $purchaseItem, PurchaseService $purchaseService): Response
    {
        $form = $this->createForm(PurchaseItemType::class, $purchaseItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseItem->setTotal($purchaseItem->getProductPrice() * $purchaseItem->getQuantity());
            $purchase = $purchaseItem->getPurchase();
            $purchase->setTotal($purchaseService->totalPurchase($purchase));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_boutique_purchase_item_index', ['id_purchase' => $purchaseItem->getPurchase()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('gestion/boutique/purchase_item/edit.html.twig', [
            'purchase_item' => $purchaseItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/livraison/{id}", name="_livraison", methods={"GET","POST"})
     */
    public function livraison($id, PurchaseItemRepository $purchaseItemRepository, EntityManagerInterface $em): Response
    {
        $purchaseItem = $purchaseItemRepository->find($id);
        $purchaseItem->setLivraison(($purchaseItem->getLivraison() ? '0' : '1'));

        $em->flush();

        return $this->redirectToRoute('gestion_boutique_purchase_item_index', ['id_purchase' => $purchaseItem->getPurchase()->getId()]);
    }

    /**
     * @Route("/livraisonGroupe/{ids}", name="_livraisonGroupe", methods={"GET","POST"})
     */
    public function livraisonGroupe($ids, PurchaseItemRepository $purchaseItemRepository, PurchaseRepository $purchaseRepository, EntityManagerInterface $em): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $purchase = $purchaseRepository->find($id);
            if ($purchase->getPurchaseItems()) {
                $purchaseItems = $purchase->getPurchaseItems();
                foreach ($purchaseItems as $purchaseItem) {
                    $purchaseItem->setLivraison(($purchaseItem->getLivraison() ? '0' : '1'));
                }
            }
        }

        $em->flush();

        return new Response('L\'état de livraison des commandes ont changé', Response::HTTP_OK);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete($id, Request $request, PurchaseItemRepository $purchaseItemRepository, PurchaseRepository $purchaseRepository): Response
    {
        $item = $purchaseItemRepository->find($id);

        if ($item) {
            $purchase = $purchaseRepository->findBy(['id' => $item->getPurchase()->getId()]);
            if ($this->isCsrfTokenValid('delete'.$item->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($item);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('gestion_boutique_purchase_item_index', ['id_purchase' => $purchase()->getId()], Response::HTTP_SEE_OTHER);
    }
}
