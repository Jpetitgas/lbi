<?php

namespace App\Service;

use App\Entity\Saison;
use App\Repository\SaisonRepository;
use App\Service\InfosDossierEncours;
use App\Repository\DossierRepository;

class Alerte
{
    protected $saisonRepository;
    protected $dossierRepository;
    protected $infosDossierEncours;

    public function __construct(SaisonRepository $saisonRepository, DossierRepository $dossierRepository, InfosDossierEncours $infosDossierEncours)
    {
        $this->saisonRepository = $saisonRepository;
        $this->dossierRepository = $dossierRepository;
        $this->infosDossierEncours = $infosDossierEncours;
    }

    public function tousLesManquesParSaison(Saison $saison)
    {
        $dossiers = $this->dossierRepository->findBy(['saison' => $saison]);

        $alert['bulletin'] = 0;
        $alert['bulletin_a_valider'] = 0;
        $alert['cotisation'] = 0.00;
        $alert['nb_certificat'] = 0;
        $alert['certificat_a_valider'] = 0;
        $alert['a_transmettre'] = 0;
        $alert['complet']=0;

        foreach ($dossiers as $dossier) {
            $aCompleterUtilisateur = $this->infosDossierEncours->aCompleterUtilisateur($dossier);
            if ($aCompleterUtilisateur['bulletin'] == 'Non') {
                ++$alert['bulletin'];
            }
            $alert['nb_certificat'] += $aCompleterUtilisateur['certificat'];

            if ($aCompleterUtilisateur['cotisation']=="Non") {
                ++$alert['cotisation'];
            }

            $aFaireGestion = $this->infosDossierEncours->aFaireGestion($dossier);
            if ($aFaireGestion['complet'] == 'Oui') {
                ++$alert['complet'];
            }
            if ($aFaireGestion['bulletin'] == 'Non' && $aCompleterUtilisateur['bulletin'] == 'Oui') {
                ++$alert['bulletin_a_valider'];
            }
            $alert['certificat_a_valider'] += $aFaireGestion['certificat'];
            $alert['a_transmettre'] += $aFaireGestion['assureur'];
        }

        return $alert;
    }
}
