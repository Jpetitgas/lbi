<?php
// src/EventListener/InscriptionUpdatedListener.php

namespace App\EventListener;

use App\Event\InscriptionUpdatedEvent;
use App\Entity\Messages;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\InformationRepository;
use App\Repository\SujetRepository;
use App\Service\SendEmail;

class InscriptionUpdatedListener
{

    private $sendEmail;

    public function __construct(
        SendEmail $sendEmail
    ) {
        $this->sendEmail = $sendEmail;
    }

    public function onInscriptionUpdated(InscriptionUpdatedEvent $event): void
    {
        $inscription = $event->getInscription();

        $message = 'Le cours de ' . $inscription->getPratiquant()->getPrenom() . ' ' . $inscription->getPratiquant()->getNom() . ' a été modifié: ' . $inscription->getCours()->getNom();

        $this->sendEmail->send([
            'recipient_email' => $inscription->getDossier()->getAdherent()->getEmail(),
            'sujet' => 'Modification cours',
            'htmlTemplate' => 'email/email.html.twig',
            'destinataire' => $inscription->getDossier()->getAdherent(),
            'contenu' => $message,
        ]);
    }
}
