<?php

namespace App\Controller\Gestion;

use App\Service\Gps;
use App\Entity\Messages;
use App\Service\EnCours;
use App\Form\BulletinType;
use App\Service\SendEmail;
use Doctrine\DBAL\Exception;
use App\Form\UtilisateurType;
use App\SearchData\DossierData;
use App\SearchForm\DossierForm;
use App\Service\StatutInscription;
use App\Service\VerificationActivite;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\InformationRepository;
use App\Repository\InscriptionRepository;
use App\Repository\UtilisateurRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Dossier;
use App\Message\SendEmailMessage;
use App\Repository\DossierRepository;
use App\Service\InfosDossierEncours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/dossier", name="gestion_dossier")
 */
class DossierController extends AbstractController
{

    private $infosDossierEncours;
    private $messageBus;

    public function __construct(InfosDossierEncours $infosDossierEncours, MessageBusInterface $messageBus)
    {
        $this->infosDossierEncours = $infosDossierEncours;
        $this->messageBus = $messageBus;
    }


    /**
     * @Route("/{id_saison}/index", name="_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, InfosDossierEncours $infosDossier, Request $request, EnCours $encours, DossierRepository $dossierRepository): Response
    {
        $data = new DossierData();
        $form = $this->createForm(DossierForm::class, $data);
        $form->handleRequest($request);
        $saison = $encours->saison();

        $essai = $dossierRepository->dossiers($saison, $data);
        $results = $essai->getResult();
        foreach ($results as $result) {
            /** @var Dossier $result  */
            $result->setSolde($infosDossier->solde($result));
            $result->setBulletinPresent($infosDossier->bulletinPresent($result));
            $result->setNbCertificatManquant($infosDossier->nbCertificatManquant($result));
            $result->setCotisationAJour($infosDossier->cotisationAJour($result));
            $result->setNbCertificatAValider($infosDossier->nbCertificatAValider($result));
            $result->setNbTransmissionAssureur($infosDossier->nbTransmissionAssureur($result));

            if ($data->bulletinPresent !== null && !($data->bulletinPresent === $result->getBulletinPresent())) {
                unset($results[array_search($result, $results)]);
            }
            if ($data->nbCertificatManquant !== null && !($data->nbCertificatManquant === $result->getNbCertificatManquant())) {
                unset($results[array_search($result, $results)]);
            }
            if ($data->cotisationAJour !== null && !($data->cotisationAJour === $result->getCotisationAJour())) {
                unset($results[array_search($result, $results)]);
            }
        }

        $dossiers = $paginator->paginate(
            $results,
            $request->query->getInt('page', 1),
            $data->nb
        );

        if (empty($dossiers)) {
            $this->addFlash('danger', 'Il n\'y a pas encore de dossier crées');

            return $this->redirectToRoute('gestion');
        }

        return $this->render('gestion/dossier/index.html.twig', [
            'saison' => $saison,
            'dossiers' => $dossiers,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new/{id_utilisateur}", name="_new", methods={"GET","POST"})
     */
    public function new($id_utilisateur, EnCours $enCours, UtilisateurRepository $utilisateurRepository, Request $request): Response
    {
        $utilisateur = $utilisateurRepository->find($id_utilisateur);
        $dossier = new Dossier();
        $dossier->setAdherent($utilisateur)
            ->setLocked(false)
            ->setDossierVerrouille(false)
            ->setSaison($enCours->saison())
            ->setBulletinValide(false);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($dossier);
        $entityManager->flush();
        $ancre = 'detail_none_none';

        return $this->redirectToRoute('gestion_dossier_edit', ['id' => $dossier->getId(), 'ancre' => $ancre]);
    }

    /**
     * @Route("/show/{id}", name="_show", methods={"GET"})
     */
    public function show(Dossier $dossier): Response
    {
        return $this->render('gestion/dossier/show.html.twig', [
            'dossier' => $dossier,
        ]);
    }

    /**
     * @Route("/{id}/edit/{ancre}", name="_edit", methods={"GET","POST"})
     */
    public function edit($ancre, Request $request, InfosDossierEncours $infosDossierEncours, InformationRepository $informationRepository, StatutInscription $statutInscription, VerificationActivite $verificationActivite, Dossier $dossier): Response
    {
        // ancre
        $chemin = explode('_', $ancre);
        $ancre = [];

        $pratiquants = $dossier->getAdherent()->getPratiquants();
        $inscriptions = $dossier->getInscriptions();
        $paiements = $dossier->getPaiements();
        $reductions = $dossier->getReductions();

        $ancre['pill_dossier'] = $chemin[0];
        if ($chemin[1] == 'none') {
            if (null !== $pratiquants[0]) {
                $ancre['pratiquant'] = $pratiquants[0]->getId();
            }
        } else {
            $ancre['pratiquant'] = $chemin[1];
        }

        $ancre['pill_pratiquant'] = $chemin[2];

        $information = $informationRepository->findOneBy([]);
        $formUtilisateur = $this->createForm(UtilisateurType::class, $dossier->getAdherent());
        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $ancre = 'detail_none_none';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $dossier->getId(), 'ancre' => $ancre]);
        }

        $formBulletin = $this->createForm(BulletinType::class);
        $formBulletin->handleRequest($request);

        if ($formBulletin->isSubmitted() && $formBulletin->isValid()) {
            $image = $formBulletin->get('image')->getData();
            $fichier = md5(uniqid()) . '.' . $image->guessExtension();
            $image->move(
                $this->getParameter('images_repertoire'),
                $fichier
            );
            $dossier->setBulletinAdhesion($fichier);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dossier);
            $entityManager->flush();
            $ancre = 'detail_none_none';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $dossier->getId(), 'ancre' => $ancre]);
        }

        $paiement = $infosDossierEncours->paiement($dossier);
        $cotisation = $infosDossierEncours->cotisation($dossier);
        $solde = $cotisation['total'] - $paiement['total'];

        return $this->render('gestion/dossier/edit.html.twig', [
            'ancre' => $ancre,
            'dossier' => $dossier,
            'formUtilisateur' => $formUtilisateur->createView(),
            'pratiquants' => $pratiquants,
            'inscriptions' => $inscriptions,
            'paiements' => $paiements,
            'reductions' => $reductions,
            'paiement' => $paiement,
            'cotisation' => $cotisation,
            'solde' => $solde,
            'information' => $information,
            'formBulletin' => $formBulletin->createView(),
        ]);
    }

    /**
     * @Route("/deleteGroupe/{ids}", name="_deleteGroupe", methods={"POST", "GET"})
     */
    public function deleteGroupe($ids, Request $request, DossierRepository $dossierRepository): Response
    {
        $ids = explode(',', $ids);
        $em = $this->getDoctrine()->getManager();
        $nb = 0;
        try {
            foreach ($ids as $id) {
                $dossier = $dossierRepository->find($id);
                $id_saison = $dossier->getSaison()->getId();
                if ($dossier->getInscriptions()->isEmpty() && $dossier->getPaiements()->isEmpty() && $dossier->getReductions()->isEmpty()) {
                    $em->remove($dossier);
                    ++$nb;
                }
            }
            $em->flush();
        } catch (Exception  $e) {
            return new Response('Une erreur s\'est produite', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        if ($nb > 1) {
            return new Response($nb . ' dossiers vides ont été supprimés', Response::HTTP_OK);
        } elseif ($nb == 0) {
            return new Response('aucun dossier n\'a été supprimé', Response::HTTP_OK);
        } else {
            return new Response('Un dossier a été supprimé', Response::HTTP_OK);
        }
    }

    /**
     * @Route("/delete/{id}", name="_delete", methods={"POST", "GET"})
     */
    public function delete($id, Request $request, DossierRepository $dossierRepository, EntityManagerInterface $em): Response
    {
        $dossier = $dossierRepository->find($id);
        $id_saison = $dossier->getSaison()->getId();
        if ($dossier->getInscriptions()->isEmpty() && $dossier->getPaiements()->isEmpty() && $dossier->getReductions()->isEmpty()) {
            $em->remove($dossier);
            $em->flush();
        } else {
            $this->addFlash('danger', 'Le dossier n\'est pas vide. Il ne peut être supprimé');
            $ancre = 'detail_none_none';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $dossier->getId(), 'ancre' => $ancre]);
        }

        $this->addFlash('success', 'Un dossier a été supprimé');

        return $this->redirectToRoute('gestion_dossier_index', ['id_saison' => $id_saison]);
    }

    /**
     * @Route("/localisation/commune", name="_localisation_commune", methods={"GET"})
     */
    public function localisationCommune(EnCours $encours, Gps $gps, InscriptionRepository $inscriptionRepository, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        $coordonneeAsso = $gps->coordonnees($information->getAdresse() . ' ' . $information->getCodePostal() . ' ' . $information->getVille());
        $saison = $encours->saison();
        $villes = $inscriptionRepository->nbInscriptionParVilleParSaison($saison);
        $villesCoordonnees = [];
        foreach ($villes as $ville) {
            $coordonnees = $gps->coordonnees($ville['codePostal'] . ' ' . $ville['ville']);
            if (!empty($coordonnees)) {
                $commune = ['ref' => $ville['nb'], 'latitude' => $coordonnees['latitude'], 'longitude' => $coordonnees['longitude']];
                array_push($villesCoordonnees, $commune);
            }
        }

        return $this->render('gestion/gps/localisation.html.twig', [
            'titre' => 'Total des inscriptions par ville',
            'coordonneeAsso' => $coordonneeAsso,
            'villes' => $villesCoordonnees,
        ]);
    }

    /**
     * @Route("/localisation/tousdossiers", name="_localisation_tousdossiers", methods={"GET"})
     */
    public function localisationTousDossiers(EnCours $encours, Gps $gps, DossierRepository $dossierRepository, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        $coordonneeAsso = $gps->coordonnees($information->getAdresse() . ' ' . $information->getCodePostal() . ' ' . $information->getVille());
        $saison = $encours->saison();
        $dossiers = $dossierRepository->findBy(['saison' => $saison]);
        $villesCoordonnees = [];
        foreach ($dossiers as $dossier) {
            $coordonnees = $gps->coordonnees($dossier->getAdherent()->getAdresse() . ' ' . $dossier->getAdherent()->getCodePostal() . ' ' . $dossier->getAdherent()->getVille());
            if (!empty($coordonnees)) {
                $commune = ['ref' => $dossier->getAdherent()->getNom() . ' ' . $dossier->getAdherent()->getPrenom(), 'latitude' => $coordonnees['latitude'], 'longitude' => $coordonnees['longitude']];
                array_push($villesCoordonnees, $commune);
            }
        }

        return $this->render('gestion/gps/localisation.html.twig', [
            'titre' => 'Localisation de tous les utilisateurs',
            'coordonneeAsso' => $coordonneeAsso,
            'villes' => $villesCoordonnees,
        ]);
    }

    /**
     * @Route("/localisation/dossier/{id}", name="_localisation_dossier", methods={"GET"})
     */
    public function localisationDossier($id, Gps $gps, DossierRepository $dossierRepository, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        $coordonneeAsso = $gps->coordonnees($information->getAdresse() . ' ' . $information->getCodePostal() . ' ' . $information->getVille());

        $dossier = $dossierRepository->find($id);
        $villesCoordonnees = [];

        $coordonnees = $gps->coordonnees($dossier->getAdherent()->getAdresse() . ' ' . $dossier->getAdherent()->getCodePostal() . ' ' . $dossier->getAdherent()->getVille());
        if (!empty($coordonnees)) {
            $commune = ['ref' => $dossier->getAdherent()->getNom() . ' ' . $dossier->getAdherent()->getPrenom(), 'latitude' => $coordonnees['latitude'], 'longitude' => $coordonnees['longitude']];
            array_push($villesCoordonnees, $commune);
        }

        return $this->render('gestion/gps/localisation.html.twig', [
            'titre' => "Localisation de l'utilisateur : " . $dossier->getAdherent()->getNom() . ' ' . $dossier->getAdherent()->getPrenom(),
            'coordonneeAsso' => $coordonneeAsso,
            'villes' => $villesCoordonnees,
        ]);
    }

    /**
     * @Route("/validationBulletin/{id}", name="_validationBulletin", methods={"GET"})
     */
    public function validationBulletin($id, DossierRepository $dossierRepository): Response
    {
        $dossier = $dossierRepository->find($id);
        if ($dossier) {
            $dossier->setBulletinValide(($dossier->getBulletinValide() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($dossier);
            $em->flush();
            $this->addFlash('success', 'Le bulletin a changé d\'état');
        }

        return new Response('Le bulletin a changé d\'état', Response::HTTP_OK);
    }

    /**
     * @Route("/validationBulletinGroupe/{ids}", name="_validationBulletinGroupe", methods={"GET"})
     */
    public function validationBulletinGroupe($ids, DossierRepository $dossierRepository): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $dossier = $dossierRepository->find($id);
            if ($dossier) {
                $dossier->setBulletinValide(($dossier->getBulletinValide() ? '0' : '1'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($dossier);
            }
        }
        $em->flush();

        return new Response('L\état des validations des bulletins ont changé', Response::HTTP_OK);
    }

    /**
     * @Route("/bulletinSupprimer/{id}", name="_bulletinSupprimer", methods={"GET"})
     */
    public function bulletinSupprimer($id, Request $request, SendEmail $sendEmail, DossierRepository $dossierRepository, MessageBusInterface $messageBus): Response
    {
        $dossier = $dossierRepository->find($id);
        $nom = $dossier->getBulletinAdhesion();
        unlink($this->getParameter('images_repertoire') . '/' . $nom);

        $dossier->setBulletinAdhesion(null)
            ->setBulletinValide(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($dossier);
        $em->flush();

        $envoi = new Messages();
        $envoi->setExpediteur($this->getUser())
            ->setDestinataire($dossier->getAdherent())
            ->setTitre('Bulletin d\'adhésion')
            ->setMessage('Votre bulletin d\'adhésion n\'est pas conforme. Ceci est souvent du à une signature et\ou date non présentes. Merci de bien vouloir le téléverser à nouveau');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($envoi);
        $em->flush();


        $sendEmail->send([
            'recipient_email' => $dossier->getAdherent()->getEmail(),
            'sujet' => 'Vous avez reçu un nouveau message',
            'htmlTemplate' => 'email/infoMessage.html.twig',
            'destinataire' => $dossier->getAdherent(),
            'context' => [],
        ]);

        $referer = $request->headers->get('referer');
        // if (str_contains($referer, 'edit')) {
        //
        // }
        $this->addFlash('success', 'Le bulletin d\'inscription a été supprimer et un message à l\'utilisateur a été envoyé');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/verrouille/{id}", name="_verrouille", methods={"GET"})
     */
    public function verrouille($id, DossierRepository $dossierRepository): Response
    {
        $dossier = $dossierRepository->find($id);
        if ($dossier) {
            $dossier->setDossierVerrouille(($dossier->getDossierVerrouille() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($dossier);
            $em->flush();
        }

        return new Response('Le dossier a changé d\'etat', Response::HTTP_OK);
    }

    /**
     * @Route("/verrouilleGroupe/{ids}", name="_verrouilleGroupe", methods={"GET"})
     */
    public function verrouilleGroupe(
        $ids,
        DossierRepository $dossierRepository,
        MessageBusInterface $messageBus
    ): Response {
        $ids = explode(',', $ids);
        $em = $this->getDoctrine()->getManager();

        foreach ($ids as $id) {
            $dossier = $dossierRepository->find($id);
            if ($dossier) {
                $etatPrecedent = $dossier->getDossierVerrouille();
                $nouvelEtat = !$etatPrecedent;
                $dossier->setDossierVerrouille($nouvelEtat);

                // Si le dossier passe de non verrouillé à verrouillé
                if (!$etatPrecedent && $nouvelEtat) {
                    $this->envoyerEmailValidation($dossier, $messageBus);
                }

                $em->persist($dossier);
            }
        }
        $em->flush();

        return new Response('L\'état de verrouillage des dossiers a changé', Response::HTTP_OK);
    }

    private function envoyerEmailValidation(Dossier $dossier, MessageBusInterface $messageBus)
    {
        $contenu = $this->prepareMessageContentValidation($dossier);

        $messageBus->dispatch(new SendEmailMessage(
            $dossier->getAdherent()->getEmail(),
            'Votre dossier a été validé',
            'email/email.html.twig',
            $dossier->getAdherent(),
            $contenu,
        ));
    }

    private function prepareMessageContentValidation(Dossier $dossier): string
    {
        $content = "<p>Votre dossier pour la saison en cours a été validé par l'administrateur.</p>";

        $content .= "<h3>Détails du dossier :</h3>";
        $content .= "<ul style='list-style-type: none; padding-left: 0;'>";
        foreach ($dossier->getInscriptions() as $inscription) {
            $content .= sprintf(
                "<li style='margin-bottom: 10px;'>• <strong>Activité :</strong> %s - <strong>Pratiquant :</strong> %s %s</li>",
                htmlspecialchars($inscription->getActivite()->getNom()),
                htmlspecialchars($inscription->getPratiquant()->getPrenom()),
                htmlspecialchars($inscription->getPratiquant()->getNom())
            );
        }
        $content .= "</ul>";

        $content .= "<p>Merci pour votre inscription. N'hésitez pas à nous contacter si vous avez des questions.</p>";

        return $content;
    }
    /**
     * @Route("/lockedFile/{ids}", name="_lockedFile", methods={"GET"})
     */
    public function lockedFile($ids, DossierRepository $dossierRepository): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $dossier = $dossierRepository->find($id);
            if ($dossier) {
                $dossier->setLocked(($dossier->getLocked() ? '0' : '1'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($dossier);
            }
        }
        $em->flush();


        return new Response('L\'etat de suspension des dossiers ont changé', Response::HTTP_OK);
    }

    /**
     * @Route("/relance-incomplet", name="gestion_dossier_relance_incomplet", methods={"POST"})
     */
    public function relanceIncomplet(Request $request, DossierRepository $dossierRepository, MessageBusInterface $messageBus): Response
    {
        $data = json_decode($request->getContent(), true);
        $ids = $data['ids'] ?? [];

        $dossiers = $dossierRepository->findBy(['id' => $ids]);
        $emailsEnvoyes = 0;

        foreach ($dossiers as $dossier) {
            if (!$dossier->getDossierVerrouille()) {
                if ($this->dossierNecessiteNotification($dossier)) {
                    $contenu = $this->prepareMessageContent($dossier);

                    $this->messageBus->dispatch(new SendEmailMessage(
                        $dossier->getAdherent()->getEmail(),
                        'Votre dossier nécessite votre attention',
                        'email/email.html.twig',
                        $dossier->getAdherent(),
                        $contenu
                    ));

                    $emailsEnvoyes++;
                }
            }
        }
        $reponse = sprintf('%d emails de relance ont été envoyés.', $emailsEnvoyes);
        return new Response($reponse, Response::HTTP_OK);
    }
    private function dossierNecessiteNotification(Dossier $dossier): bool
    {
        $cotisationNonReglee = $this->infosDossierEncours->solde($dossier) > 0;
        $bulletinNonPresent = empty($dossier->getBulletinAdhesion());
        $certificatManquant = $this->certificatManquant($dossier);

        return $cotisationNonReglee || $bulletinNonPresent || $certificatManquant;
    }

    private function certificatManquant(Dossier $dossier): bool
    {
        foreach ($dossier->getInscriptions() as $inscription) {
            if ($inscription->getCertificatObligatoire() && !$inscription->getDocument()) {
                return true;
            }
        }
        return false;
    }

    private function prepareMessageContent(Dossier $dossier): string
    {
        $content = "<p>Votre dossier pour la saison en cours nécessite votre attention :</p>";

        $content .= "<ul>";

        $solde = $this->infosDossierEncours->solde($dossier);
        if ($solde > 0) {
            $content .= sprintf("<li>Votre cotisation n'est pas entièrement réglée. Solde restant à payer : %.2f €</li>", $solde);
        }

        if (empty($dossier->getBulletinAdhesion())) {
            $content .= "<li>Votre bulletin d'adhésion n'a pas été fourni.</li>";
        }

        foreach ($dossier->getInscriptions() as $inscription) {
            if ($inscription->getCertificatObligatoire() && !$inscription->getDocument()) {
                $content .= sprintf(
                    "<li>Le certificat médical pour l'activité \"%s\" de %s %s est manquant.</li>",
                    htmlspecialchars($inscription->getActivite()->getNom()),
                    htmlspecialchars($inscription->getPratiquant()->getPrenom()),
                    htmlspecialchars($inscription->getPratiquant()->getNom())
                );
            }
        }

        $content .= "</ul>";

        $content .= "<p>Veuillez vous connecter à votre espace personnel pour compléter votre dossier.</p>";

        return $content;
    }
}
