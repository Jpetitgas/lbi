<?php

namespace App\EventListener;

use App\Entity\Cours;
use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class CoursListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison = $encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $cours = $event->getObject();

        if ($cours instanceof Cours) {
            if ($this->saison) {
                $cours->setSaison($this->saison);
            }
        }
    }
}
