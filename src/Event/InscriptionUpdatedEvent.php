<?php


namespace App\Event;

use App\Entity\Inscription;
use Symfony\Contracts\EventDispatcher\Event;

class InscriptionUpdatedEvent extends Event
{
    public const NAME = 'inscription.updated';

    protected $inscription;

    public function __construct(Inscription $inscription)
    {
        $this->inscription = $inscription;
    }

    public function getInscription(): Inscription
    {
        return $this->inscription;
    }
}
