<?php

namespace App\Form;

use App\Service\EnCours;
use App\Entity\Information;
use Symfony\Component\Form\AbstractType;
use Eckinox\TinymceBundle\Form\Type\TinymceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class InformationType extends AbstractType
{
    protected $enCours;

    public function __construct(EnCours $enCours)
    {
        $this->enCours = $enCours;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $id_saison = $this->enCours->saison()->getId();
        $builder
            ->add('nomStructure', TextType::class, [
                'label' => 'Nom de la structure',
            ])
            ->add('signature', TextType::class, [
                'label' => 'Signature mail',
            ])


            // ->add('espace', TextType::class, [
            //     'label' => 'adresse de l\'espace',
            // ])

            ->add('creationDossier', CheckboxType::class, [
                'label' => 'Autorister la creation d\'un dossier',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])

            ->add('modificationDossier', CheckboxType::class, [
                'label' => 'Autorister les modifications d\'un dossier',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('mensurationPratiquant', CheckboxType::class, [
                'label' => 'Rendre visible les mensurations des pratiquants',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('coursVisible', CheckboxType::class, [
                'label' => 'Rendre visible les cours aux utilisateurs',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])

            ->add('adresse', TextType::class, [
                'label' => 'Adresse',
            ])
            ->add('infoInscription', TextType::class, [
                'required' => false,
                'label' => 'Lien vers une page donnant des precisions sur l\'inscription ',
            ])
            ->add('codePostal', TextType::class, [
                'label' => 'Code Postal',
            ])->add('ville', TextType::class, [
                'label' => 'Ville',
            ])
            ->add('descriptionDossier', TinymceType::class, [
                'label' => 'Information concernant la création d\'un dossier',
            ])
            ->add('descriptionInscription', TinymceType::class, [
                'label' => 'Information concernant l\'inscription',
            ])
            ->add('paiement', TinymceType::class, [
                'label' => 'Information concernant le paiement',
            ])
            ->add('stripe', CheckboxType::class, [
                'label' => 'Autorister le paiement par Stripe',
                'required' => false,
            ])
            ->add('enteteBulletin', TinymceType::class, [
                'label' => 'En-tête du bulletin d\'inscription',
            ])
            ->add('conditionDeVente', TinymceType::class, [
                'label' => 'Information concernant les conditions de vente',
            ])
            ->add('enteteFacture', TinymceType::class, [
                'label' => 'En-tête des factures',
            ])
            ->add('corpsFacture', TinymceType::class, [
                'label' => 'Corps des facture',
            ])
            ->add('piedFacture', TinymceType::class, [
                'label' => 'Pied de page des facture',
            ])
            ->add('boutiquePresentation', TinymceType::class, [
                'label' => 'Information concernant la boutique',
            ])
            ->add('boutiquePaiement', TinymceType::class, [
                'label' => 'Information concernant le paiement des articles',
            ])
            ->add('boutiqueLivraison', TinymceType::class, [
                'label' => 'Information concernant la livraison',
            ])
            ->add('boutiqueStripe', CheckboxType::class, [
                'label' => 'Autoriser le paiement par Stripe',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('boutiqueActive', CheckboxType::class, [
                'label' => 'La boutique est active',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('boutiqueOuvert', CheckboxType::class, [
                'label' => 'La boutique est ouverte',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('reservationPresentation', TinymceType::class, [
                'label' => 'Information concernant les reservations',
            ])
            ->add('reservationStripe', CheckboxType::class, [
                'label' => 'Autoriser le paiement par Stripe',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('reservationActive', CheckboxType::class, [
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
                'label' => 'La reservation est active',
                'required' => false,
            ])
            ->add('site', CheckboxType::class, [
                'label' => 'Activitation du site vitrine',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('vitrine', TextType::class, [
                'label' => 'adresse du site',
            ])

            ->add('lienLive', TextType::class, [
                'label' => 'adresse du live',
                'required' => false,
            ])
            ->add('live', CheckboxType::class, [
                'label' => 'Publier le live',
                'required' => false,
                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('ActivationRelance', CheckboxType::class, [
                'label' => 'Activer les relances dossiers',
                'required' => false,

                'label_attr' => [
                    'class' => 'checkbox-inline checkbox-switch',
                ],
            ])
            ->add('NbEssai', IntegerType::class, [
                'label' => 'Nombre d\'essais',
                'required' => false,
                'attr' => [
                    'min' => 0,
                    'placeholder' => 'Entrez le nombre d\'essais'
                ]
            ])
            ->add('Sauvegarder', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
                'label' => 'Enregistrer',
            ]);
        // ->add('Sauvegarder', SubmitType::class, [
        //     'attr' => ['class' => 'save btn-primary'],
        // ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Information::class,
        ]);
    }
}
