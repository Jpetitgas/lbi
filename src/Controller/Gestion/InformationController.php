<?php

namespace App\Controller\Gestion;

use App\Entity\Information;
use App\Form\InformationType;
use App\Repository\InformationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/information", name="gestion_information")
 */
class InformationController extends AbstractController
{
    /**
     * @Route("/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, InformationRepository $informationRepository): Response
    {
        $information=$informationRepository->findOneBy([]);
        $formInformation = $this->createForm(InformationType::class, $information);
        $formInformation->handleRequest($request);

        if ($formInformation->isSubmitted() && $formInformation->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($information);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_information_edit');
        }

        return $this->render('gestion/information/edit.html.twig', [
            'information' => $information,
            'formInformation' => $formInformation->createView(),
        ]);
    }
}
