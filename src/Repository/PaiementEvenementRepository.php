<?php

namespace App\Repository;

use App\Entity\Evenement;
use App\Entity\Saison;
use App\Entity\PaiementEvenement;
use App\SearchData\PaiementEvenementData;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\AST\Functions\SumFunction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Paiement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Paiement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Paiement[]    findAll()
 * @method Paiement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaiementEvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaiementEvenement::class);
    }

    public function paiements(Saison $saison, PaiementEvenementData $data)
    {
        $query = $this
            ->createQueryBuilder('p')
            ->select('p', 'e', 'u', 't', 'b')
            ->leftJoin('p.typeDePaiement', 't')
            ->leftJoin('p.evenement', 'e')
            ->leftJoin('p.banque', 'b')
            ->leftJoin('p.utilisateur', 'u')
            ->where('e.saison = :saison')
            ->setParameter('saison', $saison);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or p.remarque LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }
        if (!empty($data->typeDePaiement)) {
            $query = $query
                ->andWhere('p.typeDePaiement =:type')
                ->setParameter('type', $data->typeDePaiement);
        }
        if (!empty($data->banque)) {
            $query = $query
                ->andWhere('p.banque =:banque')
                ->setParameter('banque', $data->banque);
        }
        if (!empty($data->encaissement)) {
            $query = $query
                ->andWhere('p.encaissement LIKE :q')
                ->setParameter('q', "%{$data->encaissement}%");
        }
        if ($data->encaisse === true) {
            $query = $query->andWhere('p.encaisse =1');
        }
        if ($data->encaisse === false) {
            $query = $query->andWhere('p.encaisse =0');
        }

        return $query->getQuery();
    }
    public function paiementsEvenement(Evenement $event, PaiementEvenementData $data)
    {
        $query = $this
            ->createQueryBuilder('p')
            ->select('p', 'e', 'u', 't', 'b')
            ->leftJoin('p.typeDePaiement', 't')
            ->leftJoin('p.evenement', 'e')
            ->leftJoin('p.banque', 'b')
            ->leftJoin('p.utilisateur', 'u')
            ->where('p.evenement = :event')
            ->setParameter('event', $event);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or p.remarque LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }
        if (!empty($data->typeDePaiement)) {
            $query = $query
                ->andWhere('p.typeDePaiement =:type')
                ->setParameter('type', $data->typeDePaiement);
        }
        if (!empty($data->banque)) {
            $query = $query
                ->andWhere('p.banque =:banque')
                ->setParameter('banque', $data->banque);
        }
        if (!empty($data->encaissement)) {
            $query = $query
                ->andWhere('p.encaissement LIKE :q')
                ->setParameter('q', "%{$data->encaissement}%");
        }
        if ($data->encaisse === true) {
            $query = $query->andWhere('p.encaisse =1');
        }
        if ($data->encaisse === false) {
            $query = $query->andWhere('p.encaisse =0');
        }

        return $query->getQuery();
    }
}
