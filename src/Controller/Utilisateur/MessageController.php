<?php

namespace App\Controller\Utilisateur;

use App\Entity\Messages;
use App\Service\EnCours;
use App\Form\MessagesType;
use App\Form\RepondreType;
use App\Service\SendEmail;
use App\SearchData\MessageData;
use App\SearchForm\MessageForm;
use App\Message\SendEmailMessage;
use App\Repository\SujetRepository;
use Symfony\Component\Mime\Message;
use App\Repository\DossierRepository;
use App\Repository\MessagesRepository;
use App\Repository\UtilisateurRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/utilisateur/message", name="utilisateur_message")
 */
class MessageController extends AbstractController
{
    /**
     * @Route("/reception", name="_reception")
     */
    public function reception(MessagesRepository $messagesRepository, PaginatorInterface $paginator, Request $request, Security $security): Response
    {
        $nbNonLu = $messagesRepository->nonLu($this->getUser());
        $data = new MessageData();
        $form = $this->createForm(MessageForm::class, $data);
        $form->handleRequest($request);

        $destinataire = $this->getUser();
        $messages = $paginator->paginate(
            $messagesRepository->messagesReceptions($destinataire, $data),
            $request->query->getInt('page', 1),
            $data->nb
        );

        return $this->render('utilisateur/message/reception.html.twig', [
            'messages' => $messages,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/envoi", name="_envoi")
     */
    public function envoi(MessagesRepository $messagesRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $data = new MessageData();
        $form = $this->createForm(MessageForm::class, $data);
        $form->handleRequest($request);
        $expediteur = $this->getUser();
        $messages = $paginator->paginate(
            $messagesRepository->messagesEnvoye($expediteur, $data),
            $request->query->getInt('page', 1),
            $data->nb
        );

        return $this->render('utilisateur/message/envoi.html.twig', [
            'messages' => $messages,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/reception/{{id}}", name="_reception_show")
     */
    public function showReception(Messages $message, Request $request, EnCours $enCours, DossierRepository $dossierRepository): Response
    {
        if ($message->getDestinataire() === $this->getUser()) {
            $saison = $enCours->saison();
            $dossier = $dossierRepository->findOneby(['saison' => $saison, 'adherent' => $message->getExpediteur()]);
            $message->setIsRead(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            return $this->render('utilisateur/message/receptionShow.html.twig', [
                'message' => $message,
                'dossier' => $dossier,
            ]);
        }

        return $this->redirectToRoute('utilisateur_message_reception');
    }

    /**
     * @Route("/show/envoi/{{id}}", name="_envoi_show")
     */
    public function showEnvoi(Messages $message, Request $request): Response
    {
        if ($message->getExpediteur() === $this->getUser()) {
            return $this->render('utilisateur/message/envoiShow.html.twig', compact('message'));
        }

        return $this->redirectToRoute('utilisateur_message_envoi');
    }

    /**
     * @Route("/new", name="_new")
     */
    public function new(Request $request, SujetRepository $sujetRepository, SendEmail $sendEmail, MessageBusInterface $messageBus, UtilisateurRepository $utilisateurRepository): Response
    {
        $message = new Messages();
        $formMessage = $this->createForm(MessagesType::class, $message, []);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            if ($formMessage->offsetExists('destinataire')) {
                $destinataire = $formMessage->get('destinataire')->getData();
                $sujet = $formMessage->get('titre')->getData();
            } else {
                $sujet = $sujetRepository->find($request->request->get('messages')['sujet']);
                $destinataire = $sujet->getDestinataire();
                if ($sujet->getSujet() === 'Message à un intervenant') {
                    $destinataire = $utilisateurRepository->find($request->request->get('messages')['intervenant']);
                }
                $sujet = $sujet->getSujet();
            }

            $message->setExpediteur($this->getUser())
                ->setDestinataire($destinataire)
                ->setTitre($sujet);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();


            $sendEmail->send([
                'recipient_email' => $destinataire->getEmail(),
                'sujet' => 'Vous avez reçu un nouveau message',
                'htmlTemplate' => 'email/infoMessage.html.twig',
                'destinataire' => $destinataire,
                'contenu' => $message->getTitre(),
            ]);

            $this->addFlash('success', 'Votre message a été envoyé');

            return $this->redirectToRoute('utilisateur_message_reception');
        }

        return $this->render('utilisateur/message/new.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }

    /**
     * @Route("/repondre/{id_destinaire}/{titre}", name="_repondre")
     */
    public function repondre($id_destinaire, $titre, Request $request, SendEmail $sendEmail, UtilisateurRepository $utilisateurRepository, MessageBusInterface $messageBus): Response
    {
        $destinataire = $utilisateurRepository->find($id_destinaire);
        $message = new Messages();
        $formMessage = $this->createForm(RepondreType::class, $message);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $message->setExpediteur($this->getUser())
                ->setDestinataire($destinataire)
                ->setTitre($titre);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();
            $sendEmail->send([
                'recipient_email' => $destinataire->getEmail(),
                'sujet' => 'Vous avez reçu un nouveau message',
                'htmlTemplate' => 'email/infoMessage.html.twig',
                'destinataire' => $destinataire,
                'contenu' => '',
            ]);

            $this->addFlash('success', 'Votre message a été envoyé');

            return $this->redirectToRoute('utilisateur_message_reception');
        }

        return $this->render('utilisateur/message/new.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }
    /**
     * @Route("/mark-all-sent-as-read", name="_mark_all_sent_as_read")
     */
    public function markAllSentAsRead(MessagesRepository $messagesRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_GESTION');

        $user = $this->getUser();
        $unreadMessages = $messagesRepository->findBy([
            'expediteur' => $user,
            'is_read' => false
        ]);

        foreach ($unreadMessages as $message) {
            $message->setIsRead(true);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        $this->addFlash('success', 'Tous les messages envoyés ont été marqués comme lus.');

        return $this->redirectToRoute('utilisateur_message_envoi');
    }
}
