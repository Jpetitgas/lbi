let valider = document.querySelectorAll(".valider")
for (let bouton of valider) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/inscription/certificatvalider/' + this.dataset.id)
        xmlhttp.send()
    })
}
let transmis = document.querySelectorAll(".transmis")
for (let bouton of transmis) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/inscription/transmisassureur/' + this.dataset.id)
        xmlhttp.send()
    })
}
let encaisser = document.querySelectorAll(".encaisser")
for (let bouton of encaisser) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/paiement/encaisser/' + this.dataset.id)
        xmlhttp.send()
    })
}
let doc = document.querySelectorAll(".doc")
for (let select of doc) {
    select.addEventListener("change", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/inscription/document/' + this.dataset.id + '/' + this.value)
        xmlhttp.send()
    })
}


