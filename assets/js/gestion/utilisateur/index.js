
    let verified = document.querySelectorAll(".verified")
    for (let bouton of verified) {
        bouton.addEventListener("click", function () {
            let xmlhttp = new XMLHttpRequest;
            xmlhttp.open("get", '/gestion/utilisateur/verified/' + this.dataset.id)
            xmlhttp.send()
        })
    }

    let message = document.getElementById('message')

    message.addEventListener("click", function () {
        let selecteds = document.querySelectorAll(".selected")
        let ids = [];
        for (let selected of selecteds) {
            ids.push(selected.querySelector('.sorting_1').textContent);
        }
        if (ids.length) {
            window.location.href = "/gestion/correspondance/message/" + ids.toString();
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }

    });
    let email = document.getElementById('email')

    email.addEventListener("click", function () {
        let selecteds = document.querySelectorAll(".selected")
        let ids = [];
        for (let selected of selecteds) {
            ids.push(selected.querySelector('.sorting_1').textContent);
        }
        if (ids.length) {
            window.location.href = "/gestion/correspondance/email/" + ids.toString();
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }

    });
