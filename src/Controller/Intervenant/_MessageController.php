<?php

namespace App\Controller\Intervenant;

use App\Entity\Messages;
use App\Form\RepondreType;
use App\Form\MessagesGestionType;
use App\Message\SendEmailMessage;
use App\Repository\SujetRepository;
use Symfony\Component\Mime\Message;
use App\Form\MessagesIntervenantType;
use App\Repository\MessagesRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/intervenant/message", name="intervenant_message")
 */
class _MessageController extends AbstractController
{
    /**
     * @Route("/reception", name="_reception")
     */
    public function reception(MessagesRepository $messagesRepository): Response
    {
        $nbNonLu = $messagesRepository->nonLu($this->getUser());
        $messages = $messagesRepository->findBy(['destinataire' => $this->getUser()], ['id' => 'DESC']);
        return $this->render('intervenant/message/reception.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * @Route("/envoi", name="_envoi")
     */
    public function envoi(MessagesRepository $messagesRepository): Response
    {
        $messages = $messagesRepository->findBy(['expediteur' => $this->getUser()], ['id' => 'DESC']);
        return $this->render('intervenant/message/envoi.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * @Route("/show/reception/{{id}}", name="_reception_show")
     */
    public function showReception(Messages $message, Request $request): Response
    {
        $message->setIsRead(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();
        return $this->render('intervenant/message/receptionShow.html.twig', compact("message"));
    }

    /**
     * @Route("/show/envoi/{{id}}", name="_envoi_show")
     */
    public function showEnvoi(Messages $message, Request $request): Response
    {
        return $this->render('intervenant/message/envoiShow.html.twig', compact("message"));
    }

    /**
     * @Route("/new", name="_new")
     */
    public function new(Request $request, SujetRepository $sujetRepository, MessageBusInterface $messageBus): Response
    {
        $message = new Messages();
        $formMessage = $this->createForm(MessagesIntervenantType::class, $message);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $sujet = $sujetRepository->find($request->request->get('messages_intervenant')['sujet']);

            $message->setExpediteur($this->getUser())
                ->setDestinataire($sujet->getDestinataire())
                ->setTitre($sujet->getSujet());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();

            $messageBus->dispatch(new SendEmailMessage(
                $sujet->getDestinataire()->getEmail(),
                'Vous avez reçu un nouveau message',
                $sujet->getDestinataire(),
                'email/infoMessage.html.twig',
                []
            ));

            $this->addFlash('success', 'Votre message a été envoyé');

            return $this->redirectToRoute('intervenant_message_reception');
        }

        return $this->render('intervenant/message/new.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }
    /**
     * @Route("/repondre/{id_destinaire}/{titre}", name="_repondre")
     */
    public function repondre($id_destinaire, $titre, Request $request, UtilisateurRepository $utilisateurRepository, MessageBusInterface $messageBus): Response
    {
        $destinataire = $utilisateurRepository->find($id_destinaire);
        $message = new Messages();
        $formMessage = $this->createForm(RepondreType::class, $message);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $message->setExpediteur($this->getUser())
                ->setDestinataire($destinataire)
                ->setTitre($titre);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();

            $messageBus->dispatch(new SendEmailMessage(
                $destinataire->getEmail(),
                'Vous avez reçu un nouveau message',
                $destinataire,
                'email/infoMessageGestion.html.twig',
                []
            ));


            $this->addFlash('success', 'Votre message a été envoyé');

            return $this->redirectToRoute('intervenant_message_reception');
        }

        return $this->render('intervenant/message/new.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }
}
