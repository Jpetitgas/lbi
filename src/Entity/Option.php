<?php

namespace App\Entity;

use App\Repository\OptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OptionRepository::class)
 * @ORM\Table(name="`option`")
 */
class Option
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Evenement::class, inversedBy="options")
     */
    private $evenement;

    /**
     * @ORM\OneToMany(targetEntity=OptionPurchase::class, mappedBy="optionEvenement")
     */
    private $optionPurchases;

    public function __construct()
    {
        $this->optionPurchases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getEvenement(): ?Evenement
    {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): self
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * @return Collection|OptionPurchase[]
     */
    public function getOptionPurchases(): Collection
    {
        return $this->optionPurchases;
    }

    public function addOptionPurchase(OptionPurchase $optionPurchase): self
    {
        if (!$this->optionPurchases->contains($optionPurchase)) {
            $this->optionPurchases[] = $optionPurchase;
            $optionPurchase->setOptionEvenement($this);
        }

        return $this;
    }

    public function removeOptionPurchase(OptionPurchase $optionPurchase): self
    {
        if ($this->optionPurchases->removeElement($optionPurchase)) {
            // set the owning side to null (unless already changed)
            if ($optionPurchase->getOptionEvenement() === $this) {
                $optionPurchase->setOptionEvenement(null);
            }
        }

        return $this;
    }
}
