    var $table = $( '#utilisateur' );
    function getRowSelections() {
return $.map( $table.bootstrapTable( 'getSelections' ), function ( row ) {
return row;
} )
}


    $( '#message' ).click( function () {
var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each( selectedRows, function ( index, value ) {
        ids.push(value.id);
} );
    if ( ids.length ) {
        window.location.href = "/gestion/correspondance/message/" + ids.toString();
} else {
        alert('Vous devez d\'abord selectionner une ligne');
}
} );

    $( '#email' ).click( function () {
var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each( selectedRows, function ( index, value ) {
        ids.push(value.id);
} );
    if ( ids.length ) {
        window.location.href = "/gestion/correspondance/email/" + ids.toString();
} else {
        alert('Vous devez d\'abord selectionner une ligne');
}
} );

    $( '#compte' ).click( function () {
var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each( selectedRows, function ( index, value ) {
        ids.push(value.id);
} );
    if ( ids.length ) {
        window.location.href = "/gestion/utilisateur/verifiedGroupe/" + ids.toString();

} else {
        alert('Vous devez d\'abord selectionner une ligne');
}
} );
