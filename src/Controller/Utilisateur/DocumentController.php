<?php

namespace App\Controller\Utilisateur;

use App\Entity\Document;
use App\Form\DocumentType;
use App\Repository\DossierRepository;
use App\Repository\InformationRepository;
use App\Repository\PratiquantRepository;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur/document", name="utilisateur_document")
 */
class DocumentController extends AbstractController
{
    /**
     * @Route("/new/{id_pratiquant}/{id_dossier}", name="_new", methods={"GET","POST"})
     */
    public function new($id_pratiquant, $id_dossier, Request $request, DossierRepository $dossierRepository, PratiquantRepository $pratiquantRepository, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        $document = new Document();
        $pratiquant = $pratiquantRepository->find($id_pratiquant);
        $dossier = $dossierRepository->find($id_dossier);
        $formDocument = $this->createForm(DocumentType::class, $document, ['pratiquant' => $pratiquant, 'dossier' => $dossier]);
        $formDocument->handleRequest($request);

        if ($formDocument->isSubmitted() && $formDocument->isValid()) {
            if ($formDocument->get('image')->getData()) {
                $image = $formDocument->get('image')->getData();
                $fichier = md5(uniqid()).'.'.$image->guessExtension();
                $image->move(
                    $this->getParameter('images_repertoire'),
                    $fichier
                );
                $document->setNom($fichier);
            }
            $document->setPratiquant($pratiquant);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($document);
            $inscription=$formDocument->get('inscription')->getData();
            if ($inscription) {
                $inscription->setDocument($document);
            }
            $entityManager->flush();
            if ($formDocument->get('ajouter')->isClicked()) {
                return $this->redirectToRoute('utilisateur_inscription_new', ['id_pratiquant' => $pratiquant->getId(), 'id_dossier' => $dossier->getId()]);
            }
            $ancre = 'pratiquant_'.$pratiquant->getId().'_document';
            $this->addFlash('success', 'Le document a été téléverser avec succés');
            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
        }

        return $this->render('utilisateur/document/new.html.twig', [
            'document' => $document,
            'formDocument' => $formDocument->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/{id_dossier}", name="_edit", methods={"GET","POST"})
     */
    public function edit($id_dossier, Request $request, Document $document, DossierRepository $dossierRepository, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }

        $dossier = $dossierRepository->find($id_dossier);
        $formDocument = $this->createForm(DocumentType::class, $document, ['pratiquant' => $document->getPratiquant()->getId(), 'dossier' => $dossier->getId()]);
        $formDocument->handleRequest($request);

        if ($formDocument->isSubmitted() && $formDocument->isValid()) {
            if ($formDocument->get('image')->getData()) {
                $nom = $document->getNom();
                if ($this->getParameter('images_repertoire').'/'.$nom) {
                    unlink($this->getParameter('images_repertoire').'/'.$nom);
                }
                $image = $formDocument->get('image')->getData();
                $fichier = md5(uniqid()).'.'.$image->guessExtension();
                $image->move(
                    $this->getParameter('images_repertoire'),
                    $fichier
                );
                $document->setNom($fichier);
            }
            $inscription=$formDocument->get('inscription')->getData();
            if ($inscription) {
                $inscription->setDocument($document);
            }
            $this->getDoctrine()->getManager()->flush();
            if ($formDocument->get('ajouter')->isClicked()) {
                return $this->redirectToRoute('utilisateur_inscription_new', ['id_pratiquant' => $document->getPratiquant()->getId(), 'id_dossier' => $dossier->getId()]);
            }
            $ancre = 'pratiquant_'.$document->getPratiquant()->getId().'_document';

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
        }

        return $this->render('utilisateur/document/edit.html.twig', [
            'document' => $document,
            'formDocument' => $formDocument->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{id_dossier}/", name="_delete", methods={"POST"})
     */
    public function delete($id_dossier, Request $request, Document $document, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        try {
            if ($this->isCsrfTokenValid('delete'.$document->getId(), $request->request->get('_token'))) {
                $nom = $document->getNom();
                if (file_exists($this->getParameter('images_repertoire').'/'.$nom)) {
                    unlink($this->getParameter('images_repertoire').'/'.$nom);
                }
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($document);
                $entityManager->flush();
            }
        } catch (Exception  $e) {
            $this->addFlash('danger', 'Vous devez supprimer le lien de toutes les activités à ce document avant de le supprimer');
        }
        $ancre = 'pratiquant_'.$document->getPratiquant()->getId().'_document';

        return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
    }
}
