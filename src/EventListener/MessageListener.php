<?php

namespace App\EventListener;

use App\Entity\Messages;
use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class MessageListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison = $encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $message = $event->getObject();

        if ($message instanceof Messages) {
            $message->setSaison($this->saison);
        }
    }
}
