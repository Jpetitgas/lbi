<?php

namespace App\Form;

use App\Entity\Site;
use Symfony\Component\Form\AbstractType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Eckinox\TinymceBundle\Form\Type\TinymceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('slogan', TextType::class, [
                'label' => 'Votre slogan',
            ])
            ->add('description', TinymceType::class, [
                'label' => 'Description de vos activités',
            ])
            ->add('imageFile', VichFileType::class, [
                'label' => 'Choisir une photo pour ce produit',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,

            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Site::class,
        ]);
    }
}
