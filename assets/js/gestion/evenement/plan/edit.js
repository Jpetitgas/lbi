const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const selection = document.getElementById('nom');
const idEvenement = canvas.dataset.evenement;
let selectedContarnaires = [];

/**Get all data of the evenement */

fetch("/gestion/evenement/plan/data/" + idEvenement.toString())
    .then(function (response) { return response.json() })
    .then(containers => {
        render(containers);

    })
    .catch(function (error) {
        console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
    });
function recordPosition(container) {
    const Url = new URL(window.location.href);
    fetch(Url.origin + "/gestion/evenement/container/position/", {
        method: 'POST',
        body: JSON.stringify({
            id: container['id'],
            x: container['x'],
            y: container['y'],
            angle: container['angle'],
            largeur: container['largeur'],
            hauteur: container['hauteur']
        }),
        headers: {
            "Content-type": "application/json"
        }
    })
        .then(function (response) { return response.text() })
        .then(function (text) {
            if (text) {
                console.log(text);
                let contenu = "<div class='alert alert-success' role='alert'><button type='button' class='btn-close' data-bs-dismiss='alert'></button><strong>" + text + "</strong></div>";;
                let alertJs = document.getElementById("alertJs")
                alertJs.innerHTML = contenu;

            } else {
                alert('Il y a eu un pb avec l\'application: la nouvelle position n\'a pas été enregistrée!');
            }
        })
        .catch(function (error) {
            console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
        });
}

function getMousePos(e) {
    const rect = canvas.getBoundingClientRect();
    return {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top,
    }
}
function searchContainerIndex(containers, id) {
    for (let i = 0; i < containers.length; i++) {
        if (containers[i]['id'] === id) {
            return i;
        }
    }
}

function containerBuild(container, containers, select, key) {
    let largeur = container['largeur'];
    let hauteur = container['hauteur'];
    let X = container['x'];
    let Y = container['y'];
    let angleDeg = container['angle'];
    const pas = 1;

    if (key === 'up') {
        Y = Y - pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['y'] = Y

    }
    if (key === 'down') {
        Y = Y + pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['y'] = Y

    }
    if (key === 'left') {
        X = X - pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['x'] = X

    }
    if (key === 'right') {
        X = X + pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['x'] = X

    }
    if (key === 'rotationAnti') {
        angleDeg = angleDeg - pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['angle'] = angleDeg

    }
    if (key === 'rotation') {
        angleDeg = angleDeg + pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['angle'] = angleDeg

    }
    if (key === 'elargir') {
        largeur = largeur + pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['largeur'] = largeur

    }
    if (key === 'rapetissir') {
        largeur = largeur - pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['largeur'] = largeur

    }
    if (key === 'epaisir') {
        hauteur = hauteur + pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['hauteur'] = hauteur

    }
    if (key === 'retraisir') {
        hauteur = hauteur - pas;
        index = searchContainerIndex(containers, container['id']);
        containers[index]['hauteur'] = hauteur

    }

    let nbPlace = container[0].length;
    let nbPlaceParLigne = Math.ceil(nbPlace / container['nbLigne']);

    let positionX = 0;
    let positionY = 0;
    let angle = angleDeg * (Math.PI / 180);

    ctx.translate(X, Y)
    ctx.rotate(angle);
    if (select == "selected") {
        ctx.fillStyle = 'silver';
    } else { ctx.fillStyle = 'beige'; }
    for (let i = 0; i < nbPlace; i++) {
        ctx.fillRect(positionX, positionY, largeur, hauteur);
        ctx.strokeRect(positionX, positionY, largeur, hauteur);
        positionX += largeur;
        if (i != 0 && ((i + 1) % nbPlaceParLigne) == 0) {
            positionY += hauteur;
            if (((i + 1) % nbPlaceParLigne) == 0) {
                positionX = 0;
            }
        }
    }
    ctx.resetTransform();
}
function containerDelete(container) {
    let largeur = container['largeur'] + 5;
    let hauteur = container['hauteur'] + 5;
    let X = container['x'];
    let Y = container['y'];

    let nbPlace = container[0].length;
    let nbPlaceParLigne = Math.ceil(nbPlace / container['nbLigne']);

    let positionX = 0;
    let positionY = 0;
    let angle = container['angle'] * (Math.PI / 180);

    ctx.translate(X, Y)
    ctx.rotate(angle);

    for (let i = 0; i < nbPlace; i++) {
        ctx.clearRect(positionX - 5, positionY - 5, largeur, hauteur + 5);
        positionX += largeur;
        if (i != 0 && ((i + 1) % nbPlaceParLigne) == 0) {
            positionY += hauteur;
            if (((i + 1) % nbPlaceParLigne) == 0) {
                positionX = 0;
            }
        }
    }
    ctx.resetTransform();
}
/**
* Find point after rotation around another point by X degrees
*
* @param {Array} point The point to be rotated [X,Y]
* @param {Array} rotationCenterPoint The point that should be rotated around [X,Y]
* @param {Number} degrees The degrees to rotate the point
* @return {Array} Returns point after rotation [X,Y]
*/
function rotatePoint(point, rotationCenterPoint, degrees) {
    // Using radians for this formula
    var radians = degrees * Math.PI / 180;

    // Translate the plane on which rotation is occurring.
    // We want to rotate around 0,0. We'll add these back later.
    point[0] -= rotationCenterPoint[0];
    point[1] -= rotationCenterPoint[1];

    // Perform the rotation
    var newPoint = [];
    newPoint[0] = point[0] * Math.cos(radians) - point[1] * Math.sin(radians);
    newPoint[1] = point[0] * Math.sin(radians) + point[1] * Math.cos(radians);


    // Translate the plane back to where it was.
    newPoint[0] += rotationCenterPoint[0];
    newPoint[1] += rotationCenterPoint[1];

    return newPoint;
}
/**
* Find the vertices of a rotating rectangle
*
* @param {Array} position From left, top [X,Y]
* @param {Array} size Lengths [X,Y]
* @param {Number} degrees Degrees rotated around center
* @return {Object} Arrays LT, RT, RB, LB [X,Y]
*/
function findRectVertices(position, size, degrees) {
    var left = position[0];
    var right = position[0] + size[0];
    var top = position[1];
    var bottom = position[1] + size[1];
    var radians = degrees * Math.PI / 180;
    var center = [left + (((right - left) / 2) * Math.cos(radians)), top + (((bottom - top) / 2) * Math.sin(radians))];
    var LT = [left, top];
    var RT = [right, top];
    var RB = [right, bottom];
    var LB = [left, bottom];

    return {
        // LT: rotatePoint(LT, center, degrees),
        LT,
        RT: rotatePoint(RT, LT, degrees),
        RB: rotatePoint(RB, LT, degrees),
        LB: rotatePoint(LB, LT, degrees)
    };
}
/**
* Distance formula
*
* @param {Array} p1 First point [X,Y]
* @param {Array} p2 Second point [X,Y]
* @return {Number} Returns distance between points
*/
function distance(p1, p2) {
    return Math.sqrt(Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2));
}

/**
* Heron's formula (triangle area)
*
* @param {Number} d1 Distance, side 1
* @param {Number} d2 Distance, side 2
* @param {Number} d3 Distance, side 3
* @return {Number} Returns area of triangle
*/
function triangleArea(d1, d2, d3) {
    // See https://en.wikipedia.org/wiki/Heron's_formula
    var s = (d1 + d2 + d3) / 2;
    return Math.sqrt(s * (s - d1) * (s - d2) * (s - d3));
}
/**
* Determine if a click hit a rotated rectangle
*
* @param {Array} click Click position [X,Y]
* @param {Array} position Rect from left, top [X,Y]
* @param {Array} size Rect size as lengths [X,Y]
* @param {Number} degrees Degrees rotated around center
* @return {Boolean} Returns true if hit, false if miss
*/
function clickHit(click, position, size, degrees) {
    // Find the area of the rectangle
    // Round to avoid small JS math differences
    var rectArea = Math.round(size[0] * size[1]);

    // Find the vertices
    var vertices = findRectVertices(position, size, degrees);

    // Create an array of the areas of the four triangles
    var triArea = [
        // Click, LT, RT
        triangleArea(
            distance(click, vertices.LT),
            distance(vertices.LT, vertices.RT),
            distance(vertices.RT, click)
        ),
        // Click, RT, RB
        triangleArea(
            distance(click, vertices.RT),
            distance(vertices.RT, vertices.RB),
            distance(vertices.RB, click)
        ),
        // Click, RB, LB
        triangleArea(
            distance(click, vertices.RB),
            distance(vertices.RB, vertices.LB),
            distance(vertices.LB, click)
        ),
        // Click, LB, LT
        triangleArea(
            distance(click, vertices.LB),
            distance(vertices.LB, vertices.LT),
            distance(vertices.LT, click)
        )
    ];

    // Reduce this array with a sum function
    // Round to avoid small JS math differences
    triArea = Math.round(triArea.reduce(function (a, b) { return a + b; }, 0));

    // Finally do that simple thing we visualized earlier
    if (triArea > rectArea) {
        return false;
    }
    return true;
}
function findContainer(mousePose, containers) {

    containers.forEach(container => {
        let click = [mousePose.x, mousePose.y];
        let position = [container['x'], container['y']];
        let nbPlace = container[0].length;
        let nbPlaceParLigne = Math.ceil(nbPlace / container['nbLigne']);
        let size = [container['largeur'] * nbPlaceParLigne, container['hauteur'] * container['nbLigne']]
        let degrees = container['angle']

        if (clickHit(click, position, size, degrees) == true) {
            if (selectedContarnaires.includes(container['id'])) {
                let index = selectedContarnaires.indexOf(container['id']);
                selectedContarnaires.splice(index, 1);
            } else {
                selectedContarnaires.push(container['id']);
            }
        };
    })
    let select = '';
    containers.forEach(container => {

        if (selectedContarnaires.includes(container['id'])) {
            index = searchContainerIndex(containers, container['id'])
            select += containers[index]['nom'] + ', '
        }

    });
    selection.innerText = select;

}

function fond() {
    ctx.drawImage(document.getElementById('fond'),
        0, 0);
}

function render(containers) {
    fond();
    containers.forEach(container => { containerBuild(container, containers, '', ''); });
    canvas.addEventListener("click", (e) => {
        e.preventDefault();
        const mousePos = getMousePos(e);
        findContainer(mousePos, containers);
        containers.forEach(container => {
            if (selectedContarnaires.includes(container['id'])) {
                containerBuild(container, containers, 'selected', '');
            } else {
                containerBuild(container, containers, '', '');
            }
        });
    })
    document.addEventListener("keyup", (e) => {
        containers.forEach(container => {
            if (selectedContarnaires.includes(container['id'])) {
                index = searchContainerIndex(containers, container['id'])
                recordPosition(containers[index]);
            }
        });
    });
    document.addEventListener("keydown", (e) => {
        const touche = e.key;
        if (touche === 'z') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'up');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 'x') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'down');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 'q') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'left');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 'd') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'right');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 'a') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'rotationAnti');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 'e') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'rotation');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 'w') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'elargir');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 'c') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'rapetissir');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 's') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'epaisir');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
        if (touche === 'f') {
            fond();
            containers.forEach(container => {
                if (selectedContarnaires.includes(container['id'])) {
                    containerDelete(container);
                    containerBuild(container, containers, 'selected', 'retraisir');
                } else { containerBuild(container, containers, '', ''); }
            });
        }
    })
}