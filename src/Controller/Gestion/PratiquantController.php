<?php

namespace App\Controller\Gestion;

use App\Service\EnCours;
use App\Entity\Pratiquant;
use App\Form\PratiquantType;
use Doctrine\DBAL\Exception;
use App\Repository\DossierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/pratiquant", name="gestion_pratiquant")
 */
class PratiquantController extends AbstractController
{
    /**
     * @Route("/{id_dossier}/new", name="_new", methods={"GET","POST"})
     */
    public function new($id_dossier, Request $request, EnCours $enCours, DossierRepository $dossierRepository): Response
    {
        $dossier = $dossierRepository->find($id_dossier);
        $pratiquant = new Pratiquant();
        $formPratiquant = $this->createForm(PratiquantType::class, $pratiquant);
        $formPratiquant->handleRequest($request);

        if ($formPratiquant->isSubmitted() && $formPratiquant->isValid()) {
            $pratiquant->setUtilisateur($dossier->getAdherent());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pratiquant);
            $entityManager->flush();

            $ancre = 'pratiquant_'.$pratiquant->getId().'_none';
            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $dossier->getId(), 'ancre' => $ancre]);
        }

        return $this->render('gestion/pratiquant/new.html.twig', [
            'dossier'=>$dossier,
            'pratiquant' => $pratiquant,
            'formPratiquant' => $formPratiquant->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pratiquant $pratiquant, EnCours $enCours): Response
    {
        $formPratiquant = $this->createForm(PratiquantType::class, $pratiquant);
        $formPratiquant->handleRequest($request);
        $dossier = $enCours->dossierAdherent($pratiquant->getUtilisateur());
        if ($formPratiquant->isSubmitted() && $formPratiquant->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            /* @var Dossier $dossier*/

            $ancre = 'pratiquant_'.$pratiquant->getId().'_detail';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $dossier->getId(), 'ancre' => $ancre]);
        }

        return $this->render('gestion/pratiquant/edit.html.twig', [
            'dossier'=>$dossier,
            'pratiquant' => $pratiquant,
            'formPratiquant' => $formPratiquant->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Pratiquant $pratiquant, EnCours $enCours): Response
    {
        $dossier = $enCours->dossierAdherent($pratiquant->getUtilisateur());
        $ancre = 'pratiquant_none_none';
        try {
            if ($this->isCsrfTokenValid('delete'.$pratiquant->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($pratiquant);
                $entityManager->flush();
            }
        } catch (Exception  $e) {
            $this->addFlash('danger', 'Vous devez supprimer tous les éléments de ce pratiquant avant de le supprimer');
        }

        return $this->redirectToRoute('gestion_dossier_edit', ['id' => $dossier->getId(), 'ancre' => $ancre]);
    }
}
