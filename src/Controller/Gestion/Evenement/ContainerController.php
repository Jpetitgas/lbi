<?php

namespace App\Controller\Gestion\Evenement;

use App\Entity\Container;
use App\Entity\Evenement;
use App\Entity\Place;
use App\Form\ContainerType;
use App\Repository\ContainerRepository;
use App\Repository\EvenementRepository;
use App\Repository\PlaceRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("gestion/evenement/container", name="gestion_evenement_container")
 */
class ContainerController extends AbstractController
{
    /**
     * @Route("/{id_evenement}/index", name="_index", methods={"GET"})
     */
    public function index(ContainerRepository $containerRepository): Response
    {
        return $this->render('container/index.html.twig', [
            'containers' => $containerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id_evenement}/new", name="_new", methods={"GET","POST"})
     */
    public function new($id_evenement, Request $request, EvenementRepository $evenementRepository): Response
    {
        $evenement = $evenementRepository->find($id_evenement);
        $container = new Container();
        $formContainer = $this->createForm(ContainerType::class, $container);
        $formContainer->handleRequest($request);

        if ($formContainer->isSubmitted() && $formContainer->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            for ($p = 1; $p <= $formContainer->get('nbPlace')->getData(); ++$p) {
                $place = new Place();
                $place->setContainer($container)
                        ->setNom('vide')
                        ->setVerrouiller(false);
                $entityManager->persist($place);
            }
            $h = 10;
            $l = 10;
            $angle = 0;
            $x = 100;
            $y = 100;
            $z = 2000 + $container->getId();

            $container->setEvenement($evenement)
                    ->setHauteur($h)
                    ->setLargeur($l)
                    ->setAngle($angle)
                    ->setX($x)
                    ->setY($y)
                    ->setZ($z);

            $entityManager->persist($container);

            $entityManager->flush();

            $evenement = $container->getEvenement();

            return $this->redirectToRoute('gestion_evenement_edit', ['id' => $evenement->getid(),'ancre'=>'container'], Response::HTTP_SEE_OTHER);
        }

        return $this->render('gestion/container/new.html.twig', [
            'id_evenement' => $id_evenement,
            'container' => $container,
            'formContainer' => $formContainer->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Container $container): Response
    {
        $formContainer = $this->createForm(ContainerType::class, $container);
        $formContainer->handleRequest($request);

        if ($formContainer->isSubmitted() && $formContainer->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $evenement = $container->getEvenement();

            return $this->redirectToRoute('container_index', ['id_evenement' => $evenement->getid()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('gestion/container/edit.html.twig', [
            'container' => $container,
            'formContainer' => $formContainer->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Container $container): Response
    {
        $evenement = $container->getEvenement();
        if ($this->isCsrfTokenValid('delete'.$container->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($container);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $evenement->getId(),'ancre'=>'container']);
    }


    /**
     * @Route("/nomcontainer/{id_container}/{nom}", name="_nomcontainer", methods={"GET"})
     */
    public function nomContainer($id_container, $nom, ContainerRepository $containerRepository): Response
    {
        $container = $containerRepository->find($id_container);
        if ($container) {
            $container->setNom($nom);
            $em = $this->getDoctrine()->getManager();
            $em->persist($container);
            $em->flush();
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $container->getEvenement()->getId(),'ancre'=>'container']);
    }

    /**
     * @Route("/xcontainer/{id_container}/{x}", name="_xcontainer", methods={"GET"})
     */
    public function xcontainer($id_container, $x, ContainerRepository $containerRepository): Response
    {
        $container = $containerRepository->find($id_container);
        if ($container) {
            $container->setX($x);
            $em = $this->getDoctrine()->getManager();
            $em->persist($container);
            $em->flush();
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $container->getEvenement()->getId(),'ancre'=>'container']);
    }
    /**
     * @Route("/ycontainer/{id_container}/{y}", name="_ycontainer", methods={"GET"})
     */
    public function ycontainer($id_container, $y, ContainerRepository $containerRepository): Response
    {
        $container = $containerRepository->find($id_container);
        if ($container) {
            $container->setY($y);
            $em = $this->getDoctrine()->getManager();
            $em->persist($container);
            $em->flush();
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $container->getEvenement()->getId(),'ancre'=>'container']);
    }


    /**
     * @Route("/price/{id_container}/{price}", name="_price", methods={"GET"})
     */
    public function price($id_container, $price, ContainerRepository $containerRepository): Response
    {
        $container = $containerRepository->find($id_container);
        if ($container) {
            $container->setPricePlace($price);
            $em = $this->getDoctrine()->getManager();
            $em->persist($container);
            $em->flush();
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $container->getEvenement()->getId(),'ancre'=>'container']);
    }


    /**
     * @Route("/position/", name="_position", methods={"GET", "POST"})
     */
    public function position(Request $request, ContainerRepository $containerRepository): Response
    {
        $params = json_decode($request->getContent(), true);
        extract($params);

        $container = $containerRepository->find($id);
        $container->setX($x)
                ->setY($y)
                ->setAngle($angle)
                ->setLargeur($largeur)
                ->setHauteur($hauteur);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return new Response('Le plan a été enregistré', Response::HTTP_OK);
    }

    /**
     * @Route("/dupliquer/{id_container}", name="_dupliquer", methods={"GET", "POST"})
     */
    public function dupliquer($id_container, ContainerRepository $containerRepository)
    {
        $container = $containerRepository->find($id_container);
        $containerClone = clone $container;
        $em = $this->getDoctrine()->getManager();
        $em->persist($containerClone);
        $em->flush();

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $container->getEvenement()->getId(),'ancre'=>'container']);
    }
}
