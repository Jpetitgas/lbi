<?php

namespace App\EventDispatcher;

use App\Entity\UserChatNotification;
use App\Event\NouveauMessageChatEvent;
use App\Repository\UserChatNotificationRepository;
use App\Service\SendEmail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class NouveauMessageChatSubscriber implements EventSubscriberInterface
{
    private $sendEmail;
    private $security;
    private $entityManager;
    private $userChatNotificationRepository;

    public function __construct(
        SendEmail $sendEmail,
        Security $security,
        EntityManagerInterface $entityManager,
        UserChatNotificationRepository $userChatNotificationRepository
    ) {
        $this->sendEmail = $sendEmail;
        $this->security = $security;
        $this->entityManager = $entityManager;
        $this->userChatNotificationRepository = $userChatNotificationRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            'ajout.message.chat' => 'onNewChatMessage',
        ];
    }

    private function sendNotification($recipient, $sender, $chat)
    {
        $this->sendEmail->send([
            'recipient_email' => $recipient->getEmail(),
            'sujet' => 'Nouveau message dans le chat',
            'htmlTemplate' => 'email/newChatMessage.html.twig',
            'destinataire' => $recipient,
            'contenu' => [
                'courseName' => $chat->getCours()->getNom(),
                'sender' => $sender->getUsername(), // ou $sender->getEmail() selon votre configuration
                'messagePreview' => substr($chat->getMessage(), 0, 50) . '...',
                'courseId' => $chat->getCours()->getId(),
                // Ajoutez ici d'autres variables si nécessaire
            ],
        ]);

        // Ici, vous pouvez également ajouter la logique pour envoyer une notification in-app
    }
    public function onNewChatMessage(NouveauMessageChatEvent $event)
    {
        $chat = $event->getChat();
        $cours = $chat->getCours();
        $sender = $chat->getExpediteur();

        $recipients = $this->getRecipients($cours);

        foreach ($recipients as $recipient) {
            if ($recipient === $sender) {
                continue;
            }

            $notification = $this->userChatNotificationRepository->findOneBy(['user' => $recipient]);
            if (!$notification) {
                $notification = new UserChatNotification();
                $notification->setUser($recipient);
                $this->entityManager->persist($notification);
            }

            if (!$notification->getHasUnreadMessages()) {
                $notification->setHasUnreadMessages(true);
                $notification->setLastNotificationSent(new \DateTime());
                $this->sendNotification($recipient, $sender, $chat);
            }

            $notification->addUnreadCourse($cours->getId());
        }

        $this->entityManager->flush();
    }

    private function getRecipients($cours)
    {
        $recipients = [$cours->getIntervenant()];
        foreach ($cours->getInscriptions() as $inscription) {
            $recipients[] = $inscription->getDossier()->getAdherent();
        }
        return array_unique($recipients);
    }
}
