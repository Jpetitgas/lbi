<?php

namespace App\Controller\Gestion;

use App\Repository\ActiviteRepository;
use App\Repository\DossierRepository;
use App\Repository\InscriptionRepository;
use App\Repository\PaiementRepository;
use App\Repository\PratiquantRepository;
use App\Service\Alerte;
use App\Service\EnCours;
use App\Service\InfosDossierEncours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class GestionController extends AbstractController
{
    /**
     * @Route("/gestion", name="gestion")
     */
    public function index(Alerte $alerte, EnCours $enCours, InfosDossierEncours $infosDossierEncours, ActiviteRepository $activiteRepository, InscriptionRepository $inscriptionRepository, PaiementRepository $paiementRepository, DossierRepository $dossierRepository, ChartBuilderInterface $chartBuilder): Response
    {
        $saison = $enCours->saison();
        $dossiers = $dossierRepository->findBy(['saison' => $saison]);
        $totalCotisation = 0;
        foreach ($dossiers as $dossier) {
            $totalCotisation += $infosDossierEncours->cotisation($dossier)['total'];
        }

        $paiement = $paiementRepository->totalPaiementParSaison($saison);
        if ($paiement[0]['totalPaiement']) {
            $totalPaiement = $paiement[0]['totalPaiement'];
        } else {
            $totalPaiement = 0;
        }

        $pratiquants = $inscriptionRepository->pratiquantParSaison($saison);

        $inscriptions = $inscriptionRepository->ToutesInscriptionsParSaison($saison);

        // graphique à faire
        $alertes = $alerte->tousLesManquesParSaison($saison);
        $data = [];
        foreach ($alertes as $alerte) {
            array_push($data, $alerte);
        }

        $chartAFaire = $chartBuilder->createChart(Chart::TYPE_BAR_HORIZONTAL);

        $chartAFaire->setData([
            'labels' => ['bulletins d\'inscription manquant', 'bulletins d\'inscription à valider', 'Cotisations à régler', 'Certificats manquants', 'Certificats à valider', 'A transmettre', 'Dossier complets'],
            'datasets' => [
                [
                    'label' => 'Elements à traiter',
                    'backgroundColor' => [
                        'rgb(255, 99, 132)',
                        'rgb(75, 192, 192)',
                        'rgb(255, 205, 86)',
                        'rgb(201, 203, 207)',
                        'rgb(54, 162, 235)',
                        'rgb(75, 192, 192)',
                        'rgb(51, 255, 122)',
                    ],
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $data,
                ],
            ],
        ]);

        $chartAFaire->setOptions([
            'scales' => [
                'xAxes' => [
                    ['ticks' => ['min' => 0]],
                ],
            ],
        ]);

        $labels = [];
        $nbPlace = [];
        $nbInscription = [];
        $activites = $activiteRepository->tauxInscriptionsParActiviteParSaison($saison);
        foreach ($activites as $activite) {
            array_push($labels, $activite['nom']);
            array_push($nbPlace, $activite['nbDePlace']);
            array_push($nbInscription, $activite['nb']);
        }

        // graphique remplissage cours
        $chartCours = $chartBuilder->createChart(Chart::TYPE_BAR);

        $chartCours->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Nb de places',
                    'backgroundColor' => 'rgb(209, 205, 206)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $nbPlace,
                ],
                [
                    'label' => 'Inscriptions',
                    'backgroundColor' => 'rgb(182, 208, 222)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $nbInscription,
                ],
            ],
        ]);

        return $this->render('gestion/accueil.html.twig', [
            'Nb_dossier' => count($dossiers),
            'alertes' => $alertes,
            'totalCotisation' => $totalCotisation,
            'pratiquants' => $pratiquants,
            'inscriptions' => $inscriptions,
            'totalPaiement' => $totalPaiement,
            'chartCours' => $chartCours,
            'chartAFaire' => $chartAFaire,
        ]);
    }
}
