<?php

namespace App\Twig;

use App\Repository\SaisonRepository;
use App\Repository\DossierRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ExistanceDossierEnCours extends AbstractExtension
{
    protected $saison;
    protected $dossierRepository;

    public function __construct(SaisonRepository $saisonRepository, DossierRepository $dossierRepository)
    {
        $this->dossierRepository=$dossierRepository;
        $this->saison=$saisonRepository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('dossierEnCours', [$this, 'dossierEnCours']),
        ];
    }

    public function dossierEnCours($id_utilisateur)
    {
        $id=$this->saison->saisonCourant()->getId();
        $existanceDossier=$this->dossierRepository->findOneBy(['adherent'=>$id_utilisateur], ['saison'=>$this->saison]);
        if ($existanceDossier ? $existance=1 : $existance=0);
        return $existance;
    }
}
