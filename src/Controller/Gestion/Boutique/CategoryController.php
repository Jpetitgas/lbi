<?php

namespace App\Controller\Gestion\Boutique;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class CategoryController extends AbstractController
{
    /**
     * @Route("gestion/boutique/categorie", name="gestion_boutique_category_index")
     */
    public function indexCategory(CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();

        return $this->render(
            'gestion/boutique/category/index.html.twig',
            [
                'categories' => $categories,
            ]
        );
    }

    /**
     * @Route("gestion/boutique/categorie/edit/{id}", name="gestion_boutique_category_edit")
     */
    public function editCategory($id, Request $request, SluggerInterface $slugger, CategoryRepository $categoryRepository): Response
    {
        $category = $categoryRepository->find($id);
        $formCategory = $this->createForm(CategoryType::class, $category);
        $formCategory->handleRequest($request);

        if ($formCategory->isSubmitted() && $formCategory->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_boutique_category_index');
        }

        return $this->render('gestion/boutique/category/edit.html.twig', [
            'category' => $category,
            'formCategory' => $formCategory->createView(),
        ]);
    }

    /**
     * @Route("gestion/boutique/category/new", name="gestion_boutique_category_new")
     */
    public function newCategory(Request $request, SluggerInterface $slugger): Response
    {
        $category = new Category();
        $formCategory = $this->createForm(CategoryType::class, $category);
        $formCategory->handleRequest($request);

        if ($formCategory->isSubmitted() && $formCategory->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_boutique_category_index');
        }

        return $this->render('gestion/boutique/category/new.html.twig', [
            'category' => $category,
            'formCategory' => $formCategory->createView(),
        ]);
    }

    /**
     * @Route("gestion/boutique/category/delete/{id}", name="gestion_boutique_category_delete")
     */
    public function delete($id, Request $request, CategoryRepository $categoryRepository): Response
    {
        $categery = $categoryRepository->find($id);
        if ($categery) {
            if ($this->isCsrfTokenValid('delete'.$categery->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($categery);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('gestion_boutique_category_index');
    }
}
