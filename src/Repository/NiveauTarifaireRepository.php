<?php

namespace App\Repository;

use App\Entity\NiveauTarifaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NiveauTarifaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method NiveauTarifaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method NiveauTarifaire[]    findAll()
 * @method NiveauTarifaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NiveauTarifaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NiveauTarifaire::class);
    }

    // /**
    //  * @return NiveauTarifaire[] Returns an array of NiveauTarifaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NiveauTarifaire
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
