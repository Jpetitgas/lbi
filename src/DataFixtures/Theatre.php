<?php

namespace App\DataFixtures;

use App\Entity\Activite;
use App\DataFixtures\Parametres;
use App\DataFixtures\Utilisateurs;
use App\Entity\Cours;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class Theatre extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $activite = new Activite();
        $activite->setSaison($this->getReference('saison'))
            ->setSection($this->getReference('section_theatre'))
            ->setCategorie($this->getReference('categorie_ce2_cm2'))
            ->setCotisation(0)
            ->setDuree('1h30')
            ->setNbDePlace(36)
            ->setCertificatObligatoire(false)
            ->setNom("theatre-ini-lun")
            ->setSousCondition(false)
            ->setTarif(140);
        $manager->persist($activite);
        $this->addReference('activite5', $activite);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant1'))
            ->setNom("theatre-ini-lun-17h30-18h30");
        $manager->persist($cours);
        $this->addReference('cours_5_1', $cours);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant1'))
            ->setNom("theatre-ini-lun-18h30-19h30");
        $manager->persist($cours);
        $this->addReference('cours_5_2', $cours);

        $activite = new Activite();
        $activite->setSaison($this->getReference('saison'))
            ->setSection($this->getReference('section_theatre'))
            ->setCategorie($this->getReference('categorie_college'))
            ->setCotisation(0)
            ->setDuree('1h30')
            ->setNbDePlace(26)
            ->setCertificatObligatoire(false)
            ->setNom("theatre-mar-college")
            ->setSousCondition(false)
            ->setTarif(140);
        $manager->persist($activite);
        $this->addReference('activite6', $activite);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant3'))
            ->setNom("theatre-college-mar-17h30-18h30");
        $manager->persist($cours);
        $this->addReference('cours_6_1', $cours);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant3'))
            ->setNom("theatre-college-mar-18h30-19h30");
        $manager->persist($cours);
        $this->addReference('cours_6_2', $cours);


        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            Parametres::class,
            Utilisateurs::class,

        ];
    }
}
