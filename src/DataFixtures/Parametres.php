<?php

namespace App\DataFixtures;

use App\Entity\CategorieActivite;
use App\Entity\CategorieSection;
use App\Entity\RegleTarifaire;
use App\Entity\Saison;
use App\Entity\Section;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class Parametres extends Fixture implements DependentFixtureInterface
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
        $section = new CategorieSection();
        $section->setNom('Bien-être');
        $manager->persist($section);

        $libele = new Section();
        $libele->setCategorieSection($section)
                        ->setLibelle('Yoga Qi-Gong')
                        ->setRefAssureur(22007);
        $manager->persist($libele);
        $this->addReference('section_yoga', $libele);

        $section = new CategorieSection();
        $section->setNom('Théâtre');
        $manager->persist($section);
        $libele = new Section();
        $libele->setCategorieSection($section)
                        ->setLibelle('Spectacle vivant (théâtre, art du cirque)')
                        ->setRefAssureur(31100);
        $manager->persist($libele);
        $this->addReference('section_theatre', $libele);

        $section = new CategorieSection();
        $section->setNom('Danse');
        $manager->persist($section);
        $libele = new Section();
        $libele->setCategorieSection($section)
                        ->setLibelle('Danses modernes')
                        ->setRefAssureur(30405);
        $manager->persist($libele);
        $this->addReference('section_danse', $libele);

        $categorie = new CategorieActivite();
        $categorie->setDesignation('CE2 à CM2');
        $manager->persist($categorie);
        $this->addReference('categorie_ce2_cm2', $categorie);
        $categorie = new CategorieActivite();
        $categorie->setDesignation('Collège');
        $manager->persist($categorie);
        $this->addReference('categorie_college', $categorie);
        $categorie = new CategorieActivite();
        $categorie->setDesignation('Lycée');
        $manager->persist($categorie);
        $this->addReference('categorie_lycee', $categorie);
        $categorie = new CategorieActivite();
        $categorie->setDesignation('Adulte');
        $manager->persist($categorie);
        $this->addReference('categorie_adulte', $categorie);

        $saison = new Saison();

        $saison->setSaison('2020-2021')
                        ->setDebut(new DateTimeImmutable('9/1/2021'))
                        ->setFin(new DateTimeImmutable('6/30/2022'));
        $manager->persist($saison);
        $this->addReference('saison', $saison);

        $regle = new RegleTarifaire();
        $regle->setSaison($this->getReference('saison'))
                        ->setNom('Cotisation')
                        ->setMontant(10)
                        ->setNiveau($this->getReference('UTILISATEUR'))
                        ->setOperateur($this->getReference('UNIQUE'));
        $manager->persist($regle);

        $regle = new RegleTarifaire();
        $regle->setSaison($this->getReference('saison'))
                        ->setNom('Réduction famille')
                        ->setMontant(-10)
                        ->setNiveau($this->getReference('PRATIQUANT'))
                        ->setOperateur($this->getReference('NOMBRE>='))
                        ->setParametre(2);
        $manager->persist($regle);

        $regle = new RegleTarifaire();
        $regle->setSaison($this->getReference('saison'))
                        ->setNom('Réduction multi-cours')
                        ->setMontant(-10)
                        ->setNiveau($this->getReference('ACTIVITE'))
                        ->setOperateur($this->getReference('NOMBRE>='))
                        ->setParametre(2);
        $manager->persist($regle);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
                        IndispensableFixtures::class,
                ];
    }
}
