
let verrouiller = document.querySelectorAll(".verrouiller")
for (let bouton of verrouiller) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/container/verrouiller/' + this.dataset.id)
        xmlhttp.send()
    })
}


let utilisateur = document.querySelectorAll(".utilisateur")
for (let select of utilisateur) {
    select.addEventListener("change", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/container/utilisateur/' + this.dataset.id + '/' + this.value)
        xmlhttp.send()
    })
}
let nom = document.querySelectorAll(".nom")
for (let input of nom) {
    input.addEventListener("change", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/container/nom/' + this.dataset.id + '/' + this.value)
        xmlhttp.send()
    })
}
let nomcontainer = document.querySelectorAll(".nomcontainer")
for (let input of nomcontainer) {
    input.addEventListener("change", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/container/nomcontainer/' + this.dataset.id + '/' + this.value)
        xmlhttp.send()
    })
}
let xcontainer = document.querySelectorAll(".xcontainer")
for (let input of xcontainer) {
    input.addEventListener("change", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/container/xcontainer/' + this.dataset.id + '/' + this.value)
        xmlhttp.send()
    })
}
let ycontainer = document.querySelectorAll(".ycontainer")
for (let input of ycontainer) {
    input.addEventListener("change", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/container/ycontainer/' + this.dataset.id + '/' + this.value)
        xmlhttp.send()
    })
}
let prices = document.querySelectorAll(".price")
for (let input of prices) {
    input.addEventListener("change", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/container/price/' + this.dataset.id + '/' + this.value)
        xmlhttp.send()
    })
}

