<?php

namespace App\Controller\Gestion;

use App\Entity\Paiement;
use App\Form\PaiementType;
use App\Repository\SaisonRepository;
use App\Repository\DossierRepository;
use App\Repository\PaiementRepository;
use App\SearchData\PaiementDossierData;
use App\SearchForm\PaiementDossierForm;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/paiement", name="gestion_paiement")
 */
class PaiementController extends AbstractController
{
    /**
     * @Route("/{id_saison}/index", name="_index", methods={"GET"})
     */
    public function index($id_saison, PaginatorInterface $paginator, Request $request, SaisonRepository $saisonRepository, PaiementRepository $paiementRepository): Response
    {
        $data = new PaiementDossierData();
        $form = $this->createForm(PaiementDossierForm::class, $data);
        $form->handleRequest($request);

        $saison = $saisonRepository->find($id_saison);
        $paiements = $paginator->paginate(
            $paiementRepository->paiements($saison, $data),
            $request->query->getInt('page', 1),
            $data->nb
        );

        return $this->render('gestion/paiement/index.html.twig', [
            'saison' => $saison,
            'paiements' => $paiements,
            'form'=>$form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Paiement $paiement): Response
    {
        $formPaiement = $this->createForm(PaiementType::class, $paiement);
        $formPaiement->handleRequest($request);
        $dossier = $paiement->getDossier()->getId();
        if ($formPaiement->isSubmitted() && $formPaiement->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $saison = $paiement->getSaison();
            $ancre = 'paiement_none_none';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $paiement->getDossier()->getId(), 'ancre' => $ancre]);
        }

        return $this->render('gestion/paiement/edit.html.twig', [
            'dossier' => $dossier,
            'paiement' => $paiement,
            'formPaiement' => $formPaiement->createView(),
        ]);
    }

    /**
     * @Route("/new/{id_dossier}", name="_new", methods={"GET","POST"})
     */
    public function new($id_dossier, Request $request, DossierRepository $dossierRepository): Response
    {
        $paiement = new Paiement();
        $formPaiement = $this->createForm(PaiementType::class, $paiement);
        $formPaiement->handleRequest($request);
        $dossier = $dossierRepository->find($id_dossier);
        if ($formPaiement->isSubmitted() && $formPaiement->isValid()) {
            $paiement->setDossier($dossier);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($paiement);
            $entityManager->flush();

            $ancre = 'paiement_none_none';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $id_dossier, 'ancre' => $ancre]);
        }

        return $this->render('gestion/paiement/new.html.twig', [
            'dossier' => $id_dossier,
            'paiement' => $paiement,
            'formPaiement' => $formPaiement->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Paiement $paiement): Response
    {
        $dossier = $paiement->getDossier();
        if ($this->isCsrfTokenValid('delete'.$paiement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($paiement);
            $entityManager->flush();
        }
        $this->addFlash('success', 'Paiement supprimé');
        $ancre = 'paiement_none_none';

        return $this->redirectToRoute('gestion_dossier_edit', ['id' => $paiement->getDossier()->getId(), 'ancre' => $ancre]);
    }

    /**
     * @Route("/encaisserGroupe/{ids}", name="_encaisserGroupe", methods={"GET"})
     */
    public function encaisserGroupe($ids, PaiementRepository $paiementRepository): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $paiement = $paiementRepository->find($id);
            $paiement->setEncaisse(($paiement->getEncaisse() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($paiement);
        }
        $em->flush();

        return new Response('Le statut "encaissé" des paiements a changé d\'état', Response::HTTP_OK);
    }

    /**
     * @Route("/encaisser/{id}", name="_encaisser", methods={"GET"})
     */
    public function encaisser($id, PaiementRepository $paiementRepository): Response
    {
        $paiement = $paiementRepository->find($id);
        $paiement->setEncaisse(($paiement->getEncaisse() ? '0' : '1'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($paiement);

        $em->flush();
        $this->addFlash('success', 'Les paiements (encaissé) ont changé d\'état');
        $id_saison = $paiement->getSaison()->getId();

        return $this->redirectToRoute('gestion_paiement_index', ['id_saison' => $id_saison]);
    }
}
