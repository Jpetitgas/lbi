import infoJs from '../../js/module/general.js'
// cacher/visible colonnes
const tableName = document.querySelector("table").id;

if (document.getElementById('itemBox')) {
    function box(content) {
        const html = '<div class="form-check"><input class="form-check-input" type ="checkbox" value="' + content.cellIndex + '" name="box' + content.cellIndex + '" id="box' + content.cellIndex + '" ' + (content.classList.contains('cache') ? "" : "checked") + '><label class="form-check-label" for="box' + content.cellIndex + '">' + content.textContent.trim() + '</label></div>'
        const ul = document.getElementById('itemBox')
        let li = document.createElement('li');
        li.innerHTML = html;
        ul.appendChild(li);
        
        document.getElementById("box" + content.cellIndex).addEventListener('click', function () {
            const colonne = document.querySelectorAll('thead th')[content.cellIndex];
            colonne.classList.toggle('cache');
            var storedArray = JSON.parse(sessionStorage.getItem(tableName));
            colonne.classList.contains('cache') ? storedArray[content.cellIndex] = 'cache' : storedArray[content.cellIndex] = '';
            window.sessionStorage.setItem(tableName, JSON.stringify(storedArray));
            const rows = document.querySelectorAll('tbody tr');
        for (let c = 0; c < rows.length; c++) {
            let cells = rows[c].querySelectorAll('td')
            cells[content.cellIndex].classList.toggle('cache');
        }
    })
}
const colonnes = document.getElementsByClassName('colonne');

if (sessionStorage.getItem(tableName)) {
    var storedArray = JSON.parse(sessionStorage.getItem(tableName));
    var i;
    for (i = 0; i < storedArray.length; i++) {
        if (storedArray[i] === 'cache') {
            document.querySelectorAll('thead th')[i].classList.add('cache');
            const rows = document.querySelectorAll('tbody tr');
            for (let c = 0; c < rows.length; c++) {
                let cells = rows[c].querySelectorAll('td')
                cells[i].classList.add('cache');
            }
        } else {
            document.querySelectorAll('thead th')[i].classList.remove('cache');
            const rows = document.querySelectorAll('tbody tr');
            for (let c = 0; c < rows.length; c++) {
                let cells = rows[c].querySelectorAll('td')
                cells[i].classList.remove('cache');
            }
        };
    }
} else {
    let storedArray = [];
    for (let c = 0; c < colonnes.length; c++) {
        
            if (colonnes[c].classList.contains('cache')) {
            storedArray.push('cache');
        } else { storedArray.push(''); }
      
        
    }
    console.log(storedArray);
    window.sessionStorage.setItem(tableName, JSON.stringify(storedArray));
}
for (let c = 0; c < colonnes.length; c++) {
    if (!colonnes[c].classList.contains('notSelected')) {
        box(colonnes[c]);
    }
}



}
// selectionner toutes les lignes
const rows = document.querySelectorAll('tbody tr');
if (document.getElementById('selection')) {
    document.getElementById('selection').addEventListener('click', function () {
        rows.forEach(row => {
            row.classList.toggle('selection');
        })
    })
}
//selectionner une ligne
rows.forEach(row => {
    row.addEventListener('click', function () {
        row.classList.toggle('selection');
    })
});

//CSV
if (document.getElementById("CSV")) {
    document.getElementById("CSV").addEventListener("click", function () { tableToCSV() });
    function tableToCSV() {
        // Variable to store the final csv data
        let csv_data = [];
        let csvrow = [];
        let rows = document.getElementsByTagName('tr');
        let theadCols = rows[0].querySelectorAll('th');
        for (let j = 0; j < theadCols.length; j++) {

            // Get the text data of each cell of
            // a row and push it to csvrow

            if (!theadCols[j].classList.contains('cache') && theadCols[j].classList.contains('colonne')) {
                csvrow.push(theadCols[j].innerText);
            }
        }
        csv_data.push(csvrow.join(";"));
        // Get each row data
        // Combine each column value with comma
        for (let i = 1; i < rows.length; i++) {

            // Get each column data
            let cols = rows[i].querySelectorAll('td');

            // Stores each csv row data
            let csvrow = [];
            for (let j = 0; j < cols.length; j++) {

                // Get the text data of each cell of
                // a row and push it to csvrow
                if (!cols[j].classList.contains('cache')) {
                    csvrow.push(cols[j].innerText);
                }
            }

            // Combine each column value with comma
            csv_data.push(csvrow.join(";"));
        }
        // combine each row data with new line character
        csv_data = csv_data.join('\n');

        // Call this function to download csv file 
        downloadCSVFile(csv_data);
    }
    function downloadCSVFile(csv_data) {
        // Create CSV file object and feed our
        // csv_data into it
        let CSVFile = new Blob([csv_data], { type: "text/csv" });

        // Create to temporary link to initiate
        // download process
        let temp_link = document.createElement('a');

        // Download csv file
        temp_link.download = "Export.csv";
        let url = window.URL.createObjectURL(CSVFile);
        temp_link.href = url;

        // This link should not be displayed
        temp_link.style.display = "none";
        document.body.appendChild(temp_link);

        // Automatically click the link to trigger download
        temp_link.click();
        document.body.removeChild(temp_link);
    }
}

//
function rowsSelectedUtilisateur() {
    const rows = document.querySelectorAll('.selection');
    let ids = [];
    for (var i = 0; i < rows.length; i++) {
        ids.push(rows[i].querySelectorAll('td')[0].innerText);
    }
    return ids;

}
function rowsSelectedTable() {
    const rows = document.querySelectorAll('.selection');
    let ids = [];
    for (var i = 0; i < rows.length; i++) {
        ids.push(rows[i].querySelectorAll('td')[1].innerText);
    }
    return ids;

}


// message(s) utilisateur
if (document.getElementById('messageUtilisateur')) {
    document.getElementById('messageUtilisateur').addEventListener('click', function () {
        let ids = rowsSelectedUtilisateur();
        if (ids.length) {
            window.location.href = "/gestion/correspondance/message/" + ids.toString();
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}
// Email(s) utilisateur
if (document.getElementById('email')) {
    document.getElementById('email').addEventListener('click', function () {
        let ids = rowsSelectedUtilisateur();
        if (ids.length) {
            window.location.href = "/gestion/correspondance/email/" + ids.toString();
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}
//message pratiquant
if (document.getElementById('messagePratiquant')) {
    document.getElementById('messagePratiquant').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            window.location.href = "/gestion/correspondance/message/pratiquant/" + ids.toString();
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}
// Activer compte utilisateur
if (document.getElementById('compte')) {
    document.getElementById('compte').addEventListener('click', function () {
        let ids = rowsSelectedUtilisateur();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir activer les comptes séléctionnés?");
            if (r == true) {
                fetch("/gestion/utilisateur/verifiedGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let compte = row.querySelector('.compte').innerText;
                            if (compte === "oui") {
                                row.querySelector('.compte').innerText = "non";
                            } else { row.querySelector('.compte').innerText = "oui" }
                        });
                        infoJs(text, 'success');
                    });
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}

// validation Bulletin(s)
if (document.getElementById('validationBulletin')) {
    document.getElementById('validationBulletin').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir valider ce(s) bulletin(s) d\'adhésion?");
            if (r == true) {
                fetch("/gestion/dossier/validationBulletinGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let validationBulletin = row.querySelector('.validationBulletin').innerText;
                            if (validationBulletin === "Oui") {
                                row.querySelector('.validationBulletin').innerText = "Non";
                            } else { row.querySelector('.validationBulletin').innerText = "Oui" }
                        });
                        infoJs(text, 'success');
                    }
                    );
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}
// dossier(s) verrouillé(s)
if (document.getElementById('verrouilleDossier')) {
    document.getElementById('verrouilleDossier').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir changer l\'état du verrouillage de ce(s) dossier(s)?");
            if (r == true) {
                fetch("/gestion/dossier/verrouilleGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let verrouilleDossier = row.querySelector('.verrouilleDossier').innerText;
                            if (verrouilleDossier === "Oui") {
                                row.querySelector('.verrouilleDossier').innerText = "Non";
                                row.classList.toggle('incomplet');
                                row.classList.toggle('complet');
                                row.classList.toggle('selection');
                            } else {
                                row.querySelector('.verrouilleDossier').innerText = "Oui";
                                row.classList.toggle('incomplet');
                                row.classList.toggle('complet');
                                row.classList.toggle('selection');
                            }
                        });
                        infoJs(text, 'success');
                    }
                    );
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}

// dossier(s) suspendu(s)
if (document.getElementById('lockedFile')) {
    document.getElementById('lockedFile').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir changer l\'état de suspension de ce(s) dossier(s)?");
            if (r == true) {
                fetch("/gestion/dossier/lockedFile/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let lockedFile = row.querySelector('.lockedFile').innerText;
                            if (lockedFile === "Oui") {
                                row.querySelector('.lockedFile').innerText = "Non";
                                row.classList.toggle('locked');
                                row.classList.toggle('selection');
                                
                            } else {
                                row.querySelector('.lockedFile').innerText = "Oui";
                                row.classList.toggle('locked');
                                row.classList.toggle('selection');
                            }
                        });
                        infoJs(text, 'success');
                    }
                    );
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}


// dossier(s) supprimé(s)
if (document.getElementById('suppressionDossier')) {
    document.getElementById('suppressionDossier').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir supprimer ce(s) dossier(s)?");
            if (r == true) {
                fetch("/gestion/dossier/deleteGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        infoJs(text, 'success');
                    });
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}

// certificats validé
if (document.getElementById('valider')) {
    document.getElementById('valider').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir valider ce(s) certificat(s) médical(aux)?");
            if (r == true) {
                fetch("/gestion/inscription/certificatvaliderGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let certificat = row.querySelector('.certificat').innerText;
                            if (certificat === "oui") {
                                row.querySelector('.certificat').innerText = "non";
                            } else { row.querySelector('.certificat').innerText = "oui" }
                        });
                        infoJs(text, 'success');
                    }
                    );
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}
//transmis à l'assureur
if (document.getElementById('assureur')) {
    document.getElementById('assureur').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir transmettre ces inscriptions?");
            if (r == true) {
                fetch("/gestion/inscription/transmisassureurgroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let certificat = row.querySelector('.assureur').innerText;
                            if (certificat === "oui") {
                                row.querySelector('.assureur').innerText = "non";
                            } else { row.querySelector('.assureur').innerText = "oui" }
                        });
                        infoJs(text, 'success');
                    }
                    );
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}

//annulation inscription(s)
if (document.getElementById('canceledbuttom')) {
    document.getElementById('canceledbuttom').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir annuler ces inscriptions?");
            if (r == true) {
                fetch("/gestion/inscription/canceled/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let certificat = row.querySelector('.canceled').innerText;
                            if (certificat === "oui") {
                                row.querySelector('.canceled').innerText = "non";
                                row.querySelector('.canceledDate').innerText = new Date().toLocaleString();
                                row.classList.toggle('locked');
                                row.classList.toggle('selection');
                            } else {
                                row.querySelector('.canceled').innerText = "oui";
                                row.querySelector('.canceledDate').innerText = new Date().toLocaleString();
                                row.classList.toggle('locked');
                                row.classList.toggle('selection');
                            }
                        });
                        infoJs(text, 'success');
                    }
                    );
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}

//modification cours
if (document.querySelectorAll(".coursModif")) {
    let cours = document.querySelectorAll(".coursModif")
    for (let select of cours) {
        select.addEventListener("change", function (e) {
            let id = e.target.dataset.id;
            let value = e.target.options[e.target.selectedIndex].value;
            fetch('/gestion/inscription/cours/' + id + '/' + value)
                .then(function (response) { return response.text() })
                .then(function (text) {
                    e.target.parentElement.parentElement.querySelector('.cours').innerText = e.target.options[e.target.selectedIndex].text;
                    infoJs(text, 'success');
                }
                );

        })
    }
}
// paiement encaissement
if (document.getElementById('encaisser')) {
    document.getElementById('encaisser').addEventListener('click', function () {
        const rows = document.querySelectorAll('.selection');
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir changer l\'etat d\'encaissement des paiements séléctionnés?");
            if (r == true) {
                fetch("/gestion/paiement/encaisserGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let encaisse = row.querySelector('.encaisse').innerText;
                            if (encaisse === "oui") {
                                row.querySelector('.encaisse').innerText = "non";
                            } else { row.querySelector('.encaisse').innerText = "oui" }
                        });
                        infoJs(text, 'success');
                    });
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}
// paiementBoutique encaissement
if (document.getElementById('encaisserBoutique')) {
    document.getElementById('encaisserBoutique').addEventListener('click', function () {
        const rows = document.querySelectorAll('.selection');
        let ids = rowsSelectedTable();

        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir changer l\'etat d\'encaissement des paiements séléctionnés?");
            if (r == true) {
                fetch("/gestion/boutique/paiement/encaisserGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let encaisse = row.querySelector('.encaisse').innerText;
                            if (encaisse === "oui") {
                                row.querySelector('.encaisse').innerText = "non";
                            } else { row.querySelector('.encaisse').innerText = "oui" }
                        });
                        infoJs(text, 'success');
                    });
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}
// paiementEvenement encaissement
if (document.getElementById('encaisserEvenement')) {
    document.getElementById('encaisserEvenement').addEventListener('click', function () {
        const rows = document.querySelectorAll('.selection');
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir changer l\'etat d\'encaissement des paiements séléctionnés?");
            if (r == true) {
                fetch("/gestion/evenement/paiement/encaisserGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let encaisse = row.querySelector('.encaisse').innerText;
                            if (encaisse === "oui") {
                                row.querySelector('.encaisse').innerText = "non";
                            } else { row.querySelector('.encaisse').innerText = "oui" }
                        });
                        infoJs(text, 'success');
                    });
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}

//Ajout pratiquant comme present
if (document.getElementById('envoiPresent')) {
    document.getElementById('envoiPresent').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        let id_cours = document.getElementById('coursActuel').value;

        if (id_cours != "Indiquer le cours actuel") {
            if (ids.length) {
                const Url = new URL(window.location.href);
                fetch(Url.pathname + "ajout/", {
                    method: 'POST',
                    body: JSON.stringify({
                        cours: id_cours,
                        pratiquants: ids
                    }),
                    headers: {
                        "Content-type": "application/json"
                    }
                }).then(function (response) {
                    if (response.ok) {
                        alert('la liste des presents a été envoyé');

                    } else {
                        alert('Il y a eu un pb avec l\'application: la liste des presents n\'a pas été enregistrée!');
                    }
                })
                    .catch(function (error) {
                        console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
                    });
            } else {
                alert('Vous devez d\'abord selectionner une ligne');
            }
        } else {
            alert('Vous devez d\'abord selectionner un cours');
        }
    })
}
//commandes livrées
if (document.getElementById('Livraison')) {
    document.getElementById('Livraison').addEventListener('click', function () {
        const rows = document.querySelectorAll('.selection');
        let ids = rowsSelectedTable();

        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir changer l\'etat de livraison de ces commandes?");
            if (r == true) {
                fetch("/gestion/boutique/purchase/item/livraisonGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let livre = row.querySelector('.livre').innerText;
                            if (livre === "Oui") {
                                row.querySelector('.livre').innerText = "Non";
                            } else { row.querySelector('.livre').innerText = "Oui" }
                        });
                        infoJs(text, 'success');
                    });
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}


//option evenement livrée
if (document.getElementById('LivraisonOption')) {
    document.getElementById('LivraisonOption').addEventListener('click', function () {
        const rows = document.querySelectorAll('.selection');
        let ids = rowsSelectedTable();

        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir changer l\'etat de livraison de ces options?");
            if (r == true) {
                fetch("/gestion/evenement/option/purchase/livraisonGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let livre = row.querySelector('.livre').innerText;
                            if (livre === "Oui") {
                                row.querySelector('.livre').innerText = "Non";
                            } else { row.querySelector('.livre').innerText = "Oui" }
                        });
                        infoJs(text, 'success');
                    });
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}
// article(s) du site mis en ligne
if (document.getElementById('enLigne')) {
    document.getElementById('enLigne').addEventListener('click', function () {
        let ids = rowsSelectedTable();
        if (ids.length) {
            let r = confirm("Etes-vous sur de vouloir changer l\'état de mise en ligne de ces articles?");
            if (r == true) {
                fetch("/gestion/site/article/enLigneGroupe/" + ids.toString())
                    .then(function (response) { return response.text() })
                    .then(function (text) {
                        let rows = document.querySelectorAll('.selection');
                        rows.forEach(row => {
                            let enLigneGroupe = row.querySelector('.enLigne').innerText;
                            if (enLigneGroupe === "Oui") {
                                row.querySelector('.enLigne').innerText = "Non";
                                row.classList.toggle('selection');
                            } else {
                                row.querySelector('.enLigne').innerText = "Oui";
                                row.classList.toggle('selection');
                            }
                        });
                        infoJs(text, 'success');
                    }
                    );
            }
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    })
}