<?php

namespace App\Repository;

use App\Entity\Inscription;
use App\Entity\Saison;
use App\SearchData\InscriptionData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Inscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inscription[]    findAll()
 * @method Inscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inscription::class);
    }

    /**
     * @return Inscription[] Returns an array of Inscription objects
     */
    public function InscriptionParSaisonPourActivite($activite)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.activite = :activite')
            ->setParameter('activite', $activite)
            ->getQuery()
            ->getResult();
    }

    public function ToutesInscriptionsParSaison($saison)
    {
        return $this->createQueryBuilder('i')
            ->join('i.dossier', 'd', 'WITH', 'i.dossier=d.id')
            ->andWhere('d.saison = :saison')
            ->setParameter('saison', $saison)
            ->getQuery()
            ->getResult();
    }

    public function nbInscriptionParVilleParSaison($saison)
    {
        return $this->createQueryBuilder('i')
            ->join('i.dossier', 'd', 'WITH', 'i.dossier=d.id')
            ->join('d.adherent', 'u', 'WITH', 'd.adherent=u.id')
            ->andWhere('d.saison = :saison')
            ->setParameter('saison', $saison)
            ->select('u.codePostal, u.ville, count(u.ville) as nb')
            ->groupBy('u.ville')
            ->getQuery()
            ->getResult();
    }

    public function pratiquantParIntervenantParSaison($intervenant, $saison)
    {
        return $this->createQueryBuilder('i')
            ->join('i.dossier', 'd')
            ->where('d.saison = :saison')
            ->setParameter('saison', $saison)
            ->join('i.cours', 'c')
            ->andWhere('c.intervenant = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->getQuery()
            ->getResult();
    }

    public function pratiquantParSaison($saison)
    {
        return $this->createQueryBuilder('i')
            ->join('i.dossier', 'd')
            ->where('d.saison = :saison')
            ->setParameter('saison', $saison)
            ->groupBy('i.pratiquant')
            ->getQuery()
            ->getResult();
    }

    public function coursParSaisonUtilisateur($dossier)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.dossier = :dossier')
            ->setParameter('dossier', $dossier)
            ->groupBy('i.cours')
            ->getQuery()
            ->getResult();
    }

    public function premierListeAttente($activite)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.activite = :activite')
            ->setParameter('activite', $activite)
            ->andWhere('i.statut = :statut')
            ->setParameter('statut', 'LISTE D\'ATTENTE')
            ->orderBy('i.dateChangementStatut', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function inscriptions(Saison $saison, InscriptionData $data)
    {
        $query = $this
            ->createQueryBuilder('i')
            ->select('i', 'd', 'p', 'a', 's', 'c', 'cs', 'int', 'th', 'tb')
            ->leftJoin('i.dossier', 'd')
            ->leftJoin('i.pratiquant', 'p')
            ->leftJoin('i.activite', 'a')
            ->leftJoin('a.section', 's')
            ->leftJoin('p.tailleHaut', 'th')
            ->leftJoin('p.tailleBas', 'tb')
            ->leftJoin('s.categorieSection', 'cs')
            ->leftJoin('i.cours', 'c')
            ->leftJoin('c.intervenant', 'int')
            ->where('d.saison = :saison')
            ->setParameter('saison', $saison);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('p.nom LIKE :q or p.prenom LIKE :q or c.nom LIKE :q or a.nom LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }
        if (!empty($data->section)) {
            $query = $query
                ->andWhere('s.categorieSection =:section')
                ->setParameter('section', $data->section);
        }
        if (!empty($data->refAssureur)) {
            $query = $query
                ->andWhere('a.section =:ref')
                ->setParameter('ref', $data->refAssureur);
        }
        if (!empty($data->activite)) {
            $query = $query
                ->andWhere('i.activite =:activite')
                ->setParameter('activite', $data->activite);
        }
        if (!empty($data->cours)) {
            $query = $query
                ->andWhere('i.cours =:cours')
                ->setParameter('cours', $data->cours);
        }
        if (!empty($data->intervenant)) {
            $query = $query
                ->andWhere('c.intervenant =:intervenant')
                ->setParameter('intervenant', $data->intervenant);
        }
        if ($data->certificatObligatoire === true) {
            $query = $query->andWhere('i.CertificatObligatoire =1');
        }
        if ($data->certificatObligatoire === false) {
            $query = $query->andWhere('i.CertificatObligatoire =0');
        }
        if ($data->certificatPresent === true) {
            $query = $query->andWhere('i.document IS NOT NULL');
        }
        if ($data->certificatPresent === false) {
            $query = $query->andWhere('i.document IS NULL');
        }
        if ($data->certificatValide === true) {
            $query = $query->andWhere('i.CertificatValide =1');
        }
        if ($data->certificatValide === false) {
            $query = $query->andWhere('i.CertificatValide =0 or i.CertificatValide is null');
        }
        if ($data->transmisAssureur === true) {
            $query = $query->andWhere('i.transmisAssureur =1');
        }
        if ($data->transmisAssureur === false) {
            $query = $query->andWhere('i.transmisAssureur =0 or i.transmisAssureur is null');
        }
        if ($data->dossierComplet === true) {
            $query = $query->andWhere('d.dossierVerrouille =1');
        }
        if ($data->dossierComplet === false) {
            $query = $query->andWhere('d.dossierVerrouille =0 or d.dossierVerrouille is null');
        }
        if (!empty($data->statut)) {
            $query = $query
                ->andWhere('i.statut =:statut')
                ->setParameter('statut', $data->statut);
        }
        if ($data->canceled === true) {
            $query = $query->andWhere('i.canceled =1');
        } else if ($data->canceled === false) {
            $query = $query->andWhere('i.canceled =0');
        }

        return $query->getQuery();
    }

    public function presenceIntervenant($intervenant, $saison, $data)
    {
        $query = $this
            ->createQueryBuilder('i')
            ->select('i', 'd', 'p', 'c')
            ->leftJoin('i.dossier', 'd')
            ->leftJoin('i.pratiquant', 'p')
            ->join('i.cours', 'c')
            ->andWhere('c.intervenant = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->andWhere('d.saison = :saison')
            ->setParameter('saison', $saison);

        if (!empty($data->cours)) {
            $query = $query
                ->andWhere('i.cours =:cours')
                ->setParameter('cours', $data->cours);
        }

        return $query->getQuery();
    }


    public function inscriptionsParSaisonContact($saison)
    {
        return $this->createQueryBuilder('i')
            ->join('i.dossier', 'd')
            ->join('d.adherent', 'adherent') // Assurez-vous que cette relation existe dans votre entité Dossier
            ->leftJoin('i.cours', 'c')
            ->leftJoin('c.activite', 'a')
            ->leftJoin('a.section', 'section')
            ->leftJoin('section.categorieSection', 'categorieSection')
            ->leftJoin('c.intervenant', 'intervenant') // Assurez-vous que cette relation existe dans votre entité Cours
            ->where('d.saison = :saison')
            ->andWhere('i.canceled = :canceled')
            ->setParameter('saison', $saison)
            ->setParameter('canceled', false) // Ajoutez cette ligne pour définir le paramètre canceled
            ->select(
                'adherent.nom AS adherentNom',
                'adherent.prenom AS adherentPrenom',
                'adherent.portable AS adherentPortable',
                'c.nom AS coursNom',
                'categorieSection.nom AS sectionNom',
                'intervenant.nom AS intervenantNom',
                'intervenant.prenom AS intervenantPrenom'
            )
            ->getQuery()
            ->getResult();
    }
}
