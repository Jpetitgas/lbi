<?php

namespace App\Controller\Utilisateur;

use App\Service\EnCours;
use App\Entity\Utilisateur;
use App\Form\MotDepasseType;
use App\Form\UtilisateurType;
use App\Form\Utilisateur1Type;
use App\Form\UtilisateurUtilisateurType;
use App\Repository\InformationRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EspaceController extends AbstractController
{
/**
     * @Route("/espace", name="espace", methods={"GET","POST"})
     */
    public function show(EnCours $enCours, Request $request, Security $security, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        $utilisateur = $security->getUser();
        $dossier = $enCours->dossierAdherentConnecte();
        $formUtilisateur = $this->createForm(UtilisateurType::class, $utilisateur);
        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'notice',
                'Vos changements sont sauvegardé'
            );

            return $this->redirectToRoute('utilisateur_dossier_show');
        }

        return $this->render('utilisateur/show.html.twig', [
            'information' => $information,
            'utilisateur' => $utilisateur,
            'dossier' => $dossier,
            'formUtilisateur' => $formUtilisateur->createView(),
        ]);
    }
}
