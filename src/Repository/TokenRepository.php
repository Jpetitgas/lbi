<?php

namespace App\Repository;

use App\Entity\Token;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Token::class);
    }

    public function storeToken($token, $activiteId)
    {
        $tokenEntity = new Token();
        $tokenEntity->setToken($token);
        $tokenEntity->setActiviteId($activiteId);
        $tokenEntity->setExpiration(new \DateTime('+1 hour'));

        $this->_em->persist($tokenEntity);
        $this->_em->flush();
    }

    public function isValidToken($token, $activiteId)
    {
        $tokenEntity = $this->findOneBy(['token' => $token, 'activiteId' => $activiteId]);

        if ($tokenEntity && $tokenEntity->getExpiration() > new \DateTime()) {
            return true;
        }

        return false;
    }
}
