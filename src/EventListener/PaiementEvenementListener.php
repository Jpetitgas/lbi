<?php

namespace App\EventListener;

use App\Entity\PaiementEvenement;
use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class PaiementEvenementListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison=$encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $paiement=$event->getObject();

        if ($paiement instanceof PaiementEvenement) {
            if ($this->saison) {
                $paiement->setSaison($this->saison);
            }
        }
    }
}
