<?php

namespace App\Repository;

use App\Entity\Evenement;
use App\Entity\OptionPurchase;
use App\Entity\Utilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OptionPurchase|null find($id, $lockMode = null, $lockVersion = null)
 * @method OptionPurchase|null findOneBy(array $criteria, array $orderBy = null)
 * @method OptionPurchase[]    findAll()
 * @method OptionPurchase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OptionPurchaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OptionPurchase::class);
    }

    // /**
    //  * @return OptionPurchase[] Returns an array of OptionPurchase objects
    //  */

    public function findByEvenementAndUtilisateur(Evenement $evenement, Utilisateur $utilisateur)
    {
        return $this->createQueryBuilder('op')
            ->select('o.name', 'o.price', 'count(o.name) as qty')
            ->leftJoin('op.optionEvenement', 'o')
            ->andWhere('o.evenement = :evenement')
            ->andWhere('op.utilisateur = :utilisateur')
            ->setParameter('evenement', $evenement)
            ->setParameter('utilisateur', $utilisateur)
            ->addGroupBy('o.name')
            ->getQuery()
            ->getResult();
    }
    public function findAllOptionByEvenementAndUtilisateur(Evenement $evenement, Utilisateur $utilisateur)
    {
        return $this->createQueryBuilder('op')
            ->select('op')
            ->leftJoin('op.optionEvenement', 'o')
            ->andWhere('o.evenement = :evenement')
            ->andWhere('op.utilisateur = :utilisateur')
            ->setParameter('evenement', $evenement)
            ->setParameter('utilisateur', $utilisateur)
            ->getQuery()
            ->getResult();
    }

    public function findByEvenement(Evenement $evenement, $data)
    {
        $query = $this->createQueryBuilder('op')
            ->select('op', 'u', 'o')
            ->leftJoin('op.utilisateur', 'u')
            ->leftJoin('op.optionEvenement', 'o')
            ->where('o.evenement = :val')
            ->setParameter('val', $evenement);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or o.name LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }
        if (!empty($data->livre)) {
            $query = $query
                ->andWhere('op.Delivery =:type')
                ->setParameter('type', $data->livre);
        }

        return $query->getQuery();
    }

    /*
    public function findOneBySomeField($value): ?OptionPurchase
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
