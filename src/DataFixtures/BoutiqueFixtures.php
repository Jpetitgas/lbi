<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Taille;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BoutiqueFixtures extends Fixture
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        $origine = './public/assets/sauvegarde/boutique/';
        $destination = './public/assets/boutique/';
        $files = ['casquette', 'echarpe', 'mug', 'survetementB', 'survetementH', 'sweat', 'tutu'];
        foreach ($files as $file) {
            copy($origine.$file.'.jpg', $destination.$file.'.jpg');
        }

        $categorie = new Category();
        $categorie->setName('Accessoire');
        $manager->persist($categorie);

        $product = new Product();
        $product->setCategory($categorie)
            ->setName('Casquette')
            ->setActif(true)
            ->setShortDescription($this->faker->paragraph(3))
            ->setEnAvant(true)
            ->setImage('casquette.jpg');
        $manager->persist($product);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('blanche')
            ->setPrice(10);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('noire')
            ->setPrice(10);
        $manager->persist($taille);

        $product = new Product();
        $product->setCategory($categorie)
            ->setName('Echarpe')
            ->setActif(true)
            ->setShortDescription($this->faker->paragraph(3))
            ->setEnAvant(true)
            ->setImage('echarpe.jpg');
        $manager->persist($product);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('unique')
            ->setPrice(15);
        $manager->persist($taille);

        $product = new Product();
        $product->setCategory($categorie)
            ->setName('Mug')
            ->setActif(true)
            ->setShortDescription($this->faker->paragraph(3))
            ->setEnAvant(true)
            ->setImage('mug.jpg');
        $manager->persist($product);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('blanc')
            ->setPrice(5);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('rouge')
            ->setPrice(5);
        $manager->persist($taille);

        $categorie = new Category();
        $categorie->setName('Vétement');
        $manager->persist($categorie);

        $product = new Product();
        $product->setCategory($categorie)
            ->setName('Bas de survetement')
            ->setActif(true)
            ->setShortDescription($this->faker->paragraph(3))
            ->setEnAvant(true)
            ->setImage('survetementB.jpg');
        $manager->persist($product);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('enfant')
            ->setPrice(10);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('adulte')
            ->setPrice(20);
        $manager->persist($taille);

        $product = new Product();
        $product->setCategory($categorie)
            ->setName('Haut de survetement')
            ->setActif(true)
            ->setShortDescription($this->faker->paragraph(3))
            ->setEnAvant(true)
            ->setImage('survetementH.jpg');
        $manager->persist($product);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('enfant')
            ->setPrice(10);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('adulte')
            ->setPrice(10);
        $manager->persist($taille);

        $product = new Product();
        $product->setCategory($categorie)
            ->setName('Sweat')
            ->setActif(true)
            ->setShortDescription($this->faker->paragraph(3))
            ->setEnAvant(true)
            ->setImage('sweat.jpg');
        $manager->persist($product);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('10 ans')
            ->setPrice(10);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('12 ans')
            ->setPrice(10);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('L')
            ->setPrice(15);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('Xl')
            ->setPrice(20);
        $manager->persist($taille);

        $product = new Product();
        $product->setCategory($categorie)
            ->setName('Tutu de danse')
            ->setActif(true)
            ->setShortDescription($this->faker->paragraph(3))
            ->setEnAvant(true)
            ->setImage('tutu.jpg');
        $manager->persist($product);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('6 ans')
            ->setPrice(10);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('8 ans')
            ->setPrice(10);
        $manager->persist($taille);
        $taille = new Taille();
        $taille->setProduct($product)
            ->setName('10 ans')
            ->setPrice(15);
        $manager->persist($taille);

        $manager->flush();
    }
}
