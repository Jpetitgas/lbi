<?php

namespace App\Controller\Site;

use App\Form\ContatType;
use App\Service\SendEmail;
use App\Entity\Utilisateur;
use App\Message\SendEmailMessage;
use App\Repository\MenuRepository;
use App\Repository\SiteRepository;
use App\Repository\SujetRepository;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CategoryArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SiteController extends AbstractController
{
    /**
     * @Route("/", name="accueil", methods={"GET", "POST"})
     */
    public function accueil(Request $request, MessageBusInterface $messageBus, SendEmail $sendEmail, SujetRepository $sujetRepository, CategoryArticleRepository $categoryArticleRepository, EntityManagerInterface $entityManager, SiteRepository $siteRepository, MenuRepository $menuRepository, ArticleRepository $articleRepository): Response
    {
        $site = $siteRepository->findOneBy([]);
        $activite = $categoryArticleRepository->findBy(['nom' => 'Activité']);
        $activites = $articleRepository->findBy(['enLigne' => true, 'categorie' => $activite]);
        $actualite = $categoryArticleRepository->findBy(['nom' => 'Actualité']);
        $actualites = $articleRepository->findBy(['enLigne' => true, 'categorie' => $actualite], ['updated_at' => 'DESC'], 3);
        $menus = $menuRepository->findAll();
        $form = $this->createForm(ContatType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $sujet = $sujetRepository->findOneBy(['sujet' => 'Question divers']);
            /** @var Utilisateur $destinataire */
            $destinataire = $sujet->getDestinataire();
            $message = 'Vous avez recu un message de ' . $data['nom'] . '(' . $data['email'] . ') : ' . $data['message'];

            $sendEmail->send([
                'recipient_email' => $destinataire->getEmail(),
                'sujet' => $sujet->getSujet(),
                'htmlTemplate' => 'email/email.html.twig',
                'destinataire' => $destinataire,
                'context' => ['contenu' => $message],
            ]);

            return $this->redirectToRoute('accueil', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('site/accueil.html.twig', [
            'site' => $site,
            'activites' => $activites,
            'actualites' => $actualites,
            'menus' => $menus,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/menu/{id}", name="menu", methods={"GET", "POST"})
     */
    public function menu($id, Request $request, CategoryArticleRepository $categoryArticleRepository, SiteRepository $siteRepository, MenuRepository $menuRepository, ArticleRepository $articleRepository): Response
    {
        $site = $siteRepository->findOneBy([]);
        $menus = $menuRepository->findAll();
        $menu = $menuRepository->find($id);

        $activites = $articleRepository->findBy(['menu' => $menu]);

        return $this->renderForm('site/menu.html.twig', [
            'menu' => $menu,
            'site' => $site,
            'activites' => $activites,
            'menus' => $menus,
        ]);
    }

    /**
     * @Route("/actualite", name="actualite", methods={"GET", "POST"})
     */
    public function actualites(Request $request, CategoryArticleRepository $categoryArticleRepository, SiteRepository $siteRepository, MenuRepository $menuRepository, ArticleRepository $articleRepository): Response
    {
        $site = $siteRepository->findOneBy([]);
        $menus = $menuRepository->findAll();

        $actualite = $categoryArticleRepository->findBy(['nom' => 'Actualité']);
        $actualites = $articleRepository->findBy(['enLigne' => true, 'categorie' => $actualite], ['updated_at' => 'DESC']);

        return $this->renderForm('site/actualite.html.twig', [
            'site' => $site,
            'actualites' => $actualites,
            'menus' => $menus,
        ]);
    }

    /**
     * @Route("/article/{id}", name="article", methods={"GET", "POST"})
     */
    public function article($id, SiteRepository $siteRepository, MenuRepository $menuRepository, ArticleRepository $articleRepository): Response
    {
        $site = $siteRepository->findOneBy([]);
        $menus = $menuRepository->findAll();
        $article = $articleRepository->find($id);

        return $this->renderForm('site/article.html.twig', [
            'site' => $site,
            'article' => $article,
            'menus' => $menus,
        ]);
    }
}
