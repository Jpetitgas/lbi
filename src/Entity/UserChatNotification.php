<?php

namespace App\Entity;

use App\Repository\UserChatNotificationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserChatNotificationRepository::class)
 */
class UserChatNotification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasUnreadMessages = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastNotificationSent;

    /**
     * @ORM\Column(type="json")
     */
    private $unreadCourses = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?Utilisateur
    {
        return $this->user;
    }

    public function setUser(?Utilisateur $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getHasUnreadMessages(): bool
    {
        return $this->hasUnreadMessages;
    }

    public function setHasUnreadMessages(bool $hasUnreadMessages): self
    {
        $this->hasUnreadMessages = $hasUnreadMessages;
        return $this;
    }

    public function getLastNotificationSent(): ?\DateTimeInterface
    {
        return $this->lastNotificationSent;
    }

    public function setLastNotificationSent(?\DateTimeInterface $lastNotificationSent): self
    {
        $this->lastNotificationSent = $lastNotificationSent;
        return $this;
    }

    public function getUnreadCourses(): array
    {
        return $this->unreadCourses;
    }

    public function setUnreadCourses(array $unreadCourses): self
    {
        $this->unreadCourses = $unreadCourses;
        return $this;
    }

    public function addUnreadCourse(int $courseId): self
    {
        if (!in_array($courseId, $this->unreadCourses)) {
            $this->unreadCourses[] = $courseId;
        }
        return $this;
    }

    public function removeUnreadCourse(int $courseId): self
    {
        $this->unreadCourses = array_diff($this->unreadCourses, [$courseId]);
        return $this;
    }
}
