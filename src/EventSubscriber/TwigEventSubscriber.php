<?php

namespace App\EventSubscriber;

use App\Repository\InformationRepository;
use App\Repository\InscriptionRepository;
use App\Repository\TodolistRepository;
use App\Service\EnCours;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

class TwigEventSubscriber implements EventSubscriberInterface
{
    private $twig;
    protected $utilisateur;
    private $informationRepository;
    private $enCours;
    private $todolistRepository;
    private $security;

    public function __construct(Environment $twig, InscriptionRepository $inscriptionRepository, TodolistRepository $todolistRepository, Security $security, EnCours $enCours, InformationRepository $informationRepository)
    {
        $this->todolistRepository = $todolistRepository;
        $this->security = $security;
        $this->enCours = $enCours;
        $this->informationRepository = $informationRepository;
        $this->inscriptionRepository = $inscriptionRepository;

        $this->twig = $twig;
    }

    public function onKernelController(ControllerEvent $event)
    {
        $this->twig->addGlobal('saisonEnCours', $this->enCours->saison());
        $information = $this->informationRepository->findOneBy([]);

        $taches = $this->todolistRepository->findBy(['utilisateur' => $this->security->getUser(), 'done' => false]);
        $this->twig->addGlobal('signatureMail', $information->getSignature());
        $this->twig->addGlobal('mensurationPratiquant', $information->getMensurationPratiquant());
        $this->twig->addGlobal('nomStructure', $information->getNomStructure());
        $this->twig->addGlobal('espace', $information->getEspace());
        $this->twig->addGlobal('creationDossier', $information->getCreationDossier());
        $this->twig->addGlobal('modificationDossier', $information->getModificationDossier());


        // $this->twig->addGlobal('espace', $information->getEspace());
        $this->twig->addGlobal('nbSms', $information->getNbSms());
        $this->twig->addGlobal('stripe', $information->getStripe());
        $this->twig->addGlobal('taches', $taches);
        /**Site vitrine */
        $this->twig->addGlobal('vitrine', $information->getVitrine());
        $this->twig->addGlobal('siteActive', $information->getSite());
        /**Boutique */
        $this->twig->addGlobal('boutiqueStripe', $information->getBoutiqueStripe());
        $this->twig->addGlobal('boutiqueActive', $information->getBoutiqueActive());
        $this->twig->addGlobal('boutiqueOuverte', $information->getBoutiqueOuvert());
        /**Reservation */
        $this->twig->addGlobal('reservationStripe', $information->getReservationStripe());
        $this->twig->addGlobal('reservationActive', $information->getReservationActive());
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.controller' => 'onKernelController',
        ];
    }

    private function chatList() {}
}
