<?php

namespace App\Entity;

use App\Repository\InformationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InformationRepository::class)
 */
class Information
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionInscription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $conditionDeVente;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $paiement;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $enteteBulletin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomStructure;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $signature;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $enteteFacture;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $piedFacture;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $corpsFacture;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $creationDossier;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $modificationDossier;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $coursVisible;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionDossier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $vitrine;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $espace;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbSms;

    /**
     * @ORM\Column(type="boolean")
     */
    private $stripe;

    /**
     * @ORM\Column(type="text")
     */
    private $boutiquePresentation;

    /**
     * @ORM\Column(type="text")
     */
    private $boutiquePaiement;

    /**
     * @ORM\Column(type="text")
     */
    private $boutiqueLivraison;

    /**
     * @ORM\Column(type="boolean")
     */
    private $boutiqueStripe;
    /**
     * @ORM\Column(type="boolean")
     */
    private $boutiqueActive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $boutiqueOuvert;

    /**
     * @ORM\Column(type="text")
     */
    private $reservationPresentation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $reservationStripe;
    /**
     * @ORM\Column(type="boolean")
     */
    private $reservationActive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mensurationPratiquant;

    /**
     * @ORM\Column(type="boolean")
     */
    private $site;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */

    private $lienLive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $live;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $infoInscription;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbEssai;

    /**
     * @ORM\Column(type="boolean")
     */
    private $ActivationRelance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescriptionInscription(): ?string
    {
        return $this->descriptionInscription;
    }

    public function setDescriptionInscription(?string $descriptionInscription): self
    {
        $this->descriptionInscription = $descriptionInscription;

        return $this;
    }

    public function getConditionDeVente(): ?string
    {
        return $this->conditionDeVente;
    }

    public function setConditionDeVente(?string $conditionDeVente): self
    {
        $this->conditionDeVente = $conditionDeVente;

        return $this;
    }

    public function getPaiement(): ?string
    {
        return $this->paiement;
    }

    public function setPaiement(?string $paiement): self
    {
        $this->paiement = $paiement;

        return $this;
    }

    public function getEnteteBulletin(): ?string
    {
        return $this->enteteBulletin;
    }

    public function setEnteteBulletin(?string $enteteBulletin): self
    {
        $this->enteteBulletin = $enteteBulletin;

        return $this;
    }

    public function getNomStructure(): ?string
    {
        return $this->nomStructure;
    }

    public function setNomStructure(string $nomStructure): self
    {
        $this->nomStructure = $nomStructure;

        return $this;
    }

    public function getSignature(): ?string
    {
        return $this->signature;
    }

    public function setSignature(string $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    public function getEnteteFacture(): ?string
    {
        return $this->enteteFacture;
    }

    public function setEnteteFacture(string $enteteFacture): self
    {
        $this->enteteFacture = $enteteFacture;

        return $this;
    }

    public function getPiedFacture(): ?string
    {
        return $this->piedFacture;
    }

    public function setPiedFacture(string $piedFacture): self
    {
        $this->piedFacture = $piedFacture;

        return $this;
    }

    public function getCorpsFacture(): ?string
    {
        return $this->corpsFacture;
    }

    public function setCorpsFacture(string $corpsFacture): self
    {
        $this->corpsFacture = $corpsFacture;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->codePostal;
    }

    public function setCodePostal(int $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCreationDossier(): ?bool
    {
        return $this->creationDossier;
    }

    public function setCreationDossier(bool $creationDossier): self
    {
        $this->creationDossier = $creationDossier;

        return $this;
    }

    public function getModificationDossier(): ?bool
    {
        return $this->modificationDossier;
    }

    public function setModificationDossier(bool $modificationDossier): self
    {
        $this->modificationDossier = $modificationDossier;

        return $this;
    }

    public function getCoursVisible(): ?bool
    {
        return $this->coursVisible;
    }

    public function setCoursVisible(bool $coursVisible): self
    {
        $this->coursVisible = $coursVisible;

        return $this;
    }

    public function getDescriptionDossier(): ?string
    {
        return $this->descriptionDossier;
    }

    public function setDescriptionDossier(string $descriptionDossier): self
    {
        $this->descriptionDossier = $descriptionDossier;

        return $this;
    }

    public function getVitrine(): ?string
    {
        return $this->vitrine;
    }

    public function setVitrine(string $vitrine): self
    {
        $this->vitrine = $vitrine;

        return $this;
    }

    public function getEspace(): ?string
    {
        return $this->espace;
    }

    public function setEspace(string $espace): self
    {
        $this->espace = $espace;

        return $this;
    }

    public function getNbSms(): ?int
    {
        return $this->nbSms;
    }

    public function setNbSms(?int $nbSms): self
    {
        $this->nbSms = $nbSms;

        return $this;
    }

    public function getStripe(): ?bool
    {
        return $this->stripe;
    }

    public function setStripe(bool $stripe): self
    {
        $this->stripe = $stripe;

        return $this;
    }

    public function getBoutiquePresentation(): ?string
    {
        return $this->boutiquePresentation;
    }

    public function setBoutiquePresentation(string $boutiquePresentation): self
    {
        $this->boutiquePresentation = $boutiquePresentation;

        return $this;
    }

    public function getBoutiquePaiement(): ?string
    {
        return $this->boutiquePaiement;
    }

    public function setBoutiquePaiement(string $boutiquePaiement): self
    {
        $this->boutiquePaiement = $boutiquePaiement;

        return $this;
    }

    public function getBoutiqueLivraison(): ?string
    {
        return $this->boutiqueLivraison;
    }

    public function setBoutiqueLivraison(string $boutiqueLivraison): self
    {
        $this->boutiqueLivraison = $boutiqueLivraison;

        return $this;
    }

    public function getBoutiqueStripe(): ?bool
    {
        return $this->boutiqueStripe;
    }

    public function setBoutiqueStripe(bool $boutiqueStripe): self
    {
        $this->boutiqueStripe = $boutiqueStripe;

        return $this;
    }

    public function getBoutiqueActive(): ?bool
    {
        return $this->boutiqueActive;
    }

    public function setBoutiqueActive(bool $boutiqueActive): self
    {
        $this->boutiqueActive = $boutiqueActive;

        return $this;
    }

    public function getBoutiqueOuvert(): ?bool
    {
        return $this->boutiqueOuvert;
    }

    public function setBoutiqueOuvert(bool $boutiqueOuvert): self
    {
        $this->boutiqueOuvert = $boutiqueOuvert;

        return $this;
    }

    public function getReservationStripe(): ?bool
    {
        return $this->reservationStripe;
    }

    public function setReservationStripe(bool $reservationStripe): self
    {
        $this->reservationStripe = $reservationStripe;

        return $this;
    }

    public function getReservationActive(): ?bool
    {
        return $this->reservationActive;
    }

    public function setReservationActive(bool $reservationActive): self
    {
        $this->reservationActive = $reservationActive;

        return $this;
    }
    public function getReservationPresentation(): ?string
    {
        return $this->reservationPresentation;
    }

    public function setReservationPresentation(string $reservationPresentation): self
    {
        $this->reservationPresentation = $reservationPresentation;

        return $this;
    }

    public function getMensurationPratiquant(): ?bool
    {
        return $this->mensurationPratiquant;
    }

    public function setMensurationPratiquant(?bool $mensurationPratiquant): self
    {
        $this->mensurationPratiquant = $mensurationPratiquant;

        return $this;
    }

    public function getSite(): ?bool
    {
        return $this->site;
    }

    public function setSite(bool $site): self
    {
        $this->site = $site;

        return $this;
    }
    public function getLienLive(): ?string
    {
        return $this->lienLive;
    }

    public function setLienLive(?string $lienLive): self
    {
        $this->lienLive = $lienLive;

        return $this;
    }

    public function getLive(): ?bool
    {
        return $this->live;
    }

    public function setLive(bool $live): self
    {
        $this->live = $live;

        return $this;
    }

    public function getInfoInscription(): ?string
    {
        return $this->infoInscription;
    }

    public function setInfoInscription(?string $infoInscription): self
    {
        $this->infoInscription = $infoInscription;

        return $this;
    }

    public function getNbEssai(): ?int
    {
        return $this->NbEssai;
    }

    public function setNbEssai(?int $NbEssai): self
    {
        $this->NbEssai = $NbEssai;

        return $this;
    }

    public function isActivationRelance(): ?bool
    {
        return $this->ActivationRelance;
    }

    public function setActivationRelance(bool $ActivationRelance): self
    {
        $this->ActivationRelance = $ActivationRelance;

        return $this;
    }
}
