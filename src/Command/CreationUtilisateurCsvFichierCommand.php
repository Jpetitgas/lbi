<?php

namespace App\Command;

use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UtilisateurRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreationUtilisateurCsvFichierCommand extends Command
{
    private EntityManagerInterface $entityManager;
    private string $dataDirectory;
    private UtilisateurRepository $utilisateurRepository;

    protected static $defaultName = 'app:import-utilisateur';
    protected static $defaultDescription = 'Import des utilisateurs à partir d\'un fichier CSV';

    public function __construct(EntityManagerInterface $entityManager, string $dataDirectory, UtilisateurRepository $utilisateurRepository)
    {
        parent::__construct();
        $this->entityManager=$entityManager;
        $this->dataDirectory=$dataDirectory;
        $this->utilisateurRepository=$utilisateurRepository;
    }

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription);
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io= new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->createUtilisateur();

        return Command::SUCCESS;
    }

    private function getDataFromFile()
    {
        $file=$this->dataDirectory.'utilisateur.csv';
        $normalizers=[new ObjectNormalizer()];
        $encoders=[new CsvEncoder()];
        $serializer =new Serializer($normalizers, $encoders);
        $fileString=file_get_contents($file);
        $data=$serializer->decode($fileString, 'csv');

        return $data;
    }

    private function createUtilisateur()
    {
        $this->io->section('CREATION DES UTILISATEURS');
        $utilisateurCreated=0;
        foreach ($this->getDataFromFile() as $row) {
            if (array_key_exists('email', $row) && !empty($row['email'])) {
                $utilisateur=$this->utilisateurRepository->findBy([
                    'email'=>$row['email']
                ]);
                if (!$utilisateur) {
                    $utilisateur=new Utilisateur();
                    $utilisateur->setEmail($row['email'])
                            ->setPassword($row['password'])
                            ->setNom($row['nom'])
                            ->setPrenom($row['prenom'])
                            ->setPortable($row['portable'])
                            ->setAdresse($row['adresse'])
                            ->setCodePostal($row['codePostal'])
                            ->setVille($row['ville'])
                            ->setIsVerified(true);
                    $this->entityManager->persist($utilisateur);
                    $utilisateurCreated++;
                }
            }
        }
        $this->entityManager->flush();

        if ($utilisateurCreated>1) {
            $string="{$utilisateurCreated} UTILISATEURS CREES EN BASE";
        } elseif ($utilisateurCreated==1) {
            $string="1 UTILISATEUR CREE EN BASE";
        } else {
            $string="AUCUN UTILISATEUR CREE EN BASE";
        }
        $this->io->success($string);
    }
}
