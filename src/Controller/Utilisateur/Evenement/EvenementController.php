<?php

namespace App\Controller\Utilisateur\Evenement;

use App\Entity\Evenement;
use App\Repository\EvenementRepository;
use App\Repository\OptionPurchaseRepository;
use App\Repository\PaiementEvenementRepository;
use App\Service\EnCours;
use App\Service\EvenementService;
use App\Stripe\StripeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("utilisateur/evenement", name="utilisateur_evenement")
 */
class EvenementController extends AbstractController
{
    /**
     * @Route("/index", name="_index", methods={"GET"})
     */
    public function index(EnCours $enCours, EvenementRepository $evenementRepository): Response
    {
        $saison = $enCours->saison();
        $evenements = $evenementRepository->findBy(['saison' => $saison->getId(), 'actif' => true], ['id' => 'DESC']);

        return $this->render('utilisateur/evenement/index.html.twig', [
            'evenements' => $evenements,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Evenement $evenement, Security $security, OptionPurchaseRepository $optionPurchaseRepository, EvenementService $evenementService, PaiementEvenementRepository $paiementEvenementRepository, StripeService $stripeService): Response
{
    $utilisateur = $security->getUser();
    if (!$evenementService->autorisationEvenement($utilisateur, $evenement->getSection())) {
        $this->addFlash('danger', 'Vous n\'avez pas accés à cet évenement');

        return $this->redirectToRoute('utilisateur_evenement_index');
    }
    if (!$evenement->getActif()) {
        $this->addFlash('danger', 'Cet évenement n\'est pas ouvert à la réservation');

        return $this->redirectToRoute('utilisateur_evenement_index');
    }
    $places = $evenementService->placesEvenement($evenement);
    $reservationData = $evenementService->montantTotalReservation($utilisateur, $evenement);
    $totalReservation = $reservationData['totalReservation'];
    $reductionAmount = $reservationData['reductionAmount'];
    $totalPaiement = $evenementService->montantTotalPaiement($security->getUser(), $evenement);
    $options = $optionPurchaseRepository->findByEvenementAndUtilisateur($evenement, $utilisateur);
    $totalOptions = 0;
    if ($options) {
        foreach ($options as $option) {
            $totalOptions += $option['price'] * $option['qty'];
        }
    }
    $solded = ($totalPaiement - ($totalReservation + $totalOptions)) >= 0 ? true : false;
    $paiements = $paiementEvenementRepository->findBy(['utilisateur' => $security->getUser(), 'evenement' => $evenement]);
    $coordonnees = explode(',', $evenement->getLocalisation());

    $longitude = $coordonnees[1];
    $latitude = $coordonnees[0];

    /** Verification qu'il a des reservations sinon on supprime les options */
    $reservation = $evenementService->reservation($utilisateur, $evenement) ? true : false;
    if (!$reservation) {
        $evenementService->deletedOptions($evenement, $utilisateur);
    }

    return $this->render('utilisateur/evenement/edit.html.twig', [
        'solded' => $solded,
        'totalReservation' => $totalReservation,
        'reductionAmount' => $reductionAmount,
        'reservation' => $reservation,
        'paiementEvenements' => $paiements,
        'totalPaiement' => $totalPaiement,
        'longitude' => $longitude,
        'latitude' => $latitude,
        'evenement' => $evenement,
        'places' => $places,
        'stripePublicCle' => $stripeService->ObtenirPublicCle(),
        'options' => $options,
        'totalOptions' => $totalOptions,
    ]);
}



    /**
     * @Route("/plan/data/{id}", name="_plan_data", methods={"GET","POST"})
     */
    public function evenementData(Evenement $evenement): Response
    {
        $data = [];

        foreach ($evenement->getContainers() as $c) {
            $container = [
                'id' => $c->getId(),
                'nom' => $c->getNom(),
                'x' => $c->getX(),
                'y' => $c->getY(),
                'angle' => $c->getAngle(),
                'largeur' => $c->getLargeur(),
                'hauteur' => $c->getHauteur(),
                'nbLigne' => $c->getNbLigne(),
            ];
            $places = [];

            foreach ($c->getPlaces() as $p) {
                /** @var Place $p */
                if ($p->getUtilisateur()) {
                    $utilisateur = $p->getUtilisateur()->getId();
                } else {
                    $utilisateur = null;
                }

                array_push($places, ['id' => $p->getId(), 'nom' => $p->getNom(), 'utilisateur' => $utilisateur, 'verrouiller' => $p->getVerrouiller()]);
            }
            array_push($container, $places);
            array_push($data, $container);
        }

        $response = new JsonResponse($data);

        return $response;
    }
}
