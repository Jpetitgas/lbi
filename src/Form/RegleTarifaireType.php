<?php

namespace App\Form;

use App\Entity\NiveauTarifaire;
use App\Entity\Operateur;
use App\Entity\RegleTarifaire;
use App\Entity\Ville;
use App\Repository\OperateurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegleTarifaireType extends AbstractType
{
    public $operateurRepository;

    public function __construct(OperateurRepository $operateurRepository)
    {
        $this->operateurRepository = $operateurRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => true,
            ])
            ->add('niveau', EntityType::class, [
                'class' => NiveauTarifaire::class,
                'placeholder' => 'selectionner un niveau',
                'required' => true, ])
            ->add('operateur', HiddenType::class)

            ->add('villes', HiddenType::class, [
                'label' => 'Villes',
                'mapped' => false,
            ])
            ->add('parametre', HiddenType::class)
            ->add('montant', HiddenType::class);

        $ajoutPara = function (FormInterface $form, $operateur = null) {
            if ($operateur) {
                $op = $this->operateurRepository->find($operateur);
                switch ($op->getNom()) {
                    case 'INTRA-COMMUNES':
                        $form->add('villes', EntityType::class, [
                        'class' => Ville::class,
                        'multiple' => true,
                        'choice_label' => 'nomMaj',
                        'by_reference' => false,
                        'attr' => [
                            'class' => 'select-villes',
                        ],
                        ])
                        ->add('parametre', HiddenType::class);
                        break;
                    case 'EXTRA-COMMUNES':
                        $form->add('villes', EntityType::class, [
                        'class' => Ville::class,
                        'multiple' => true,
                        'choice_label' => 'nomMaj',
                        'by_reference' => false,
                        'attr' => [
                            'class' => 'select-villes',
                        ],
                        ])
                        ->add('parametre', HiddenType::class);
                        break;
                    case 'UNIQUE':
                        $form->add('parametre', HiddenType::class)
                        ->add('villes', HiddenType::class, [
                            'mapped' => false,
                        ]);
                        break;
                    default:
                    $form->add('parametre', IntegerType::class)
                        ->add('villes', HiddenType::class, [
                            'mapped' => false,
                        ]);
               }
                $form->add('montant', MoneyType::class, [
                    'required' => true,
                    ]);
            } else {
                $form->add('villes', HiddenType::class, [
                        'label' => 'Villes',
                        'mapped' => false,
                        ])
                    ->add('parametre', HiddenType::class)
                    ->add('montant', HiddenType::class);
            }
        };
        $ajoutOperateur = function (FormInterface $form, NiveauTarifaire $niveau = null) use ($ajoutPara) {
            $operateurs = (null === $niveau) ? [] : $niveau->getOperateurs();

            $builder = $form->getConfig()->getFormFactory()->createNamedBuilder('operateur', EntityType::class, null, [
                    'class' => Operateur::class,
                    'placeholder' => 'selectionner un opérateur',
                    'choice_label' => 'nom',
                    'choices' => $operateurs,
                    'auto_initialize' => false,
                    'required' => true,
                    ]);
            $builder->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($ajoutPara) {
                            $operateur = $event->getData();
                            $form = $event->getForm();
                            $ajoutPara($form->getParent(), $operateur);
                        }
            );
            $form->add($builder->getForm());
        };
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($ajoutOperateur, $ajoutPara) {
                 /** @var RegleTarifaire $regleTarifaire */
                 $regleTarifaire = $event->getData();
                 $form = $event->getForm();
                 $niveau = $regleTarifaire->getNiveau();
                 if (!($regleTarifaire->getId() === null)) {
                     $niveau = $regleTarifaire->getNiveau();
                     $builder = $form->getConfig()->getFormFactory()->createNamedBuilder('niveau', EntityType::class, null, [
                        'class' => NiveauTarifaire::class,
                        'placeholder' => 'selectionner un niveau',
                        'data' => $niveau,
                        'required' => true,
                        'auto_initialize' => false,
                         ])
                        ;
                     $builder->addEventListener(
                         FormEvents::POST_SUBMIT,
                         function (FormEvent $event) use ($ajoutOperateur, $ajoutPara) {
                            $niveau = $event->getForm()->getData();
                            $form = $event->getForm();
                            $ajoutOperateur($form->getParent(), $niveau);
                            if ($niveau == '') {
                                $ajoutPara($form->getParent(), '');
                            }
                        }
                     );
                     $form->add($builder->getForm());
                     $ajoutOperateur($form, $niveau);
                     $ajoutPara($form, $regleTarifaire->getOperateur());
                 }
             }
        );

        $builder->get('niveau')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($ajoutOperateur, $ajoutPara) {
                /** @var NiveauTarifaire $niveau */
                $niveau = $event->getForm()->getData();
                $form = $event->getForm();
                $ajoutOperateur($form->getParent(), $niveau);
                $ajoutPara($form->getParent(), '');
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RegleTarifaire::class,
        ]);
    }
}
