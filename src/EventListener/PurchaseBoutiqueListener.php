<?php

namespace App\EventListener;

use App\Entity\Purchase;
use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class PurchaseBoutiqueListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison=$encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $purchase=$event->getObject();

        if ($purchase instanceof Purchase) {
            $purchase->setSaison($this->saison);
        }
    }
}
