<?php

namespace App\Controller\Gestion\Boutique;

use App\Entity\Purchase;
use App\Service\EnCours;
use App\Event\PurchaseEvent;
use Doctrine\DBAL\Exception;
use App\SearchData\PurchaseData;
use App\SearchForm\PurchaseForm;
use App\Repository\PurchaseRepository;
use App\Service\PurchaseService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PurchaseController extends AbstractController
{
    /**
     * @Route("gestion/boutique/purchase", name="gestion_boutique_purchase_index")
     */
    public function index(Request $request, PurchaseService $purchaseService, PaginatorInterface $paginator, EnCours $encours, PurchaseRepository $purchaseRepository): Response
    {
        $saison=$encours->saison();
        $data = new PurchaseData();
        $form = $this->createForm(PurchaseForm::class, $data);
        $form->handleRequest($request);
        $essai=$purchaseRepository->purchases($saison, $data);
        $results=$essai->getResult();
        foreach ($results as $result) {
            /** @var Purchase $result  */
            $result->setLivre($purchaseService->purchaseDelevred($result));
            $result->setPaye($purchaseService->solde($result));


            if ($data->livre!==null && !($data->livre===$result->getLivre())) {
                unset($results[array_search($result, $results)]);
            }
            if ($data->paye!==null && !($data->paye===$result->getPaye())) {
                unset($results[array_search($result, $results)]);
            }
        }
        $purchases = $paginator->paginate(
            $results,
            $request->query->getInt('page', 1),
            $data->nb
        );
        return $this->render('gestion/boutique/purchase/index.html.twig', [
            'purchases' => $purchases,
            'form' => $form->createView(),
        ]);
    }
    /**
    * @Route("gestion/boutique/purchase/items", name="gestion_boutique_purchase_items_index")
    */
    public function indexPurchaseItems(Request $request, EnCours $encours, PurchaseRepository $purchaseRepository): Response
    {
        $saison=$encours->saison();
        $purchases = $purchaseRepository->findby(['saison'=>$saison], ['id' => 'ASC']);

        return $this->render('gestion/boutique/purchase/items/index.html.twig', [
            'purchases' => $purchases,
        ]);
    }

    /**
     * @Route("gestion/boutique/purchase/delete/{id}", name="gestion_boutique_purchase_delete")
     */
    public function delete(Request $request, EventDispatcherInterface $dispatcher, Purchase $purchase): Response
    {
        try {
            if ($this->isCsrfTokenValid('delete'.$purchase->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($purchase);
                $entityManager->flush();
                $purchaseEvent = new PurchaseEvent($purchase);
                $dispatcher->dispatch($purchaseEvent, 'suppression.purchase');
            }
        } catch (Exception  $e) {
            $this->addFlash('danger', 'Supprimer tous les paiements avant de supprimer la commande');
        }

        return $this->redirectToRoute('gestion_boutique_purchase_index');
    }
}
