<?php

namespace App\Controller\Gestion;

use App\Entity\CategorieSection;
use App\Form\CategorieSectionType;
use App\Repository\CategorieSectionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("gestion/categoriesection", name="gestion_categoriesection"))
 */
class CategorieSectionController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(CategorieSectionRepository $categorieSectionRepository): Response
    {
        return $this->render('gestion/categoriesection/index.html.twig', [
            'categoriesections' => $categorieSectionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $categorieSection = new CategorieSection();
        $formCategoriesection = $this->createForm(CategorieSectionType::class, $categorieSection);
        $formCategoriesection->handleRequest($request);

        if ($formCategoriesection->isSubmitted() && $formCategoriesection->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categorieSection);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_categoriesection_index');
        }

        return $this->render('gestion/categoriesection/new.html.twig', [
            'categorie_section' => $categorieSection,
            'formCategoriesection' => $formCategoriesection->createView(),
        ]);
    }



    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CategorieSection $categorieSection): Response
    {
        $formCategoriesection = $this->createForm(CategorieSectionType::class, $categorieSection);
        $formCategoriesection->handleRequest($request);

        if ($formCategoriesection->isSubmitted() && $formCategoriesection->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_categoriesection_index');
        }

        return $this->render('gestion/categoriesection/edit.html.twig', [
            'categorie_section' => $categorieSection,
            'formCategoriesection' => $formCategoriesection->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, CategorieSection $categorieSection): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categorieSection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categorieSection);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_categoriesection_index');
    }
}
