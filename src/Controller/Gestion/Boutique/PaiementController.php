<?php

namespace App\Controller\Gestion\Boutique;

use App\Entity\Purchase;
use App\Service\EnCours;
use FontLib\EncodingMap;
use App\Entity\PaiementBoutique;
use App\Form\PaiementBoutiqueType;
use App\Repository\PaiementRepository;
use App\Repository\PurchaseRepository;
use App\SearchData\PaiementBoutiqueData;
use App\SearchForm\PaiementBoutiqueForm;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\PaiementBoutiqueRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/boutique/paiement", name="gestion_boutique_paiement")
 */
class PaiementController extends AbstractController
{
    /**
     * @Route("/index", name="_index", methods={"GET"})
     */
    public function index(PaiementBoutiqueRepository $paiementBoutiqueRepository, PaginatorInterface $paginator, Request $request, EnCours $enCours): Response
    {
        $data = new PaiementBoutiqueData();
        $form = $this->createForm(PaiementBoutiqueForm::class, $data);
        $form->handleRequest($request);

        $saison = $enCours->saison();
        $paiements = $paginator->paginate(
            $paiementBoutiqueRepository->paiements($saison, $data),
            $request->query->getInt('page', 1),
            $data->nb
        );

        return $this->render('gestion/boutique/paiement/index.html.twig', [

            'paiements' => $paiements,
            'form'=>$form->createView(),
        ]);
    }
    /**
     * @Route("/purchase/index/{id_purchase}", name="_purchase_index", methods={"GET"})
     */
    public function indexPurchase($id_purchase, PurchaseRepository $purchaseRepository): Response
    {
        $purchase = $purchaseRepository->find($id_purchase);

        return $this->render('gestion/boutique/paiement/purchase/index.html.twig', [

            'purchase' => $purchase
        ]);
    }

    /**
     * @Route("/purchase/edit/{id}", name="_purchase_edit", methods={"GET","POST"})
     */
    public function edit($id, Request $request, PaiementBoutiqueRepository $paiementBoutiqueRepository): Response
    {
        $paiement=$paiementBoutiqueRepository->find($id);
        $formPaiement = $this->createForm(PaiementBoutiqueType::class, $paiement);
        $formPaiement->handleRequest($request);

        if ($formPaiement->isSubmitted() && $formPaiement->isValid()) {
            $this->getDoctrine()->getManager()->flush();


            return $this->redirectToRoute('gestion_boutique_paiement_purchase_index', ['id_purchase' => $paiement->getPurchase()->getId()]);
        }

        return $this->render('gestion/boutique/paiement/purchase/edit.html.twig', [

            'paiement' => $paiement,
            'formPaiement' => $formPaiement->createView(),
        ]);
    }

    /**
     * @Route("/purchase/new/{id_purchase}", name="_purchase_new", methods={"GET","POST"})
     */
    public function new($id_purchase, Request $request, PurchaseRepository $purchaseRepository): Response
    {
        $purchase=$purchaseRepository->find($id_purchase);
        $paiement = new PaiementBoutique();
        $formPaiement = $this->createForm(PaiementBoutiqueType::class, $paiement);
        $formPaiement->handleRequest($request);

        if ($formPaiement->isSubmitted() && $formPaiement->isValid()) {
            $paiement->setPurchase($purchase);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($paiement);
            $entityManager->flush();



            return $this->redirectToRoute('gestion_boutique_paiement_purchase_index', ['id_purchase' => $paiement->getPurchase()->getId()]);
        }

        return $this->render('gestion/boutique/paiement/purchase/new.html.twig', [

            'purchase' => $purchase,
            'formPaiement' => $formPaiement->createView(),
        ]);
    }

    /**
     * @Route("purchase/delete/{id}", name="_purchase_delete", methods={"POST"})
     */
    public function delete($id, Request $request, PaiementBoutiqueRepository $paiementBoutiqueRepository): Response
    {
        $paiement=$paiementBoutiqueRepository->find($id);
        if ($this->isCsrfTokenValid('delete'.$paiement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($paiement);
            $entityManager->flush();
        }
        $this->addFlash('success', 'Paiement supprimé');


        return $this->redirectToRoute('gestion_boutique_paiement_purchase_index', ['id_purchase' => $paiement->getPurchase()->getId()]);
    }

    /**
     * @Route("/encaisserGroupe/{ids}", name="_encaisserGroupe", methods={"GET"})
     */
    public function encaisserGroupe($ids, PaiementBoutiqueRepository $paiementBoutiqueRepository): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $paiement = $paiementBoutiqueRepository->find($id);
            $paiement->setEncaisse(($paiement->getEncaisse() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($paiement);
        }
        $em->flush();
        return new Response('Le statut "encaissé" des paiements a changé d\'état', Response::HTTP_OK);
    }
    /**
     * @Route("/encaisser/{id}", name="_encaisser", methods={"GET"})
     */
    public function encaisser($id, PaiementBoutiqueRepository $paiementBoutiqueRepository): Response
    {
        $paiement = $paiementBoutiqueRepository->find($id);
        $paiement->setEncaisse(($paiement->getEncaisse() ? '0' : '1'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($paiement);

        $em->flush();
        $this->addFlash('success', 'Les paiements (encaissé) ont changé d\'état');
        $id_saison = $paiement->getSaison()->getId();

        return $this->redirectToRoute('gestion_boutique_paiement_index');
    }
}
