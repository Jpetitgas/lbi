$(document).ready(function () {
    var table = $("#dossierTable").DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
           this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
        'dom': "Bfrtip",
        'scrollY': "700px",
        'scrollX': true,
        'scrollCollapse': true,
        'paging': false,
        'buttons': [
            {
                'extend': 'colvisGroup',
                'text': 'Réduire',
                'show': [0,1,2, 14,15],
                'hide': [ 3, 4, 5,6, 7,8, 9,10, 11, 12, 13],
            },
            {
                'extend': 'colvisGroup',
                'text': 'Tout',
                'show': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],

            },
            {
                'extend': 'colvisGroup',
                'text': 'Indicateur',
                'show': [2, 3, 4, 5, 6, 7, 8, 9],
                'hide': [1, 10, 11, 12, 13, 14, 15]
            },
            {
                'extend': 'colvisGroup',
                'text': 'Bulletin',
                'show': [2, 3, 6, 10, 11, 12],
                'hide': [1, 4, 5, 7, 8, 9, 13, 14, 15]
            },
            'colvis',

        ],

        'columnDefs': [

            {
                'orderable': false,
                'className': 'select-checkbox',
                'targets': 0,
            }],

        'select': {
            'style': 'multi',
            'selector': 'td:first-child'
        },
        'order': [[1, 'asc']],

        "language": {
            'buttons': {
                'colvis': 'Colonnes cachées'
            },
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            }
        },
    });
    $(".selectAll").on("click", function (e) {
        if ($(this).is(":checked")) {
            table.rows({ search: 'applied' }).select()
        } else {
            table.rows({ search: 'applied' }).deselect();
        }
    });

    
});
