<?php

namespace App\Entity;

use App\Repository\CoursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoursRepository::class)
 */
class Cours
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity=Activite::class, inversedBy="cours")
     * @ORM\JoinColumn(nullable=false)
     */
    private $activite;

    /**
     * @ORM\OneToMany(targetEntity=Inscription::class, mappedBy="cours",fetch="EAGER")
     */
    private $inscriptions;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="cours")
     * @ORM\JoinColumn(nullable=false)
     */
    private $intervenant;

    /**
     * @ORM\OneToMany(targetEntity=Chat::class, mappedBy="cours")
     */
    private $chats;

    /**
     * @ORM\OneToMany(targetEntity=Presence::class, mappedBy="cours")
     */
    private $presences;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class, inversedBy="cours")
     * @ORM\JoinColumn(nullable=false)
     */
    private $saison;

    /**
     * @ORM\OneToMany(targetEntity=ConsultationChat::class, mappedBy="cours", orphanRemoval=true)
     */
    private $consultationChats;


    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
        $this->chats = new ArrayCollection();
        $this->presences = new ArrayCollection();
        $this->consultationChats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getActivite(): ?Activite
    {
        return $this->activite;
    }

    public function setActivite(?Activite $activite): self
    {
        $this->activite = $activite;

        return $this;
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->setCours($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->removeElement($inscription)) {
            // set the owning side to null (unless already changed)
            if ($inscription->getCours() === $this) {
                $inscription->setCours(null);
            }
        }

        return $this;
    }

    public function getIntervenant(): ?Utilisateur
    {
        return $this->intervenant;
    }

    public function setIntervenant(?Utilisateur $intervenant): self
    {
        $this->intervenant = $intervenant;

        return $this;
    }

    /**
     * @return Collection|Chat[]
     */
    public function getChats(): Collection
    {
        return $this->chats;
    }

    public function addChat(Chat $chat): self
    {
        if (!$this->chats->contains($chat)) {
            $this->chats[] = $chat;
            $chat->setCours($this);
        }

        return $this;
    }

    public function removeChat(Chat $chat): self
    {
        if ($this->chats->removeElement($chat)) {
            // set the owning side to null (unless already changed)
            if ($chat->getCours() === $this) {
                $chat->setCours(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNom();
    }

    /**
     * @return Collection|Presence[]
     */
    public function getPresences(): Collection
    {
        return $this->presences;
    }

    public function addPresence(Presence $presence): self
    {
        if (!$this->presences->contains($presence)) {
            $this->presences[] = $presence;
            $presence->setCours($this);
        }

        return $this;
    }

    public function removePresence(Presence $presence): self
    {
        if ($this->presences->removeElement($presence)) {
            // set the owning side to null (unless already changed)
            if ($presence->getCours() === $this) {
                $presence->setCours(null);
            }
        }

        return $this;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    /**
     * @return Collection|ConsultationChat[]
     */
    public function getConsultationChats(): Collection
    {
        return $this->consultationChats;
    }

    public function addConsultationChat(ConsultationChat $consultationChat): self
    {
        if (!$this->consultationChats->contains($consultationChat)) {
            $this->consultationChats[] = $consultationChat;
            $consultationChat->setCours($this);
        }

        return $this;
    }

    public function removeConsultationChat(ConsultationChat $consultationChat): self
    {
        if ($this->consultationChats->removeElement($consultationChat)) {
            // set the owning side to null (unless already changed)
            if ($consultationChat->getCours() === $this) {
                $consultationChat->setCours(null);
            }
        }

        return $this;
    }

    public function __clone()
    {
        $this->id=null;
    }
}
