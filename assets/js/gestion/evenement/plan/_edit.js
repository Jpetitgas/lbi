window.addEventListener('DOMContentLoaded',responsivePlan )
window.addEventListener('resize', responsivePlan);
document.getElementById('largeurM').addEventListener('click', largeurM);
document.getElementById('largeurP').addEventListener('click', largeurP);
document.getElementById('hauteurM').addEventListener('click', hauteurM);
document.getElementById('hauteurP').addEventListener('click', hauteurP);
document.getElementById('save').addEventListener('click', u = function () {
    recordDimension();
});

let balls = document.querySelectorAll(".ball")
for (let ball of balls) {
    ball.addEventListener('mouseover', function () {
        c = document.getElementById('nom')
        c.innerHTML = this.dataset['nom']
    })
    ball.addEventListener('mouseout', function () {
        c = document.getElementById('nom')
        c.innerHTML = ""
    })

    let newPosX = 0, newPosY = 0, startPosX = 0, startPosY = 0;
    // when the user clicks down on the element
    ball.addEventListener('mousedown', d = function (e) {
        e.preventDefault();
        // get the starting position of the cursor
        startPosX = e.clientX;
        startPosY = e.clientY;
        ball.addEventListener('mousemove', mouseMove);
        document.addEventListener('mouseup', u = function () {
            reset();
            recordPosition();
        });
        document.addEventListener('keydown', k = (event) => {
            var name = event.key;

            if (name === 'a') {
                ball.removeEventListener('mousemove', mouseMove);
                let ang = angle(ball) - 15
                if (interieurPlan(ball, ang, ball.offsetLeft, ball.offsetTop)) {
                    ball.style.transform = "rotate(" + ang + "deg)"
                };
            }
            if (name === 'z') {
                ball.removeEventListener('mousemove', mouseMove);
                let ang = angle(ball) + 15;
                if (interieurPlan(ball, ang, ball.offsetLeft, ball.offsetTop)) {
                    ball.style.transform = "rotate(" + ang + "deg)";
                };
            }

            if (name === 'w') {
                ball.removeEventListener('mousemove', mouseMove);
                tr = ball.getElementsByTagName('table')[0].getElementsByTagName('tr')
                largeur = tr[0].cells[0].offsetWidth + 10
                for (var i = 0; i < tr[0].cells.length; i++) {
                    tr[0].cells[i].style.width = largeur + "px"
                }
            }
            if (name === 'x') {
                ball.removeEventListener('mousemove', mouseMove);

                tr = ball.getElementsByTagName('table')[0].getElementsByTagName('tr')
                largeur = tr[0].cells[0].offsetWidth - 10
                for (var i = 0; i < tr[0].cells.length; i++) {
                    tr[0].cells[i].style.width = largeur + "px"
                }
            }
            if (name === 'q') {
                ball.removeEventListener('mousemove', mouseMove);
                let p = document.getElementById('plan');
                tr = ball.getElementsByTagName('table')[0].getElementsByTagName('tr')
                let hauteur = tr[0].cells[0].offsetHeight + 10
                let bottomP = p.offsetHeight;
                let bottomB = ball.offsetTop + ball.offsetHeight + 10;

                if (bottomB < bottomP) {
                    for (var i = 0; i < tr[0].cells.length; i++) {
                        tr[0].cells[i].style.height = hauteur + "px"
                    }
                }
            }
            if (name === 's') {
                ball.removeEventListener('mousemove', mouseMove);
                tr = ball.getElementsByTagName('table')[0].getElementsByTagName('tr')
                hauteur = tr[0].cells[0].offsetHeight - 10;

                for (var i = 0; i < tr[0].cells.length; i++) {
                    tr[0].cells[i].style.height = hauteur + "px"
                }
            }
        })
    });

    function interieurPlan(ball, angle, X, Y) {
        let p = document.getElementById('plan');
        let xMax = p.offsetWidth;
        let yMax = p.offsetHeight;

        function interieur(x, y) {
            let angelR = (angle * Math.PI) / 180.0;
            let xp = Math.cos(angelR) * x - Math.sin(angelR) * y;
            let yp = Math.sin(angelR) * x + Math.cos(angelR) * y;
            reelXp = xp + (ball.offsetWidth / 2) + X;
            reelYp = yp + (ball.offsetHeight / 2) + Y;

            let Ok = true;
            if (reelXp <= 0 || reelXp >= xMax) {
                Ok = false
            }
            if (reelYp <= 0 || reelYp >= yMax) {
                Ok = false
            }
            return Ok;
        }
        //A        
        let x = -(ball.offsetWidth / 2) // X du coin en haut à gauche par rapport au centre du rect
        let y = -(ball.offsetHeight / 2) // Y du coin en haut à gauche par rapport au centre du rect
        let a = interieur(x, y);

        //B
        x = (ball.offsetWidth / 2) // X du coin en haut à gauche par rapport au centre du rect
        y = -(ball.offsetHeight / 2) // Y du coin en haut à gauche par rapport au centre du rect
        let b = interieur(x, y);

        //C
        x = (ball.offsetWidth / 2) // X du coin en haut à gauche par rapport au centre du rect
        y = (ball.offsetHeight / 2) // Y du coin en haut à gauche par rapport au centre du rect
        let c = interieur(x, y);

        //D
        x = -(ball.offsetWidth / 2) // X du coin en haut à gauche par rapport au centre du rect
        y = (ball.offsetHeight / 2) // Y du coin en haut à gauche par rapport au centre du rect
        let d = interieur(x, y);
        if (a && b && c && d) {

            return true;
        }

        return false;
    }

    function reset() {
        ball.removeEventListener('mousemove', mouseMove);
        document.removeEventListener('mouseup', u);
        document.removeEventListener('keydown', k)
    }

    function mouseMove(e) {
        let p = document.getElementById('plan');
        // calculate the new position
        newPosX = startPosX - e.clientX;
        newPosY = startPosY - e.clientY;

        // with each move we also want to update the start X and Y
        startPosX = e.clientX;
        startPosY = e.clientY;

        let xNew = (ball.offsetLeft - newPosX);
        let yNew = (ball.offsetTop - newPosY);
        if (interieurPlan(ball, angle(ball), xNew, yNew)) {
            console.log("ok");
            ball.style.left = xNew + "px";
            ball.style.top = yNew + "px";
        };
    }
    function angle(el) {
        var st = window.getComputedStyle(el, null);
        var tr = st.getPropertyValue("-webkit-transform") ||
            st.getPropertyValue("-moz-transform") ||
            st.getPropertyValue("-ms-transform") ||
            st.getPropertyValue("-o-transform") ||
            st.getPropertyValue("transform") ||
            "FAIL";
        // rotation matrix - http://en.wikipedia.org/wiki/Rotation_matrix
        var values = tr.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var c = values[2];
        var d = values[3];
        var scale = Math.sqrt(a * a + b * b);
        // arc sin, convert from radians to degrees, round
        var sin = b / scale;
        // next line works for 30deg but not 130deg (returns 50);
        // var angle = Math.round(Math.asin(sin) * (180/Math.PI));
        var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
        return angle;
    }
    function recordPosition() {
        const Url = new URL(window.location.href);
        fetch(Url.origin + "/gestion/evenement/container/position/", {
            method: 'POST',
            body: JSON.stringify({
                id: ball.dataset['id'],
                x: (ball.offsetLeft - newPosX),
                y: (ball.offsetTop - newPosY),
                angle: angle(ball),
                largeur: ball.getElementsByTagName('table')[0].getElementsByTagName('tr')[0].cells[0].offsetWidth,
                hauteur: ball.getElementsByTagName('table')[0].getElementsByTagName('tr')[0].cells[0].offsetHeight
            }),
            headers: {
                "Content-type": "application/json"
            }
        })
            .then(function (response) { return response.text() })
            .then(function (text) {
                if (text) {
                    console.log(text);
                    let contenu = "<div class='alert alert-success' role='alert'><button type='button' class='btn-close' data-bs-dismiss='alert'></button><strong>" + text + "</strong></div>";;
                    let alertJs = document.getElementById("alertJs")
                    alertJs.innerHTML = contenu;

                } else {
                    alert('Il y a eu un pb avec l\'application: la nouvelle position n\'a pas été enregistrée!');
                }
            })
            .catch(function (error) {
                console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
            });
    }
}
function largeurM() {
    plan = document.getElementById("plan")
    plan.style.width = (plan.offsetWidth - 10) + "px"
}
function largeurP() {
    plan = document.getElementById("plan")
    plan.style.width = (plan.offsetWidth + 10) + "px"
}
function hauteurM() {
    plan = document.getElementById("plan")
    plan.style.height = (plan.offsetHeight - 10) + "px"
}
function hauteurP() {
    plan = document.getElementById("plan")
    plan.style.height = (plan.offsetHeight + 10) + "px"
}
function recordDimension() {
    const Url = new URL(window.location.href);
    fetch(Url.origin + "/gestion/evenement/dimension/", {
        method: 'POST',
        body: JSON.stringify({
            id: document.getElementById("plan").dataset['id'],
            largeur: document.getElementById("plan").offsetWidth,
            hauteur: document.getElementById("plan").offsetHeight
        }),
        headers: {
            "Content-type": "application/json"
        }
    }).then(function (response) {
        if (response.ok) {
            responsivePlan();
        } else {
            alert('Il y a eu un pb avec l\'application: la nouvelle position n\'a pas été enregistrée!');
        }
    })
        .catch(function (error) {
            console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
        });
}

function responsivePlan() {
    
    let plan = document.getElementById('plan');
    let contenu = document.querySelector('.paquet-contenu');
    let largeurEcran = contenu.clientWidth-20;
    let hauteurReglage = document.getElementById('reglage').offsetHeight;
    let largeur = plan.dataset['width']
    let hauteur = plan.dataset['height']    
    plan.style.setProperty('--height-plan', hauteur + "px")
    plan.style.setProperty('--width-plan', largeur + "px")
    if ((largeurEcran) < largeur) {
        let echelle = largeurEcran / largeur;
          
        let deplacement = -(hauteur - (hauteur * echelle)) / 2;
        plan.style.setProperty('--translate-plan', deplacement + "px")
        plan.style.setProperty('--scale', echelle);
        let newHauteurContenu = hauteurReglage+ (hauteur * echelle)+40;
        
        console.log(newHauteurContenu);
        contenu.style.setProperty('height', newHauteurContenu + "px")
    }
}

