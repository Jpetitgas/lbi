<?php

namespace App\Controller\Gestion\Todolist;

use App\Entity\Todolist;
use App\Repository\TodolistRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("gestion/todolist", name="gestion_todolist")
 */
class TodolistController extends AbstractController
{
    /**
    * @Route("/ajout/", name="_ajout", methods={"GET", "POST"})
    */
    public function position(Request $request)
    {
        $params = json_decode($request->getContent(), true);
        extract($params);
        if ($tache && $tache!=="") {
            $todo=new Todolist();
            $todo->setTache($tache)
            ->setDone(false);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($todo);
            $entityManager->flush();
        }


        return new JsonResponse(array('id' => $todo->getId()));
    }
    /**
     * @Route("/suppression/", name="_suppression", methods={"GET", "POST"})
     */
    public function suppression(Request $request, TodolistRepository $todolistRepository)
    {
        $params = json_decode($request->getContent(), true);
        extract($params);
        $tache=$todolistRepository->find($id);
        if ($tache) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tache);
            $entityManager->flush();
        }


        return new JsonResponse();
    }
}
