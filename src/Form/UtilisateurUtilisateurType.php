<?php

namespace App\Form;

use App\Entity\CP;
use App\Entity\Ville;
use App\Entity\Utilisateur;
use App\Repository\VilleRepository;
use Doctrine\ORM\Query\Expr\Select;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class UtilisateurUtilisateurType extends AbstractType
{
    public $villeRepository;

    public function __construct(Security $security, VilleRepository $villeRepository)
    {
        $this->security = $security;
        $this->villeRepository = $villeRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('adresse', TextType::class)
            ->add('CP', EntityType::class, [
                'label' => 'Code Postal',
                'class' => CP::class,
                'placeholder' => 'selectionner un code postal',
                'choice_label' => 'code',
            ])

            ->add('town', EntityType::class, [
                'class' => Ville::class,
                'label' => 'Ville',
                'placeholder' => 'selectionner une ville',
                'choice_label' => 'nomMaj',
                ])
            ->add('portable', TelType::class, [
                'constraints'=> [ new Regex('#^0[0-9]{9}$#')]
            ]);

        $ajoutChampVille = function (FormInterface $form, CP $cp = null) {
            $villes = $this->villeRepository->findBy(['cp' => $cp], ['nomMaj'=>'ASC']);

            if ($villes) {
                $form->add('town', EntityType::class, [
                'class' => Ville::class,
                'label' => 'Ville',
                'placeholder' => 'selectionner une ville',
                'choice_label' => 'nomMaj',
                'choices' => $villes,
                ]);
            } else {
                $form->add('town', ChoiceType::class, [
                'label' => 'Ville',
                'placeholder' => 'Choisir un code postal',
            ]);
            }
        };
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($ajoutChampVille) {
                 $utilisateur = $event->getData();
                 $form = $event->getForm();
                 if (!($utilisateur->getId() === null)) {
                     $cp = $utilisateur->getCP();
                     $builder = $form->getConfig()->getFormFactory()->createNamedBuilder('CP', EntityType::class, null, [
                         'label' => 'Code Postal',
                         'class' => CP::class,
                         'placeholder' => 'selectionner un code postal',
                         'choice_label' => 'code',
                         'data' => $cp,
                         'auto_initialize' => false,
                     ]);
                     $builder->addEventListener(
                         FormEvents::POST_SUBMIT,
                         function (FormEvent $event) use ($ajoutChampVille) {
                         $cp = $event->getForm()->getData();
                         $form = $event->getForm();
                         $ajoutChampVille($form->getParent(), $cp);
                     }
                     );
                     $form->add($builder->getForm());

                     $ajoutChampVille($form, $cp);
                 }
             }
        );
        /*$builder->addEventListener(
            FormEvents::POST_SET_DATA,
             function (FormEvent $event) use ($ajoutChampVille) {
                 $utilisateur = $event->getData();
                 $form = $event->getForm();
             });*/
        $builder->get('CP')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($ajoutChampVille) {
                $cp = $event->getForm()->getData();
                $form = $event->getForm();
                $ajoutChampVille($form->getParent(), $cp);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
