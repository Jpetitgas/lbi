<?php

namespace App\Repository;

use App\Entity\Article;
use App\SearchData\ArticleData;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }
    public function inscriptions(ArticleData $data)
    {
        $query = $this
            ->createQueryBuilder('a')
            ->select('a', 'c', 'm')
            ->join('a.categorie', 'c')
            ->leftJoin('a.menu', 'm');

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('a.titre LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }
        if (!empty($data->categorie)) {
            $query = $query
                ->andWhere('a.categorie =:categorie')
                ->setParameter('categorie', $data->section);
        }
        if (!empty($data->menu)) {
            $query = $query
                ->andWhere('a.menu =:menu')
                ->setParameter('menu', $data->menu);
        }

        return $query->getQuery();
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
