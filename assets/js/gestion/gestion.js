
let liste = document.getElementById('liste');
let buttons = liste.getElementsByClassName('btn-close')
for (let button of buttons) {
    button.addEventListener('click', function () {
        suppression(this);
    })
}
function suppression(button) {
    const Url = new URL(window.location.href);
    fetch(Url.origin + "/gestion/todolist/suppression/", {
        method: 'POST',
        body: JSON.stringify({
            id: button.dataset['id']
        }),
        headers: {
            "Content-type": "application/json"
        }
    }).then(function (response) {
        if (response.ok) {

        } else {
            alert('Il y a eu un pb avec l\'application: la nouvelle position n\'a pas été enregistrée!');
        }
    })
        .catch(function (error) {
            console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
        });

}
let tache = document.querySelector(".tache")
tache.addEventListener("change", function () {
    const Url = new URL(window.location.href);
    fetch(Url.origin + "/gestion/todolist/ajout/", {
        method: 'POST',
        body: JSON.stringify({
            tache: this.value
        }),
        headers: {
            "Content-type": "application/json"
        }
    }).then(response => response.json())
        .then(data => {

            let id = data.id
            let tache = document.querySelector(".tache")
            let liste = document.getElementById('liste');
            let alert = document.createElement('div');
            alert.setAttribute('id', "tache" + id);
            alert.setAttribute('class', "alert alert-warning d-flex alert-dismissible fade show");
            alert.setAttribute('role', "alert");
            alert.appendChild(document.createTextNode(tache.value));
            let button = document.createElement('button');
            button.setAttribute('type', 'button')
            button.setAttribute('class', 'btn-close')
            button.setAttribute('data-bs-dismiss', 'alert')
            button.setAttribute('aria-label', 'Close')
            alert.appendChild(button)
            liste.appendChild(alert)
            tache.value = "";

        })
        .catch(function (error) {
            console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
        });
})
