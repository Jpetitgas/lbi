<?php

namespace App\Command;

use DateTimeImmutable;
use App\Entity\Pratiquant;
use App\Repository\PratiquantRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UtilisateurRepository;
use PhpParser\Node\Stmt\TryCatch;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreationPratiquantCsvFichierCommand extends Command
{
    private EntityManagerInterface $entityManager;
    private string $dataDirectory;
    private PratiquantRepository $pratiquantRepository;
    private UtilisateurRepository $utilisateurRepository;
    protected static $defaultName = 'app:import-pratiquant';
    protected static $defaultDescription = 'Import des pratiquants à partir d\'un fichier CSV';

    public function __construct(EntityManagerInterface $entityManager, string $dataDirectory, UtilisateurRepository $utilisateurRepository, PratiquantRepository $pratiquantRepository)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->dataDirectory = $dataDirectory;
        $this->pratiquantRepository = $pratiquantRepository;
        $this->utilisateurRepository = $utilisateurRepository;
    }

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription);
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->createUtilisateur();

        return Command::SUCCESS;
    }

    private function getDataFromFile()
    {
        $file = $this->dataDirectory.'pratiquant.csv';
        $normalizers = [new ObjectNormalizer()];
        $encoders = [new CsvEncoder()];
        $serializer = new Serializer($normalizers, $encoders);
        $fileString = file_get_contents($file);
        $data = $serializer->decode($fileString, 'csv');

        return $data;
    }

    private function createUtilisateur()
    {
        $this->io->section('CREATION DES PRATIQUANTS');
        $pratiquantCreated = 0;
        foreach ($this->getDataFromFile() as $row) {
            if ($utilisateur = $this->utilisateurRepository->find($row['utilisateur'])) {
                $pratiquant = new Pratiquant();
                $pratiquant->setNom($row['nom'])
                            ->setSexe($row['sexe'])
                            ->setPrenom($row['prenom'])
                            ->setNaissance(new \DateTimeImmutable($row['datenaissance']))
                            ->setUtilisateur($utilisateur);

                $this->entityManager->persist($pratiquant);
                ++$pratiquantCreated;
            }
        }
        $this->entityManager->flush();

        if ($pratiquantCreated > 1) {
            $string = "{$pratiquantCreated} PRATIQUANTS CREES EN BASE";
        } elseif ($pratiquantCreated == 1) {
            $string = '1 PRATIQUANT CREE EN BASE';
        } else {
            $string = 'AUCUN PRATIQUANT CREE EN BASE';
        }
        $this->io->success($string);
    }
}
