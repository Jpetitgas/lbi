<?php

namespace App\Form;

use App\Entity\Banque;
use App\Entity\Dossier;
use App\Entity\Paiement;

use App\Service\EnCours;
use App\Entity\TypeDePaiement;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PaiementType extends AbstractType
{
    protected $saison;

    public function __construct(EnCours $enCours)
    {
        $this->saison=$enCours->saison();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $id_saison=$this->saison->getId();
        $builder

            ->add(
                'montant',
                MoneyType::class,
                [
                            'required' => true, ]
            )
            ->add('typeDePaiement', EntityType::class, [
                'class' => TypeDePaiement::class,
                'placeholder' => 'selectionner un type de paiement',
                'required' => true,
                ])
            ->add('encaissement', TextType::class, [
                'required' => false,
            ])

            ->add('banque', EntityType::class, [
                'class' => Banque::class,
                'placeholder' => 'selectionner une banque',
                'required' => false,
                ])
            ->add('reference', TextType::class, [
                'required' => false,
            ])
            ->add('encaisse', ChoiceType::class, [
                'choices'  => [
                    'Non' => false,
                    'Oui' => true,
                ],
            ])

            ->add('remarque', TextareaType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Paiement::class,
        ]);
    }
}
