<?php

namespace App\Controller\Gestion;

use App\Entity\TypeDePaiement;
use App\Form\TypeDePaiementType;
use App\Repository\TypeDePaiementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/type_paiement", name="gestion_type_paiement")
 */
class TypeDePaiementController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(TypeDePaiementRepository $typeDePaiementRepository): Response
    {
        return $this->render('gestion/type_paiement/index.html.twig', [
            'type_de_paiements' => $typeDePaiementRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeDePaiement = new TypeDePaiement();
        $formTypeDePaiement = $this->createForm(TypeDePaiementType::class, $typeDePaiement);
        $formTypeDePaiement->handleRequest($request);

        if ($formTypeDePaiement->isSubmitted() && $formTypeDePaiement->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeDePaiement);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_type_paiement_index');
        }

        return $this->render('gestion/type_paiement/new.html.twig', [
            'type_de_paiement' => $typeDePaiement,
            'formTypeDePaiement' => $formTypeDePaiement->createView(),
        ]);
    }



    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeDePaiement $typeDePaiement): Response
    {
        $formTypeDePaiement = $this->createForm(TypeDePaiementType::class, $typeDePaiement);
        $formTypeDePaiement->handleRequest($request);

        if ($formTypeDePaiement->isSubmitted() && $formTypeDePaiement->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_type_paiement_index');
        }

        return $this->render('gestion/type_paiement/edit.html.twig', [
            'type_de_paiement' => $typeDePaiement,
            'formTypeDePaiement' => $formTypeDePaiement->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, TypeDePaiement $typeDePaiement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeDePaiement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeDePaiement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_type_paiement_index');
    }
}
