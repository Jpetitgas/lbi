<?php

namespace App\Service;

use App\Repository\TailleRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Cart
{
    protected $session;
    protected $tailleRepository;

    public function __construct(SessionInterface $session, TailleRepository $tailleRepository)
    {
        $this->session = $session;
        $this->tailleRepository = $tailleRepository;
    }

    protected function getCart()
    {
        return $cart = $this->session->get('cart', []);
    }

    protected function saveCart($cart)
    {
        return $cart = $this->session->set('cart', $cart);
    }

    public function empty()
    {
        $this->saveCart([]);
    }

    public function add($id)
    {
        $cart = $this->getCart();
        if (array_key_exists($id, $cart)) {
            ++$cart[$id];
        } else {
            $cart[$id] = 1;
        }
        $this->saveCart($cart);
    }

    public function remove($id)
    {
        $cart = $this->getCart();
        unset($cart[$id]);
        $this->saveCart($cart);
    }

    public function decrement($id)
    {
        $cart = $this->getCart();
        if (!array_key_exists($id, $cart)) {
            return;
        }
        if ($cart[$id] === 1) {
            $this->remove($id);

            return;
        }
        --$cart[$id];

        $this->saveCart($cart);
    }

    public function getTotal()
    {
        $total = 0;
        foreach ($this->getCart() as $id => $qty) {
            $taille = $this->tailleRepository->find($id);
            if (!$taille) {
                continue;
            }
            $total += $taille->getPrice() * $qty;
        }

        return $total;
    }

    /**
     * @return CartItem[]
     */
    public function getDetailedCartItems()
    {
        $detailCart = [];

        foreach ($this->getCart() as $id => $qty) {
            $taille = $this->tailleRepository->find($id);
            if (!$taille) {
                continue;
            }
            $detailCart[] = new CartItem($taille, $qty);
        }

        return $detailCart;
    }
}
