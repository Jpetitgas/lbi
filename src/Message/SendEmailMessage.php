<?php

namespace App\Message;

final class SendEmailMessage
{
    private $recipientEmail;
    private $sujet;
    private $htmlTemplate;
    private $destinataire;
    private $context;

    public function __construct($recipientEmail, $sujet, $htmlTemplate, $destinataire, $context)
    {
        $this->recipientEmail = $recipientEmail;
        $this->sujet = $sujet;
        $this->htmlTemplate = $htmlTemplate;
        $this->destinataire = $destinataire;
        $this->context = $context;
    }
    public function getRecipientEmail()
    {
        return $this->recipientEmail;
    }
    public function getSujet()
    {
        return $this->sujet;
    }
    public function getHtmlTemplate()
    {
        return $this->htmlTemplate;
    }
    public function getContext()
    {
        return $this->context;
    }
    public function getDestinataire()
    {
        return $this->destinataire;
    }
}
