

if (document.getElementById('canvas')) {
    window.addEventListener('DOMContentLoaded', responsivePlan)
    window.addEventListener('resize', responsivePlan);
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');
    const contenant = document.getElementById('interface-reservation');
    let echelle = 1;
    let largeur = canvas.width;
    let hauteur = canvas.height;
    const selection = document.getElementById('nom');
    const ut = document.getElementById('utilisateur');
    const placeLibre = document.getElementById('libre');
    const paiementOption = document.getElementById('paiementOption');
    const qtyOptions = document.querySelectorAll('.qtyOption');
    const totalPaiement = document.getElementById('totalPaiement');
    const stripeDiv = document.getElementById('stripeDiv');
    const billetDiv = document.getElementById('billetDiv');
    const idUtilisateur = ut.dataset.utilisateur;
    const idEvenement = canvas.dataset.evenement;
    let reservations = document.querySelectorAll('.placeReserved');
    const totalReservation = document.getElementById('total-reservation');
    const montantTotal = document.getElementById('montant-total');
    const reservationCol = document.getElementById('reservation');
    const optionCol = document.getElementById('option');
    const ancre = document.getElementById('ancre');
    let containers = [];

    

    /**Get all data of the evenement */

    fetch("/utilisateur/evenement/plan/data/" + idEvenement.toString())
        .then(function (response) { return response.json() })
        .then(r => {
            containers = r
            render();

        })
        .catch(function (error) {
            console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
        });
    function getMousePos(e) {
        const rect = canvas.getBoundingClientRect();

        return {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top,
        }
    }
    function responsivePlan() {
        let contenu = document.getElementById('reglage');
        let largeurEcran = contenu.clientWidth;

        if ((largeurEcran) < largeur) {
            echelle = largeurEcran / largeur;
        } else { echelle = 1 }

        // ctx.clearRect(0, 0, largeur, hauteur);
        canvas.width = largeur * echelle;
        canvas.height = hauteur * echelle;
        canvas.removeEventListener("click", selectionPlace);
        render();
    }

    function containerBuild(container) {
        let largeur = container['largeur'] * echelle;
        let hauteur = container['hauteur'] * echelle;
        let X = container['x'] * echelle;
        let Y = container['y'] * echelle;
        let angleDeg = container['angle'];

        let nbPlace = container[0].length;
        let nbPlaceParLigne = Math.ceil(nbPlace / container['nbLigne']);

        let positionX = 0;
        let positionY = 0;
        let angle = angleDeg * (Math.PI / 180);

        ctx.translate(X, Y)
        ctx.rotate(angle);

        for (let i = 0; i < nbPlace; i++) {
            ctx.fillStyle = 'rgb(0,0,0)';
            if (container[0][i]['utilisateur'] == idUtilisateur ) {
                ctx.fillStyle = 'rgb(255,0,0)';
            }
            if (container[0][i]['utilisateur'] == null ) {
                ctx.fillStyle = 'rgb(0,255,0)';
            }
            if (container[0][i]['verrouiller'] == 1) {
                ctx.fillStyle = 'rgb(0,0,0)';
            }

            ctx.fillRect(positionX, positionY, largeur, hauteur);
            ctx.strokeRect(positionX, positionY, largeur, hauteur);
            positionX += largeur;
            if (i != 0 && ((i + 1) % nbPlaceParLigne) == 0) {
                positionY += hauteur;
                if (((i + 1) % nbPlaceParLigne) == 0) {
                    positionX = 0;
                }
            }
        }
        ctx.resetTransform();
    }

    /**
    * Find point after rotation around another point by X degrees
    *
    * @param {Array} point The point to be rotated [X,Y]
    * @param {Array} rotationCenterPoint The point that should be rotated around [X,Y]
    * @param {Number} degrees The degrees to rotate the point
    * @return {Array} Returns point after rotation [X,Y]
    */
    function rotatePoint(point, rotationCenterPoint, degrees) {
        // Using radians for this formula
        var radians = degrees * Math.PI / 180;

        // Translate the plane on which rotation is occurring.
        // We want to rotate around 0,0. We'll add these back later.
        point[0] -= rotationCenterPoint[0];
        point[1] -= rotationCenterPoint[1];

        // Perform the rotation
        var newPoint = [];
        newPoint[0] = point[0] * Math.cos(radians) - point[1] * Math.sin(radians);
        newPoint[1] = point[0] * Math.sin(radians) + point[1] * Math.cos(radians);


        // Translate the plane back to where it was.
        newPoint[0] += rotationCenterPoint[0];
        newPoint[1] += rotationCenterPoint[1];

        return newPoint;
    }
    /**
    * Find the vertices of a rotating rectangle
    *
    * @param {Array} position From left, top [X,Y]
    * @param {Array} size Lengths [X,Y]
    * @param {Number} degrees Degrees rotated around center
    * @return {Object} Arrays LT, RT, RB, LB [X,Y]
    */
    function findRectVertices(position, size, degrees) {
        var left = position[0];
        var right = position[0] + size[0];
        var top = position[1];
        var bottom = position[1] + size[1];
        var radians = degrees * Math.PI / 180;
        var center = [left + (((right - left) / 2) * Math.cos(radians)), top + (((bottom - top) / 2) * Math.sin(radians))];
        var LT = [left, top];
        var RT = [right, top];
        var RB = [right, bottom];
        var LB = [left, bottom];

        return {
            LT,
            RT: rotatePoint(RT, LT, degrees),
            RB: rotatePoint(RB, LT, degrees),
            LB: rotatePoint(LB, LT, degrees),
        };
    }
    /**
    * Distance formula
    *
    * @param {Array} p1 First point [X,Y]
    * @param {Array} p2 Second point [X,Y]
    * @return {Number} Returns distance between points
    */
    function distance(p1, p2) {
        return Math.sqrt(Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2));
    }

    /**
    * Heron's formula (triangle area)
    *
    * @param {Number} d1 Distance, side 1
    * @param {Number} d2 Distance, side 2
    * @param {Number} d3 Distance, side 3
    * @return {Number} Returns area of triangle
    */
    function triangleArea(d1, d2, d3) {
        // See https://en.wikipedia.org/wiki/Heron's_formula
        var s = (d1 + d2 + d3) / 2;
        return Math.sqrt(s * (s - d1) * (s - d2) * (s - d3));
    }
    /**
    * Determine if a click hit a rotated rectangle
    *
    * @param {Array} click Click position [X,Y]
    * @param {Array} position Rect from left, top [X,Y]
    * @param {Array} size Rect size as lengths [X,Y]
    * @param {Number} degrees Degrees rotated around center
    * @return {Boolean} Returns true if hit, false if miss
    */
    function clickHit(click, position, size, degrees) {
        // Find the area of the rectangle
        // Round to avoid small JS math differences
        var rectArea = Math.round(size[0] * size[1]);

        // Find the vertices
        var vertices = findRectVertices(position, size, degrees);

        // Create an array of the areas of the four triangles

        var triArea = [
            // Click, LT, RT
            triangleArea(
                distance(click, vertices.LT),
                distance(vertices.LT, vertices.RT),
                distance(vertices.RT, click)
            ),
            // Click, RT, RB
            triangleArea(
                distance(click, vertices.RT),
                distance(vertices.RT, vertices.RB),
                distance(vertices.RB, click)
            ),
            // Click, RB, LB
            triangleArea(
                distance(click, vertices.RB),
                distance(vertices.RB, vertices.LB),
                distance(vertices.LB, click)
            ),
            // Click, LB, LT
            triangleArea(
                distance(click, vertices.LB),
                distance(vertices.LB, vertices.LT),
                distance(vertices.LT, click)
            )
        ];

        // Reduce this array with a sum function
        // Round to avoid small JS math differences
        triArea = Math.round(triArea.reduce(function (a, b) { return a + b; }, 0));

        // Finally do that simple thing we visualized earlier
        if (triArea > rectArea) {
            return false;
        }
        return true;
    }
    function findContainer(mousePose) {

        let resultOfSearch = []
        containers.forEach(container => {
            let click = [mousePose.x, mousePose.y];
            let position = [container['x'] * echelle, container['y'] * echelle];
            let largeurPlace = container['largeur'] * echelle;
            let hauteurPlace = container['hauteur'] * echelle
            let nbPlace = container[0].length;
            let nbPlaceParLigne = Math.ceil(nbPlace / container['nbLigne']);
            let size = [largeurPlace * nbPlaceParLigne, hauteurPlace * container['nbLigne']]
            let degrees = container['angle']

            if (clickHit(click, position, size, degrees) == true) {

                let ligne = 0;
                let place = 0;
                let sizePlace = [largeurPlace, hauteurPlace]

                for (let p = 0; p < nbPlace; p++) {
                    if (p != 0 && (p % nbPlaceParLigne) == 0) {
                        ligne++;
                        place = 0;
                    }
                    let positionPlaceX = container['x'] * echelle + (place * largeurPlace);
                    let positionPlaceY = container['y'] * echelle + (ligne * hauteurPlace)
                    let xM, yM, x, y;
                    let angle = degrees * (Math.PI / 180);
                    xM = positionPlaceX - position[0];
                    yM = positionPlaceY - position[1];

                    x = Math.cos(angle) * xM - Math.sin(angle) * yM + position[0];
                    y = Math.sin(angle) * xM + Math.cos(angle) * yM + position[1];

                    let positionPlace = [x, y];

                    place++;

                    if (clickHit(click, positionPlace, sizePlace, degrees) == true) {

                        resultOfSearch = [container['id'], container['nom'], container[0][p]['id'], container[0][p]['nom']];

                    };
                }
            };
        });

        return resultOfSearch;
    }

    function fond() {
        ctx.drawImage(document.getElementById('fond'),
            0, 0, largeur * echelle, hauteur * echelle);
    }

    function render() {
        fond();

        containers.forEach(container => { containerBuild(container); });
        canvas.addEventListener("click", selectionPlace);
    }
    function selectionPlace(e) {
        e.preventDefault();
        const mousePos = getMousePos(e);
        let selectedPlace;
        selectedPlace = findContainer(mousePos, containers);
        reservation(selectedPlace[2]);
    }
    function reservation(ipPlace) {
        fetch("/utilisateur/place/reserverPlan/" + ipPlace.toString())
            .then(function (response) { return response.json() })
            .then(r => {
                actualisation(r);
            })
            .catch(function (error) {
                console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
            });

    }
    function infoJs(message, type) {
        let contenu = "<div class='alert alert-" + type + "' role='alert'><button type='button' class='btn-close' data-bs-dismiss='alert'></button><strong>" + message + "</strong></div>";;
        notification = document.getElementById("notification")
        notification.innerHTML = contenu;
        // notification.classList.add("notification");
    }
    function actualisation(r) {

        containers.forEach(c => {
            if (c['id'] == r['idContainer']) {
                c[0].forEach(p => {
                    if (p['id'] == r['idPlace']) {
                        switch (r['etat']) {
                            case 'cancel':
                                p['utilisateur'] = null;
                                deletedReservation(r)
                                break;
                            case 'ok':
                                p['utilisateur'] = idUtilisateur;
                                addReservation(r)
                                break;
                        }
                    }
                });
                containerBuild(c)
            }
        });

        infoJs(r['reponse'], 'success');
    }

    

    function deletedReservation(p) {
        let newReservationsToDelete = document.querySelectorAll('.placeReserved');
        for (let res = 0; res < newReservationsToDelete.length; res++) {

            if (newReservationsToDelete[res].dataset.idplacereserved == p['idPlace']) {
                newReservationsToDelete[res].remove()
            }
        }
        if (newReservationsToDelete.length == 1 && optionCol) {
            optionCol.style.display = "none"
        }
        priceTotalReservation();
        let nbLigne = parseInt(placeLibre.textContent);
        placeLibre.textContent = nbLigne + 1;
    }
    function addReservation(r) {
        let newReservation = document.createElement("div");
        newReservation.classList.add("row", "placeReserved");
        newReservation.dataset.idplacereserved = r['idPlace'];
        newReservation.dataset.price = r['pricePlace'];
        let newColNom = document.createElement("div");
        newColNom.classList.add("col-8");
        newColNom.textContent = r['nomContainer'] + ' ' + r['nomPlace'];
        let newColPrice = document.createElement("div");
        newColPrice.classList.add("col-4");
        let price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(r['pricePlace']);
        newColPrice.textContent = price;
        newReservation.appendChild(newColNom);
        newReservation.appendChild(newColPrice);
        if (optionCol) {
            optionCol.style.display = "block"
        }

        reservationCol.insertBefore(newReservation, totalReservation);
        priceTotalReservation();
        let nbLigne = parseInt(placeLibre.textContent);
        placeLibre.textContent = nbLigne - 1;
    }
    function priceTotalReservation() {
        let newReservationsForTotal = document.querySelectorAll('.placeReserved');
        let newMontantTotal = 0;
        for (let res = 0; res < newReservationsForTotal.length; res++) {
            newMontantTotal += parseInt(newReservationsForTotal[res].dataset.price);
        }
        montantTotal.textContent = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(newMontantTotal);
        let paiement = parseInt(totalPaiement.textContent);
        let optionP = 0;
        if (paiementOption) {
            optionP = parseInt(paiementOption.textContent);
            
            if (newMontantTotal == 0) {
                let price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(0);
                paiementOption.textContent = price;
                
                qtyOptions.forEach(qtyOption => (
                    qtyOption.textContent = 0
                )
                )
            }
        }

        if ((newMontantTotal + optionP) - paiement > 0) {
            stripeDiv.style.display = "block"
            billetDiv.style.display = "none"
            ancre.innerHTML = '<a href="#stripeDiv">Paiement</a>';
        } else {
            stripeDiv.style.display = "none"
            newMontantTotal == 0 ? billetDiv.style.display = "none" : billetDiv.style.display = "block";
            ancre.innerHTML = '<a href="#billetDiv">Vos billet(s)</a>'; 
        }

    }
}
/* localition */
var macarte = null;
// Fonction d'initialisation de la carte
function initMap() { // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
    macarte = L.map('localisation').setView([
        lat, lon
    ], 15);
    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', { // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20,

    }).addTo(macarte);

}

// Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
initMap();

var marker = L.marker([
    lat,
    lon
]).addTo(macarte);
// Nous ajoutons la popup. A noter que son contenu (ici la variable ville) peut être du HTML
marker.bindPopup('Lieu');



/* Paiment stripe */
var evenement = document.getElementById('evenement');
var id_evenement = evenement.dataset.id;
var utilisateur = document.getElementById('utilisateur');
const ut = document.getElementById('utilisateur');
var id_utilisateur = ut.dataset.utilisateur;
const stripe = Stripe(strikePublicCle);
var checkoutButton = document.getElementById('checkout-button');
if (checkoutButton != undefined) {
    checkoutButton.addEventListener('click', function () {
        // Create a new Checkout Session using the server-side endpoint you
        // created in step 3.
        fetch('/evenement/create-checkout-session', {
            method: 'POST',
            body: JSON.stringify({
                id_evenement: id_evenement,
                id_utilisateur: id_utilisateur
            }),
            headers: {
                "Content-type": "application/json"
            }
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (session) {
                return stripe.redirectToCheckout({ sessionId: session.id });
            })
            .then(function (result) {
                // If `redirectToCheckout` fails due to a browser or network
                // error, you should display the localized error message to your
                // customer using `error.message`.
                if (result.error) {
                    alert(result.error.message);
                }
            })
            .catch(function (error) {
                console.error('Error:', error);
            });
    });
}