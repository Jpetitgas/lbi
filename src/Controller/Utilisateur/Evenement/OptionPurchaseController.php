<?php

namespace App\Controller\Utilisateur\Evenement;

use App\Entity\OptionPurchase;
use App\Form\OptionPurchaseType;

use Gedmo\Tree\RepositoryInterface;
use App\Repository\OptionRepository;
use App\SearchData\OptionPurchaseData;
use App\SearchForm\OptionPurchaseForm;
use App\Repository\EvenementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\OptionPurchaseRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("Utilisateur/evenement/option/purchase", name="utilisateur_evenement_option_purchase")
 */
class OptionPurchaseController extends AbstractController
{
    /**
     * @Route("/add/{id}", name="_add", methods={"GET"})
     */
    public function add($id, Security $security, OptionRepository $optionRepository, ManagerRegistry $doctrine): Response
    {
        $utilisateur=$security->getUser();
        $option=$optionRepository->find($id);
        $optionPurchase=new OptionPurchase();
        $optionPurchase->setDelivery(false)
                    ->setOptionEvenement($option)
                    ->setUtilisateur($utilisateur);
        $entityManager = $doctrine->getManager();
        $entityManager->persist($optionPurchase);
        $entityManager->flush();
        return $this->redirectToRoute('utilisateur_evenement_edit', ['id' => $option->getEvenement()->getId()]);
    }

    /**
     * @Route("/delete/{id}", name="_delete", methods={"GET"})
     */
    public function delete($id, Security $security, OptionRepository $optionRepository, ManagerRegistry $doctrine, OptionPurchaseRepository $optionPurchaseRepository): Response
    {
        $utilisateur=$security->getUser();
        $option=$optionRepository->find($id);
        $optionPurchase=$optionPurchaseRepository->findOneBy(['utilisateur'=>$utilisateur,'optionEvenement'=>$option]);
        if ($optionPurchase) {
            $entityManager = $doctrine->getManager();
            $entityManager->remove($optionPurchase);
            $entityManager->flush();
        }

        return $this->redirectToRoute('utilisateur_evenement_edit', ['id' => $option->getEvenement()->getId()]);
    }
}
