<?php

namespace App\Controller\Utilisateur\Boutique;

use App\Service\Cart;
use App\Entity\Taille;

use Doctrine\ORM\EntityRepository;
use App\Repository\TailleRepository;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use App\Repository\InformationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BoutiqueController extends AbstractController
{
    /**
     * @Route("boutique", name="utilisateur_boutique")
     */
    public function boutique(CategoryRepository $categoryRepository, InformationRepository $informationRepository, ProductRepository $productRepository): Response
    {
        $categories = $categoryRepository->findBy([], ['name' => 'ASC']);
        $products = $productRepository->findBy(['enAvant' => true, 'actif' => true]);
        return $this->render(
            'utilisateur/boutique/boutique.html.twig',
            [
                'categories' => $categories,
                'products' => $products,
                'information' => $informationRepository->findOneBy([])
            ]
        );
    }

    /**
     * @Route("boutique/categorie/{slug}", name="utilisateur_boutique_category")
     */
    public function category($slug, CategoryRepository $categoryRepository): Response
    {
        $category = $categoryRepository->findOneBy(['slug' => $slug]);
        $categories = $categoryRepository->findBy([], ['name' => 'ASC']);
        if (!$category) {
            throw new NotFoundHttpException("La catégorie demandée n'existe pas!");
        }

        return $this->render('utilisateur/boutique/category.html.twig', [
            'category' => $category,
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("boutique/produit/{category_slug}/{slug}", name="utilisateur_boutique_product_show")
     */
    public function show($slug, CategoryRepository $categoryRepository, Request $request, Cart $cart, ProductRepository $productRepository, TailleRepository $tailleRepository): Response
    {
        $product = $productRepository->findOneBy(['slug' => $slug, 'actif' => true]);
        if (!$product) {
            $this->addFlash('danger', 'Le produit demandé n\'existe pas!');
            return $this->redirectToRoute('utilisateur_boutique');
        }
        $categories = $categoryRepository->findBy([], ['name' => 'ASC']);
        $form = $this->createFormBuilder()
            ->add('taille', EntityType::class, [
                'required' => true,
                'class' => Taille::class,
                'label' => false,
                'placeholder' => 'Selectionner une taille',
                'query_builder' => function (EntityRepository $er) use ($product) {
                    return $er->createQueryBuilder('t')
                        ->where('t.product = :product')
                        ->orderBy('t.name', 'ASC')
                        ->setParameter('product', $product->getId());
                },
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $taille = $form->getData();
            $id = $taille['taille']->getId();
            if (!$tailleRepository->find($id)) {
                throw $this->createNotFoundException("le produit n'existe pas! ");
            }
            $cart->add($id);
            $this->addFlash('success', 'Le produit ' . $product->getName() . ' en taille ' . $taille['taille']->getName() . ' a été ajouté au panier');
            return $this->redirectToRoute('utilisateur_boutique_product_show', ['category_slug' => $product->getCategory()->getSlug(), 'slug' => $product->getSlug()]);
        }

        return $this->render('utilisateur/boutique/show.html.twig', [
            'product' => $product,
            'categories' => $categories,
            'form' => $form->createView(),
        ]);
    }
}
