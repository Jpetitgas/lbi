<?php

namespace App\Service;

class Csv
{
    function extractCourseDetails($courseName)
    {
        // Expression régulière pour extraire les différentes parties du nom du cours
        $pattern = '/([^-]+)-((?:[^-]+-)*[^-]+)-([^-]+)-([^-]+)-([^-]+)/';
        preg_match($pattern, $courseName, $matches);

        if (count($matches) >= 6) {
            $activite = $matches[1];
            $tranche = str_replace('-', ' ', $matches[2]); // Remplacer les tirets par des espaces pour la tranche
            $jour = $matches[3];
            $heureDebut = $matches[4];
            $heureFin = $matches[5];

            return [
                'activite' => $activite,
                'tranche' => $tranche,
                'jour' => $jour,
                'heureDebut' => $heureDebut,
                'heureFin' => $heureFin,
            ];
        }

        return [
            'activite' => '',
            'tranche' => '',
            'jour' => '',
            'heureDebut' => '',
            'heureFin' => '',
        ];
    }
}
