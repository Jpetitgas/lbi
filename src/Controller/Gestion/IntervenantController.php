<?php

namespace App\Controller\Gestion;

use App\Service\EnCours;
use App\Entity\Utilisateur;
use App\Form\UtilisateurType;
use App\SearchData\UtilisateurData;
use App\SearchForm\UtilisateurForm;
use App\Repository\UtilisateurRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/intervenant", name="gestion_intervenant")
 */
class IntervenantController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, UtilisateurRepository $utilisateurRepository, EnCours $enCours): Response
    {
        $data = new UtilisateurData();
        $form = $this->createForm(UtilisateurForm::class, $data);
        $form->handleRequest($request);

        $saisonCourante = $enCours->saison();

        $intervenants = $paginator->paginate(
            $utilisateurRepository->intervenants($data, $saisonCourante),
            $request->query->getInt('page', 1),
            $data->nb
        );

        return $this->render('gestion/intervenant/index.html.twig', [
            'intervenants' => $intervenants,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Utilisateur $utilisateur): Response
    {
        $formUtilisateur = $this->createForm(UtilisateurType::class, $utilisateur);
        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_intervenant_index');
        }

        return $this->render('gestion/intervenant/edit.html.twig', [
            'intervenant' => $utilisateur,
            'formUtilisateur' => $formUtilisateur->createView(),
        ]);
    }
}
