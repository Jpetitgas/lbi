<?php

namespace App\Entity;

use App\Repository\OperateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OperateurRepository::class)
 */
class Operateur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=RegleTarifaire::class, mappedBy="operateur",fetch="EAGER")
     */
    private $regleTarifaires;

    /**
     * @ORM\ManyToMany(targetEntity=NiveauTarifaire::class, inversedBy="operateurs")
     */
    private $niveauTarifaire;

    public function __construct()
    {
        $this->regleTarifaires = new ArrayCollection();
        $this->niveauTarifaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|RegleTarifaire[]
     */
    public function getRegleTarifaires(): Collection
    {
        return $this->regleTarifaires;
    }

    public function addRegleTarifaire(RegleTarifaire $regleTarifaire): self
    {
        if (!$this->regleTarifaires->contains($regleTarifaire)) {
            $this->regleTarifaires[] = $regleTarifaire;
            $regleTarifaire->setOperateur($this);
        }

        return $this;
    }

    public function removeRegleTarifaire(RegleTarifaire $regleTarifaire): self
    {
        if ($this->regleTarifaires->removeElement($regleTarifaire)) {
            // set the owning side to null (unless already changed)
            if ($regleTarifaire->getOperateur() === $this) {
                $regleTarifaire->setOperateur(null);
            }
        }

        return $this;
    }
    public function __toString(): string
    {
        return $this->getNom();
    }

    /**
     * @return Collection|NiveauTarifaire[]
     */
    public function getNiveauTarifaire(): Collection
    {
        return $this->niveauTarifaire;
    }

    public function addNiveauTarifaire(NiveauTarifaire $niveauTarifaire): self
    {
        if (!$this->niveauTarifaire->contains($niveauTarifaire)) {
            $this->niveauTarifaire[] = $niveauTarifaire;
        }

        return $this;
    }

    public function removeNiveauTarifaire(NiveauTarifaire $niveauTarifaire): self
    {
        $this->niveauTarifaire->removeElement($niveauTarifaire);

        return $this;
    }
}
