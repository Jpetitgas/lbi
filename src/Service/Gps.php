<?php

namespace App\Service;

class Gps
{
    public function __construct()
    {
    }

    public function coordonnees($address)
    {
        $params = http_build_query(['q' => $address, 'format' => 'json']);
        $url = 'http://nominatim.openstreetmap.org/search?';
        $httpOptions = [
    'http' => [
        'method' => 'GET',
        'header' => 'User-Agent: Nominatim-Test',
    ],
];
        $streamContext = stream_context_create($httpOptions);
        $appel_api = file_get_contents($url.$params, false, $streamContext);
        $resultat= json_decode($appel_api, true);
        $geolocal=[];
        if ($resultat) {
            $geolocal['latitude'] = $resultat[0]['lat'];
            $geolocal['longitude'] = $resultat[0]['lon'];
        }


        return $geolocal;
    }
}
