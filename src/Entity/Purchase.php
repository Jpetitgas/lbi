<?php

namespace App\Entity;

use App\Repository\PurchaseRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PurchaseRepository::class)
 */
class Purchase
{
    public const STATUS_PENDING='En attente';
    public const STATUS_PAID='Payée';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status=Purchase::STATUS_PENDING;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="purchases")
     */
    private $utilisateur;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $purchaseAt;

    /**
     * @ORM\OneToMany(targetEntity=PurchaseItem::class, mappedBy="purchase", orphanRemoval=true)
     */
    private $purchaseItems;

    /**
     * @ORM\OneToMany(targetEntity=PaiementBoutique::class, mappedBy="purchase")
     */
    private $paiementBoutiques;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class, inversedBy="purchases")
     */
    private $saison;

    private $livre;
    private $paye;

    public function getLivre()
    {
        return $this->livre;
    }

    public function setLivre($livre)
    {
        $this->livre = $livre;

        return $this;
    }
    public function getPaye()
    {
        return $this->paye;
    }

    public function setPaye($paye)
    {
        $this->paye = $paye;

        return $this;
    }

    public function __construct()
    {
        $this->purchaseItems = new ArrayCollection();
        $this->paiementBoutiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }



    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }
    public function getPurchaseAt(): ?DateTimeImmutable
    {
        return $this->purchaseAt;
    }

    public function setPurchaseAt(DateTimeImmutable $purchaseAt): self
    {
        $this->purchaseAt = $purchaseAt;

        return $this;
    }

    /**
     * @return Collection|PurchaseItem[]
     */
    public function getPurchaseItems(): Collection
    {
        return $this->purchaseItems;
    }

    public function addPurchaseItem(PurchaseItem $purchaseItem): self
    {
        if (!$this->purchaseItems->contains($purchaseItem)) {
            $this->purchaseItems[] = $purchaseItem;
            $purchaseItem->setPurchase($this);
        }

        return $this;
    }

    public function removePurchaseItem(PurchaseItem $purchaseItem): self
    {
        if ($this->purchaseItems->removeElement($purchaseItem)) {
            // set the owning side to null (unless already changed)
            if ($purchaseItem->getPurchase() === $this) {
                $purchaseItem->setPurchase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PaiementBoutique[]
     */
    public function getPaiementBoutiques(): Collection
    {
        return $this->paiementBoutiques;
    }

    public function addPaiementBoutique(PaiementBoutique $paiementBoutique): self
    {
        if (!$this->paiementBoutiques->contains($paiementBoutique)) {
            $this->paiementBoutiques[] = $paiementBoutique;
            $paiementBoutique->setPurchase($this);
        }

        return $this;
    }

    public function removePaiementBoutique(PaiementBoutique $paiementBoutique): self
    {
        if ($this->paiementBoutiques->removeElement($paiementBoutique)) {
            // set the owning side to null (unless already changed)
            if ($paiementBoutique->getPurchase() === $this) {
                $paiementBoutique->setPurchase(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getId();
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }
}
