<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => "les mots de passe saisis ne correspondent pas.",
                "required" => true,
                "first_options" => [
                    'label' => "Mot de passe",
                    'label_attr' => [
                        'title' => 'Pour des raisons de sécurité, votre mot de pass doit contenir au minimum 6 caratères dont 1 lettres majuscule, 1 chiffre et un caractère spécial'
                    ],
                    'attr' => [
                        'pattern'   => "^(?=.*[a-zà-ÿ])(?=.*[A-ZÀ-Ý])(?=.*[0-9])(?=.*[^a-zà-ÿA-ZÀ-Ý0-9]).{6,}$",
                        'title' => "Pour des raisons de sécurité, votre mot de passe doit contenir au minimum 6 caratères dont 1 lettres majuscule, 1 chiffre et un caractère spécial",
                        'maxlength' => 255,
                    ]
                ],
                "second_options" => [
                    'label' => "Confirmer le mot de passe",
                    'label_attr' => [
                        'title' => 'Confirmer le mot de passe'
                    ],
                    'attr' => [
                        'pattern'   => "^(?=.*[a-zà-ÿ])(?=.*[A-ZÀ-Ý])(?=.*[0-9])(?=.*[^a-zà-ÿA-ZÀ-Ý0-9]).{6,}$",
                        'title' => "Confirmer le mot de passe",
                        'maxlength' => 255,
                    ]
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
