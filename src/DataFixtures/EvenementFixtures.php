<?php

namespace App\DataFixtures;

use App\Entity\Container;
use App\Entity\Evenement;
use App\Entity\Option;
use App\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class EvenementFixtures extends Fixture implements DependentFixtureInterface
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        $origine = './public/assets/sauvegarde/evenement/';
        $destination = './public/assets/evenement/';
        $files = ['planBourse', 'planSpectacle'];
        foreach ($files as $file) {
            copy($origine.$file.'.jpg', $destination.$file.'.jpg');
        }
        /**PERMANENCE */
        $evenement = new Evenement();
        $evenement->setNom('Exemple d\'évenement type agenda')
            ->setActif(true)
            ->setDescription('Parfait pour organiser la reservation de permanence d\'inscription, de stage etc')
            ->setLocalisation('45.775411559930596, 3.082372539688902')
            ->setPayant(false)
            ->setPlan(false)
            ->setQuota(1);
        $manager->persist($evenement);
        $evenement->setSaison($this->getReference('saison'));
        for ($c = 1; $c < 5; ++$c) {
            $container = new Container();
            $container->setEvenement($evenement)
            ->setNom($c.' septembre')
            ->setNbLigne(1);
            $manager->persist($container);
            for ($i = 0; $i < 6; ++$i) {
                $place = new Place();
                $place->setContainer($container)
            ->setNom('10h'.$i * 10)
            ->setVerrouiller(false);
                $manager->persist($place);
            }
        }
        /**GALA */
        $evenement = new Evenement();
        $evenement->setNom('Exemple d\'évenement avec plan interactif')
            ->setActif(true)
            ->setDescription('<p>Avec le plan interatif, vous pouvez organiser la reservation de place pour un spectacle, un repas, une bourse aux v&eacute;tements.</p>

<p>Les containers contiennent des places.&nbsp;Vous pouvez les positionner en accord avec l&#39;agencement de la salle et les faire tourner en fonction.</p>

<p>Chaque container peut avoir des prix de place diff&eacute;rent des autres containers.</p>

<p>L&#39;ajout d&#39;option permet de pr&eacute;-commander un DVD, des cartons pour un loto etc</p>')
            ->setLocalisation('45.775411559930596, 3.082372539688902')
            ->setPayant(true)
            ->setPlan(true)
            ->setLargeur(800)
            ->setHauteur(1200)
            ->setQuota(4)
            ->setImage('planSpectacle.jpg');
        $manager->persist($evenement);

        $option = new Option();
        $option->setEvenement($evenement)
            ->setName('DVD')
            ->setPrice(10);
        $manager->persist($option);
        $alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        $evenement->setSaison($this->getReference('saison'));
        /**gauche */
        for ($c = 0; $c < 12; ++$c) {
            $container = new Container();
            $container->setEvenement($evenement)
            ->setNom('Gauche '.$alpha[$c])
            ->setX(50)
            ->setY(285 + ($c * 30))
            ->setAngle(-15)
            ->setLargeur(20)
            ->setHauteur(20)
            ->setNbLigne(1)
            ->setPricePlace(10);
            $manager->persist($container);
            for ($i = 0; $i < 9; ++$i) {
                $place = new Place();
                $place->setContainer($container)
                    ->setNom('A'.$i)
                    ->setVerrouiller(false);
                $manager->persist($place);
            }
        }
        /**centre */
        for ($c = 0; $c < 13; ++$c) {
            $container = new Container();
            $container->setEvenement($evenement)
            ->setNom('Centre '.$alpha[$c])
            ->setX(295)
            ->setY(240 +($c * 30))
            ->setAngle(0)
            ->setLargeur(20)
            ->setHauteur(20)
            ->setNbLigne(1)
            ->setPricePlace(15);
            $manager->persist($container);
            for ($i = 0; $i < 10; ++$i) {
                $place = new Place();
                $place->setContainer($container)
                    ->setNom('A'.$i)
                    ->setVerrouiller(false);
                $manager->persist($place);
            }
        }
        /**droite */
        for ($c = 0; $c < 12; ++$c) {
            $container = new Container();
            $container->setEvenement($evenement)
            ->setNom('Droite '.$alpha[$c])
            ->setX(570)
            ->setY(240 + ($c * 30))
            ->setAngle(15)
            ->setLargeur(20)
            ->setHauteur(20)
            ->setNbLigne(1)
            ->setPricePlace(10);
            $manager->persist($container);
            for ($i = 0; $i < 9; ++$i) {
                $place = new Place();
                $place->setContainer($container)
                    ->setNom('A'.$i)
                    ->setVerrouiller(false);
                $manager->persist($place);
            }
        }
        /**fosse */
        $container = new Container();
        $container->setEvenement($evenement)
            ->setNom('Fosse')
            ->setX(70)
            ->setY(760)
            ->setAngle(0)
            ->setLargeur(20)
            ->setHauteur(20)
            ->setNbLigne(10)
            ->setPricePlace(5);
        $manager->persist($container);
        for ($i = 0; $i < 330; ++$i) {
            $place = new Place();
            $place->setContainer($container)
                    ->setNom('A'.$i)
                    ->setVerrouiller(false);
            $manager->persist($place);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            Parametres::class,
        ];
    }
}
