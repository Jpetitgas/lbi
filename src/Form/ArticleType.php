<?php

namespace App\Form;

use App\Entity\Menu;
use App\Entity\Article;
use App\Entity\CategoryArticle;
use Symfony\Component\Form\AbstractType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Eckinox\TinymceBundle\Form\Type\TinymceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'label' => 'Titre',
            ])

            ->add('contenu', TinymceType::class, [
                'label' => 'Contenu de l\'article',
            ])
            ->add('imageFile', VichFileType::class, [
                'label' => 'Choisir une photo pour cet article',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,

            ])
            ->add('categorie', EntityType::class, [
                'label' => 'Choisir une catégorie',
                'class' => CategoryArticle::class,
                'placeholder' => 'selectionner une catégorie',
                'required' => false,
            ])
            ->add('menu', EntityType::class, [
                'label' => 'Choisir un menu',
                'class' => Menu::class,
                'placeholder' => 'selectionner un menu',
                'required' => false,
            ])
            ->add('enLigne', CheckboxType::class, [
                'label' => 'Mis en ligne',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
