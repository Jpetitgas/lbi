<?php

namespace App\Controller\Gestion\Evenement;

use App\Entity\Container;
use App\Entity\Evenement;
use App\Entity\Place;
use App\Form\ContainerType;
use App\Repository\ContainerRepository;
use App\Repository\EvenementRepository;
use App\Repository\PlaceRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("gestion/evenement/container", name="gestion_evenement_container")
 */
class PlaceController extends AbstractController
{
    /**
     * @Route("/verrouiller/{id}", name="_verrouiller", methods={"GET"})
     */
    public function verrouiller($id, PlaceRepository $placeRepository): Response
    {
        $place = $placeRepository->find($id);
        if ($place) {
            $place->setVerrouiller(($place->getVerrouiller() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($place);
            $em->flush();
            $this->addFlash('success', 'La place est verrouillé');
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $place->getContainer()->getEvenement()->getId(),'ancre'=>'container']);
    }

    /**
     * @Route("/utilisateur/{id_place}/{id_utilisateur}", name="_utilisateur", methods={"GET"})
     */
    public function utilisateur($id_place, $id_utilisateur, PlaceRepository $placeRepository, UtilisateurRepository $utilisateurRepository): Response
    {
        $utilisateur = $utilisateurRepository->find($id_utilisateur);
        $place = $placeRepository->find($id_place);
        if ($place && $utilisateur) {
            $place->setUtilisateur($utilisateur);
            $em = $this->getDoctrine()->getManager();
            $em->persist($place);
            $em->flush();
            $this->addFlash('success', 'La place a été attribuée');
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $place->getContainer()->getEvenement()->getId(),'ancre'=>'container']);
    }

    /**
     * @Route("/nom/{id_place}/{nom}", name="_nom", methods={"GET"})
     */
    public function nom($id_place, $nom, PlaceRepository $placeRepository): Response
    {
        $place = $placeRepository->find($id_place);
        if ($place) {
            $place->setNom($nom);
            $em = $this->getDoctrine()->getManager();
            $em->persist($place);
            $em->flush();
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $place->getContainer()->getEvenement()->getId(),'ancre'=>'container']);
    }


    /**
     * @Route("/annuler/{id_place}", name="_annuler", methods={"GET", "POST"})
     */
    public function annuler($id_place, PlaceRepository $placeRepository, UtilisateurRepository $utilisateurRepository): Response
    {
        $place = $placeRepository->find($id_place);
        if ($place) {
            $place->setUtilisateur(null);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('success', 'Votre reservation est annulée!');
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $place->getContainer()->getEvenement()->getId(),'ancre'=>'container']);
    }


    /**
     * @Route("/ajout/place/{id_container}", name="_ajout_place", methods={"GET", "POST"})
     */
    public function ajoutPlace($id_container, ContainerRepository $containerRepository)
    {
        $container = $containerRepository->find($id_container);
        $place = new Place();
        $place->setNom('vide')
            ->setVerrouiller(false);
        $place->setContainer($container);
        $em = $this->getDoctrine()->getManager();
        $em->persist($place);

        $em->flush();

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $container->getEvenement()->getId(),'ancre'=>'container']);
    }

    /**
     * @Route("place/{id_place}", name="_place_delete", methods={"POST"})
     */
    public function deletePlace(Request $request, $id_place, PlaceRepository $placeRepository): Response
    {
        $place = $placeRepository->find($id_place);

        if ($this->isCsrfTokenValid('delete'.$place->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($place);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $place->getContainer()->getEvenement()->getId(),'ancre'=>'container']);
    }
}
