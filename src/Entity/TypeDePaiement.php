<?php

namespace App\Entity;

use App\Repository\TypeDePaiementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeDePaiementRepository::class)
 */
class TypeDePaiement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;

    /**
     * @ORM\OneToMany(targetEntity=Paiement::class, mappedBy="typeDePaiement")
     */
    private $paiements;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $verrouille;

    public function __construct()
    {
        $this->paiements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|Paiement[]
     */
    public function getPaiements(): Collection
    {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): self
    {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements[] = $paiement;
            $paiement->setTypeDePaiement($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): self
    {
        if ($this->paiements->removeElement($paiement)) {
            // set the owning side to null (unless already changed)
            if ($paiement->getTypeDePaiement() === $this) {
                $paiement->setTypeDePaiement(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getDesignation();
    }

    public function getVerrouille(): ?bool
    {
        return $this->verrouille;
    }

    public function setVerrouille(?bool $verrouille): self
    {
        $this->verrouille = $verrouille;

        return $this;
    }
}
