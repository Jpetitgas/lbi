<?php

namespace App\Form;

use App\Entity\CP;
use App\Entity\Ville;
use App\Entity\Saison;
use App\Entity\Utilisateur;
use Doctrine\ORM\EntityRepository;
use App\Repository\VilleRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class UtilisateurType extends AbstractType
{
    public $villeRepository;

    public function __construct(Security $security, VilleRepository $villeRepository)
    {
        $this->security = $security;
        $this->villeRepository = $villeRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('email', EmailType::class)
            ->add('adresse', TextType::class)
            ->add('CP', EntityType::class, [
                'label' => 'Code Postal',
                'class' => CP::class,
                'placeholder' => 'selectionner un code postal',
                'choice_label' => 'code',
            ])

            ->add('town', EntityType::class, [
                'class' => Ville::class,
                'label' => 'Ville',
                'placeholder' => 'selectionner une ville',
                'choice_label' => 'nomMaj',
            ])

            ->add('portable', TelType::class, [
                'constraints' => [new Regex('#^0[0-9]{9}$#')],
            ])

            ->add('remarque', TextType::class, [
                'required' => false,
            ]);
        if ($this->security->isGranted('ROLE_GESTION')) {
            $builder->add('quotaSms', IntegerType::class, [
                'required' => false
            ]);
            $builder->add('saison', EntityType::class, [
                'class' => Saison::class,
                'required' => false,
                'placeholder' => 'Selectionner une saison',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.saison', 'DESC');
                },
            ]);
            $builder->add('roles', ChoiceType::class, [
                'choices' => [
                    'Utilisateur' => 'ROLE_USER',
                    'Gestionnaire' => 'ROLE_GESTION',
                    'Intervenant' => 'ROLE_INTERVENANT',
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles',
            ]);
        }
        $ajoutChampVille = function (FormInterface $form, CP $cp = null) {
            $villes = $this->villeRepository->findBy(['cp' => $cp], ['nomMaj' => 'ASC']);

            if ($villes) {
                $form->add('town', EntityType::class, [
                    'class' => Ville::class,
                    'label' => 'Ville',
                    'placeholder' => 'selectionner une ville',
                    'choice_label' => 'nomMaj',
                    'choices' => $villes,
                ]);
            } else {
                $form->add('town', ChoiceType::class, [
                    'label' => 'Ville',
                    'placeholder' => 'Choisir un code postal',
                ]);
            }
        };
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($ajoutChampVille) {
                $utilisateur = $event->getData();
                $form = $event->getForm();
                if (!($utilisateur->getId() === null)) {
                    $cp = $utilisateur->getCP();
                    $builder = $form->getConfig()->getFormFactory()->createNamedBuilder('CP', EntityType::class, null, [
                        'label' => 'Code Postal',
                        'class' => CP::class,
                        'placeholder' => 'selectionner un code postal',
                        'choice_label' => 'code',
                        'data' => $cp,
                        'auto_initialize' => false,
                    ]);
                    $builder->addEventListener(
                        FormEvents::POST_SUBMIT,
                        function (FormEvent $event) use ($ajoutChampVille) {
                            $cp = $event->getForm()->getData();
                            $form = $event->getForm();
                            $ajoutChampVille($form->getParent(), $cp);
                        }
                    );
                    $form->add($builder->getForm());

                    $ajoutChampVille($form, $cp);
                }
            }
        );
        /*$builder->addEventListener(
            FormEvents::POST_SET_DATA,
             function (FormEvent $event) use ($ajoutChampVille) {
                 $utilisateur = $event->getData();
                 $form = $event->getForm();
             });*/
        $builder->get('CP')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($ajoutChampVille) {
                $cp = $event->getForm()->getData();
                $form = $event->getForm();
                $ajoutChampVille($form->getParent(), $cp);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
