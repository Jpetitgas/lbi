$(document).ready(function () {	 
var $table = $('#inscription');

    function getRowSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row;
        })
    }

    $('#message').click(function () {
        var selectedRows = getRowSelections();
        var selectedItems = '\n';
        let ids = [];
        $.each(selectedRows, function (index, value) {
            ids.push(value.id);

        });


        if (ids.length) {
            window.location.href = "/gestion/correspondance/message/pratiquant/" + ids.toString();
        } else {
            alert('Vous devez d\'abord selectionner une ligne');
        }
    });
    });