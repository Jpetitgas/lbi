<?php

namespace App\Entity;

use App\Repository\PratiquantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use function Symfony\Component\String\u;

/**
 * @ORM\Entity(repositoryClass=PratiquantRepository::class)
 */
class Pratiquant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $naissance;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="pratiquants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\OneToMany(targetEntity=Inscription::class, mappedBy="pratiquant",fetch="EAGER")
     */
    private $inscriptions;

    /**
     * @ORM\OneToMany(targetEntity=ContactPratiquant::class, mappedBy="pratiquant",fetch="EAGER")
     */
    private $contactPratiquants;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="pratiquant")
     */
    private $documents;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $sexe;

    /**
     * @ORM\OneToMany(targetEntity=Presence::class, mappedBy="pratiquant")
     */
    private $presences;

    /**
     * @ORM\ManyToOne(targetEntity=Mensuration::class, inversedBy="pratiquants")
     */
    private $tailleHaut;

    /**
     * @ORM\ManyToOne(targetEntity=Mensuration::class, inversedBy="pratiquants")
     */
    private $tailleBas;

    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
        $this->contactPratiquants = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->presences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNaissance(): ?\DateTimeInterface
    {
        return $this->naissance;
    }

    public function setNaissance(\DateTimeInterface $naissance): self
    {
        $this->naissance = $naissance;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getFullName(): string
    {
        return u(sprintf("%s %s", $this->nom, $this->prenom))->upper()->toString();
    }

    public function __toString(): string
    {
        return $this->getFullName();
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->setPratiquant($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->removeElement($inscription)) {
            // set the owning side to null (unless already changed)
            if ($inscription->getPratiquant() === $this) {
                $inscription->setPratiquant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ContactPratiquant[]
     */
    public function getContactPratiquants(): Collection
    {
        return $this->contactPratiquants;
    }

    public function addContactPratiquant(ContactPratiquant $contactPratiquant): self
    {
        if (!$this->contactPratiquants->contains($contactPratiquant)) {
            $this->contactPratiquants[] = $contactPratiquant;
            $contactPratiquant->setPratiquant($this);
        }

        return $this;
    }

    public function removeContactPratiquant(ContactPratiquant $contactPratiquant): self
    {
        if ($this->contactPratiquants->removeElement($contactPratiquant)) {
            // set the owning side to null (unless already changed)
            if ($contactPratiquant->getPratiquant() === $this) {
                $contactPratiquant->setPratiquant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setPratiquant($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getPratiquant() === $this) {
                $document->setPratiquant(null);
            }
        }

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * @return Collection|Presence[]
     */
    public function getPresences(): Collection
    {
        return $this->presences;
    }

    public function addPresence(Presence $presence): self
    {
        if (!$this->presences->contains($presence)) {
            $this->presences[] = $presence;
            $presence->setPratiquant($this);
        }

        return $this;
    }

    public function removePresence(Presence $presence): self
    {
        if ($this->presences->removeElement($presence)) {
            // set the owning side to null (unless already changed)
            if ($presence->getPratiquant() === $this) {
                $presence->setPratiquant(null);
            }
        }

        return $this;
    }

    public function getTailleHaut(): ?Mensuration
    {
        return $this->tailleHaut;
    }

    public function setTailleHaut(?Mensuration $tailleHaut): self
    {
        $this->tailleHaut = $tailleHaut;

        return $this;
    }

    public function getTailleBas(): ?Mensuration
    {
        return $this->tailleBas;
    }

    public function setTailleBas(?Mensuration $tailleBas): self
    {
        $this->tailleBas = $tailleBas;

        return $this;
    }
}
