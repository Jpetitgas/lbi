<?php

namespace App\EventListener;

use App\Entity\PaiementBoutique;
use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class PaiementBoutiqueListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison=$encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $paiement=$event->getObject();

        if ($paiement instanceof PaiementBoutique) {
            if ($this->saison) {
                $paiement->setSaison($this->saison);
            }
        }
    }
}
