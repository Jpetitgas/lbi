<?php

namespace App\Controller\Gestion;

use App\Entity\RegleTarifaire;
use App\Form\RegleTarifaireType;
use App\Repository\RegleTarifaireRepository;
use App\Repository\SaisonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/regle", name="gestion_regle")
 */
class RegleTarifaireController extends AbstractController
{
    /**
     * @Route("/{id_saison}/index", name="_index", methods={"GET"})
     */
    public function index($id_saison, SaisonRepository $saisonRepository, RegleTarifaireRepository $regleTarifaireRepository): Response
    {
        $saison = $saisonRepository->find($id_saison);
        $regleTarifaire = $regleTarifaireRepository->findBy(['saison' => $id_saison]);

        return $this->render('gestion/regle/index.html.twig', [
            'saison' => $saison,
            'regleTarifaires' => $regleTarifaire,
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $regleTarifaire = new RegleTarifaire();
        $formRegle = $this->createForm(RegleTarifaireType::class, $regleTarifaire);
        $formRegle->handleRequest($request);

        if ($formRegle->isSubmitted() && $formRegle->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($regleTarifaire);
            $entityManager->flush();
            $saison = $regleTarifaire->getSaison()->getId();

            return $this->redirectToRoute('gestion_regle_index', ['id_saison' => $saison]);
        }

        return $this->render('gestion/regle/new.html.twig', [
            'regle_tarifaire' => $regleTarifaire,
            'formRegle' => $formRegle->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RegleTarifaire $regleTarifaire): Response
    {
        $formRegle = $this->createForm(RegleTarifaireType::class, $regleTarifaire);
        $formRegle->handleRequest($request);

        if ($formRegle->isSubmitted() && $formRegle->isValid()) {
            if (strpos($formRegle->getData()->getOperateur()->getNom(), 'COMMUNE') || strpos($formRegle->getData()->getOperateur()->getNom(), 'UNIQUE')) {
                $formRegle->getData()->setParametre(null);
            } else {
                $villes=$regleTarifaire->getVilles();
                foreach ($villes as $ville) {
                    $formRegle->getData()->removeVille($ville);
                }
            }

            $this->getDoctrine()->getManager()->flush();
            $saison = $regleTarifaire->getSaison()->getId();

            return $this->redirectToRoute('gestion_regle_index', ['id_saison' => $saison]);
        }

        return $this->render('gestion/regle/edit.html.twig', [
            'regle_tarifaire' => $regleTarifaire,
            'formRegle' => $formRegle->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, RegleTarifaire $regleTarifaire): Response
    {
        $saison = $regleTarifaire->getSaison()->getId();
        if ($this->isCsrfTokenValid('delete'.$regleTarifaire->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($regleTarifaire);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_regle_index', ['id_saison' => $saison]);
    }
}
