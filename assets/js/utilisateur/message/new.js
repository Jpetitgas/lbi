
let sujet = document.querySelector('#messages_sujet')
sujet.addEventListener("change", function () {
    let form = this.closest("form");
    let data = this.name + "=" + this.value

    fetch(form.action, {
        method: form.getAttribute("method"),
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset:utf-8"
        }
    })
        .then(response => response.text())
        .then(html => {
            let content = document.createElement("html");
            content.innerHTML = html;
            let nouveauSelect = content.querySelector("#messages_intervenant")
            let select = document.querySelector("#messages_intervenant");
            if (nouveauSelect === null && select !== null) {
                select.parentNode.parentNode.removeChild(select.parentNode);

            } else {
                let enfant = document.querySelector("#messages_message").parentNode;
                enfant.parentNode.insertBefore(nouveauSelect.parentNode, enfant);
            }


        })
})
