<?php

namespace App\Controller\Stripe;

use App\Entity\Messages;
use App\Service\SendEmail;
use App\Stripe\StripeService;
use App\Entity\PaiementBoutique;
use App\Message\SendEmailMessage;
use App\Repository\SujetRepository;
use App\Repository\BanqueRepository;
use App\Repository\PurchaseRepository;
use App\Repository\InformationRepository;
use App\Repository\TypeDePaiementRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StripeBoutiqueController extends AbstractController
{
    /**
     *  @Route("/boutique/create-checkout-session", name="boutique_checkout", methods={"POST"})
     */
    public function checkout(Request $request, InformationRepository $informationRepository, PurchaseRepository $purchaseRepository, StripeService $stripeService)
    {
        $params = json_decode($request->getContent(), true);
        extract($params);
        $information = $informationRepository->findOneBy([]);
        if ($information->getBoutiqueStripe() == false) {
            $this->addFlash('danger', 'Le paiement par Stripe n\'est pas autorisé!');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        \Stripe\Stripe::setApiKey($stripeService->ObtenirSecretCle());
        $purchase = $purchaseRepository->find($id_purchase);

        $totalPaiement = 0;
        foreach ($purchase->getPaiementBoutiques() as $paiement) {
            $totalPaiement += $paiement->getMontant();
        }
        $montant = $purchase->getTotal() - $totalPaiement;
        $checkout_session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $montant * 100,
                    'product_data' => [
                        'name' => 'Achat boutique',
                    ],
                ],
                'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $this->generateUrl('boutique_stripe_succes', ['id_purchase' => $id_purchase], UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('boutique_stripe_erreur', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        return  new JsonResponse(['id' => $checkout_session->id]);
    }

    /**
     *  @Route("/boutique/stripe/succes/{id_purchase}", name="boutique_stripe_succes")
     */
    public function succes($id_purchase, PurchaseRepository $purchaseRepository, SendEmail $sendEmail, SujetRepository $sujetRepository, MessageBusInterface $messageBus, TypeDePaiementRepository $typeDePaiementRepository, BanqueRepository $banqueRepository)
    {
        $purchase = $purchaseRepository->find($id_purchase);
        $totalPaiement = 0;
        foreach ($purchase->getPaiementBoutiques() as $paiement) {
            $totalPaiement += $paiement->getMontant();
        }
        $typeDePaiement = $typeDePaiementRepository->findOneBy(['designation' => 'Carte bancaire']);
        $banque = $banqueRepository->findOneBy(['nom' => 'Stripe']);
        $paiement = new PaiementBoutique();
        $paiement->setPurchase($purchase)
            ->setBanque($banque)
            ->setMontant($purchase->getTotal() - $totalPaiement)
            ->setTypeDePaiement($typeDePaiement)
            ->setEncaisse(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($paiement);
        $entityManager->flush();
        /** @var Sujet $sujet */
        $sujet = $sujetRepository->findOneBy(['sujet' => 'Paiement en ligne']);
        if ($sujet) {
            $envoi = new Messages();
            $envoi->setExpediteur($sujet->getDestinataire())
                ->setDestinataire($purchase->getUtilisateur())
                ->setTitre($sujet->getSujet())
                ->setMessage('Votre paiement s\'est correctement déroulé. Vous trouverez votre facture dans votre espace, boutique, vos commandes');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($envoi);
            $entityManager->flush();
        }

        $sendEmail->send([
            'recipient_email' => $purchase->getUtilisateur()->getEmail(),
            'sujet' => 'Paiement de votre commande en ligne',
            'htmlTemplate' => 'email/paiementBoutique.html.twig',
            'destinataire' => $purchase->getUtilisateur(),
            'contenu' => [
                'utilisateur' => $purchase->getUtilisateur(),
                'montant' => $purchase->getTotal(),
            ],
        ]);

        $this->addFlash('success', 'La commande a été payé!');

        return $this->redirectToRoute('utilisateur_boutique_purchase_index');
    }

    /**
     *  @Route("/boutique/stripe/erreur", name="boutique_stripe_erreur")
     */
    public function erreur()
    {
        return $this->render('stripe/erreur.html.twig');
    }
}
