var id_purchase = document.getElementById('purchase').innerText;
const stripe = Stripe(strikePublicCle);
var checkoutButton = document.getElementById('checkout-button');
if (checkoutButton != undefined) {
    checkoutButton.addEventListener('click', function () {
        // Create a new Checkout Session using the server-side endpoint you
        // created in step 3.
        fetch('/boutique/create-checkout-session', {
            method: 'POST',
            body: JSON.stringify({
                id_purchase: id_purchase
            }),
            headers: {
                "Content-type": "application/json"
            }
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (session) {
                return stripe.redirectToCheckout({ sessionId: session.id });
            })
            .then(function (result) {
                // If `redirectToCheckout` fails due to a browser or network
                // error, you should display the localized error message to your
                // customer using `error.message`.
                if (result.error) {
                    alert(result.error.message);
                }
            })
            .catch(function (error) {
                console.error('Error:', error);
            });
    });
}

