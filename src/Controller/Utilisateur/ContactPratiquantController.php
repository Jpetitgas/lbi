<?php

namespace App\Controller\Utilisateur;

use App\Entity\ContactPratiquant;
use App\Form\ContactPratiquantType;
use App\Repository\InformationRepository;
use App\Repository\PratiquantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur/contact", name="utilisateur_contact")
 */
class ContactPratiquantController extends AbstractController
{
    /**
     * @Route("/new/{id_pratiquant}/{id_dossier}", name="_new", methods={"GET","POST"})
     */
    public function new($id_pratiquant, $id_dossier, Request $request, PratiquantRepository $pratiquantRepository, InformationRepository $informationRepository): Response
    {
        $contactPratiquant = new ContactPratiquant();
        $formContact = $this->createForm(ContactPratiquantType::class, $contactPratiquant);
        $formContact->handleRequest($request);

        if ($formContact->isSubmitted() && $formContact->isValid()) {
            $pratiquant = $pratiquantRepository->find($id_pratiquant);
            $contactPratiquant->setPratiquant($pratiquant);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contactPratiquant);
            $entityManager->flush();

            $ancre = 'pratiquant_' . $pratiquant->getId() . '_contact';

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
        }

        return $this->render('utilisateur/contact_pratiquant/new.html.twig', [
            'contact_pratiquant' => $contactPratiquant,
            'formContact' => $formContact->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/{id_dossier}", name="_edit", methods={"GET","POST"})
     */
    public function edit($id_dossier, Request $request, ContactPratiquant $contactPratiquant, InformationRepository $informationRepository): Response
    {
        $formContact = $this->createForm(ContactPratiquantType::class, $contactPratiquant);
        $formContact->handleRequest($request);

        if ($formContact->isSubmitted() && $formContact->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $ancre = 'pratiquant_' . $contactPratiquant->getPratiquant()->getId() . '_contact';

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
        }

        return $this->render('utilisateur/contact_pratiquant/edit.html.twig', [
            'contact_pratiquant' => $contactPratiquant,
            'formContact' => $formContact->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{id_dossier}/", name="_delete", methods={"POST"})
     */
    public function delete($id_dossier, Request $request, ContactPratiquant $contactPratiquant, InformationRepository $informationRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $contactPratiquant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contactPratiquant);
            $entityManager->flush();
        }
        $ancre = 'pratiquant_' . $contactPratiquant->getPratiquant()->getId() . '_contact';

        return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
    }
}
