<?php

namespace App\SearchForm;

use App\Entity\Activite;
use App\Entity\CategorieSection;
use App\Entity\Cours;
use App\Entity\Section;
use App\Entity\Utilisateur;
use App\SearchData\InscriptionData;
use App\Service\EnCours;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionForm extends AbstractType
{
    public $enCours;

    public function __construct(EnCours $enCours)
    {
        $this->enCours = $enCours;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $saison = $this->enCours->saison();
        $builder

            ->add('q', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Rechercher',
                ],
            ])

            ->add('section', EntityType::class, [
                'required' => false,
                'class' => CategorieSection::class,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.nom', 'ASC');
                },
            ])

            ->add('refAssureur', EntityType::class, [
                'required' => false,
                'class' => Section::class,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.libelle', 'ASC');
                },
            ])
            ->add('activite', EntityType::class, [
                'required' => false,
                'class' => Activite::class,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) use ($saison) {
                    return $er->createQueryBuilder('u')
                        ->where('u.saison = :saison')
                        ->setParameter('saison', $saison)
                        ->orderBy('u.nom', 'ASC');
                },
            ])
            ->add('cours', EntityType::class, [
                'required' => false,
                'class' => Cours::class,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) use ($saison) {
                    return $er->createQueryBuilder('u')
                        ->where('u.saison = :saison')
                        ->setParameter('saison', $saison)
                        ->orderBy('u.nom', 'ASC');
                },
            ])
            ->add('intervenant', EntityType::class, [
                'required' => false,
                'class' => Utilisateur::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.roles LIKE :role')
                        ->orderBy('u.nom', 'ASC')
                        ->setParameter('role', '%' . 'ROLE_INTERVENANT' . '%');
                },
                'multiple' => false,
            ])
            ->add('certificatObligatoire', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('Nbpresence', IntegerType::class, [
                'required' => false,
            ])
            ->add('certificatPresent', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('certificatValide', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('transmisAssureur', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('dossierComplet', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('canceled', ChoiceType::class, [
                'label' => 'Annulé',
                'required' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('statut', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'LISTE PRINCIPALE' => 'LISTE PRINCIPALE',
                    'LISTE D\'ATTENTE' => 'LISTE D\'ATTENTE',
                    'EN ATTENTE' => 'EN ATTENTE',
                ],
            ])

            ->add('nb', IntegerType::class, [
                'required' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => InscriptionData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
