<?php

namespace App\Form;

use App\Entity\Section;
use App\Entity\CategorieSection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class, [
                'label'=>'libellé',
                'required'=>true
            ])
            ->add('categorieSection', EntityType::class, [
                'label'=>'Section',
                'class' => CategorieSection::class,
                'placeholder' => 'selectionner une section',
                'required' => true,
            ])

            ->add('refAssureur', TextType::class, [
                'required'=>true
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Section::class,
        ]);
    }
}
