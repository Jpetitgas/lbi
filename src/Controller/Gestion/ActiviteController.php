<?php

namespace App\Controller\Gestion;

use App\Entity\Activite;
use App\Form\ActiviteType;
use App\Repository\TokenRepository;
use App\Repository\SaisonRepository;
use App\Repository\ActiviteRepository;
use App\SearchData\ActiviteData;
use App\SearchForm\ActiviteForm;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/gestion/activite", name="gestion_activite")
 */
class ActiviteController extends AbstractController
{
    private $activiteRepository;
    private $entityManager;
    private $tokenRepository;

    public function __construct(ActiviteRepository $activiteRepository, EntityManagerInterface $entityManager, TokenRepository $tokenRepository)
    {
        $this->activiteRepository = $activiteRepository;
        $this->entityManager = $entityManager;
        $this->tokenRepository = $tokenRepository;
    }
    /**
     * @Route("/essai", name="_essai", methods={"GET"})
     */
    public function copyFile()
    {
        $origine = './assets/sauvegarde/site/';
        $destination = './assets/site/';
        $files = ['cirque', 'danse', 'escalade', 'foot', 'instruments', 'multi', 'theatre', 'yoga', 'yogabis'];
        foreach ($files as $file) {
            copy($origine . $file . '.jpg', $destination . $file . '.jpg');
        }

        return new Response('ok');
    }

    /**
     * @Route("/{id_saison}/index", name="_index", methods={"GET"})
     */
    public function index($id_saison, PaginatorInterface $paginator, Request $request, SaisonRepository $saisonRepository, ActiviteRepository $activiteRepository): Response
    {
        $data = new ActiviteData();
        $form = $this->createForm(ActiviteForm::class, $data);

        $form->handleRequest($request);
        $saison = $saisonRepository->find($id_saison);
        $essai = $activiteRepository->findByActiviteCriteria($saison, $data);
        $results = $essai->getResult();


        $activites = $paginator->paginate(
            $results,
            $request->query->getInt('page', 1),
            $data->nb
        );

        if (empty($activites)) {
            $this->addFlash('danger', 'Il n\'y a pas encore d\'activités crées');

            return $this->redirectToRoute('gestion');
        }
        return $this->render('gestion/activite/index.html.twig', [
            'saison' => $saison,
            'activites' => $activites,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $activite = new Activite();
        $formActivite = $this->createForm(ActiviteType::class, $activite);
        $formActivite->handleRequest($request);

        if ($formActivite->isSubmitted() && $formActivite->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($activite);
            $entityManager->flush();
            $saison = $activite->getSaison();

            return $this->redirectToRoute('gestion_activite_index', ['id_saison' => $saison->getid()]);
        }

        return $this->render('gestion/activite/new.html.twig', [
            'activite' => $activite,
            'formActivite' => $formActivite->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/{id_saison}", name="_edit", methods={"GET","POST"})
     */
    public function edit($id, $id_saison, Request $request, Activite $activite): Response
    {
        $formActivite = $this->createForm(ActiviteType::class, $activite);
        $formActivite->handleRequest($request);

        if ($formActivite->isSubmitted() && $formActivite->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_activite_index', ['id_saison' => $id_saison]);
        }

        return $this->render('gestion/activite/edit.html.twig', [
            'activite' => $activite,
            'formActivite' => $formActivite->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Activite $activite): Response
    {
        if ($this->isCsrfTokenValid('delete' . $activite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($activite);
            $entityManager->flush();
        }
        $id_saison = $activite->getSaison()->getId();

        return $this->redirectToRoute('gestion_activite_index', ['id_saison' => $id_saison]);
    }

    /**
     * @Route("/certificat/{id}", name="_certificat", methods={"GET"})
     */
    public function certificat($id, ActiviteRepository $activiteRepository): Response
    {
        $activite = $activiteRepository->find($id);
        $activite->setCertificatObligatoire(($activite->getCertificatObligatoire() ? '0' : '1'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($activite);
        $em->flush();
        $saison = $activite->getSaison();

        return $this->redirectToRoute('gestion_activite_index', ['id_saison' => $saison->getid()]);
    }

    /**
     * @Route("/condition/{id}", name="_condition", methods={"GET"})
     */
    public function condition($id, ActiviteRepository $activiteRepository): Response
    {
        $activite = $activiteRepository->find($id);
        $activite->setSousCondition(($activite->getSousCondition() ? '0' : '1'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($activite);
        $em->flush();
        $saison = $activite->getSaison();

        return $this->redirectToRoute('gestion_activite_index', ['id_saison' => $saison->getid()]);
    }
    /**
     * @Route("/update-sous-condition/{id}", name="app_activite_update_sous_condition", methods={"GET"})
     * @IsGranted("ROLE_GESTION")
     */
    public function updateSousCondition(Request $request, $id): Response
    {
        $token = $request->query->get('token');

        // Vérifier le token
        if (!$this->isValidToken($token, $id)) {
            throw $this->createAccessDeniedException('Token invalide ou expiré.');
        }

        // Récupérer l'activité et mettre à jour le champ sousCondition
        $activite = $this->activiteRepository->find($id);
        if ($activite) {
            // Inverser la valeur de sousCondition
            $activite->setSousCondition(!$activite->getSousCondition());
            $this->entityManager->flush();

            // Message de succès
            $this->addFlash('success', 'L\'activité a été mise à jour avec succès.');
        } else {
            throw $this->createNotFoundException('Activité non trouvée.');
        }

        // Rediriger vers gestion_activite_index avec l'id de la saison
        $saisonId = $activite->getSaison()->getId();
        return $this->redirectToRoute('gestion_activite_index', ['id_saison' => $saisonId]);
    }

    private function isValidToken($token, $id)
    {
        // Implémentez la vérification du token ici
        // Vous pouvez utiliser une base de données ou un autre mécanisme de stockage sécurisé
        // Exemple simplifié, à remplacer par votre logique de vérification
        return true;
    }

    private function storeToken($token, $activiteId)
    {
        // Stockez le token dans votre base de données avec une expiration
        // Exemple simplifié
        $expiration = new \DateTime('+1 hour');
        // Code pour stocker le token, l'activiteId et l'expiration dans la base de données
    }
}
