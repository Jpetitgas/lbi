<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Activite;
use App\Service\EnCours;
use App\Entity\Utilisateur;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CoursType extends AbstractType
{
    protected $enCours;

    public function __construct(EnCours $enCours)
    {
        $this->enCours = $enCours;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $id_saison=$this->enCours->saison()->getId();
        $builder
            ->add('nom')
            ->add('intervenant', EntityType::class, [
                'class' => Utilisateur::class,
                'placeholder' => 'Selectionner un intervenant',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                                    ->where('u.roles LIKE :role')
                                    ->orderBy('u.nom', 'ASC')
                                    ->setParameter('role', '%'.'ROLE_INTERVENANT'.'%');
                }, ])
            ->add('Activite', EntityType::class, [
                'class' => Activite::class,
                'placeholder' => 'Selectionner une activité',
                'query_builder' => function (EntityRepository $er) use ($id_saison) {
                    return $er->createQueryBuilder('a')
                                    ->where('a.saison = :saison')
                                    ->orderBy('a.nom', 'ASC')
                                    ->setParameter('saison', $id_saison);
                }, ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cours::class,
        ]);
    }
}
