<?php

namespace App\Service;

use App\Entity\Purchase;
use App\Repository\PurchaseRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PurchaseService
{
    protected $purchaseRepository;

    public function __construct(SessionInterface $session, PurchaseRepository $purchaseRepository)
    {
        $this->session = $session;
        $this->purchaseRepository = $purchaseRepository;
    }

    public function totalPurchase(Purchase $purchase)
    {
        $total=0;
        foreach ($purchase->getPurchaseItems() as $item) {
            $total+=$item->getTotal();
        }
        return $total;
    }
    public function purchaseDelevred(Purchase $purchase)
    {
        $delevred = true;
        /** @var PurchaseItem $item */
        foreach ($purchase->getPurchaseItems() as $item) {
            if ($item->getLivraison() === false || $item->getLivraison()==null) {
                $delevred = false;
            }
        }
        return $delevred;
    }
    public function solde(Purchase $purchase)
    {
        $paiements=$purchase->getPaiementBoutiques();
        $totalPaiement=0;
        foreach ($paiements as $paiement) {
            $totalPaiement+=$paiement->getMontant();
        }
        $commandes=$purchase->getPurchaseItems();
        $totalCommande=0;
        foreach ($commandes as $commande) {
            $totalCommande+=$commande->getQuantity()*$commande->getProductPrice();
        }

        $solde=$totalCommande-$totalPaiement;
        if (!$solde==0) {
            return false;
        }
        return true;
    }
}
