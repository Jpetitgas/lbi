<?php

namespace App\Entity;

use App\Repository\OptionPurchaseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OptionPurchaseRepository::class)
 */
class OptionPurchase
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Option::class, inversedBy="optionPurchases")
     */
    private $optionEvenement;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="optionPurchases")
     */
    private $utilisateur;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Delivery;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOptionEvenement(): ?Option
    {
        return $this->optionEvenement;
    }

    public function setOptionEvenement(?Option $optionEvenement): self
    {
        $this->optionEvenement = $optionEvenement;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getDelivery(): ?bool
    {
        return $this->Delivery;
    }

    public function setDelivery(bool $Delivery): self
    {
        $this->Delivery = $Delivery;

        return $this;
    }
}
