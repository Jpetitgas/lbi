<?php

namespace App\Event;

use App\Entity\Inscription;
use Symfony\Contracts\EventDispatcher\Event;

class InscriptionEvent extends Event
{
    private $inscription;

    public function __construct(Inscription $inscription)
    {
        $this->inscription= $inscription;
    }

    public function getInscription(): Inscription
    {
        return $this->inscription;
    }
}
