<?php

namespace App\Event;

use App\Entity\Chat;
use Symfony\Contracts\EventDispatcher\Event;

class NouveauMessageChatEvent extends Event
{
    private $chat;

    public function __construct(Chat $chat)
    {
        $this->chat= $chat;
    }

    public function getChat(): Chat
    {
        return $this->chat;
    }
}
