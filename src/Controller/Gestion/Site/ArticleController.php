<?php

namespace App\Controller\Gestion\Site;

use App\Entity\Article;
use App\Form\ArticleType;
use App\SearchData\ArticleData;
use App\SearchForm\ArticleForm;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("gestion/site/article", name="gestion_site_article")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/index", name="_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, ArticleRepository $articleRepository): Response
    {
        $data = new ArticleData();
        $form = $this->createForm(ArticleForm::class, $data);
        $form->handleRequest($request);

        $articles = $paginator->paginate(
            $articleRepository->inscriptions($data),
            $request->query->getInt('page', 1),
            $data->nb
        );
        return $this->render('gestion/site/article/index.html.twig', [
            'articles' => $articles,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_site_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gestion/site/article/new.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }


    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Article $article, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('gestion_site_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gestion/site/article/edit.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Article $article, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_site_article_index', [], Response::HTTP_SEE_OTHER);
    }
    /**
     * @Route("/enLigneGroupe/{ids}", name="_enLigneGroupe", methods={"GET"})
     */
    public function enLigneGroupe($ids, Request $request, ArticleRepository $articleRepository): Response
    {
        $referer = $request->headers->get('referer');
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $article = $articleRepository->find($id);
            $article->setEnLigne(($article->getEnLigne() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
        }
        $em->flush();


        return new Response('La mise en ligne des articles a changé d\'état', Response::HTTP_OK);
    }
}
