<?php

namespace App\Service;

use App\Entity\Taille;
use App\Repository\TailleRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartItem
{
    public $taille;
    public $qty;

    public function __construct(Taille $taille, $qty)
    {
        $this->taille = $taille;
        $this->qty = $qty;
    }
    public function getTotal()
    {
        return $this->taille->getPrice()*$this->qty;
    }
}
