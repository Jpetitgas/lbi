<?php

namespace App\Service;

use App\Entity\Activite;
use App\Service\EnCours;
use App\Repository\ActiviteRepository;
use App\Repository\InscriptionRepository;
use Symfony\Component\Security\Core\Security;

class VerificationActivite
{
    protected $activiteRepository;
    protected $enCours;

    public function __construct(EnCours $enCours, ActiviteRepository $activiteRepository)
    {
        $this->activiteRepository=$activiteRepository;
    }

    /**
     * Verifie si l'activité demande un certificat médical
     * @param Activite $activite
     *
     * @return [type]
     */
    public function certificatMedical(Activite $activite)
    {
        $activiteRef=$this->activiteRepository->find($activite);
        return $activiteRef->getCertificatObligatoire();
    }
    public function certificatMedicalIdActivite($id_activite)
    {
        $activiteRef=$this->activiteRepository->findOneBy(['id'=>$id_activite]);
        return $activiteRef->getCertificatObligatoire();
    }
}
