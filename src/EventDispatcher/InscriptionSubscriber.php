<?php

namespace App\EventDispatcher;

use App\Entity\Messages;
use App\Entity\Token;
use App\Event\InscriptionEvent;
use App\Repository\SujetRepository;
use App\Repository\TokenRepository;
use App\Repository\InformationRepository;
use App\Service\SendEmail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class InscriptionSubscriber implements EventSubscriberInterface
{
    protected $sujetRepository;
    protected $entityManager;
    private $sendEmail;
    private $informationRepository;
    private $tokenRepository;
    private $router;
    private $twig;

    public function __construct(
        InformationRepository $informationRepository,
        SendEmail $sendEmail,
        SujetRepository $sujetRepository,
        EntityManagerInterface $entityManager,
        TokenRepository $tokenRepository,
        UrlGeneratorInterface $router,
        Environment $twig
    ) {
        $this->sujetRepository = $sujetRepository;
        $this->entityManager = $entityManager;
        $this->sendEmail = $sendEmail;
        $this->informationRepository = $informationRepository;
        $this->tokenRepository = $tokenRepository;
        $this->router = $router;
        $this->twig = $twig;
    }

    public static function getSubscribedEvents()
    {
        return [
            'ajout.inscription' => 'sendInscription',
            'suppression.inscription' => 'suppressionInscription'
        ];
    }

    public function sendInscription(InscriptionEvent $InscriptionEvent)
    {
        $inscription = $InscriptionEvent->getInscription();
        $activite = $inscription->getActivite();
        $sujet = $this->sujetRepository->findOneBy(['sujet' => 'Inscription']);

        // Vérification du nombre d'inscriptions
        $nb = $activite->getInscriptions()->count() + 1;
        if ($nb >= $activite->getNbDePlace()) {
            // L'activité est complète
            $message = "<h3>Notification d'activité complète</h3>";
            $message .= "<p>L'activité <strong>" . htmlspecialchars($activite->getNom()) . "</strong> est maintenant complète.</p>";
            $message .= "<ul style='list-style-type: none; padding-left: 0;'>";
            $message .= "<li style='margin-bottom: 10px;'>• <strong>Nombre d'inscriptions :</strong> " . $nb . "</li>";
            $message .= "<li style='margin-bottom: 10px;'>• <strong>Limite fixée :</strong> " . $activite->getNbDePlace() . "</li>";
            $message .= "<li style='margin-bottom: 10px;'>• <strong>Sous condition :</strong> " . ($activite->getSousCondition() ? 'Oui' : 'Non') . "</li>";
            $message .= "</ul>";

            // Générer un token sécurisé
            $token = bin2hex(random_bytes(32));

            // Stocker le token en base de données avec une expiration
            $this->storeToken($token, $activite->getId());

            // Ajouter le lien de modification sécurisé
            if (!$activite->getSousCondition()) {
                $url = $this->router->generate('gestion_activiteapp_activite_update_sous_condition', [
                    'id' => $activite->getId(),
                    'token' => $token,
                ], UrlGeneratorInterface::ABSOLUTE_URL);

                $message .= "Cliquez sur ce lien pour mettre à jour l'activité : <a href=\"$url\">Modifier l'activité</a>";
            }

            // Envoyer une notification par email
            $this->sendEmail->send([
                'recipient_email' => $sujet->getDestinataire()->getEmail(),
                'sujet' => 'Notification d\'activité',
                'htmlTemplate' => 'email/email.html.twig',
                'destinataire' => $sujet->getDestinataire(),
                'contenu' => $message,
            ]);
        }

        $information = $this->informationRepository->findOneBy([]);

        $message = $inscription->getPratiquant()->getPrenom() . ' ' . $inscription->getPratiquant()->getNom() . " est inscrit(e) sur l'activité : " . $inscription->getActivite()->getNom();

        if ($inscription->getCours()) {
            $message .= "(" . $inscription->getCours()->getNom() . ")";
        }


        if ($sujet) {
            $envoi = new Messages();
            $envoi->setExpediteur($sujet->getDestinataire())

                ->setDestinataire($inscription->getDossier()->getAdherent())
                ->setTitre($sujet->getSujet())
                ->setMessage($message);
            $this->entityManager->persist($envoi);
            $this->entityManager->flush();
        }
        $this->sendEmail->send([
            'recipient_email' => $sujet->getDestinataire()->getEmail(),
            'sujet' => 'Nouvelle inscription',
            'htmlTemplate' => 'email/email.html.twig',
            'destinataire' => $sujet->getDestinataire(),
            'contenu' => $message,
        ]);
        if ($information->getInfoInscription()) {
            $message .= "\n" . "Information concernant la rentrée: " . $information->getInfoInscription();
        }
        $this->sendEmail->send([
            'recipient_email' => $inscription->getDossier()->getAdherent()->getEmail(),
            'sujet' => 'Vous avez reçu un nouveau message',
            'htmlTemplate' => 'email/email.html.twig',
            'destinataire' => $inscription->getDossier()->getAdherent(),
            'contenu' => $message,
        ]);
    }

    public function suppressionInscription(InscriptionEvent $InscriptionEvent)
    {
        $inscription = $InscriptionEvent->getInscription();
        $activite = $inscription->getActivite();
        $sujet = $this->sujetRepository->findOneBy(['sujet' => 'Inscription']);
        // Vérification du nombre d'inscriptions
        $nb = $activite->getInscriptions()->count() - 1;
        if ($activite->getInscriptions()->count() - 1 == $activite->getNbDePlace() - 1) {
            // Des places sont à nouveau disponibles
            $message = 'Des places sont maintenant disponibles pour l\'activité ' . $activite->getNom();
            $message .= "Sous condition : " . ($activite->getSousCondition() ? 'Oui' : 'Non');
            // Vérifier si sousCondition est à true
            if ($activite->getSousCondition()) {
                // Générer un token sécurisé
                $token = bin2hex(random_bytes(32));

                // Stocker le token en base de données avec une expiration
                $this->storeToken($token, $activite->getId());

                // Ajouter le lien de modification sécurisé pour désactiver sousCondition
                $url = $this->router->generate('gestion_activiteapp_activite_update_sous_condition', [
                    'id' => $activite->getId(),
                    'token' => $token,
                ], UrlGeneratorInterface::ABSOLUTE_URL);

                $message .= "Cliquez sur ce lien rendre l'activité à nouveau accessible : <a href=\"$url\">Modifier l'activité</a>";
            }

            $this->sendEmail->send([
                'recipient_email' => $sujet->getDestinataire()->getEmail(),
                'sujet' => 'Notification d\'activité',
                'htmlTemplate' => 'email/email.html.twig',
                'destinataire' => $sujet->getDestinataire(),
                'contenu' => $message,
            ]);
        }

        $message = 'L\'inscription de ' . $inscription->getPratiquant()->getPrenom() . ' ' . $inscription->getPratiquant()->getNom() . ' à l\'activité ' . $inscription->getActivite()->getNom() . ' a été annulée.';

        if ($sujet) {
            $envoi = new Messages();
            $envoi->setExpediteur($sujet->getDestinataire())
                ->setDestinataire($inscription->getDossier()->getAdherent())
                ->setTitre($sujet->getSujet())
                ->setMessage($message);
            $this->entityManager->persist($envoi);
            $this->entityManager->flush();
        }

        $this->sendEmail->send([
            'recipient_email' => $inscription->getDossier()->getAdherent()->getEmail(),
            'sujet' => 'Vous avez reçu un nouveau message',
            'htmlTemplate' => 'email/email.html.twig',
            'destinataire' => $inscription->getDossier()->getAdherent(),
            'contenu' => $message,
        ]);

        $this->sendEmail->send([
            'recipient_email' => $sujet->getDestinataire()->getEmail(),
            'sujet' => 'Annulation d\'inscription',
            'htmlTemplate' => 'email/email.html.twig',
            'destinataire' => $sujet->getDestinataire(),
            'contenu' => $message,
        ]);
    }

    private function storeToken(string $token, int $activiteId): void
    {
        // Stocker le token dans la base de données avec une expiration
        $expiration = new \DateTime('+1 hour');
        $tokenEntity = new Token();
        $tokenEntity->setToken($token);
        $tokenEntity->setActiviteId($activiteId);
        $tokenEntity->setExpiration($expiration);

        $this->entityManager->persist($tokenEntity);
        $this->entityManager->flush();
    }
}
