
    var $table = $( '#present' );
    function getRowSelections() {
return $.map( $table.bootstrapTable( 'getSelections' ), function ( row ) {
return row;
} )
}

    $( '#envoi' ).click( function () {
var $id_cours = $( '#cours' ).val();
    if ( $id_cours != "Choisir un cours" ) {
var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each( selectedRows, function ( index, value ) {
        ids.push(value.id);
} );
    if ( ids.length ) {
const Url = new URL( window.location.href );
    fetch( Url.pathname + "ajout/", {
        method: 'POST',
    body: JSON.stringify(
    {cours: $id_cours, pratiquants: ids }
    ),
    headers: {
        "Content-type": "application/json"
}
} ).then( function ( response ) {
if ( response.ok ) {
        alert('la liste des presents a été envoyé');

} else {
        alert('Il y a eu un pb avec l\'application: la liste des presents n\'a pas été enregistrée!');
}
} ).catch( function ( error ) {
        console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
} );
} else {
        alert('Vous devez d\'abord selectionner une ligne');
}
} else {
        alert('Vous devez d\'abord selectionner un cours');
}
} );
