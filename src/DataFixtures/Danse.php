<?php

namespace App\DataFixtures;

use App\Entity\Cours;
use App\Entity\Activite;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class Danse extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $activite = new Activite();
        $activite->setSaison($this->getReference('saison'))
            ->setSection($this->getReference('section_danse'))
            ->setCategorie($this->getReference('categorie_ce2_cm2'))
            ->setCotisation(0)
            ->setDuree('1h')
            ->setNbDePlace(36)
            ->setCertificatObligatoire(false)
            ->setNom('Jazz-ini-jeu')
            ->setSousCondition(false)
            ->setTarif(140);
        $manager->persist($activite);
        $this->addReference('activite1', $activite);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant1'))
            ->setNom('Jazz-ini-jeu-17h30-18h30');
        $manager->persist($cours);
        $this->addReference('cours_1_1', $cours);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant1'))
            ->setNom('Jazz-ini-jeu-18h30-19h30');
        $manager->persist($cours);
        $this->addReference('cours_1_2', $cours);

        $activite = new Activite();
        $activite->setSaison($this->getReference('saison'))
            ->setSection($this->getReference('section_danse'))
            ->setCategorie($this->getReference('categorie_college'))
            ->setCotisation(0)
            ->setDuree('1h')
            ->setNbDePlace(26)
            ->setCertificatObligatoire(false)
            ->setNom('Jazz-ven-college')
            ->setSousCondition(false)
            ->setTarif(140);
        $manager->persist($activite);
        $this->addReference('activite2', $activite);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant1'))
            ->setNom('Jazz-college-ven-17h30-18h30');
        $manager->persist($cours);
        $this->addReference('cours_2_1', $cours);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant1'))
            ->setNom('Jazz-college-ven-18h30-19h30');
        $manager->persist($cours);
        $this->addReference('cours_2_2', $cours);

        $activite = new Activite();
        $activite->setSaison($this->getReference('saison'))
            ->setSection($this->getReference('section_danse'))
            ->setCategorie($this->getReference('categorie_lycee'))
            ->setCotisation(0)
            ->setDuree('1h15')
            ->setNbDePlace(30)
            ->setCertificatObligatoire(false)
            ->setNom('Jazz-jeu-lycée')
            ->setSousCondition(false)
            ->setTarif(170);
        $manager->persist($activite);
        $this->addReference('activite3', $activite);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant2'))
            ->setNom('Jazz-lycee-jeu-17h30-18h45');
        $manager->persist($cours);
        $this->addReference('cours_3_1', $cours);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant2'))
            ->setNom('Jazz-lycee-jeu-18h45-20h');
        $manager->persist($cours);
        $this->addReference('cours_3_2', $cours);

        $activite = new Activite();
        $activite->setSaison($this->getReference('saison'))
            ->setSection($this->getReference('section_danse'))
            ->setCategorie($this->getReference('categorie_adulte'))
            ->setCotisation(0)
            ->setDuree('1h30')
            ->setNbDePlace(36)
            ->setCertificatObligatoire(false)
            ->setNom('Jazz-jeu-adulte')
            ->setSousCondition(false)
            ->setTarif(200);
        $manager->persist($activite);
        $this->addReference('activite4', $activite);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant3'))
            ->setNom('Jazz-adul-lun-17h30-19h');
        $manager->persist($cours);
        $this->addReference('cours_4_1', $cours);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant3'))
            ->setNom('Jazz-adul-lun-19h-20h30');
        $manager->persist($cours);
        $this->addReference('cours_4_2', $cours);



        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            Parametres::class,
            Utilisateurs::class,
        ];
    }
}
