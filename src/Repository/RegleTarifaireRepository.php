<?php

namespace App\Repository;

use App\Entity\RegleTarifaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegleTarifaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegleTarifaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegleTarifaire[]    findAll()
 * @method RegleTarifaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegleTarifaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegleTarifaire::class);
    }

    /**
    * @return RegleTarifaire[] Returns an array of RegleTarifaire objects
    */

    public function findByniveau($saison, $niveau)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.saison = :saison')
            ->setParameter('saison', $saison)
            ->join('r.niveau', 'n', 'WITH', 'n.id=r.niveau')
            ->andWhere('n.nom = :niveau')
            ->setParameter('niveau', $niveau)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?RegleTarifaire
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
