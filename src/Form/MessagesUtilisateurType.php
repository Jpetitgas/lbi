<?php

namespace App\Form;

use App\Entity\Messages;
use Symfony\Component\Form\AbstractType;
use Eckinox\TinymceBundle\Form\Type\TinymceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MessagesUtilisateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('titre', TextType::class, [
                'required' => true,
            ])
            ->add('message', TinymceType::class, [
                'label' => 'Description',
                'required' => true,
                'constraints' => [
                    new NotBlank(),

                ],
            ])
            ->add('copie', ChoiceType::class, [
                'choices' => [
                    'Messagerie' => 'Messagerie',
                    'SMS' => 'SMS',

                ],
                'mapped' => false,
                'expanded' => true,
                'multiple' => true,
                'label' => 'Copie(s)',
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Messages::class,
        ]);
    }
}
