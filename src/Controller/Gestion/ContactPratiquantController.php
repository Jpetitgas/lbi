<?php

namespace App\Controller\Gestion;

use App\Entity\ContactPratiquant;
use App\Form\ContactPratiquantType;
use App\Repository\DossierRepository;
use App\Repository\PratiquantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/contact", name="gestion_contact")
 */
class ContactPratiquantController extends AbstractController
{
    /**
     * @Route("/new/{id_pratiquant}/{id_dossier}", name="_new", methods={"GET","POST"})
     */
    public function new($id_pratiquant, $id_dossier, DossierRepository $dossierRepository, Request $request, PratiquantRepository $pratiquantRepository): Response
    {
        $contactPratiquant = new ContactPratiquant();
        $formContact = $this->createForm(ContactPratiquantType::class, $contactPratiquant);
        $formContact->handleRequest($request);
        $dossier=$dossierRepository->find($id_dossier);
        $pratiquant = $pratiquantRepository->find($id_pratiquant);
        if ($formContact->isSubmitted() && $formContact->isValid()) {
            $contactPratiquant->setPratiquant($pratiquant);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contactPratiquant);
            $entityManager->flush();

            $ancre = 'pratiquant_'.$pratiquant->getId().'_contact';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $id_dossier, 'ancre' => $ancre]);
        }

        return $this->render('gestion/contact_pratiquant/new.html.twig', [
            'dossier'=>$dossier,
            'pratiquant'=>$pratiquant,
            'contact_pratiquant' => $contactPratiquant,
            'formContact' => $formContact->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/{id_dossier}", name="_edit", methods={"GET","POST"})
     */
    public function edit($id_dossier, DossierRepository $dossierRepository, Request $request, ContactPratiquant $contactPratiquant): Response
    {
        $dossier=$dossierRepository->find($id_dossier);
        $formContact = $this->createForm(ContactPratiquantType::class, $contactPratiquant);
        $formContact->handleRequest($request);
        $pratiquant=$contactPratiquant->getPratiquant();
        if ($formContact->isSubmitted() && $formContact->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $ancre = 'pratiquant_'.$contactPratiquant->getPratiquant()->getId().'_contact';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $id_dossier, 'ancre' => $ancre]);
        }

        return $this->render('gestion/contact_pratiquant/edit.html.twig', [
            'dossier'=>$dossier,
            'pratiquant'=>$pratiquant,
            'contact_pratiquant' => $contactPratiquant,
            'formContact' => $formContact->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{id_dossier}/", name="_delete", methods={"POST"})
     */
    public function delete($id_dossier, Request $request, ContactPratiquant $contactPratiquant): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contactPratiquant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contactPratiquant);
            $entityManager->flush();
        }
        $ancre = 'pratiquant_'.$contactPratiquant->getPratiquant()->getId().'_contact';

        return $this->redirectToRoute('gestion_dossier_edit', ['id' => $id_dossier, 'ancre' => $ancre]);
    }
}
