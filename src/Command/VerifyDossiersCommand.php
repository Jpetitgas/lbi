<?php

namespace App\Command;

use App\Entity\Dossier;
use App\Message\SendEmailMessage;
use App\Service\EnCours;
use App\Service\InfosDossierEncours;
use App\Repository\DossierRepository;
use App\Repository\InformationRepository;
use App\Repository\PresenceRepository;
use App\Repository\PratiquantRepository;
use App\Repository\SujetRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class VerifyDossiersCommand extends Command
{
    protected static $defaultName = 'app:verify-dossiers';
    private const EMAIL_TEMPLATE = 'email/email.html.twig';

    private $dossierRepository;
    private $enCours;
    private $infosDossierEncours;
    private $messageBus;
    private $informationRepository;
    private $presenceRepository;
    private $pratiquantRepository;
    private $sujetRepository;

    public function __construct(
        DossierRepository $dossierRepository,
        EnCours $enCours,
        InfosDossierEncours $infosDossierEncours,
        MessageBusInterface $messageBus,
        InformationRepository $informationRepository,
        PresenceRepository $presenceRepository,
        PratiquantRepository $pratiquantRepository,
        SujetRepository $sujetRepository
    ) {
        parent::__construct();
        $this->dossierRepository = $dossierRepository;
        $this->enCours = $enCours;
        $this->infosDossierEncours = $infosDossierEncours;
        $this->messageBus = $messageBus;
        $this->informationRepository = $informationRepository;
        $this->presenceRepository = $presenceRepository;
        $this->pratiquantRepository = $pratiquantRepository;
        $this->sujetRepository = $sujetRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $information = $this->informationRepository->findOneBy([]);
        if (!$information || !$information->isActivationRelance()) {
            $output->writeln('La relance n\'est pas activée.');
            return Command::SUCCESS;
        }

        $nbEssai = $information->getNbEssai();
        $saison = $this->enCours->saison();

        $pratiquantsAVerifier = $this->getPratiquantsAvecPresencesSuffisantes($nbEssai, $saison);

        $dossiersANotifier = [];

        foreach ($pratiquantsAVerifier as $pratiquant) {
            $dossier = $this->dossierRepository->findOneBy(['saison' => $saison, 'adherent' => $pratiquant->getUtilisateur()]);

            if ($dossier && !$dossier->getDossierVerrouille() && !$dossier->isLocked() && $this->dossierNecessiteNotification($dossier)) {
                $dossiersANotifier[$dossier->getId()] = $dossier;
            }
        }

        $emailsEnvoyes = $this->envoyerNotifications($dossiersANotifier);

        $output->writeln(sprintf('%d dossiers nécessitent une notification.', count($dossiersANotifier)));
        $output->writeln(sprintf('%d emails ont été envoyés.', $emailsEnvoyes));

        $this->envoyerRecapitulatifGestionnaire($dossiersANotifier, $emailsEnvoyes);

        return Command::SUCCESS;
    }

    private function getPratiquantsAvecPresencesSuffisantes(int $nbEssai, $saison): array
    {
        $pratiquants = $this->pratiquantRepository->findAll();
        $pratiquantsAVerifier = [];

        foreach ($pratiquants as $pratiquant) {
            $presences = $this->presenceRepository->findBy(['pratiquant' => $pratiquant, 'saison' => $saison]);
            if (count($presences) >= $nbEssai) {
                $pratiquantsAVerifier[] = $pratiquant;
            }
        }

        return $pratiquantsAVerifier;
    }

    private function dossierNecessiteNotification(Dossier $dossier): bool
    {
        $cotisationNonReglee = $this->infosDossierEncours->solde($dossier) > 0;
        $bulletinNonPresent = empty($dossier->getBulletinAdhesion());
        $certificatManquant = $this->certificatManquant($dossier);

        return $cotisationNonReglee || $bulletinNonPresent || $certificatManquant;
    }

    private function certificatManquant(Dossier $dossier): bool
    {
        foreach ($dossier->getInscriptions() as $inscription) {
            if ($inscription->getCertificatObligatoire() && !$inscription->getDocument()) {
                return true;
            }
        }
        return false;
    }

    private function envoyerNotifications(array $dossiers): int
    {
        $emailsEnvoyes = 0;
        foreach ($dossiers as $dossier) {
            $contenu = $this->prepareMessageContent($dossier);

            $this->messageBus->dispatch(new SendEmailMessage(
                $dossier->getAdherent()->getEmail(),
                'Votre dossier nécessite votre attention',
                self::EMAIL_TEMPLATE,
                $dossier->getAdherent(),
                $contenu,
            ));

            $emailsEnvoyes++;
        }
        return $emailsEnvoyes;
    }

    private function prepareMessageContent(Dossier $dossier): string
    {
        $content = "<p>Votre dossier pour la saison en cours nécessite votre attention :</p>";

        $content .= "<ul>";

        $solde = $this->infosDossierEncours->solde($dossier);
        if ($solde > 0) {
            $content .= sprintf("<li>Votre cotisation n'est pas entièrement réglée. Solde restant à payer : %.2f €</li>", $solde);
        }

        if (empty($dossier->getBulletinAdhesion())) {
            $content .= "<li>Votre bulletin d'adhésion n'a pas été fourni.</li>";
        }

        foreach ($dossier->getInscriptions() as $inscription) {
            if ($inscription->getCertificatObligatoire() && !$inscription->getDocument()) {
                $content .= sprintf(
                    "<li>Le certificat médical pour l'activité \"%s\" de %s %s est manquant.</li>",
                    htmlspecialchars($inscription->getActivite()->getNom()),
                    htmlspecialchars($inscription->getPratiquant()->getPrenom()),
                    htmlspecialchars($inscription->getPratiquant()->getNom())
                );
            }
        }

        $content .= "</ul>";

        $content .= "<p>Veuillez vous connecter à votre espace personnel pour compléter votre dossier.</p>";

        return $content;
    }

    private function envoyerRecapitulatifGestionnaire(array $dossiersANotifier, int $emailsEnvoyes): void
    {
        $sujet = $this->sujetRepository->findOneBy(['sujet' => 'Inscription']);
        if (!$sujet || !$sujet->getDestinataire()) {
            return;
        }

        $contenu = $this->prepareRecapitulatifContent($dossiersANotifier, $emailsEnvoyes);

        $this->messageBus->dispatch(new SendEmailMessage(
            $sujet->getDestinataire()->getEmail(),
            'Récapitulatif des relances de dossiers',
            self::EMAIL_TEMPLATE,
            $sujet->getDestinataire(),
            $contenu
        ));
    }

    private function prepareRecapitulatifContent(array $dossiersANotifier, int $emailsEnvoyes): string
    {
        $content = "<h2>Récapitulatif des relances de dossiers</h2>";
        $content .= "<p>Date de la relance : " . date('d/m/Y H:i:s') . "</p>";
        $content .= "<p>Nombre d'adhérents concernés : " . count($dossiersANotifier) . "</p>";
        $content .= "<p>Nombre d'emails envoyés : " . $emailsEnvoyes . "</p>";

        if (!empty($dossiersANotifier)) {
            $content .= "<h3>Liste des adhérents concernés :</h3><ul>";
            foreach ($dossiersANotifier as $dossier) {
                $adherent = $dossier->getAdherent();
                $content .= sprintf(
                    "<li>%s %s</li>",
                    htmlspecialchars($adherent->getNom()),
                    htmlspecialchars($adherent->getPrenom())
                );
            }
            $content .= "</ul>";
        }

        return $content;
    }
}
