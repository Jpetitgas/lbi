<?php

namespace App\DataFixtures;

use App\Entity\Banque;
use App\Entity\CategoryArticle;
use App\Entity\CP;
use App\Entity\Information;
use App\Entity\Mensuration;
use App\Entity\NiveauTarifaire;
use App\Entity\Operateur;
use App\Entity\Sujet;
use App\Entity\TypeDeDocument;
use App\Entity\TypeDePaiement;
use App\Entity\Utilisateur;
use App\Entity\Ville;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use SebastianBergmann\Type\FalseType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class IndispensableFixtures extends Fixture
{
    private $faker;
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
        /**MENSURATION PRATIQUANT */
        $tailles = ['4 ans', '6 ans', '8 ans', '10 ans', '12 ans', 'XS', 'S', 'M', 'L', 'XL', 'XXL', '7 ans'];

        foreach ($tailles as $item) {
            $mensuration = new Mensuration();
            $mensuration->setTaille($item);
            $manager->persist($mensuration);
        }
        /** BANQUE */
        $banque = new Banque();
        $banque->setNom('Stripe')
            ->setVerrouille(true);
        $this->addReference('stripe', $banque);
        $manager->persist($banque);

        $banques = ['Crédit Agricole Centre France', 'Banque Postale', 'BNP', 'Banque Populaire', 'Caisse d\'Epargne'];
        foreach ($banques as $item) {
            $banque = new Banque();
            $banque->setNom($item)
            ->setVerrouille(false);
            $manager->persist($banque);
        }

        /**TYPE DE PAIEMENT */

        $typeDePaiement = new TypeDePaiement();
        $typeDePaiement->setDesignation('Carte bancaire')
            ->setVerrouille(true);
        $this->addReference('cb', $typeDePaiement);
        $manager->persist($typeDePaiement);

        $typeDePaiements = ['Chèque', 'ANCV', 'Coupons Sport', 'Pass Sport 2021'];
        foreach ($typeDePaiements as $item) {
            $typeDePaiement = new TypeDePaiement();
            $typeDePaiement->setDesignation($item)
            ->setVerrouille(false);
            $manager->persist($typeDePaiement);
        }

        /**TYPE DE DOCUMENT */

        $typedocument = new TypeDeDocument();
        $typedocument->setType('Certificat medical')
            ->setVerrouille(true);
        $this->addReference('doc', $typedocument);
        $manager->persist($typedocument);

        /**REGLE TARIFAIRE */
        /**NIVEAU */
        $niveauTarifaire = new NiveauTarifaire();
        $niveauTarifaire->setNom('UTILISATEUR');
        $this->addReference('UTILISATEUR', $niveauTarifaire);
        $manager->persist($niveauTarifaire);

        $niveauTarifaire = new NiveauTarifaire();
        $niveauTarifaire->setNom('PRATIQUANT');
        $this->addReference('PRATIQUANT', $niveauTarifaire);
        $manager->persist($niveauTarifaire);

        $niveauTarifaire = new NiveauTarifaire();
        $niveauTarifaire->setNom('ACTIVITE');
        $this->addReference('ACTIVITE', $niveauTarifaire);
        $manager->persist($niveauTarifaire);
        /**OPERATEUR */
        $operateur = new Operateur();
        $operateur->setNom('UNIQUE')
                ->addNiveauTarifaire($this->getReference('UTILISATEUR'))
                ->addNiveauTarifaire($this->getReference('PRATIQUANT'))
                ->addNiveauTarifaire($this->getReference('ACTIVITE'));
        $this->addReference('UNIQUE', $operateur);
        $manager->persist($operateur);
        $operateur = new Operateur();
        $operateur->setNom('NOMBRE>=')
                ->addNiveauTarifaire($this->getReference('PRATIQUANT'))
                ->addNiveauTarifaire($this->getReference('ACTIVITE'));
        $this->addReference('NOMBRE>=', $operateur);
        $manager->persist($operateur);
        $operateur = new Operateur();
        $operateur->setNom('INTRA-COMMUNES')
                ->addNiveauTarifaire($this->getReference('UTILISATEUR'))
                ->addNiveauTarifaire($this->getReference('PRATIQUANT'));

        $manager->persist($operateur);
        $operateur = new Operateur();
        $operateur->setNom('EXTRA-COMMUNES')
                ->addNiveauTarifaire($this->getReference('UTILISATEUR'))
                ->addNiveauTarifaire($this->getReference('PRATIQUANT'));


        $manager->persist($operateur);
        $operateur = new Operateur();
        $operateur->setNom('AGE>=')
                ->addNiveauTarifaire($this->getReference('PRATIQUANT'));

        $manager->persist($operateur);
        $operateur = new Operateur();
        $operateur->setNom('AGE<=')
                ->addNiveauTarifaire($this->getReference('PRATIQUANT'));

        $manager->persist($operateur);

        $information = new Information();

        $information
            ->setNomStructure('Votre asso')
            ->setSignature('Gestionnaire de votre asso')
            ->setVitrine('https://demo.assoclic.fr')
            ->setEspace('https://demo.assoclic.fr/espace')
            ->setAdresse('votre adresse')
            ->setCodePostal('63000')
            ->setVille('Ville')
            ->setCreationDossier(true)
            ->setModificationDossier(true)
            ->setCoursVisible(true)
            ->setDescriptionDossier("L'inscription se fait au travers de la création d'un dossier. Un dossier contient les pratiquants de l'utilisateur, les activités de ces derniers avec le certificat médical téléversable (si l'activité l'impose),
le bulletin d'adhésion signé, téléversé et le paiement de la cotisation.")
            ->setDescriptionInscription('La description de votre procedure d\'inscription peut etre personnalisée en fonction de votre organisation')
            ->setEnteteBulletin('Le bulletin d\'adhésion possède deux zone personnalisable: l\'entêtes')
            ->setConditionDeVente('Le bulletin d\'adhésion possède deux zone personnalisable: le corps')
            ->setPaiement('Les modalités de paiement peuvent etre personnalisées en fonction de votre organisation. Le paiement en ligne peut être déactivé')
            ->setStripe(true)
            ->setBoutiqueStripe(true)
            ->setBoutiquePresentation('La boutique permet de vendre des articles de toutes sortes avec le paiement en ligne intégré')
            ->setBoutiquePaiement('Les modalités de paiement peuvent être personnalisées en fonction de votre organisation. Le paiement en ligne peut être déactivé')
            ->setBoutiqueLivraison('Les modalités de livraison peuvent etre personnalisées en fonction de votre organisation')
            ->setBoutiqueOuvert(true)
            ->setBoutiqueActive(true)
            ->setReservationActive(true)
            ->setReservationPresentation('Le systeme de réservations permet de gérer des évenements comme des permanences, des spéctacles, des stages des bourses etc avec une interface de reservation sur plan avec le paiement en ligne intégré.')
            ->setReservationStripe(true)
            ->setSite(true)
            ->setLive(false);

        $manager->persist($information);

        $cp = new CP();
        $cp->setCode(63000);
        $manager->persist($cp);
        $this->addReference('cp', $cp);
        $ville = new Ville();
        $ville->setCp($cp)
            ->setNomMaj('CLERMONT-FD')
            ->setNomMin('Clermont-Fd');
        $manager->persist($ville);
        $this->addReference('ville', $ville);

        $utilisateur = new Utilisateur();
        $hash = $this->userPasswordEncoder->encodePassword($utilisateur, 'Gestionnaire_2022');

        $utilisateur->setEmail('gestionnaire@demo.fr')
            ->setPassword($hash)
            ->setNom($this->faker->lastName())
            ->setPrenom($this->faker->firstName())
            ->Setroles(['ROLE_GESTION'])
            ->setPortable($this->faker->PhoneNumber())
            ->setAdresse($this->faker->address())
            ->setCP($cp)
            ->setTown(($ville))
            ->setIsVerified(true);

        $manager->persist($utilisateur);
        /**SUJET MESSAGE */
        $sujet = new Sujet();
        $sujet->setSujet($sujet::SUJET_PAIEMENT_LIGNE)
            ->setDestinataire($utilisateur)
            ->setCategorie('UTILISATEUR')
            ->setVerrouille(true);
        $manager->persist($sujet);

        $sujet = new Sujet();
        $sujet->setSujet($sujet::SUJET_LISTE_ATTENTE)
            ->setDestinataire($utilisateur)
            ->setCategorie('UTILISATEUR')
            ->setVerrouille(true);
        $manager->persist($sujet);

        $sujet = new Sujet();
        $sujet->setSujet($sujet::SUJET_FORMULAIRE_CONTACT)
            ->setDestinataire($utilisateur)
            ->setCategorie('UTILISATEUR')
            ->setVerrouille(true);
        $manager->persist($sujet);

        $sujet = new Sujet();
        $sujet->setSujet($sujet::SUJET_INSCRIPTION)
            ->setDestinataire($utilisateur)
            ->setCategorie('UTILISATEUR')
            ->setVerrouille(true);
        $manager->persist($sujet);

        $sujet = new Sujet();
        $sujet->setSujet($sujet::SUJET_BOUTIQUE)
            ->setDestinataire($utilisateur)
            ->setCategorie('UTILISATEUR')
            ->setVerrouille(true);
        $manager->persist($sujet);
        $sujet = new Sujet();
        $sujet->setSujet($sujet::SUJET_MESSAGE_INTERVENANT)
            ->setDestinataire($utilisateur)
            ->setCategorie('UTILISATEUR')
            ->setVerrouille(true);
        $manager->persist($sujet);
        $sujet->setSujet($sujet::SUJET_FORMULAIRE_CONTACT)
            ->setDestinataire($utilisateur)
            ->setCategorie('UTILISATEUR')
            ->setVerrouille(true);
        $manager->persist($sujet);
        $sujet->setSujet($sujet::SUJET_INTERVENANT)
            ->setDestinataire($utilisateur)
            ->setCategorie('INTERVENANT')
            ->setVerrouille(true);
        $manager->persist($sujet);

        /**SITE */
        $activite = new CategoryArticle();
        $activite->setNom('Activité');
        $this->addReference('activiteArticle', $activite);
        $manager->persist($activite);
        $actualite = new CategoryArticle();
        $actualite->setNom('Actualité');
        $this->addReference('actualiteArticle', $actualite);
        $manager->persist($actualite);

        $manager->flush();
    }
}
