import infoJs from '../../module/general'

let tailleHaut = document.querySelectorAll(".tailleHaut")
for (let haut of tailleHaut) {
    haut.addEventListener("change", function () {
        fetch('/utilisateur/pratiquant/taillehaut/' + this.dataset.id + '/' + this.value)
            .then(response => response.text())
            .then(() => {
                infoJs("La taille haut est enregistrée", "success");
            }
            )
    })
}
let tailleBas = document.querySelectorAll(".tailleBas")
for (let bas of tailleBas) {
    bas.addEventListener("change", function () {
        fetch('/utilisateur/pratiquant/taillebas/' + this.dataset.id + '/' + this.value)
            .then(response => response.text())
            .then(() => {
                infoJs("La taille bas est enregistrée", "success");
            })

    })
}

