<?php

namespace App\SearchData;

class ActiviteData
{
    public $q = '';

    public $section;
    public $categorieAge;
    public $certificat;
    public $condition;
    public $nb = 20;
}
