<?php

namespace App\Controller\Gestion;

use App\Entity\Cours;
use App\Form\CoursType;
use App\Service\EnCours;
use App\Repository\CoursRepository;
use App\Repository\SaisonRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/cours", name="gestion_cours")
 */
class CoursController extends AbstractController
{
    /**
     * @Route("/{id_saison}/index", name="_index", methods={"GET"})
     */
    public function index($id_saison, PaginatorInterface $paginator, Request $request, SaisonRepository $saisonRepository, CoursRepository $coursRepository): Response
    {
        $saison = $saisonRepository->find($id_saison);
        $cours= $paginator->paginate(
            $coursRepository->coursParSaison($saison),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('gestion/cours/index.html.twig', [
            'saison' => $saison,
            'cours' => $cours,
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $cours = new Cours();
        $formCours = $this->createForm(CoursType::class, $cours);
        $formCours->handleRequest($request);

        if ($formCours->isSubmitted() && $formCours->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cours);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_cours_index', ['id_saison' => $cours->getActivite()->getSaison()->getId()]);
        }

        return $this->render('gestion/cours/new.html.twig', [
            'cours' => $cours,
            'formCours' => $formCours->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cours $cours): Response
    {
        $formCours = $this->createForm(CoursType::class, $cours);
        $formCours->handleRequest($request);

        if ($formCours->isSubmitted() && $formCours->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_cours_index', ['id_saison' => $cours->getActivite()->getSaison()->getId()]);
        }

        return $this->render('gestion/cours/edit.html.twig', [
            'cours' => $cours,
            'formCours' => $formCours->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Cours $cours): Response
    {
        $saison = $cours->getSaison()->getId();
        if ($this->isCsrfTokenValid('delete' . $cours->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cours);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_cours_index', ['id_saison' => $saison]);
    }
}
