<?php

namespace App\Repository;

use App\Entity\Place;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }



    public function findPlaceByEvenementReserved($evenement, $data)
    {
        $query = $this->createQueryBuilder('p')
            ->select('p', 'c', 'u')
            ->leftJoin('p.container', 'c')
            ->innerJoin('p.utilisateur', 'u')
            ->groupBy('u.id')
            ->andWhere('c.evenement = :evenement')
            ->setParameter('evenement', $evenement);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or c.nom LIKE :q or p.nom LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }


        return $query->getQuery();
    }
}
