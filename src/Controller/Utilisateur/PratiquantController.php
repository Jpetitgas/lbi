<?php

namespace App\Controller\Utilisateur;

use App\Entity\Mensuration;
use App\Service\EnCours;
use App\Entity\Pratiquant;
use App\Form\PratiquantType;

use Doctrine\DBAL\Exception;
use App\Repository\DossierRepository;
use App\Repository\InformationRepository;
use App\Repository\MensurationRepository;
use App\Repository\PratiquantRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/utilisateur/pratiquant", name="utilisateur_pratiquant")
 */
class PratiquantController extends AbstractController
{
    /**
     * @Route("/{id_dossier}/new", name="_new", methods={"GET","POST"})
     */
    public function new($id_dossier, Request $request, EnCours $enCours, DossierRepository $dossierRepository, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        $dossier = $dossierRepository->find($id_dossier);
        $pratiquant = new Pratiquant();
        $formPratiquant = $this->createForm(PratiquantType::class, $pratiquant);
        $formPratiquant->handleRequest($request);

        if ($formPratiquant->isSubmitted() && $formPratiquant->isValid()) {
            $pratiquant->setUtilisateur($dossier->getAdherent());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pratiquant);
            $entityManager->flush();

            $ancre = 'pratiquant_'.$pratiquant->getId().'_none';

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
        }

        return $this->render('utilisateur/pratiquant/new.html.twig', [
            'pratiquant' => $pratiquant,
            'formPratiquant' => $formPratiquant->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pratiquant $pratiquant, EnCours $enCours, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        $formPratiquant = $this->createForm(PratiquantType::class, $pratiquant);
        $formPratiquant->handleRequest($request);

        if ($formPratiquant->isSubmitted() && $formPratiquant->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            /* @var Dossier $dossier*/
            $dossier = $enCours->dossierAdherent($pratiquant->getUtilisateur());
            $ancre = 'pratiquant_'.$pratiquant->getId().'_detail';

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
        }

        return $this->render('utilisateur/pratiquant/edit.html.twig', [
            'pratiquant' => $pratiquant,
            'formPratiquant' => $formPratiquant->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Pratiquant $pratiquant, EnCours $enCours, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        $ancre = 'pratiquant_none_none';
        try {
            if ($this->isCsrfTokenValid('delete'.$pratiquant->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($pratiquant);
                $entityManager->flush();
            }
        } catch (Exception  $e) {
            $this->addFlash('danger', 'Vous devez supprimer tous les éléments de ce pratiquant avant de le supprimer');
        }

        return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
    }
    /**
     * @Route("/taillehaut/{id_pratiquant}/{id_taille}", name="_taillehaut", methods={"GET","POST"})
     */
    public function tailleHaut($id_pratiquant, $id_taille, InformationRepository $informationRepository, PratiquantRepository $pratiquantRepository, MensurationRepository $mensurationRepository)
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getMensurationPratiquant() == false) {
            $this->addFlash('danger', 'La modification des mensurations n\'est pas autorisé');

            return new Response('Accés non autorisé!!', Response::HTTP_FORBIDDEN);
            ;
        }
        $pratiquant = $pratiquantRepository->find($id_pratiquant);
        $taille = $mensurationRepository->find($id_taille);
        if ($pratiquant) {
            $pratiquant->setTailleHaut($taille);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        } else {
            return new Response('L\'utilisateur n\'existe pas', Response::HTTP_BAD_REQUEST);
        }
        return new Response('Taille enregistrée', Response::HTTP_OK);
    }
    /**
     * @Route("/taillebas/{id_pratiquant}/{id_taille}", name="_taillebas", methods={"GET","POST"})
     */
    public function tailleBas($id_pratiquant, $id_taille, InformationRepository $informationRepository, PratiquantRepository $pratiquantRepository, MensurationRepository $mensurationRepository)
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getMensurationPratiquant() == false) {
            $this->addFlash('danger', 'La modification des mensurations n\'est pas autorisé');

            return new Response('Accés non autorisé!!', Response::HTTP_FORBIDDEN);
        }
        $pratiquant = $pratiquantRepository->find($id_pratiquant);
        $taille = $mensurationRepository->find($id_taille);

        if ($pratiquant) {
            $pratiquant->setTailleBas($taille);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        } else {
            return new Response('L\'utilisateur n\'existe pas', Response::HTTP_BAD_REQUEST);
        }
        return new Response('Taille enregistrée', Response::HTTP_OK);
    }
}
