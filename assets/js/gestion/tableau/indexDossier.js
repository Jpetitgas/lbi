    var $table = $( '#dossier' );
    function getRowSelections() {
return $.map( $table.bootstrapTable( 'getSelections' ), function ( row ) {
return row;
} )
}

    $( '#validationBulletinGroupe' ).click( function () {
var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each( selectedRows, function ( index, value ) {
        ids.push(value.id);
} );
    if ( ids.length ) {
var r = confirm( "Etes-vous sur de vouloir changer l\'etat des bulletins d\'adhésion?" );
    if ( r == true ) {
        window.location.href = "/gestion/dossier/validationBulletinGroupe/" + ids.toString();
}
} else {
        alert('Vous devez d\'abord selectionner une ligne');
}
} );

    $( '#verrouilleGroupe' ).click( function () {
var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each( selectedRows, function ( index, value ) {
        ids.push(value.id);
} );
    if ( ids.length ) {
var r = confirm( "Etes-vous sur de vouloir changer l\'etat de ce(s) dossier(s)?" );
    if ( r == true ) {
        window.location.href = "/gestion/dossier/verrouilleGroupe/" + ids.toString();
}

} else {
        alert('Vous devez d\'abord selectionner une ligne');
}
} );
    $( '#suppressionGroupe' ).click( function () {
var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each( selectedRows, function ( index, value ) {
        ids.push(value.id);
} );
    if ( ids.length ) {
var r = confirm( "Etes-vous sur de vouloir supprimer les dossiers sélectionnés?" );
    if ( r == true ) {
        window.location.href = "/gestion/dossier/deleteGroupe/" + ids.toString();
}
} else {
        alert('Vous devez d\'abord selectionner une ligne');
}
} );

    $( '#message' ).click( function () {
var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each( selectedRows, function ( index, value ) {
        ids.push(value.utilisateur);
} );
    if ( ids.length ) {
        window.location.href = "/gestion/correspondance/message/" + ids.toString();
} else {
        alert('Vous devez d\'abord selectionner une ligne');
}
} );
