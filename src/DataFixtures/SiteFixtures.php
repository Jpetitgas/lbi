<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Menu;
use App\Entity\Site;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SiteFixtures extends Fixture implements FixtureGroupInterface
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        $origine = './public/assets/sauvegarde/site/';
        $destination = './public/assets/site/';
        $files = ['cirque', 'danse', 'escalade', 'foot', 'instruments', 'multi', 'theatre', 'yoga', 'yogabis'];
        foreach ($files as $file) {
            copy($origine.$file.'.jpg', $destination.$file.'.jpg');
        }

        $site = new Site();
        $site->setDescription('<p style="text-align: center;">Ce site est une d&eacute;monstration de l&#39;application Assoclic. Vous &ecirc;tes sur la partie site vitrine que cette derni&egrave;re vous propose pour votre association ou votre structure.</p>

<p style="text-align: center;">Vous pouvez vous connecter avec les droits d\'un gestionnaire en utilisant les identifiants ci-dessous pour acc&eacute;der &agrave; l\'espace personnel :</p>

<p style="text-align: center;">gestionnaire@demo.fr et Gestionnaire_2022</p>

<p style="text-align: center;">Vous avez ainsi acc&eacute;s au coeur d&#39;Assoclic. Les donn&eacute;es sont r&eacute;initialis&eacute;es toutes heures.</p>')
            ->setSlogan('votre slogan')
            ->setFile('multi.jpg');
        $manager->persist($site);
        $textActivite = '<p>Vous pouvez indiquer toutes les informations n&eacute;cessaires pour d&eacute;crire chaque activit&eacute; (horaires, jours, tarifs, contenu etc) &agrave; partir de l espace personnel.</p>

<p>Il n est pas n&eacute;cessaire de poss&eacute;der des connaissances particuli&egrave;res pour g&eacute;rer et actualiser le contenu de votre site vitrine.</p>';
        $menu = new Menu();
        $menu->setNom('Artistique')
            ->setDescription($textActivite);
        $this->addReference('menuArtistique', $menu);
        $manager->persist($menu);
        $menu = new Menu();
        $menu->setNom('Sportif')
            ->setDescription('super section à decouvrir'.$this->faker->paragraph(10));
        $this->addReference('menuSportif', $menu);
        $manager->persist($menu);
        /**ACTIVTE */
        $activite = new Article();
        $activite->setTitre('Cirque')
            ->setContenu($textActivite)
            ->setCategorie($this->getReference('activiteArticle'))
            ->setMenu($this->getReference('menuArtistique'))
            ->setFile('cirque.jpg')
            ->setEnLigne(true);
        $manager->persist($activite);

        $activite = new Article();
        $activite->setTitre('Danse')
            ->setContenu($textActivite)
            ->setCategorie($this->getReference('activiteArticle'))
            ->setMenu($this->getReference('menuArtistique'))
            ->setFile('danse.jpg')
            ->setEnLigne(true);
        $manager->persist($activite);
        $activite = new Article();
        $activite->setTitre('Théâtre')
            ->setContenu($textActivite)
            ->setCategorie($this->getReference('activiteArticle'))
            ->setMenu($this->getReference('menuArtistique'))
            ->setFile('theatre.jpg')
            ->setEnLigne(true);
        $manager->persist($activite);

        $activite = new Article();
        $activite->setTitre('Escalade')
            ->setContenu($textActivite)
            ->setCategorie($this->getReference('activiteArticle'))
            ->setMenu($this->getReference('menuSportif'))
            ->setFile('escalade.jpg')
            ->setEnLigne(true);
        $manager->persist($activite);

        $activite = new Article();
        $activite->setTitre('Yoga')
            ->setContenu($textActivite)
            ->setCategorie($this->getReference('activiteArticle'))
            ->setMenu($this->getReference('menuSportif'))
            ->setFile('yoga.jpg')
            ->setEnLigne(true);
        $manager->persist($activite);

        $activite = new Article();
        $activite->setTitre('Foot')
            ->setContenu($textActivite)
            ->setCategorie($this->getReference('activiteArticle'))
            ->setMenu($this->getReference('menuSportif'))
            ->setFile('foot.jpg')
            ->setEnLigne(true);
        $manager->persist($activite);

        /**ACTUALITE */

        $textActualite = '<p>Des articles concernant l&#39;actualit&eacute; de votre association ou structure peuvent &ecirc;tre ajout&eacute;s &agrave; partir de l espace personnel.</p>

<p>Il n est pas n&eacute;cessaire de poss&eacute;der des connaissances particuli&egrave;res pour g&eacute;rer et actualiser le contenu de votre site vitrine.</p>';
        $actualite = new Article();
        $actualite->setTitre('les instruments')
            ->setContenu($textActualite)
            ->setCategorie($this->getReference('actualiteArticle'))
            ->setFile('instruments.jpg')
            ->setEnLigne(true);
        $manager->persist($actualite);
        $actualite = new Article();
        $actualite->setTitre('Stage')
            ->setContenu($textActualite)
            ->setCategorie($this->getReference('actualiteArticle'))
            ->setFile('yogabis.jpg')
            ->setEnLigne(true);
        $manager->persist($actualite);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['site'];
    }
}
