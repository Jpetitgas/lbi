<?php

namespace App\Doctrine\Filtre;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class UtilisateurConnectedFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if ($targetEntity->getReflectionClass()->name != 'App\Entity\Utilisateur') {
            return '';
        }

        return sprintf('%s.email = %s', $targetTableAlias, $this->getParameter('utilisateur'));
    }
}
