export default function infoJs(message, type) {
    let contenu = "<div class='alert alert-" + type + "' role='alert'><button type='button' class='btn-close' data-bs-dismiss='alert'></button><strong>" + message + "</strong></div>";;
    document.getElementById("alertJs").innerHTML = contenu;
}
