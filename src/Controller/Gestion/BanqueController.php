<?php

namespace App\Controller\Gestion;

use App\Entity\Banque;
use App\Form\BanqueType;
use App\Repository\BanqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/banque", name="gestion_banque")
 */
class BanqueController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(BanqueRepository $banqueRepository): Response
    {
        return $this->render('gestion/banque/index.html.twig', [
            'banques' => $banqueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $banque = new Banque();
        $formBanque = $this->createForm(BanqueType::class, $banque);
        $formBanque->handleRequest($request);

        if ($formBanque->isSubmitted() && $formBanque->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($banque);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_banque_index');
        }

        return $this->render('gestion/banque/new.html.twig', [
            'banque' => $banque,
            'formBanque' => $formBanque->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Banque $banque): Response
    {
        $formBanque = $this->createForm(BanqueType::class, $banque);
        $formBanque->handleRequest($request);

        if ($formBanque->isSubmitted() && $formBanque->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_banque_index');
        }

        return $this->render('gestion/banque/edit.html.twig', [
            'banque' => $banque,
            'formBanque' => $formBanque->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Banque $banque): Response
    {
        if ($this->isCsrfTokenValid('delete'.$banque->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($banque);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_banque_index');
    }
}
