<?php

namespace App\SearchForm;

use App\SearchData\DossierData;
use App\SearchData\PurchaseData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PurchaseForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('q', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Rechercher',
                ],
            ])
            ->add('livre', ChoiceType::class, [
                'required' => false,
                'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
            ])
            ->add('paye', ChoiceType::class, [
                'required' => false,
                'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
            ])
            ->add('nb', IntegerType::class, [
                'required' => false,
            ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PurchaseData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
