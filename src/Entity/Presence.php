<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\PresenceRepository;

/**
 * @ORM\Entity(repositoryClass=PresenceRepository::class)
 */
class Presence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Pratiquant::class, inversedBy="presences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pratiquant;

    /**
     * @ORM\ManyToOne(targetEntity=Cours::class, inversedBy="presences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cours;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_at;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class, inversedBy="presences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $saison;

    public function __construct()
    {
        $this->create_at=new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPratiquant(): ?Pratiquant
    {
        return $this->pratiquant;
    }

    public function setPratiquant(?Pratiquant $pratiquant): self
    {
        $this->pratiquant = $pratiquant;

        return $this;
    }

    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(?Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }
}
