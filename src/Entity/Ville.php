<?php

namespace App\Entity;

use App\Repository\VilleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VilleRepository::class)
 */
class Ville
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=CP::class, inversedBy="villes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomMaj;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomMin;

    /**
     * @ORM\OneToMany(targetEntity=Utilisateur::class, mappedBy="town")
     */
    private $utilisateurs;

    /**
     * @ORM\ManyToMany(targetEntity=RegleTarifaire::class, mappedBy="villes")
     */
    private $regleTarifaires;

    public function __construct()
    {
        $this->utilisateurs = new ArrayCollection();
        $this->regleTarifaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCp(): ?CP
    {
        return $this->cp;
    }

    public function setCp(?CP $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getNomMaj(): ?string
    {
        return $this->nomMaj;
    }

    public function setNomMaj(string $nomMaj): self
    {
        $this->nomMaj = $nomMaj;

        return $this;
    }

    public function getNomMin(): ?string
    {
        return $this->nomMin;
    }

    public function setNomMin(string $nomMin): self
    {
        $this->nomMin = $nomMin;

        return $this;
    }

    /**
     * @return Collection|Utilisateur[]
     */
    public function getUtilisateurs(): Collection
    {
        return $this->utilisateurs;
    }

    public function addUtilisateur(Utilisateur $utilisateur): self
    {
        if (!$this->utilisateurs->contains($utilisateur)) {
            $this->utilisateurs[] = $utilisateur;
            $utilisateur->setTown($this);
        }

        return $this;
    }

    public function removeUtilisateur(Utilisateur $utilisateur): self
    {
        if ($this->utilisateurs->removeElement($utilisateur)) {
            // set the owning side to null (unless already changed)
            if ($utilisateur->getTown() === $this) {
                $utilisateur->setTown(null);
            }
        }

        return $this;
    }
    public function __toString(): string
    {
        return $this->getNomMaj();
    }

    /**
     * @return Collection|RegleTarifaire[]
     */
    public function getRegleTarifaires(): Collection
    {
        return $this->regleTarifaires;
    }

    public function addRegleTarifaire(RegleTarifaire $regleTarifaire): self
    {
        if (!$this->regleTarifaires->contains($regleTarifaire)) {
            $this->regleTarifaires[] = $regleTarifaire;
            $regleTarifaire->addVille($this);
        }

        return $this;
    }

    public function removeRegleTarifaire(RegleTarifaire $regleTarifaire): self
    {
        if ($this->regleTarifaires->removeElement($regleTarifaire)) {
            $regleTarifaire->removeVille($this);
        }

        return $this;
    }
}
