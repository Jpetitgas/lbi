import '../dossier/mensuration.js'

const buttons = document.querySelectorAll('.progress-button');
const stepNumbers = document.querySelectorAll('.step-number');
const items = document.querySelectorAll('.tab-item');
const alerts=document.querySelectorAll('.alert')

alerts.forEach(alert => {
    if (alert.classList.contains('bulletin')){
        alert.addEventListener('click', () => {
            activeStep('bulletin');
            activeItem('bulletin');
            activeButton('bulletin')
        })
    }
    if (alert.classList.contains('pratiquant')) {
        alert.addEventListener('click', () => {
            activeStep('pratiquant');
            activeItem('pratiquant');
            activeButton('pratiquant')
        })
    }
    if (alert.classList.contains('paiement')) {
        alert.addEventListener('click', () => {
            activeStep('paiement');
            activeItem('paiement');
            activeButton('paiement')
        })
    }
});

function activeStep(step) {
    stepNumbers.forEach(stepNumber => {
        stepNumber.classList.remove('active');
        let idStepNumber = stepNumber.id;
        if (idStepNumber.split('-')[0] == step) {
            stepNumber.classList.add('active');
        }
    })
}
function activeItem(step) {
    items.forEach(item => {
        item.classList.remove('show');
        item.classList.remove('active');
        let idItem = item.id;
        if (idItem == step) {
            item.classList.add('show');
            item.classList.add('active');
        }
    })
}
function activeButton(step) {
    buttons.forEach(button => {
        button.classList.remove('active');
        let idButton = button.id;
        if (idButton.split('-')[0] == step) {
            button.classList.add('active');
        }
    })
}
buttons.forEach(button => {
    button.addEventListener('click', () => {
        let id = button.id;
        let step = id.split('-')[0];
        activeStep(step);
    })
});
stepNumbers.forEach(stepNumber => {
    if (!stepNumber.classList.contains('disabled')) {
        stepNumber.addEventListener('click', () => {
        let id = stepNumber.id;
        let step = id.split('-')[0];
        activeStep(step);
        activeItem(step);
        activeButton(step)
    })
    }    
})


let doc = document.querySelectorAll(".doc")
for (let select of doc) {
    select.addEventListener("change", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/utilisateur/inscription/document/' + this.dataset.id + '/' + this.value)
        xmlhttp.send()
    })
}

const stripe = Stripe(strikePublicCle);
var checkoutButton = document.getElementById('checkout-button');
if (checkoutButton != undefined) {
    checkoutButton.addEventListener('click', function () {
        // Create a new Checkout Session using the server-side endpoint you
        // created in step 3.
        fetch('/dossier/create-checkout-session', {
            method: 'POST',
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (session) {
                return stripe.redirectToCheckout({ sessionId: session.id });
            })
            .then(function (result) {
                // If `redirectToCheckout` fails due to a browser or network
                // error, you should display the localized error message to your
                // customer using `error.message`.
                if (result.error) {
                    alert(result.error.message);
                }
            })
            .catch(function (error) {
                console.error('Error:', error);
            });
    });
}

