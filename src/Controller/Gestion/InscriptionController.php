<?php

namespace App\Controller\Gestion;

use DateTime;
use App\Service\Gps;
use App\Service\EnCours;
use App\Entity\Inscription;
use App\Form\InscriptionType;
use App\Event\InscriptionEvent;
use App\Service\StatutInscription;
use App\Repository\CoursRepository;
use App\SearchData\InscriptionData;
use App\SearchForm\InscriptionForm;
use App\Repository\DossierRepository;
use App\Service\VerificationActivite;
use App\Repository\DocumentRepository;
use App\Repository\PresenceRepository;
use App\Repository\PratiquantRepository;
use App\Repository\InformationRepository;
use App\Repository\InscriptionRepository;
use App\Repository\UtilisateurRepository;
use phpDocumentor\Reflection\Types\Null_;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/inscription", name="gestion_inscription")
 */
class InscriptionController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(EnCours $enCours, PaginatorInterface $paginator, Request $request, InscriptionRepository $inscriptionRepository, PresenceRepository $presenceRepository): Response
    {
        $data = new InscriptionData();

        $form = $this->createForm(InscriptionForm::class, $data);
        $form->handleRequest($request);

        $saison = $enCours->saison();


        $inscriptions = $paginator->paginate(
            $inscriptionRepository->inscriptions($saison, $data),
            $request->query->getInt('page', 1),
            $data->nb,
        );



        return $this->render('gestion/inscription/index.html.twig', [
            'inscriptions' => $inscriptions,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/deleteSession", name="_deleteSession", methods={"GET"})
     */
    public function deleteSession(Request $request)
    {
        $request->getSession()->remove('inscriptionData');

        return $this->redirectToRoute('gestion_inscription_index');
    }

    /**
     * @Route("/{id}/edit/{id_dossier}", name="_edit", methods={"GET","POST"})
     */
    public function edit($id_dossier, Request $request, VerificationActivite $verificationActivite, Inscription $inscription, DossierRepository $dossierRepository)
    {
        $dossier = $dossierRepository->find($id_dossier);
        $formInscription = $this->createForm(InscriptionType::class, $inscription);
        $formInscription->handleRequest($request);
        $pratiquant = $inscription->getPratiquant();

        if ($formInscription->isSubmitted() && $formInscription->isValid()) {
            if ($verificationActivite->certificatMedical($inscription->getActivite())) {
                $inscription->setCertificatObligatoire(true);
            } else {
                $inscription->setCertificatObligatoire(false);
            }

            $inscription->setDateChangementStatut(new DateTime());

            $this->getDoctrine()->getManager()->flush();
            if ($formInscription->offsetExists('ajouter')) {
                if ($formInscription->get('ajouter')->isClicked()) {
                    return $this->redirectToRoute('gestion_document_new', ['id_pratiquant' => $inscription->getPratiquant()->getId(), 'id_dossier' => $dossier->getId()]);
                }
            }
            $ancre = 'pratiquant_' . $inscription->getPratiquant()->getId() . '_inscription';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $id_dossier, 'ancre' => $ancre]);
        }

        return $this->render('gestion/inscription/edit.html.twig', [
            'pratiquant' => $pratiquant,
            'dossier' => $dossier,
            'inscription' => $inscription,
            'formInscription' => $formInscription->createView(),
        ]);
    }

    /**
     * @Route("/new/{id_pratiquant}/{id_dossier}", name="_new", methods={"GET","POST"})
     */
    public function new($id_pratiquant, EventDispatcherInterface $dispatcher, $id_dossier, StatutInscription $statutInscription, VerificationActivite $verificationActivite, DossierRepository $dossierRepository, PratiquantRepository $pratiquantRepository, Request $request)
    {
        $inscription = new Inscription();
        $formInscription = $this->createForm(InscriptionType::class, $inscription);
        $formInscription->handleRequest($request);

        $pratiquant = $pratiquantRepository->find($id_pratiquant);
        $dossier = $dossierRepository->find($id_dossier);

        $inscription->setPratiquant($pratiquant);


        if ($formInscription->isSubmitted() && $formInscription->isValid()) {
            $inscription
                ->setDossier($dossier)
                ->setStatut($statutInscription->statut($inscription->getActivite()))
                ->setDateChangementStatut(new DateTime())
                ->setCertificatValide(false)
                ->setTransmisAssureur((false))
                ->setCanceled(0);
            if ($verificationActivite->certificatMedical($inscription->getActivite())) {
                $inscription->setCertificatObligatoire(true);
            } else {
                $inscription->setCertificatObligatoire(false);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($inscription);
            $entityManager->flush();

            $inscriptionEvent = new InscriptionEvent($inscription);
            $dispatcher->dispatch($inscriptionEvent, 'ajout.inscription');
            if ($formInscription->offsetExists('ajouter')) {
                if ($formInscription->get('ajouter')->isClicked()) {
                    return $this->redirectToRoute('gestion_document_new', ['id_pratiquant' => $pratiquant->getId(), 'id_dossier' => $dossier->getId()]);
                }
            }
            $ancre = 'pratiquant_' . $pratiquant->getId() . '_inscription';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $id_dossier, 'ancre' => $ancre]);
        }

        return $this->render('gestion/inscription/new.html.twig', [
            'pratiquant' => $pratiquant,
            'dossier' => $dossier,
            'inscription' => $inscription,
            'formInscription' => $formInscription->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, EventDispatcherInterface $dispatcher, StatutInscription $StatutInscription, Inscription $inscription): Response
    {
        $dossier = $inscription->getDossier();
        $ancre = 'pratiquant_' . $inscription->getPratiquant()->getId() . '_inscription';
        if ($this->isCsrfTokenValid('delete' . $inscription->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            $inscriptionEvent = new InscriptionEvent($inscription);
            $dispatcher->dispatch($inscriptionEvent, 'suppression.inscription');
            $entityManager->remove($inscription);
            $entityManager->flush();
        }

        /*$StatutInscription->deleteInscription(($inscription));*/

        return $this->redirectToRoute('gestion_dossier_edit', ['id' => $inscription->getDossier()->getId(), 'ancre' => $ancre]);
    }

    /**
     * @Route("/localisation/{id}", name="_localisation", methods={"GET"})
     */
    public function localisationDossier($id, Gps $gps, UtilisateurRepository $utilisateurRepository, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        $coordonneeAsso = $gps->coordonnees($information->getAdresse() . ' ' . $information->getCodePostal() . ' ' . $information->getVille());

        $utilisateur = $utilisateurRepository->find($id);
        $villesCoordonnees = [];

        $coordonnees = $gps->coordonnees($utilisateur->getAdresse() . ' ' . $utilisateur->getCodePostal() . ' ' . $utilisateur->getVille());
        if (!empty($coordonnees)) {
            $commune = ['ref' => $utilisateur->getNom() . ' ' . $utilisateur->getPrenom(), 'latitude' => $coordonnees['latitude'], 'longitude' => $coordonnees['longitude']];
            array_push($villesCoordonnees, $commune);
        }

        return $this->render('gestion/gps/localisation.html.twig', [
            'titre' => "Localisation de l'utilisateur : " . $utilisateur->getNom() . ' ' . $utilisateur->getPrenom(),
            'coordonneeAsso' => $coordonneeAsso,
            'villes' => $villesCoordonnees,
        ]);
    }

    /**
     * @Route("/certificatvaliderGroupe/{ids}", name="_certificatvaliderGroupe", methods={"GET"})
     */
    public function certificatValiderGroupe($ids, Request $request, InscriptionRepository $inscriptionRepository): Response
    {
        $referer = $request->headers->get('referer');
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $inscription = $inscriptionRepository->find($id);
            $inscription->setCertificatValide(($inscription->getCertificatValide() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
        }
        $em->flush();


        return new Response('Les certificats ont changé d\'état', Response::HTTP_OK);
    }

    /**
     * @Route("/certificatvalider/{id}", name="_certificatvalider", methods={"GET"})
     */
    public function certificatValider($id, Request $request, InscriptionRepository $inscriptionRepository): Response
    {
        $inscription = $inscriptionRepository->find($id);
        $inscription->setCertificatValide(($inscription->getCertificatValide() ? '0' : '1'));
        $em = $this->getDoctrine()->getManager();

        $em->flush();
        $this->addFlash('success', 'Les certificats ont changé d\'état');

        return $this->redirectToRoute('gestion_inscription_index');
    }

    /**
     * @Route("/transmisassureurgroupe/{ids}", name="_transmisassureurgroupe", methods={"GET", "POST"})
     */
    public function transmisAssureurGroupe($ids, InscriptionRepository $inscriptionRepository): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $inscription = $inscriptionRepository->find($id);
            $inscription->setTransmisAssureur(($inscription->getTransmisAssureur() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
        }
        $em->flush();


        return new Response('Les transmissions à l\'assureur ont changé d\'état', Response::HTTP_OK);
    }

    /**
     * @Route("/transmisassureur/{id}", name="_transmisassureur", methods={"GET", "POST"})
     */
    public function transmisAssureur($id, InscriptionRepository $inscriptionRepository): Response
    {
        $inscription = $inscriptionRepository->find($id);
        $inscription->setTransmisAssureur(($inscription->getTransmisAssureur() ? '0' : '1'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($inscription);

        $em->flush();
        $this->addFlash('success', 'Les inscriptions ont changé d\'état');

        return $this->redirectToRoute('gestion_inscription_index');
    }
    /**
     * @Route("/canceled/{ids}", name="_canceled", methods={"GET", "POST"})
     */
    public function Canceled($ids, InscriptionRepository $inscriptionRepository): Response
    {
        $ids = explode(',', $ids);
        $em = $this->getDoctrine()->getManager();
        foreach ($ids as $id) {
            $inscription = $inscriptionRepository->find($id);
            $inscription->setcanceled(($inscription->getcanceled() ? '0' : '1'));
            $em->persist($inscription);
        }

        $em->flush();
        $this->addFlash('success', 'L\'annulation de l\'inscription a changé d\'état');

        return new Response('L\'annulation de l\'inscription a changé d\'état', Response::HTTP_OK);
    }

    /**
     * @Route("/cours/{id_inscription}/{id_cours}", name="_cours", methods={"GET"})
     */
    public function cours($id_inscription, $id_cours, InscriptionRepository $inscriptionRepository, CoursRepository $coursRepository): Response
    {
        $cours = $coursRepository->find($id_cours);
        $inscription = $inscriptionRepository->find($id_inscription);
        $inscription->setCours($cours);
        $em = $this->getDoctrine()->getManager();
        $em->persist($inscription);
        $em->flush();

        return new Response('Le cours a été modifié', Response::HTTP_OK);
    }

    /**
     * @Route("/document/{id_inscription}/{id_document}", name="_document", methods={"GET","POST"})
     */
    public function document($id_inscription, $id_document, InscriptionRepository $inscriptionRepository, DocumentRepository $documentRepository)
    {
        $inscription = $inscriptionRepository->find($id_inscription);
        $document = $documentRepository->find($id_document);
        $inscription->setDocument($document);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->redirectToRoute('gestion_inscription_index');
    }

    /**
     * @Route("/mail/{ids}", name="_mail", methods={"GET"})
     */
    public function mail($ids, InscriptionRepository $inscriptionRepository): Response
    {
        return $this->redirectToRoute('gestion_inscription_index');
    }
}
