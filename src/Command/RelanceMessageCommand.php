<?php

namespace App\Command;

use App\Service\EnCours;
use App\Message\SendEmailMessage;
use App\Repository\MessagesRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Console\Output\OutputInterface;


class RelanceMessageCommand extends Command
{
    protected static $defaultName = 'app:relance';
    protected static $defaultDescription = 'Relance des utilisateurs qui ont des messages non lus';
    protected MessagesRepository $messagesRepository;
    protected EnCours $enCours;
    //protected SendEmailMessage $sendEmailMessage;
    protected MessageBusInterface $messageBus;

    public function __construct(MessagesRepository $messagesRepository, MessageBusInterface $messageBus, EnCours $enCours)
    {    //, SendEmailMessage $sendEmailMessage

        parent::__construct();
        $this->messagesRepository = $messagesRepository;
        $this->enCours = $enCours;
        //$this->sendEmailMessage=$sendEmailMessage;
        $this->messageBus = $messageBus;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $saison = $this->enCours->saison();
        $utilisateurs = $this->messagesRepository->nonOuvertParSaison($saison);
        if ($utilisateurs) {
            foreach ($utilisateurs as $contact) {
                $this->messageBus->dispatch(new SendEmailMessage(
                    $contact['email'],
                    'Message(s) non-lu(s)',
                    'email/emailRelance.html.twig',
                    $contact,
                    "vous avez {$contact['non_lu']} message(s) non lu(s)"
                ));
            }
        }


        return Command::SUCCESS;
    }
}
