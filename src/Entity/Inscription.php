<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\InscriptionRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InscriptionRepository::class)
 */
class Inscription
{
    public const LISTE_PRINCIPALE = "LISTE PRINCIPALE";
    public const LISTE_ATTENTE = "LISTE D'ATTENTE";
    public const LISTE_EN_ATTENTE = "EN ATTENTE";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Pratiquant::class, inversedBy="inscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pratiquant;

    /**
     * @ORM\ManyToOne(targetEntity=Activite::class, inversedBy="inscriptions")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $activite;

    /**
     * @ORM\ManyToOne(targetEntity=Dossier::class, inversedBy="inscriptions", fetch="LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dossier;

    /**
     * @ORM\ManyToOne(targetEntity=Document::class, inversedBy="Inscription")
     */
    private $document;

    /**
     * @ORM\Column(type="boolean")
     */
    private $CertificatObligatoire;

    /**
     * @ORM\Column(type="boolean")
     */
    private $CertificatValide;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateChangementStatut;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $remarque;

    /**
     * @ORM\ManyToOne(targetEntity=Cours::class, inversedBy="inscriptions", fetch="LAZY")
     */
    private $cours;

    /**
     * @ORM\Column(type="boolean")
     */
    private $transmisAssureur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $demande;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $demandeParticuliere;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private $canceled;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $canceledDate;

    private $presenceCours;

    public function getPresenceCours(): ?int
    {
        return $this->presenceCours;
    }
    public function setPresenceCours($PresenceCours)
    {
        $this->presenceCours = $PresenceCours;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPratiquant(): ?Pratiquant
    {
        return $this->pratiquant;
    }

    public function setPratiquant(?Pratiquant $pratiquant): self
    {
        $this->pratiquant = $pratiquant;

        return $this;
    }

    public function getActivite(): ?Activite
    {
        return $this->activite;
    }

    public function setActivite(?Activite $activite): self
    {
        $this->activite = $activite;

        return $this;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    public function getDocument(): ?Document
    {
        return $this->document;
    }

    public function setDocument(?Document $document): self
    {
        $this->document = $document;

        return $this;
    }

    public function getCertificatObligatoire(): ?bool
    {
        return $this->CertificatObligatoire;
    }

    public function setCertificatObligatoire(bool $CertificatObligatoire): self
    {
        $this->CertificatObligatoire = $CertificatObligatoire;

        return $this;
    }

    public function getCertificatValide(): ?bool
    {
        return $this->CertificatValide;
    }

    public function setCertificatValide(bool $CertificatValide): self
    {
        $this->CertificatValide = $CertificatValide;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getDateChangementStatut(): ?\DateTimeInterface
    {
        return $this->dateChangementStatut;
    }

    public function setDateChangementStatut(\DateTimeInterface $dateChangementStatut): self
    {
        $this->dateChangementStatut = $dateChangementStatut;

        return $this;
    }
    public function __toString(): string
    {
        return $this->getActivite();
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(?Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    public function getTransmisAssureur(): ?bool
    {
        return $this->transmisAssureur;
    }

    public function setTransmisAssureur(bool $transmisAssureur): self
    {
        $this->transmisAssureur = $transmisAssureur;

        return $this;
    }

    public function getDemande(): ?string
    {
        return $this->demande;
    }

    public function setDemande(?string $demande): self
    {
        $this->demande = $demande;

        return $this;
    }

    public function getDemandeParticuliere(): ?string
    {
        return $this->demandeParticuliere;
    }

    public function setDemandeParticuliere(?string $demandeParticuliere): self
    {
        $this->demandeParticuliere = $demandeParticuliere;

        return $this;
    }

    public function isCanceled(): ?bool
    {
        return $this->canceled;
    }

    public function setCanceled(?int $canceled)
    {
        $this->canceled = $canceled;
        if ($canceled) {
            $this->canceledDate = new \DateTime();
        }
        return $this;
    }
    public function getCanceled(): ?bool
    {
        return $this->canceled;
    }

    public function getcanceledDate(): ?\DateTimeInterface
    {
        return $this->canceledDate;
    }
    public function __construct()
    {
        $this->canceledDate = new DateTime();
    }
}
