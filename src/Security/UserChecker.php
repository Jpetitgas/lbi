<?php

namespace App\Security;

use App\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $utilisateur): void
    {
        if (!$utilisateur instanceof Utilisateur) {
            return;
        }
    }

    public function checkPostAuth(UserInterface $utilisateur): void
    {
        if (!$utilisateur instanceof Utilisateur) {
            return;
        }
        if (!$utilisateur->getIsVerified()) {
            throw new CustomUserMessageAccountStatusException("Votre compte n'est pas actif. Veuillez verifier vos emails pour activer votre compte avant le {$utilisateur->getAccountMustBeVerifiedBefore()->format('d/m/y à H\hi')}");
        }
    }
}
