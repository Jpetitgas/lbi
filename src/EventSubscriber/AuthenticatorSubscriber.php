<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Http\Event\DeauthenticatedEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class AuthenticatorSubscriber implements EventSubscriberInterface
{
    private LoggerInterface $securityLogger;
    private RequestStack $requestStack;

    public function __construct(LoggerInterface $securityLogger, RequestStack $requestStack)
    {
        $this->securityLogger = $securityLogger;
        $this->requestStack = $requestStack;
    }

    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onSecurityAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onSecurityAuthenticationSuccess',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            'Symfony\Component\Security\Http\Event\LogoutEvents' => 'onSecurityLogout',
            'security.logout_on_change' => 'onSecurityLogoutOnChange',
            SecurityEvents::SWITCH_USER => 'onSecuritySwitchUser',
        ];
    }

    public function onSecurityAuthenticationFailure(AuthenticationFailureEvent $event): void
    {
        ['user_IP' => $userIP] = $this->getRouteNameAndUserIP();
        $securityToken = $event->getAuthenticationToken();
        ['email'=>$emailEntered]=$securityToken->getCredentials();
        $this->securityLogger->info("Un utilisateur anonyme ayant l'adresse IP '{$userIP}' a tenté de s'authentifier sans succés avec email suivant :'{$emailEntered}'");
    }

    public function onSecurityAuthenticationSuccess(AuthenticationSuccessEvent $event): void
    {
        [
            'user_IP' => $userIP,
            'route_name' => $routeName,
        ] = $this->getRouteNameAndUserIP();

        if (empty($event->getAuthenticationToken()->getRoleNames())) {
            $this->securityLogger->info("Un utilisateur anonyme ayant l'adresse IP '{$userIP}' est apparu sur la route :'{$routeName}'");
        } else {
            $securityToken = $event->getAuthenticationToken();
            $userEmail = $this->getUserEmail($securityToken);
            $this->securityLogger->info("Un utilisateur anonyme ayant l'adresse IP '{$userIP}' a evolué en entité utilisateur avec l'email :'{$userEmail}'");
        }
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event): void
    {
        // ...
    }

    public function onSecurityLogout(LogoutEvent $event): void
    {
        // ...
    }

    public function onSecurityLogoutOnChange(DeauthenticatedEvent $event): void
    {
        // ...
    }

    public function onSecuritySwitchUser(SwitchUserEvent $event): void
    {
        // ...
    }

    private function getRouteNameAndUserIP(): array
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request) {
            return [
                'user_IP' => 'Inconnue',
                'route_name' => 'Inconnue',
            ];
        }

        return [
                'user_IP' => $request->getClientIp() ?? 'Inconnue',
                'route_name' => $request->attributes->get('_route'),
            ];
    }

    private function getUserEmail(TokenInterface $securityToken): string
    {
        /** @var Utilisateur $user */
        $user = $securityToken->getUser();

        return $user->getEmail();
    }
}
