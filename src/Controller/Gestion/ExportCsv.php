<?php

namespace App\Controller\Gestion;

use App\Service\EnCours;
use App\Service\Csv;
use App\Repository\InscriptionRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Psr\Log\LoggerInterface;

/**
 * @Route("/gestion/exportCsv", name="gestion_exportCsv")
 */
class ExportCsv extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/adherents", name="_adherents")
     */
    public function adherents(
        EnCours $encours,
        InscriptionRepository $inscriptionRepository,
        Csv $csvService
    ) {
        $this->logger->info('adherents method called');

        try {
            $saison = $encours->saison();
            $this->logger->info('Saison retrieved: ' . $saison);

            $inscriptions = $inscriptionRepository->inscriptionsParSaisonContact($saison);
            $this->logger->info('Inscriptions retrieved: ' . count($inscriptions));

            $response = new StreamedResponse(function () use ($inscriptions, $csvService) {
                $handle = fopen('php://output', 'w');
                if (!$handle) {
                    throw new \Exception('Failed to open output stream');
                }

                // Écrire l'en-tête du CSV
                $headerWritten = fputcsv($handle, [
                    'First Name',
                    'Last Name',
                    'Phone 1 - Type',
                    'Phone 1 - Value',
                    'Labels',
                ]);
                if ($headerWritten === false) {
                    throw new \Exception('Failed to write CSV header');
                }

                // Écrire les données
                foreach ($inscriptions as $inscription) {
                    $courseDetails = $csvService->extractCourseDetails($inscription['coursNom']);
                    $labels = implode(' ::: ', [
                        'TOUT',
                        $inscription['coursNom'],
                        $inscription['sectionNom'],
                        $inscription['intervenantNom'] . ' ' . $inscription['intervenantPrenom'],
                    ]);
                    $rowWritten = fputcsv($handle, [
                        $inscription['adherentNom'],
                        $inscription['adherentPrenom'],
                        'Mobile',
                        $inscription['adherentPortable'],
                        $labels,
                    ]);
                    if ($rowWritten === false) {
                        throw new \Exception('Failed to write CSV row');
                    }
                }

                $closeResult = fclose($handle);
                if ($closeResult === false) {
                    throw new \Exception('Failed to close output stream');
                }
            });

            // Définir les en-têtes pour le téléchargement
            $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment; filename="dossiers.csv"');

            $this->logger->info('CSV response created successfully');

            return $response;
        } catch (\Exception $e) {
            $this->logger->error('Error in adherents method: ' . $e->getMessage());
            throw $e;
        }
    }
}
