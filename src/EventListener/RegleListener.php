<?php

namespace App\EventListener;

use App\Entity\RegleTarifaire;
use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class RegleListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison=$encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $regle=$event->getObject();

        if ($regle instanceof RegleTarifaire) {
            if ($this->saison) {
                $regle->setSaison($this->saison);
            }
        }
    }
}
