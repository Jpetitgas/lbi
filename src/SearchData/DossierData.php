<?php

namespace App\SearchData;

class DossierData
{
    public $q='';

    public $bulletinPresent;
    public $nbCertificatManquant;
    public $cotisationAJour;

    public $bulletinValide;

    public $dossierVerrouille;
    public $locked;

    public $nb=20;
}
