<?php

namespace App\EventListener;

use App\Entity\Presence;
use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class PresentListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison=$encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $present=$event->getObject();

        if ($present instanceof Presence) {
            $present->setSaison($this->saison);
        }
    }
}
