<?php

namespace App\Controller\Gestion\Evenement;

use App\Entity\Option;
use App\Form\OptionType;
use App\Repository\EvenementRepository;
use App\Repository\OptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *  @Route("/gestion/evenement/option", name="gestion_evenement_option")
 */
class OptionController extends AbstractController
{
    /**
     * @Route("/new/{id_evenement}", name="_new", methods={"GET", "POST"})
     */
    public function new($id_evenement, EvenementRepository $evenementRepository, Request $request, EntityManagerInterface $entityManager): Response
    {
        $evenement=$evenementRepository->find($id_evenement);
        $option = new Option();
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $option->setEvenement($evenement);
            $entityManager->persist($option);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_evenement_edit', ['id' => $evenement->getid(),'ancre'=>'option'], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gestion/evenement/option/new.html.twig', [
            'evenement'=>$evenement,
            'option' => $option,
            'form' => $form,
        ]);
    }



    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Option $option, EvenementRepository $evenementRepository, EntityManagerInterface $entityManager): Response
    {
        $evenement=$evenementRepository->find($option->getEvenement());
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('gestion_evenement_edit', ['id' => $evenement->getid(),'ancre'=>'option'], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gestion/evenement/option/edit.html.twig', [
            'evenement'=>$evenement,
            'option' => $option,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, EvenementRepository $evenementRepository, Option $option, EntityManagerInterface $entityManager): Response
    {
        $evenement=$option->getEvenement();
        if ($this->isCsrfTokenValid('delete'.$option->getId(), $request->request->get('_token'))) {
            $entityManager->remove($option);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_evenement_edit', ['id' => $evenement->getid(),'ancre'=>'option'], Response::HTTP_SEE_OTHER);
    }
}
