import './styles/css/tableau.scss';
import 'jquery'
const $ = require('jquery');
global.$ = $;
import 'jquery-ui'

import '@popperjs/core/lib/popper'
import 'bootstrap-5'
import 'bootstrap-4'

import 'tableexport.jquery.plugin/tableExport.min.js'

import 'dragtable'
import 'bootstrap-table'

import 'bootstrap-table/dist/extensions/export/bootstrap-table-export'
import 'bootstrap-table/dist/extensions/addrbar/bootstrap-table-addrbar'
import 'bootstrap-table/dist/extensions/cookie/bootstrap-table-cookie.min.js'
import 'bootstrap-table/dist/locale/bootstrap-table-fr-FR.min.js'
import 'bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control'
/*import 'bootstrap-table/dist/extensions/reorder-columns/bootstrap-table-reorder-columns'*/
