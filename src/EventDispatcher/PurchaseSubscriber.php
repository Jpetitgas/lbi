<?php

namespace App\EventDispatcher;

use App\Entity\Messages;
use App\Service\SendEmail;
use App\Event\PurchaseEvent;
use App\Message\SendEmailMessage;
use App\Repository\SujetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PurchaseSubscriber implements EventSubscriberInterface
{
    protected $messageBus;
    protected $sujetRepository;
    protected $entityManager;
    private SendEmail $sendEmail;

    public function __construct(MessageBusInterface $messageBus, SendEmail $sendEmail, SujetRepository $sujetRepository, EntityManagerInterface $entityManager)
    {
        $this->messageBus = $messageBus;
        $this->sujetRepository = $sujetRepository;
        $this->entityManager = $entityManager;
        $this->sendEmail = $sendEmail;
    }

    public static function getSubscribedEvents()
    {
        return [
            'ajout.purchase' => 'ajoutPurchase',
            'suppression.purchase' => 'suppressionPurchase'
        ];
    }

    public function ajoutPurchase(PurchaseEvent $purchaseEvent)
    {
        $purchase = $purchaseEvent->getInscription();

        $sujet = $this->sujetRepository->findOneBy(['sujet' => 'Boutique']);
        $message = $purchase->getUtilisateur()->getPrenom() . ' ' . $purchase->getUtilisateur()->getNom() . ' a passé une nouvelle commande';
        if ($sujet) {
            $envoi = new Messages();
            $envoi->setExpediteur($sujet->getDestinataire())
                ->setDestinataire($purchase->getUtilisateur())
                ->setTitre($sujet->getSujet())
                ->setMessage('Votre commande n° ' . $purchase->getId() . ' a été enregistrée');
            $this->entityManager->persist($envoi);
            $this->entityManager->flush();
        }

        // $this->messageBus->dispatch(new SendEmailMessage(
        //     $purchase->getUtilisateur()->getEmail(),
        //     'Vous avez reçu un nouveau message',
        //     'email/infoMessage.html.twig',
        //     []
        // ));
        $this->sendEmail->send([
            'recipient_email' => $purchase->getUtilisateur()->getEmail(),
            'sujet' => 'Vous avez reçu un nouveau message',
            'htmlTemplate' => 'email/infoMessage.html.twig',
            'destinataire' => $purchase->getUtilisateur(),
            'context' => [],
        ]);

        // $this->messageBus->dispatch(new SendEmailMessage(
        //     $sujet->getDestinataire()->getEmail(),
        //     'Nouvelle commande',
        //     'email/infoMessageContact.html.twig',
        //     ['contenu' => $message]
        // ));
        $this->sendEmail->send([
            'recipient_email' => $sujet->getDestinataire()->getEmail(),
            'sujet' => 'Nouvelle commande',
            'htmlTemplate' => 'email/infoMessageContact.html.twig',
            'destinataire' => $sujet->getDestinataire(),
            'context' => ['contenu' => $message],
        ]);



        return;
    }
    public function suppressionPurchase(PurchaseEvent $purchaseEvent)
    {
        $purchase = $purchaseEvent->getInscription();

        $sujet = $this->sujetRepository->findOneBy(['sujet' => 'Boutique']);
        $message = 'La commande de ' . $purchase->getUtilisateur()->getPrenom() . ' ' . $purchase->getUtilisateur()->getNom() . ' a été annulée ';
        if ($sujet) {
            $envoi = new Messages();
            $envoi->setExpediteur($sujet->getDestinataire())
                ->setDestinataire($purchase->getUtilisateur())
                ->setTitre($sujet->getSujet())
                ->setMessage('Votre commande n° ' . $purchase->getId() . ' a été annulée');
            $this->entityManager->persist($envoi);
            $this->entityManager->flush();
        }

        // $this->messageBus->dispatch(new SendEmailMessage(
        //     $purchase->getUtilisateur()->getEmail(),
        //     'Vous avez reçu un nouveau message',
        //     'email/infoMessage.html.twig',
        //     []
        // ));
        $this->sendEmail->send([
            'recipient_email' => $purchase->getUtilisateur()->getEmail(),
            'sujet' => 'Vous avez reçu un nouveau message',
            'htmlTemplate' => 'email/infoMessage.html.twig',
            'destinataire' => $purchase->getUtilisateur(),
            'context' => [],
        ]);

        // $this->messageBus->dispatch(new SendEmailMessage(
        //     $sujet->getDestinataire()->getEmail(),
        //     'Annulation d\'inscription',
        //     'email/infoMessageContact.html.twig',
        //     ['contenu' => $message]
        // ));
        $this->sendEmail->send([
            'recipient_email' => $sujet->getDestinataire()->getEmail(),
            'sujet' => 'Annulation de commande',
            'htmlTemplate' => 'email/infoMessageContact.html.twig',
            'destinataire' => $sujet->getDestinataire(),
            'context' => ['contenu' => $message],
        ]);


        return;
    }
}
