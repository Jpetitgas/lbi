<?php

namespace App\Controller\Utilisateur\Boutique;

use App\Service\Cart;
use DateTimeImmutable;
use App\Entity\Purchase;
use App\Entity\PurchaseItem;
use App\Event\PurchaseEvent;
use App\Repository\InformationRepository;
use App\Stripe\StripeService;

use App\Repository\PurchaseRepository;
use App\Repository\PaiementBoutiqueRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PurchaseController extends AbstractController
{
    /**
     * @Route("/utilisateur/boutique/purchase/index", name="utilisateur_boutique_purchase_index")
     */
    public function index(PurchaseRepository $purchaseRepository): Response
    {
        $purchases = $purchaseRepository->findBy(['utilisateur' => $this->getUser()], ['id' => 'DESC']);

        return $this->render('utilisateur/boutique/purchase/index.html.twig', [
          'purchases' => $purchases,

        ]);
    }

    /**
     * @Route("/utilisateur/boutique/purchase/confirmation", name="utilisateur_boutique_purchase_confirmation")
     */
    public function confirmation(Cart $cart, InformationRepository $informationRepository, EventDispatcherInterface $dispatcher, RouterInterface $router): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getBoutiqueOuvert() == false) {
            $this->addFlash('danger', 'La boutique n\'est ouverte aux commandes ');

            return $this->redirectToRoute('utilisateur_boutique');
        }

        $cartItems = $cart->getDetailedCartItems();
        if (!$cartItems) {
            $this->addFlash('danger', 'Votre panier est vide!');

            return $this->redirectToRoute('utilisateur_boutique');
        }
        $purchase = new Purchase();
        $purchase->setUtilisateur($this->getUser())
            ->setPurchaseAt(new DateTimeImmutable())
            ->setTotal($cart->getTotal());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($purchase);

        foreach ($cartItems as $cartItem) {
            $purchaseItem = new PurchaseItem();
            $purchaseItem->setPurchase($purchase)
              ->setTaille($cartItem->taille)
              ->setProductName($cartItem->taille->getProduct()->getName().'('.$cartItem->taille->getName().')')
              ->setQuantity($cartItem->qty)
              ->setProductPrice($cartItem->taille->getPrice())
              ->setTotal($cartItem->getTotal());

            $entityManager->persist($purchaseItem);
        }

        $entityManager->flush();
        $cart->empty();
        $purchaseEvent = new PurchaseEvent($purchase);
        $dispatcher->dispatch($purchaseEvent, 'ajout.purchase');

        $this->addFlash('success', 'Votre commande est enregistrée');

        return new RedirectResponse($router->generate('utilisateur_boutique_purchase_index'));
    }

    /**
     * @Route("/utilisateur/boutique/purchase/paiement/{id_purchase}", name="utilisateur_boutique_purchase_paiement")
     */
    public function paiement($id_purchase, PaiementBoutiqueRepository $paiementBoutiqueRepository, InformationRepository $informationRepository, PurchaseRepository $purchaseRepository, StripeService $stripeService): Response
    {
        $information = $informationRepository->findOneBy([]);
        $purchase = $purchaseRepository->find($id_purchase);
        if (!$purchase) {
        }

        $paiements = $paiementBoutiqueRepository->findBy(['purchase' => $id_purchase]);
        $totalPaiement = 0;
        foreach ($paiements as $paiement) {
            $totalPaiement += $paiement->getMontant();
        }
        $solded=($totalPaiement-$purchase->getTotal())==0 ? true : false;
        return $this->render('utilisateur/boutique/purchase/paiement.html.twig', [
          'purchase' => $purchase,
          'totalPaiement'=>$totalPaiement,
          'solded' => $solded,
          'stripePublicCle' => $stripeService->ObtenirPublicCle(),
          'stripe' => $information->getBoutiqueStripe(),
          'paiement'=>$information->getBoutiquePaiement()
        ]);
    }
}
