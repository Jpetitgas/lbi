
    let present = document.getElementById('present')
    present.addEventListener("click", function () {

        let id_cours = document.getElementById('cours').value;

        let selecteds = document.querySelectorAll(".selected")
        let ids = [];
        for (let selected of selecteds) {
            ids.push(selected.querySelector('.sorting_1').textContent);
        }

        if (id_cours != "Choisir un cours") {
            if (ids.length) {
                const Url = new URL(window.location.href);
                fetch(Url.pathname + "ajout/", {
                    method: 'POST',
                    body: JSON.stringify({
                        cours: id_cours,
                        pratiquants: ids
                    }),
                    headers: {
                        "Content-type": "application/json"
                    }
                }).then(function (response) {
                    if (response.ok) {
                        alert('la liste des presents a été envoyé');

                    } else {
                        alert('Il y a eu un pb avec l\'application: la liste des presents n\'a pas été enregistrée!');
                    }
                })
                    .catch(function (error) {
                        console.log('Il y a eu un problème lors de l\'opération: ' + error.message);
                    });
            } else {
                alert('Vous devez d\'abord selectionner une ligne');
            }
        } else {
            alert('Vous devez d\'abord selectionner un cours');
        }
    });
