<?php

namespace App\Repository;

use App\Entity\CategorieSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategorieSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieSection[]    findAll()
 * @method CategorieSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieSectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieSection::class);
    }

    // /**
    //  * @return CategorieSection[] Returns an array of CategorieSection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategorieSection
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
