<?php

namespace App\Entity;

use App\Repository\UtilisateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UtilisateurRepository::class)
 * @UniqueEntity(fields={"email"}, message="Cet email est déja liée à un compte")
 */
class Utilisateur implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="Veuillez saisir une valeur.")
     * @Assert\Email(message="L'email {{value}} n'est pas valide.")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Veuillez saisir une valeur.")
     * @Assert\NotCompromisedPassword(message="Veuillez utiliser un autre mot de passe")
     * @Assert\Regex(pattern="/^(?=.*[a-zà-ÿ])(?=.*[A-ZÀ-Ý])(?=.*[0-9])(?=.*[^a-zà-ÿA-ZÀ-Ý0-9]).{6,}$/", message="Pour des raisons de sécurité, votre mot de pass doit contenir au minimum 6 caratères dont 1 lettres majuscule, 1 chiffre et un caractère spécial")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Pratiquant::class, mappedBy="utilisateur",fetch="EAGER")
     */
    private $pratiquants;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $adresse;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank
    * @Assert\Regex(pattern="/^0\d{9}$/", message="N° de téléphone invalide.", groups={"order"})
    */
    private $portable;

    /**
     * @ORM\OneToMany(targetEntity=Dossier::class, mappedBy="adherent",fetch="EAGER")
     */
    private $dossiers;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $prenom;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class, mappedBy="intervenant")
     */
    private $cours;

    /**
     * @ORM\OneToMany(targetEntity=Messages::class, mappedBy="expediteur", orphanRemoval=true)
     */
    private $sent;

    /**
     * @ORM\OneToMany(targetEntity=Messages::class, mappedBy="destinataire", orphanRemoval=true)
     */
    private $received;

    private $nonLu;

    /**
     * @ORM\OneToMany(targetEntity=Sujet::class, mappedBy="destinataire")
     */
    private $sujets;

    /**
     * @ORM\OneToMany(targetEntity=Chat::class, mappedBy="expediteur")
     */
    private $chats;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isVerified = false;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $registeredAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $accountMustBeVerifiedBefore;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string  $registrationToken;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $accountVerifiedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $forgotPasswordToken;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $forgotPasswordTokenRequestedAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $forgotPasswordTokenMustBeVerifiedBefore;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $forgotPasswordTokenVerifiedAt;

    /**
     * @ORM\OneToMany(targetEntity=Place::class, mappedBy="utilisateur")
     */
    private $places;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $remarque;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $derniereConnexion;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $avantDerniereConnexion;

    /**
     * @ORM\OneToMany(targetEntity=ConsultationChat::class, mappedBy="utilisateur", orphanRemoval=true)
     */
    private $consultationChats;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quotaSms;

    /**
     * @ORM\OneToMany(targetEntity=Purchase::class, mappedBy="utilisateur")
     */
    private $purchases;

    /**
     * @ORM\OneToMany(targetEntity=PaiementEvenement::class, mappedBy="utilisateur")
     */
    private $paiementEvenements;

    /**
     * @ORM\OneToMany(targetEntity=Todolist::class, mappedBy="utilisateur", orphanRemoval=true)
     */
    private $todolists;

    /**
     * @ORM\ManyToOne(targetEntity=CP::class, inversedBy="utilisateurs")
     */
    private $CP;

    /**
     * @ORM\ManyToOne(targetEntity=Ville::class, inversedBy="utilisateurs")
     */
    private $town;

    /**
     * @ORM\OneToMany(targetEntity=OptionPurchase::class, mappedBy="utilisateur")
     */
    private $optionPurchases;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class)
     */
    private $saison;

    /**
     * @ORM\ManyToMany(targetEntity=Video::class, mappedBy="utilisateurs")
     */
    private $videos;

    /**
     * @ORM\OneToMany(targetEntity=UserChatNotification::class, mappedBy="user")
     */
    private $userChatNotifications;



    public function __construct()
    {
        $this->pratiquants = new ArrayCollection();
        $this->dossiers = new ArrayCollection();
        $this->cours = new ArrayCollection();
        $this->sent = new ArrayCollection();
        $this->received = new ArrayCollection();
        $this->sujets = new ArrayCollection();
        $this->chats = new ArrayCollection();
        $this->isVerified = false;
        $this->registeredAt = new \DateTimeImmutable('now');
        $this->roles = ['ROLE_USER'];
        $this->accountMustBeVerifiedBefore = (new \DateTimeImmutable('now'))->add(new \DateInterval('P1D'));
        $this->places = new ArrayCollection();
        $this->consultationChats = new ArrayCollection();
        $this->purchases = new ArrayCollection();
        $this->paiementEvenements = new ArrayCollection();
        $this->todolists = new ArrayCollection();
        $this->optionPurchases = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->userChatNotifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Pratiquant[]
     */
    public function getPratiquants(): Collection
    {
        return $this->pratiquants;
    }

    public function addPratiquant(Pratiquant $pratiquant): self
    {
        if (!$this->pratiquants->contains($pratiquant)) {
            $this->pratiquants[] = $pratiquant;
            $pratiquant->setUtilisateur($this);
        }

        return $this;
    }

    public function removePratiquant(Pratiquant $pratiquant): self
    {
        if ($this->pratiquants->removeElement($pratiquant)) {
            // set the owning side to null (unless already changed)
            if ($pratiquant->getUtilisateur() === $this) {
                $pratiquant->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }



    public function getPortable(): ?string
    {
        return $this->portable;
    }

    public function setPortable(?string $portable): self
    {
        $this->portable = $portable;

        return $this;
    }

    /**
     * @return Collection|Dossier[]
     */
    public function getDossiers(): Collection
    {
        return $this->dossiers;
    }

    public function addDossier(Dossier $dossier): self
    {
        if (!$this->dossiers->contains($dossier)) {
            $this->dossiers[] = $dossier;
            $dossier->setAdherent($this);
        }

        return $this;
    }

    public function removeDossier(Dossier $dossier): self
    {
        if ($this->dossiers->removeElement($dossier)) {
            // set the owning side to null (unless already changed)
            if ($dossier->getAdherent() === $this) {
                $dossier->setAdherent(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNom().' '.$this->getPrenom();
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setIntervenant($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->removeElement($cour)) {
            // set the owning side to null (unless already changed)
            if ($cour->getIntervenant() === $this) {
                $cour->setIntervenant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Messages[]
     */
    public function getSent(): Collection
    {
        return $this->sent;
    }

    public function addSent(Messages $sent): self
    {
        if (!$this->sent->contains($sent)) {
            $this->sent[] = $sent;
            $sent->setExpediteur($this);
        }

        return $this;
    }

    public function removeSent(Messages $sent): self
    {
        if ($this->sent->removeElement($sent)) {
            // set the owning side to null (unless already changed)
            if ($sent->getExpediteur() === $this) {
                $sent->setExpediteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Messages[]
     */
    public function getReceived(): Collection
    {
        return $this->received;
    }

    public function addReceived(Messages $received): self
    {
        if (!$this->received->contains($received)) {
            $this->received[] = $received;
            $received->setDestinataire($this);
        }

        return $this;
    }

    public function removeReceived(Messages $received): self
    {
        if ($this->received->removeElement($received)) {
            // set the owning side to null (unless already changed)
            if ($received->getDestinataire() === $this) {
                $received->setDestinataire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sujet[]
     */
    public function getSujets(): Collection
    {
        return $this->sujets;
    }

    public function addSujet(Sujet $sujet): self
    {
        if (!$this->sujets->contains($sujet)) {
            $this->sujets[] = $sujet;
            $sujet->setDestinataire($this);
        }

        return $this;
    }

    public function removeSujet(Sujet $sujet): self
    {
        if ($this->sujets->removeElement($sujet)) {
            // set the owning side to null (unless already changed)
            if ($sujet->getDestinataire() === $this) {
                $sujet->setDestinataire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Chat[]
     */
    public function getChats(): Collection
    {
        return $this->chats;
    }

    public function addChat(Chat $chat): self
    {
        if (!$this->chats->contains($chat)) {
            $this->chats[] = $chat;
            $chat->setExpediteur($this);
        }

        return $this;
    }

    public function removeChat(Chat $chat): self
    {
        if ($this->chats->removeElement($chat)) {
            // set the owning side to null (unless already changed)
            if ($chat->getExpediteur() === $this) {
                $chat->setExpediteur(null);
            }
        }

        return $this;
    }

    public function getIsVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getRegisteredAt(): \DateTimeImmutable
    {
        return $this->registeredAt;
    }

    public function setRegisteredAt(\DateTimeImmutable $registeredAt): self
    {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    public function getAccountMustBeVerifiedBefore(): \DateTimeImmutable
    {
        return $this->accountMustBeVerifiedBefore;
    }

    public function setAccountMustBeVerifiedBefore(\DateTimeImmutable $accountMustBeVerifiedBefore): self
    {
        $this->accountMustBeVerifiedBefore = $accountMustBeVerifiedBefore;

        return $this;
    }

    public function getRegistrationToken(): ?string
    {
        return $this->registrationToken;
    }

    public function setRegistrationToken(?string $registrationToken): self
    {
        $this->registrationToken = $registrationToken;

        return $this;
    }

    public function getAccountVerifiedAt(): ?\DateTimeImmutable
    {
        return $this->accountVerifiedAt;
    }

    public function setAccountVerifiedAt(?\DateTimeImmutable $accountVerifiedAt): self
    {
        $this->accountVerifiedAt = $accountVerifiedAt;

        return $this;
    }

    public function getForgotPasswordToken(): ?string
    {
        return $this->forgotPasswordToken;
    }

    public function setForgotPasswordToken(?string $forgotPasswordToken): self
    {
        $this->forgotPasswordToken = $forgotPasswordToken;

        return $this;
    }

    public function getForgotPasswordTokenRequestedAt(): ?\DateTimeImmutable
    {
        return $this->forgotPasswordTokenRequestedAt;
    }

    public function setForgotPasswordTokenRequestedAt(?\DateTimeImmutable $forgotPasswordTokenRequestedAt): self
    {
        $this->forgotPasswordTokenRequestedAt = $forgotPasswordTokenRequestedAt;

        return $this;
    }

    public function getForgotPasswordTokenMustBeVerifiedBefore(): ?string
    {
        return $this->forgotPasswordTokenMustBeVerifiedBefore;
    }

    public function setForgotPasswordTokenMustBeVerifiedBefore(string $forgotPasswordTokenMustBeVerifiedBefore): self
    {
        $this->forgotPasswordTokenMustBeVerifiedBefore = $forgotPasswordTokenMustBeVerifiedBefore;

        return $this;
    }

    public function getForgotPasswordTokenVerifiedAt(): ?\DateTimeImmutable
    {
        return $this->forgotPasswordTokenVerifiedAt;
    }

    public function setForgotPasswordTokenVerifiedAt(?\DateTimeImmutable $forgotPasswordTokenVerifiedAt): self
    {
        $this->forgotPasswordTokenVerifiedAt = $forgotPasswordTokenVerifiedAt;

        return $this;
    }

    /**
     * @return Collection|Place[]
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places[] = $place;
            $place->setUtilisateur($this);
        }

        return $this;
    }

    public function removePlace(Place $place): self
    {
        if ($this->places->removeElement($place)) {
            // set the owning side to null (unless already changed)
            if ($place->getUtilisateur() === $this) {
                $place->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getDerniereConnexion(): ?\DateTimeInterface
    {
        return $this->derniereConnexion;
    }

    public function setDerniereConnexion(?\DateTimeInterface $derniereConnexion): self
    {
        $this->derniereConnexion = $derniereConnexion;

        return $this;
    }

    public function getAvantDerniereConnexion(): ?\DateTimeImmutable
    {
        return $this->avantDerniereConnexion;
    }

    public function setAvantDerniereConnexion(?\DateTimeImmutable $avantDerniereConnexion): self
    {
        $this->avantDerniereConnexion = $avantDerniereConnexion;

        return $this;
    }

    /**
     * @return Collection|ConsultationChat[]
     */
    public function getConsultationChats(): Collection
    {
        return $this->consultationChats;
    }

    public function addConsultationChat(ConsultationChat $consultationChat): self
    {
        if (!$this->consultationChats->contains($consultationChat)) {
            $this->consultationChats[] = $consultationChat;
            $consultationChat->setUtilisateur($this);
        }

        return $this;
    }

    public function removeConsultationChat(ConsultationChat $consultationChat): self
    {
        if ($this->consultationChats->removeElement($consultationChat)) {
            // set the owning side to null (unless already changed)
            if ($consultationChat->getUtilisateur() === $this) {
                $consultationChat->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function getQuotaSms(): ?int
    {
        return $this->quotaSms;
    }

    public function setQuotaSms(?int $quotaSms): self
    {
        $this->quotaSms = $quotaSms;

        return $this;
    }

    /**
     * @return Collection|Purchase[]
     */
    public function getPurchases(): Collection
    {
        return $this->purchases;
    }

    public function addPurchase(Purchase $purchase): self
    {
        if (!$this->purchases->contains($purchase)) {
            $this->purchases[] = $purchase;
            $purchase->setUtilisateur($this);
        }

        return $this;
    }

    public function removePurchase(Purchase $purchase): self
    {
        if ($this->purchases->removeElement($purchase)) {
            // set the owning side to null (unless already changed)
            if ($purchase->getUtilisateur() === $this) {
                $purchase->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PaiementEvenement[]
     */
    public function getPaiementEvenements(): Collection
    {
        return $this->paiementEvenements;
    }

    public function addPaiementEvenement(PaiementEvenement $paiementEvenement): self
    {
        if (!$this->paiementEvenements->contains($paiementEvenement)) {
            $this->paiementEvenements[] = $paiementEvenement;
            $paiementEvenement->setUtilisateur($this);
        }

        return $this;
    }

    public function removePaiementEvenement(PaiementEvenement $paiementEvenement): self
    {
        if ($this->paiementEvenements->removeElement($paiementEvenement)) {
            // set the owning side to null (unless already changed)
            if ($paiementEvenement->getUtilisateur() === $this) {
                $paiementEvenement->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Todolist[]
     */
    public function getTodolists(): Collection
    {
        return $this->todolists;
    }

    public function addTodolist(Todolist $todolist): self
    {
        if (!$this->todolists->contains($todolist)) {
            $this->todolists[] = $todolist;
            $todolist->setUtilisateur($this);
        }

        return $this;
    }

    public function removeTodolist(Todolist $todolist): self
    {
        if ($this->todolists->removeElement($todolist)) {
            // set the owning side to null (unless already changed)
            if ($todolist->getUtilisateur() === $this) {
                $todolist->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function getCP(): ?CP
    {
        return $this->CP;
    }

    public function setCP(?CP $CP): self
    {
        $this->CP = $CP;

        return $this;
    }

    public function getTown(): ?Ville
    {
        return $this->town;
    }

    public function setTown(?Ville $town): self
    {
        $this->town = $town;

        return $this;
    }

    /**
     * @return Collection|OptionPurchase[]
     */
    public function getOptionPurchases(): Collection
    {
        return $this->optionPurchases;
    }

    public function addOptionPurchase(OptionPurchase $optionPurchase): self
    {
        if (!$this->optionPurchases->contains($optionPurchase)) {
            $this->optionPurchases[] = $optionPurchase;
            $optionPurchase->setUtilisateur($this);
        }

        return $this;
    }

    public function removeOptionPurchase(OptionPurchase $optionPurchase): self
    {
        if ($this->optionPurchases->removeElement($optionPurchase)) {
            // set the owning side to null (unless already changed)
            if ($optionPurchase->getUtilisateur() === $this) {
                $optionPurchase->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    /**
     * @return Collection<int, Video>
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->addUtilisateur($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->videos->removeElement($video)) {
            $video->removeUtilisateur($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, UserChatNotification>
     */
    public function getUserChatNotifications(): Collection
    {
        return $this->userChatNotifications;
    }

    public function addUserChatNotification(UserChatNotification $userChatNotification): self
    {
        if (!$this->userChatNotifications->contains($userChatNotification)) {
            $this->userChatNotifications[] = $userChatNotification;
            $userChatNotification->setUser($this);
        }

        return $this;
    }

    public function removeUserChatNotification(UserChatNotification $userChatNotification): self
    {
        if ($this->userChatNotifications->removeElement($userChatNotification)) {
            // set the owning side to null (unless already changed)
            if ($userChatNotification->getUser() === $this) {
                $userChatNotification->setUser(null);
            }
        }

        return $this;
    }
}
