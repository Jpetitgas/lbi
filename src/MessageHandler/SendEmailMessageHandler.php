<?php

namespace App\MessageHandler;

use App\Message\SendEmailMessage;
use App\Service\SendEmail;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;

class SendEmailMessageHandler implements MessageHandlerInterface
{
    private $sendEmail;
    private static $lastEmailSentTime = 0;
    private const MIN_INTERVAL = 3; // Minimum 0.5 second between emails

    public function __construct(SendEmail $sendEmail)
    {
        $this->sendEmail = $sendEmail;
    }

    public function __invoke(SendEmailMessage $message)
    {
        $this->rateLimitSleep();

        $retries = 0;
        $maxRetries = 3;

        while ($retries < $maxRetries) {
            try {
                $this->sendEmail->send([
                    'recipient_email' => $message->getRecipientEmail(),
                    'sujet' => $message->getSujet(),
                    'htmlTemplate' => $message->getHtmlTemplate(),
                    'destinataire' => $message->getDestinataire(),
                    'contenu' => $message->getContext(),
                ]);

                self::$lastEmailSentTime = microtime(true);
                return; // Email sent successfully
            } catch (\Exception $e) {
                if (strpos($e->getMessage(), 'Too many emails per second') !== false) {
                    $retries++;
                    $sleepTime = $this->calculateBackoff($retries);
                    sleep($sleepTime);
                } else {
                    // For other errors, don't retry
                    throw new UnrecoverableMessageHandlingException($e->getMessage(), $e->getCode(), $e);
                }
            }
        }

        // If we've exhausted all retries
        throw new UnrecoverableMessageHandlingException('Failed to send email after ' . $maxRetries . ' retries');
    }

    private function rateLimitSleep()
    {
        $currentTime = microtime(true);
        $timeSinceLastEmail = $currentTime - self::$lastEmailSentTime;

        if ($timeSinceLastEmail < self::MIN_INTERVAL) {
            $sleepTime = self::MIN_INTERVAL - $timeSinceLastEmail;
            usleep($sleepTime * 1000000); // Convert to microseconds
        }
    }

    private function calculateBackoff($retry)
    {
        return min(pow(2, $retry), 60); // Exponential backoff, max 60 seconds
    }
}
