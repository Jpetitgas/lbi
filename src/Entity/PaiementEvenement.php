<?php

namespace App\Entity;

use App\Repository\PaiementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaiementRepository::class)
 */
class PaiementEvenement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="float")
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=TypeDePaiement::class, inversedBy="paiements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeDePaiement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $encaissement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $remarque;

    /**
     * @ORM\ManyToOne(targetEntity=Banque::class, inversedBy="paiements")
     * @ORM\JoinColumn(nullable=true)
     */
    private $banque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="boolean")
     */
    private $encaisse;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class, inversedBy="paiements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $saison;

    /**
     * @ORM\ManyToOne(targetEntity=Evenement::class, inversedBy="paiementEvenements")
     */
    private $evenement;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="paiementEvenements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;



    public function getId(): ?int
    {
        return $this->id;
    }



    public function getMontant(): ?float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getTypeDePaiement(): ?TypeDePaiement
    {
        return $this->typeDePaiement;
    }

    public function setTypeDePaiement(?TypeDePaiement $typeDePaiement): self
    {
        $this->typeDePaiement = $typeDePaiement;

        return $this;
    }

    public function getEncaissement(): ?string
    {
        return $this->encaissement;
    }

    public function setEncaissement(?string $encaissement): self
    {
        $this->encaissement = $encaissement;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getBanque(): ?Banque
    {
        return $this->banque;
    }

    public function setBanque(?Banque $banque): self
    {
        $this->banque = $banque;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getEncaisse(): ?bool
    {
        return $this->encaisse;
    }

    public function setEncaisse(bool $encaisse): self
    {
        $this->encaisse = $encaisse;

        return $this;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    public function getEvenement(): ?Evenement
    {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): self
    {
        $this->evenement = $evenement;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }
}
