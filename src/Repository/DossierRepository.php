<?php

namespace App\Repository;

use App\Entity\Dossier;
use App\Entity\Saison;
use App\SearchData\DossierData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dossier|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dossier|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dossier[]    findAll()
 * @method Dossier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DossierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dossier::class);
    }

    public function dossierCourant($adherent, $saison)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.adherent = ?1')
            ->setParameter(1, $adherent)
            ->andWhere('d.saison = ?2')
            ->setParameter(2, $saison)

            //->addOrderBy('d.id','DESC')
            //->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function nbPratiquantParSaison($saison)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.saison = :saison')
            ->setParameter('saison', $saison)
            ->join('d.inscriptions', 'i')
            ->groupBy('i.pratiquant')
            ->getQuery()
            ->getResult();
    }

    public function dossiers(Saison $saison, DossierData $data)
    {
        $query = $this
            ->createQueryBuilder('d')
            ->select('d', 'u')
            ->join('d.adherent', 'u')
            ->where('d.saison = :saison')
            ->setParameter('saison', $saison);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }

        if ($data->bulletinPresent === true) {
            $query = $query->andWhere('d.bulletinAdhesion IS NOT NULL');
        }
        if ($data->bulletinPresent === false) {
            $query = $query->andWhere('d.bulletinAdhesion IS NULL');
        }

        if ($data->bulletinValide === true) {
            $query = $query->andWhere('d.bulletinValide =1');
        }
        if ($data->bulletinValide === false) {
            $query = $query->andWhere('d.bulletinValide =0');
        }
        if ($data->locked === true) {
            $query = $query->andWhere('d.locked =1');
        }
        if ($data->locked === false) {
            $query = $query->andWhere('d.locked =0');
        }

        if ($data->dossierVerrouille === true) {
            $query = $query->andWhere('d.dossierVerrouille =1');
        }
        if ($data->dossierVerrouille === false) {
            $query = $query->andWhere('d.dossierVerrouille =0');
        }

        return $query->getQuery();
    }

    public function findUsersWithDossierInPreviousSeasonButNotCurrent($previousSeason, $currentSeason)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $subQuery = $qb->select('IDENTITY(d2.adherent)')
            ->from('App\Entity\Dossier', 'd2')
            ->where('d2.saison = :currentSeason')
            ->getDQL();

        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from('App\Entity\Utilisateur', 'u')
            ->innerJoin('App\Entity\Dossier', 'd', 'WITH', 'd.adherent = u')
            ->where('d.saison = :previousSeason')
            ->andWhere($qb->expr()->notIn('u.id', '(' . $subQuery . ')'))
            ->setParameter('previousSeason', $previousSeason)
            ->setParameter('currentSeason', $currentSeason);

        return $query->getQuery()->getResult();
    }
}
