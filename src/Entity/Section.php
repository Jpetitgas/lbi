<?php

namespace App\Entity;

use App\Repository\SectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SectionRepository::class)
 */
class Section
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Activite::class, mappedBy="section",fetch="EAGER")
     */
    private $activites;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $refAssureur;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieSection::class, inversedBy="sections")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorieSection;

    public function __construct()
    {
        $this->activites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Activite[]
     */
    public function getActivites(): Collection
    {
        return $this->activites;
    }

    public function addActivite(Activite $activite): self
    {
        if (!$this->activites->contains($activite)) {
            $this->activites[] = $activite;
            $activite->setSection($this);
        }

        return $this;
    }

    public function removeActivite(Activite $activite): self
    {
        if ($this->activites->removeElement($activite)) {
            // set the owning side to null (unless already changed)
            if ($activite->getSection() === $this) {
                $activite->setSection(null);
            }
        }

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getRefAssureur(): ?string
    {
        return $this->refAssureur;
    }

    public function setRefAssureur(string $refAssureur): self
    {
        $this->refAssureur = $refAssureur;

        return $this;
    }

    public function getCategorieSection(): ?CategorieSection
    {
        return $this->categorieSection;
    }

    public function setCategorieSection(?CategorieSection $categorieSection): self
    {
        $this->categorieSection = $categorieSection;

        return $this;
    }
    public function __toString(): string
    {
        return $this->getLibelle() ;
    }
}
