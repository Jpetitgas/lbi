<?php

namespace App\Controller\Gestion;

use App\Service\Gps;
use DateTimeImmutable;
use App\Service\EnCours;
use App\Entity\Utilisateur;
use Doctrine\DBAL\Exception;
use App\Form\UtilisateurType;
use App\SearchData\UtilisateurData;
use App\SearchForm\UtilisateurForm;
use App\Repository\SaisonRepository;
use App\Repository\InformationRepository;
use App\Repository\UtilisateurRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/utilisateur", name="gestion_utilisateur")
 */
class UtilisateurGestionController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, EnCours $encours, Request $request, UtilisateurRepository $utilisateurRepository, SaisonRepository $saisonRepository): Response
    {
        $data = new UtilisateurData();
        $form = $this->createForm(UtilisateurForm::class, $data);
        $form->handleRequest($request);
        $saisonEnCours = $encours->saison();
        $utilisateurs = $paginator->paginate(
            $utilisateurRepository->utilisateurs($data),
            $request->query->getInt('page', 1),
            $data->nb
        );

        return $this->render('gestion/utilisateur/index.html.twig', [
            'saisonEnCours' => $saisonEnCours->getId(),
            'utilisateurs' => $utilisateurs,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, $id, UtilisateurRepository $utilisateurRepository): Response
    {
        $utilisateur = $utilisateurRepository->find($id);
        $formUtilisateur = $this->createForm(UtilisateurType::class, $utilisateur);
        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_utilisateur_index');
        }

        return $this->render('gestion/utilisateur/edit.html.twig', [
             'utilisateur' => $utilisateur,
            'formUtilisateur' => $formUtilisateur->createView(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $utilisateur = new Utilisateur();
        $formUtilisateur = $this->createForm(UtilisateurType::class, $utilisateur);
        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($utilisateur);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_utilisateur_index');
        }

        return $this->render('gestion/utilisateur/new.html.twig', [
            'utilisateur' => $utilisateur,
            'formUtilisateur' => $formUtilisateur->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="_show", methods={"GET","POST"})
     */
    public function show(Request $request): Response
    {
        return $this->render('gestion/utilisateur/show.html.twig');
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Utilisateur $utilisateur): Response
    {
        try {
            if ($this->isCsrfTokenValid('delete'.$utilisateur->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($utilisateur);
                $entityManager->flush();
            }
        } catch (Exception  $e) {
            $this->addFlash('danger', 'Vous devez supprimer tous les éléments de cet utilisateur avant de le supprimer');
        }

        return $this->redirectToRoute('gestion_utilisateur_index');
    }

    /**
     * @Route("/localisation/{id}", name="_localisation", methods={"GET"})
     */
    public function localisationDossier($id, Gps $gps, UtilisateurRepository $utilisateurRepository, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        $coordonneeAsso = $gps->coordonnees($information->getAdresse().' '.$information->getCodePostal().' '.$information->getVille());

        $utilisateur = $utilisateurRepository->find($id);
        $villesCoordonnees = [];

        $coordonnees = $gps->coordonnees($utilisateur->getAdresse().' '.$utilisateur->getCodePostal().' '.$utilisateur->getVille());
        if (!empty($coordonnees)) {
            $commune = ['ref' => $utilisateur->getNom().' '.$utilisateur->getPrenom(), 'latitude' => $coordonnees['latitude'], 'longitude' => $coordonnees['longitude']];
            array_push($villesCoordonnees, $commune);
        }

        return $this->render('gestion/gps/localisation.html.twig', [
            'titre' => "Localisation de l'utilisateur : ".$utilisateur->getNom().' '.$utilisateur->getPrenom(),
            'coordonneeAsso' => $coordonneeAsso,
            'villes' => $villesCoordonnees,
        ]);
    }

    /**
     * @Route("/verifiedGroupe/{ids}", name="_verifiedGroupe", methods={"GET"})
     */
    public function verifiedGroupe($ids, UtilisateurRepository $utilisateurRepository): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $utilisateur = $utilisateurRepository->find($id);
            try {
                if ($utilisateur) {
                    $utilisateur->setIsVerified(($utilisateur->getIsVerified() ? '0' : '1'))
                            ->setAccountVerifiedAt(new DateTimeImmutable());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($utilisateur);
                }
            } catch (Exception  $e) {
                return new Response('Cet tulisateur n\'existe pas', Response::HTTP_OK);
            }
        }
        $em->flush();

        return new Response('Les comptes ont changé d\'état', Response::HTTP_OK);
    }
}
