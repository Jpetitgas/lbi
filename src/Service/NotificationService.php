<?php

namespace App\Service;

use App\Repository\SaisonRepository;
use App\Repository\DossierRepository;
use App\Repository\InformationRepository;
use App\Message\SendEmailMessage;
use Symfony\Component\Messenger\MessageBusInterface;

class NotificationService
{
    private $saisonRepository;
    private $dossierRepository;
    private $informationRepository;
    private $messageBus;


    public function __construct(
        SaisonRepository $saisonRepository,
        DossierRepository $dossierRepository,
        InformationRepository $informationRepository,
        MessageBusInterface $messageBus
    ) {
        $this->saisonRepository = $saisonRepository;
        $this->dossierRepository = $dossierRepository;
        $this->informationRepository = $informationRepository;
        $this->messageBus = $messageBus;
    }

    public function notifyUsersWithoutCurrentDossier()
    {
        $currentSeason = $this->saisonRepository->saisonCourant();
        $previousSeason = $this->saisonRepository->saisonPrecedente();

        if (!$currentSeason || !$previousSeason) {
            return false;
        }

        $usersToNotify = $this->dossierRepository->findUsersWithDossierInPreviousSeasonButNotCurrent($previousSeason, $currentSeason);

        $information = $this->informationRepository->findOneBy([]);

        foreach ($usersToNotify as $user) {
            $this->dispatchNotificationEmail($user, $information);
        }

        return true;
    }

    private function dispatchNotificationEmail($user, $information)
    {
        $message = new SendEmailMessage(
            $user->getEmail(),
            'Rappel : Dossier pour la nouvelle saison',
            'email/dossier_reminder.html.twig',
            [
                'utilisateur' => $user,
                'nomStructure' => $information->getNomStructure(),
                'signatureMail' => $information->getSignature(),
            ]
        );

        $this->messageBus->dispatch($message);
    }
}
