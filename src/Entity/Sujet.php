<?php

namespace App\Entity;

use App\Repository\SujetRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SujetRepository::class)
 */
class Sujet
{
    public const SUJET_PAIEMENT_LIGNE="Paiement en ligne";
    public const SUJET_LISTE_ATTENTE="Liste d'attente";
    public const SUJET_INSCRIPTION="Inscription";
    public const SUJET_BOUTIQUE="Boutique";
    public const SUJET_MESSAGE_INTERVENANT="Message à un intervenant";
    public const SUJET_INTERVENANT="Message à un administrateur";
    public const SUJET_FORMULAIRE_CONTACT="Question divers";


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sujet;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="sujets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $destinataire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $categorie;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $verrouille;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSujet(): ?string
    {
        return $this->sujet;
    }

    public function setSujet(string $sujet): self
    {
        $this->sujet = $sujet;

        return $this;
    }

    public function getDestinataire(): ?Utilisateur
    {
        return $this->destinataire;
    }

    public function setDestinataire(?Utilisateur $destinataire): self
    {
        $this->destinataire = $destinataire;

        return $this;
    }
    public function __toString(): string
    {
        return $this->getSujet();
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getVerrouille(): ?bool
    {
        return $this->verrouille;
    }

    public function setVerrouille(?bool $verrouille): self
    {
        $this->verrouille = $verrouille;

        return $this;
    }
}
