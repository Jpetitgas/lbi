<?php

namespace App\Service;

use App\Entity\CategorieSection;
use App\Entity\Saison;
use App\Entity\Utilisateur;
use App\Repository\DossierRepository;
use App\Repository\SaisonRepository;
use Symfony\Component\Security\Core\Security;

/**
 * [saison en cours].
 */
class EnCours
{
    protected $security;
    protected $dossierRepository;
    protected $saisonRepository;

    public function __construct(SaisonRepository $saisonRepository, DossierRepository $dossierRepository, Security $security)
    {
        $this->saisonRepository = $saisonRepository;
        $this->dossierRepository = $dossierRepository;
        $this->security = $security;
    }

    public function dossierAdherentConnecte()
    {
        $adherent = $this->security->getUser();
        /* @var Dossier $dossier*/
        $dossier = $this->dossierRepository->dossierCourant($adherent, $this->saison());

        return $dossier;
    }

    public function dossierAdherent(Utilisateur $adherent)
    {
        $dossier = $this->dossierRepository->dossierCourant($adherent, $this->saison());

        return $dossier;
    }

    public function saison()
    {
        if ($this->security->getUser()) {
            $adherent = $this->security->getUser();
            if ($adherent->getSaison()) {
                return $adherent->getSaison();
            }
        }

        $saison = $this->saisonRepository->saisonCourant();

        return $saison;
    }

    public function avantDerniereSaison()
    {

        $saison = $this->saisonRepository->saisonCourant();

        return $saison;
    }

    public function UtilisateursParSection(CategorieSection $categorieSection)
    {
        /** @var Saison $saison */
        $saison = $this->saison();
        $dossiers = $saison->getDossiers();
        $utilisateurs = [];
        foreach ($dossiers as $dossier) {
            $inscriptions = $dossier->getInscriptions();
            foreach ($inscriptions as $inscription) {
                if ($inscription->getActivite()->getSection()->getCategorieSection() == $categorieSection) {
                    if (!in_array($inscription->getDossier()->getAdherent(), $utilisateurs)) {
                        array_push($utilisateurs, $inscription->getDossier()->getAdherent());
                    }
                }
            }
        }
        return $utilisateurs;
    }
}
