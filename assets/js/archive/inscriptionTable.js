$(document).ready(function () {
   

    var table = $("#inscriptionTable").DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
           this.api().columns([2,3,4,6,8,17]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
        'dom': "Bfrtip",
        'scrollY': "700px",
        'scrollX': true,
        'scrollCollapse': true,
        'paging': false,

        'buttons': [

            {
                'extend': 'colvisGroup',
                'text': 'Réduire',
                'show': [1, 6, 8, 10],
                'hide': [2, 3, 4, 5, 7, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22],
            },
            {
                'extend': 'colvisGroup',
                'text': 'Coordonées',
                'show': [1, 6, 8, 10, 11, 13, 14, 15],
                'hide': [2, 3, 4, 5, 7, 9, 12, 16, 17, 18, 19, 20, 21, 22],
            },


            {
                'extend': 'colvisGroup',
                'text': 'Modifier cours',
                'show': [4, 5, 6, 7, 10, 12],
                'hide': [1, 2, 3, 8, 9, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]
            },
            {
                'extend': 'colvisGroup',
                'text': 'Contrôle certificat',
                'show': [16, 17, 18, 19],
                'hide': [1, 2, 3, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 20, 21, 22]
            },
            {
                'extend': 'colvisGroup',
                'text': 'Tout',
                'show': ':hidden'
            },

            'colvis',
            {
                'extend': 'csvHtml5',
                'text': 'Export',
                'exportOptions': {
                    'columns': [3, 6, 9, 10, 12, 13, 14, 15, 16]
                }
            }
        ],
        'columnDefs': [

            { "width": "5px", "targets": [0, 1] },
            {
                'orderable': false,
                'className': 'select-checkbox',
                'targets': 0,
            }],
        'fixedColumns': false,
        'select': {
            'style': 'multi',
            'selector': 'td:first-child'
        },
        'order': [[1, 'asc']],
        "responsive": true,
        "language": {
            'buttons': {
                'colvis': 'Colonnes cachées'
            },
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            }
        },
    });

    $(".selectAll").on("click", function (e) {
        if ($(this).is(":checked")) {
            table.rows({ search: 'applied' }).select()
        } else {
            table.rows({ search: 'applied' }).deselect();
        }
    });


    

});