<?php

namespace App\Controller\Stripe;

use App\Entity\Messages;
use App\Service\SendEmail;
use App\Stripe\StripeService;
use App\Entity\PaiementEvenement;
use App\Message\SendEmailMessage;
use App\Service\EvenementService;
use App\Repository\SujetRepository;
use App\Repository\BanqueRepository;
use App\Repository\EvenementRepository;
use App\Repository\InformationRepository;
use App\Repository\UtilisateurRepository;
use App\Repository\OptionPurchaseRepository;
use App\Repository\TypeDePaiementRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StripeEvenementController extends AbstractController
{
    /**
     *  @Route("/evenement/create-checkout-session", name="evenement_checkout", methods={"POST"})
     */
    public function checkout(Request $request, StripeService $stripeService, OptionPurchaseRepository $optionPurchaseRepository, InformationRepository $informationRepository, EvenementService $evenementService, UtilisateurRepository $utilisateurRepository, EvenementRepository $evenementRepository)
    {
        $params = json_decode($request->getContent(), true);
        extract($params);
        $information = $informationRepository->findOneBy([]);
        if ($information->getStripe() == false) {
            $this->addFlash('danger', 'Le paiement par Stripe n\'est pas autorisé!');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        \Stripe\Stripe::setApiKey($stripeService->ObtenirSecretCle());
        $utilisateur = $utilisateurRepository->find($id_utilisateur);
        $evenement = $evenementRepository->find($id_evenement);
        $reservationData = $evenementService->montantTotalReservation($utilisateur, $evenement);
        $montantReservation = $reservationData['totalReservation'];
        $montantPaiement = $evenementService->montantTotalPaiement($utilisateur, $evenement);
        $options = $optionPurchaseRepository->findByEvenementAndUtilisateur($evenement, $utilisateur);
        $montantOptions = 0;
        if ($options) {
            foreach ($options as $option) {
                $montantOptions += $option['price'] * $option['qty'];
            }
        }
        $montant = ($montantReservation + $montantOptions) - $montantPaiement;

        $checkout_session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $montant * 100,
                    'product_data' => [
                        'name' => 'Réservation évenement',
                    ],
                ],
                'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $this->generateUrl('stripe_succes', ['id_evenement' => $id_evenement, 'id_utilisateur' => $id_utilisateur], UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('stripe_erreur', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        return new JsonResponse(['id' => $checkout_session->id]);
    }


    /**
     *  @Route("/stripe/succes/{id_evenement}/{id_utilisateur}", name="stripe_succes")
     */
    public function succes($id_evenement, $id_utilisateur, SendEmail $sendEmail, OptionPurchaseRepository $optionPurchaseRepository, EvenementService $evenementService, SujetRepository $sujetRepository, EvenementRepository $evenementRepository, UtilisateurRepository $utilisateurRepository, MessageBusInterface $messageBus, TypeDePaiementRepository $typeDePaiementRepository, BanqueRepository $banqueRepository)
    {
        $evenement = $evenementRepository->find($id_evenement);
        $utilisateur = $utilisateurRepository->find($id_utilisateur);

        $montantReservation = $evenementService->montantTotalReservation($utilisateur, $evenement);
        $options = $optionPurchaseRepository->findByEvenementAndUtilisateur($evenement, $utilisateur);
        $montantOptions = 0;
        if ($options) {
            foreach ($options as $option) {
                $montantOptions += $option['price'] * $option['qty'];
            }
        }

        $montantPaiement = $evenementService->montantTotalPaiement($utilisateur, $evenement);
        $montant = ($montantReservation['totalReservation'] + $montantOptions) - $montantPaiement;

        $typeDePaiement = $typeDePaiementRepository->findOneBy(['designation' => 'Carte bancaire']);
        $banque = $banqueRepository->findOneBy(['nom' => 'Stripe']);
        $paiement = new PaiementEvenement();
        $paiement->setEvenement($evenement)
            ->setUtilisateur($utilisateur)
            ->setBanque($banque)
            ->setMontant($montant)
            ->setTypeDePaiement($typeDePaiement)
            ->setEncaisse(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($paiement);
        $entityManager->flush();
        /** @var Sujet $sujet */
        $sujet = $sujetRepository->findOneBy(['sujet' => 'Paiement en ligne']);
        if ($sujet) {
            $envoi = new Messages();
            $envoi->setExpediteur($sujet->getDestinataire())
                ->setDestinataire($utilisateur)
                ->setTitre($sujet->getSujet())
                ->setMessage('Votre paiement s\'est correctement déroulé. Vous trouverez vos billet dans votre espace, dossier, récapitulatif');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($envoi);
            $entityManager->flush();
        }


        $sendEmail->send([
            'recipient_email' => $utilisateur->getEmail(),
            'sujet' => 'Paiement de votre réservation en ligne',
            'htmlTemplate' => 'email/paiementEvenement.html.twig',
            'destinataire' => $utilisateur,
            'contenu' => [
                'utilisateur' => $utilisateur,
                'montant' => $montant,
            ],
        ]);

        $this->addFlash('success', 'La réservation a été payée!');

        return $this->redirectToRoute('utilisateur_evenement_edit', ['id' => $id_evenement]);
    }

    /**
     *  @Route("/stripe/erreur", name="stripe_erreur")
     */
    public function erreur()
    {
        return $this->render('stripe/erreur.html.twig');
    }
}
