<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Security;


use App\Controller\Utilisateur\PratiquantController;
use App\Controller\Utilisateur\UtilisateurController;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

class ControllerListener
{
    protected $security;
    protected $em;

    public function __construct(EntityManager $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function onKernelController(ControllerEvent $event)
    {
        $controller = $event->getController();
        if (is_array($controller)) {
            $controller = $controller[0];
        }
        $utilisateur=$this->security->getUser();
        if ($controller instanceof UtilisateurController || $controller instanceof PratiquantController) {
            $this->em->getFilters()
                ->enable('utilisateur_conneted')
                ->setParameter('utilisateur', $utilisateur);
        }
    }
}
