<?php

namespace App\Command;

use App\Service\NotificationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SendDossierReminderCommand extends Command
{
    protected static $defaultName = 'app:send-dossier-reminder';
    protected static $defaultDescription = 'Envoie des rappels aux utilisateurs sans dossier pour la saison en cours';

    private NotificationService $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        parent::__construct();
        $this->notificationService = $notificationService;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Envoi des rappels de dossier');

        $result = $this->notificationService->notifyUsersWithoutCurrentDossier();

        if ($result === false) {
            $io->error('Impossible d\'envoyer les rappels : aucune saison courante ou précédente trouvée.');
            return Command::FAILURE;
        }

        $io->success('Rappels envoyés avec succès !');

        return Command::SUCCESS;
    }
}
