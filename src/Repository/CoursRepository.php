<?php

namespace App\Repository;

use App\Entity\Cours;
use App\Entity\Saison;
use App\Entity\Utilisateur;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Cours|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cours|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cours[]    findAll()
 * @method Cours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cours::class);
    }

    public function coursParSaison(Saison $saison)
    {
        return $this->createQueryBuilder('c')
            ->join('c.activite', 'a', 'WITH', 'c.activite=a.id')
            ->andWhere('a.saison = :saison')
            ->setParameter('saison', $saison)
            ->getQuery()
            ->getResult()
        ;
    }

    public function coursParSaisonParIntervenant($saison, $intervenant)
    {
        return $this->createQueryBuilder('c')
            ->where('c.saison = :saison')
            ->setParameter('saison', $saison)
            ->andWhere('c.intervenant = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->getQuery()
            ->getResult()
        ;
    }
    public function coursParSaisonIntervenant(Utilisateur $intervenant)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.intervenant = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->groupBy('c.nom')
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Cours[] Returns an array of Cours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cours
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
