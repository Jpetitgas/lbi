
var $table = $('#inscription');
function getRowSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
        return row;
    })
}


$('#message').click(function () {
    var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each(selectedRows, function (index, value) {
        ids.push(value.id);
    });
    if (ids.length) {
        window.location.href = "/gestion/correspondance/message/pratiquant/" + ids.toString();
    } else {
        alert('Vous devez d\'abord selectionner une ligne');
    }
});

$('#valider').click(function () {
    var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each(selectedRows, function (index, value) {
        ids.push(value.id);
    });
    if (ids.length) {
        var r = confirm("Etes-vous sur de vouloir valider ce(s) certificat(s) médical(aux)?");
        if (r == true) {
            window.location.href = "/gestion/inscription/certificatvaliderGroupe/" + ids.toString();
        }
    } else {
        alert('Vous devez d\'abord selectionner une ligne');
    }
});

$('#assureur').click(function () {
    var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each(selectedRows, function (index, value) {
        ids.push(value.id);
    });
    if (ids.length) {
        var r = confirm("Etes-vous sur de vouloir transmettre ces inscriptions?");
        if (r == true) {
            window.location.href = "/gestion/inscription/transmisassureurgroupe/" + ids.toString();
        }
    } else {
        alert('Vous devez d\'abord selectionner une ligne');
    }
});

/*function getval(id, sel) {
    window.location.href = '/gestion/inscription/cours/' + id + '/' + sel.value;
}*/
