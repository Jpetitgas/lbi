<?php

namespace App\Repository;

use App\Entity\Saison;
use App\Entity\Activite;
use App\Entity\CategorieActivite;
use App\SearchData\ActiviteData;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Activite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Activite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Activite[]    findAll()
 * @method Activite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActiviteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Activite::class);
    }

    public function tauxInscriptionsParActiviteParSaison($saison)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.saison = :saison')
            ->orderBy('a.nom', 'ASC')
            ->setParameter('saison', $saison)
            ->leftJoin('a.inscriptions', 'i')
            ->select('a.nom, a.nbDePlace')
            ->addSelect('COUNT(i.id) as nb')
            ->groupBy('a.nom')
            ->getQuery()
            ->getResult();
    }

    public function activiteParCategorie(?CategorieActivite $categorie, Saison $saison)
    {
        return $this->createQueryBuilder('a')
            ->where('a.saison = :saison')
            ->andWhere('a.categorie= :categorie')
            ->setParameter('saison', $saison)
            ->setParameter('categorie', $categorie);
    }
    public function activiteSansConditionParCategorie(?CategorieActivite $categorie, Saison $saison)
    {
        return $this->createQueryBuilder('a')
            ->where('a.saison = :saison')
            ->andWhere('a.categorie= :categorie')
            ->andWhere('a.sousCondition= false')
            ->orderBy('a.nom', 'ASC')
            ->setParameter('saison', $saison)
            ->setParameter('categorie', $categorie)
            ->getQuery()
            ->getResult();
    }
    public function activiteCategorie(?CategorieActivite $categorie, Saison $saison)
    {
        return $this->createQueryBuilder('a')
            ->where('a.saison = :saison')
            ->andWhere('a.categorie= :categorie')
            ->orderBy('a.nom', 'ASC')
            ->setParameter('saison', $saison)
            ->setParameter('categorie', $categorie)
            ->getQuery()
            ->getResult();
    }

    public function findByActiviteCriteria(Saison $saison, ActiviteData $data)
    {
        $query = $this
            ->createQueryBuilder('a')
            ->select('a', 'c', 'cs', 's')
            ->leftJoin('a.categorie', 'c')
            ->leftJoin('a.section', 's')
            ->leftJoin('s.categorieSection', 'cs')
            ->where('a.saison = :saison')
            ->setParameter('saison', $saison);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('a.nom LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }

        if (!empty($data->categorieAge)) {
            $query = $query
                ->andWhere('c.id = :age')
                ->setParameter('age', $data->categorieAge);
        }

        if (!empty($data->section)) {
            $query = $query
                ->andWhere('cs.id = :categorieSection')
                ->setParameter('categorieSection', $data->section);
        }

        if ($data->condition !== null) {
            $query = $query
                ->andWhere('a.sousCondition = :sousCondition')
                ->setParameter('sousCondition', $data->condition);
        }

        if ($data->certificat !== null) {
            $query = $query
                ->andWhere('a.certificatObligatoire = :certificatObligatoire')
                ->setParameter('certificatObligatoire', $data->certificat);
        }

        return $query->getQuery();
    }
}
