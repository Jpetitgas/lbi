<?php

namespace App\Controller\Utilisateur;

use App\Entity\Dossier;
use App\Entity\Utilisateur;
use App\Form\BulletinType;
use App\Form\UtilisateurType;
use App\Repository\EvenementRepository;
use App\Repository\InformationRepository;
use App\Service\EnCours;
use App\Service\InfosDossierEncours;
use App\Stripe\StripeService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 *  @Route("/utilisateur/dossier", name="utilisateur_dossier")
 */
class DossierController extends AbstractController
{
    /**
     * @Route("/edit/{ancre}", name="_edit", methods={"GET", "POST"})
     */
    public function edit($ancre, EnCours $enCours, StripeService $stripeService, InformationRepository $informationRepository, InfosDossierEncours $infosDossierEncours, Request $request): Response
    {
        $information = $informationRepository->findOneBy([]);

        // ancre
        $chemin = explode('_', $ancre);
        $ancre = [];

        $dossier = $enCours->dossierAdherentConnecte();
        if ($dossier) {
            $pratiquants = $dossier->getAdherent()->getPratiquants();
            $inscriptions = $dossier->getInscriptions();

            $ancre['pill_dossier'] = $chemin[0];
            if ($chemin[1] == 'none') {
                if (null !== $pratiquants[0]) {
                    $ancre['pratiquant'] = $pratiquants[0]->getId();
                }
            } else {
                $ancre['pratiquant'] = $chemin[1];
            }

            $ancre['pill_pratiquant'] = $chemin[2];

            $formBulletin = $this->createForm(BulletinType::class);
            $formBulletin->handleRequest($request);

            if ($formBulletin->isSubmitted() && $formBulletin->isValid()) {
                $image = $formBulletin->get('image')->getData();
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();
                $image->move(
                    $this->getParameter('images_repertoire'),
                    $fichier
                );
                $dossier->setBulletinAdhesion($fichier);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($dossier);
                $entityManager->flush();
                $ancre = 'bulletin_none_none';
                $this->addFlash('success', 'Le bulletin d\'inscription a été téléverser avec succés');
                return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
            }

            $cotisation = $infosDossierEncours->cotisation($dossier);
            $totalDesPaiements = $infosDossierEncours->paiement($dossier);
            $montantCotisation = $cotisation['total'];
            $solde = $infosDossierEncours->solde($dossier);

            $aCompleterUtilisateur = $infosDossierEncours->aCompleterUtilisateur($dossier);
            $aFaire = $infosDossierEncours->aFaireGestion(($dossier));
            $alert['bulletin'] = $aCompleterUtilisateur['bulletin'];
            $alert['nb_certificat'] = $aCompleterUtilisateur['certificat'];
            $alert['cotisation'] = $aCompleterUtilisateur['cotisation'];
            $alert['nbInscription'] = $aCompleterUtilisateur['nbInscription'];

            return $this->render('utilisateur/dossier/edit.html.twig', [
                'ancre' => $ancre,
                'dossier' => $dossier,
                'inscriptions' => $inscriptions,
                'pratiquants' => $pratiquants,
                'cotisation' => $cotisation,
                'paiement' => $totalDesPaiements,
                'solde' => $solde,
                'alerts' => $alert,
                'stripePublicCle' => $stripeService->ObtenirPublicCle(),
                'information' => $information,
                'formBulletin' => $formBulletin->createView(),
            ]);
        }

        return $this->redirectToRoute('utilisateur_dossier_show');
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request, EnCours $enCours, InformationRepository $informationRepository, InfosDossierEncours $infosDossierEncours): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getCreationDossier() == false) {
            $this->addFlash('danger', 'Les inscriptions ne sont pas ouvertes');

            return $this->redirectToRoute('utilisateur_dossier_show');
        }
        if ($infosDossierEncours->dossierEncoursExitant($this->getUser()) == true) {
            $this->addFlash('danger', 'Un dossier existe déjà');

            return $this->redirectToRoute('espace');
        }

        $dossier = new Dossier();
        $dossier->setAdherent($this->getUser())
            ->setLocked(false)
            ->setDossierVerrouille(false)
            ->setSaison($enCours->saison())
            ->setBulletinValide(false);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($dossier);
        $entityManager->flush();
        $this->addFlash('success', 'Un dossier vient d\'être crée');

        $ancre = 'information_none_none';
        return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
    }



    public function ajoutInscription($inscription, $dossier, $statutInscription, $verificationActivite)
    {
        $inscription->setDossier($dossier)
            ->setStatut($statutInscription->statut($inscription->getActivite()))
            ->setDateChangementStatut(new DateTime())
            ->setCertificatObligatoire(true)
            ->setCertificatValide(false);
        if (!$verificationActivite->certificatMedical($inscription->getActivite())) {
            $inscription->setCertificatObligatoire(false);
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($inscription);
        $entityManager->flush();

        if ($verificationActivite->certificatMedical($inscription->getActivite())) {
            return $this->redirectToRoute('utilisateur_inscription_edit', ['id' => $inscription->getId(), 'id_dossier' => $dossier->getId()]);
        }

        return $this->redirectToRoute('utilisateur_dossier_edit', ['id' => $dossier->getId()]);
    }
}
