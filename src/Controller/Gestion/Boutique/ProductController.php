<?php

namespace App\Controller\Gestion\Boutique;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductController extends AbstractController
{
    /**
     * @Route("gestion/boutique/product", name="gestion_boutique_product_index")
     */
    public function indexProduct(ProductRepository $productRepository): Response
    {
        $products = $productRepository->findAll();

        return $this->render(
            'gestion/boutique/product/index.html.twig',
            [
                'products' => $products,
            ]
        );
    }

    /**
     * @Route("gestion/boutique/product/edit/{id}", name="gestion_boutique_product_edit")
     */
    public function editProduct($id, Request $request, SluggerInterface $slugger, ProductRepository $productRepository): Response
    {
        $product = $productRepository->find($id);
        $formProduct = $this->createForm(ProductType::class, $product);
        $formProduct->handleRequest($request);

        if ($formProduct->isSubmitted() && $formProduct->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_boutique_product_index');
        }

        return $this->render('gestion/boutique/product/edit.html.twig', [
            'product' => $product,
            'formProduct' => $formProduct->createView(),
        ]);
    }

    /**
     * @Route("gestion/boutique/product/new", name="gestion_boutique_product_new")
     */
    public function newProduct(Request $request, SluggerInterface $slugger): Response
    {
        $product = new Product();
        $formProduct = $this->createForm(ProductType::class, $product);
        $formProduct->handleRequest($request);

        if ($formProduct->isSubmitted() && $formProduct->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_boutique_product_index');
        }

        return $this->render('gestion/boutique/product/new.html.twig', [
            'product' => $product,
            'formProduct' => $formProduct->createView(),
        ]);
    }
    /**
     * @Route("gestion/boutique/product/delete/{id}", name="gestion_boutique_product_delete")
     */
    public function delete($id, Request $request, ProductRepository $productRepository): Response
    {
        $product = $productRepository->find($id);
        if ($product) {
            if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($product);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('gestion_boutique_product_index');
    }
}
