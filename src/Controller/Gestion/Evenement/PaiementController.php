<?php

namespace App\Controller\Gestion\Evenement;

use App\Entity\Purchase;
use App\Service\EnCours;
use App\Entity\PaiementEvenement;

use App\Form\PaiementEvenementType;
use App\Repository\EvenementRepository;
use App\SearchData\PaiementEvenementData;
use App\SearchForm\PaiementEvenementForm;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\PaiementBoutiqueRepository;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\PaiementEvenementRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/evenement/paiement", name="gestion_evenement_paiement")
 */
class PaiementController extends AbstractController
{
    /**
     * @Route("/index", name="_index", methods={"GET"})
     */
    public function index(PaiementEvenementRepository $paiementEvenementRepository, PaginatorInterface $paginator, Request $request, EnCours $enCours): Response
    {
        $data = new PaiementEvenementData();
        $form = $this->createForm(PaiementEvenementForm::class, $data);
        $form->handleRequest($request);

        $saison = $enCours->saison();
        $paiements = $paginator->paginate(
            $paiementEvenementRepository->paiements($saison, $data),
            $request->query->getInt('page', 1),
            $data->nb
        );
        return $this->render('gestion/evenement/paiement/index.html.twig', [
            'paiements' => $paiements,
            'saison'=>$saison,
            'form'=>$form->createView(),
        ]);
    }
    /**
     * @Route("/evenement/index/{id_evenement}", name="_evenement_index", methods={"GET"})
     */
    public function indexPaiementEvenement(PaiementEvenementRepository $paiementEvenementRepository, Request $request, PaginatorInterface $paginator, $id_evenement, EvenementRepository $evenementRepository): Response
    {
        $evenement = $evenementRepository->find($id_evenement);
        if ($evenement->getPayant()==false) {
            $this->addFlash('danger', 'Cet évenement n\'est pas payant');
            return $this->redirectToRoute('gestion_evenement_index');
        }
        $data = new PaiementEvenementData();
        $form = $this->createForm(PaiementEvenementForm::class, $data);
        $form->handleRequest($request);


        $paiements = $paginator->paginate(
            $paiementEvenementRepository->paiementsEvenement($evenement, $data),
            $request->query->getInt('page', 1),
            $data->nb
        );
        return $this->render('gestion/evenement/paiement/evenement/index.html.twig', [
            'evenement'=>$evenement,
            'paiementEvenements' => $paiements,
            'form'=>$form->createView(),
        ]);
    }

    /**
     * @Route("/evenement/edit/{id}", name="_evenement_edit", methods={"GET","POST"})
     */
    public function edit($id, Request $request, PaiementEvenementRepository $paiementEvenementRepository, EvenementRepository $evenementRepository): Response
    {
        $paiement=$paiementEvenementRepository->find($id);
        $evenement=$evenementRepository->find($paiement->getEvenement());
        $formPaiement = $this->createForm(PaiementEvenementType::class, $paiement, ['evenement'=>$evenement]);
        $formPaiement->handleRequest($request);

        if ($formPaiement->isSubmitted() && $formPaiement->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_evenement_paiement_evenement_index', ['id_evenement' => $paiement->getEvenement()->getId()]);
        }

        return $this->render('gestion/evenement/paiement/evenement/edit.html.twig', [

            'paiement' => $paiement,
            'formPaiement' => $formPaiement->createView(),
        ]);
    }

    /**
     * @Route("/evenement/new/{id_evenement}", name="_evenement_new", methods={"GET","POST"})
     */
    public function new($id_evenement, Request $request, EvenementRepository $evenementRepository): Response
    {
        $evenement=$evenementRepository->find($id_evenement);
        $paiement = new PaiementEvenement();
        $formPaiement = $this->createForm(PaiementEvenementType::class, $paiement, ['evenement'=>$evenement]);
        $formPaiement->handleRequest($request);

        if ($formPaiement->isSubmitted() && $formPaiement->isValid()) {
            $paiement->setEvenement($evenement);
            $paiement->setSaison($evenement->getSaison());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($paiement);
            $entityManager->flush();



            return $this->redirectToRoute('gestion_evenement_paiement_evenement_index', ['id_evenement' => $paiement->getEvenement()->getId()]);
        }

        return $this->render('gestion/evenement/paiement/evenement/new.html.twig', [

            'evenement' => $evenement,
            'formPaiement' => $formPaiement->createView(),
        ]);
    }

    /**
     * @Route("/evenement/delete/{id}", name="_evenement_delete", methods={"POST"})
     */
    public function delete($id, Request $request, PaiementEvenementRepository $paiementEvenementRepository): Response
    {
        $paiement=$paiementEvenementRepository->find($id);
        if ($this->isCsrfTokenValid('delete'.$paiement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($paiement);
            $entityManager->flush();
        }
        $this->addFlash('success', 'Paiement supprimé');


        return $this->redirectToRoute('gestion_evenement_paiement_evenement_index', ['id_evenement' => $paiement->getEvenement()->getId()]);
    }

    /**
     * @Route("/encaisserGroupe/{ids}", name="_encaisserGroupe", methods={"GET"})
     */
    public function encaisserGroupe($ids, PaiementEvenementRepository $paiementEvenementRepository): Response
    {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $paiement = $paiementEvenementRepository->find($id);
            $paiement->setEncaisse(($paiement->getEncaisse() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($paiement);
        }
        $em->flush();
        return new Response('Le statut "encaissé" des paiements a changé d\'état', Response::HTTP_OK);
    }
    /**
     * @Route("/encaisser/{id}", name="_encaisser", methods={"GET"})
     */
    public function encaisser($id, PaiementEvenementRepository $paiementEvenementRepository)
    {
        $paiement = $paiementEvenementRepository->find($id);
        $paiement->setEncaisse(($paiement->getEncaisse() ? '0' : '1'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($paiement);

        $em->flush();
        $this->addFlash('success', 'Les paiements (encaissé) ont changé d\'état');


        return $this->redirectToRoute('gestion_evenement_paiement_evenement_index', ['id_evenement' => $paiement->getEvenement()->getId()]);
    }
}
