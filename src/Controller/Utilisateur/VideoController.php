<?php

namespace App\Controller\Utilisateur;

use App\Form\VideoUserType;
use App\Form\VideoUtilisateurType;
use App\Repository\VideoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("utilisateur/video")
 */
class VideoController extends AbstractController
{
    private $videoRepository;
    private $security;
    private $entityManager;

    public function __construct(VideoRepository $videoRepository, Security $security, EntityManagerInterface $entityManager)
    {
        $this->videoRepository = $videoRepository;
        $this->security = $security;
        $this->entityManager = $entityManager;
    }



    /**
     * @Route("/liste", name="utilisateur_evenement_liste", methods={"GET"})
     */
    public function index(VideoRepository $videoRepository, Security $security): Response
    {
        $videos = $videoRepository->findAllOrderedByDateDesc();
        $user = $security->getUser(); // Obtenez l'utilisateur connecté

        return $this->render('utilisateur/video/index.html.twig', [
            'videos' => $videos,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{videoName}/edit", name="utilisateur_evenement_video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, string $videoName, VideoRepository $videoRepository): Response
    {
        // Récupérer l'utilisateur connecté et la vidéo (comme avant)
        $user = $this->security->getUser();
        $video = $this->videoRepository->findOneBy(['file' => $videoName]);

        if (!$video) {
            throw $this->createNotFoundException('La vidéo demandée n\'existe pas.');
        }

        // Vérifier les droits d'accès (comme avant)
        if ($video->isPrivated() && !$video->getUtilisateurs()->contains($user)) {
            throw $this->createAccessDeniedException('Vous n\'avez pas accès à cette vidéo.');
        }

        // Incrémenter le nombre de vues
        $video->setView($video->getView() + 1);
        $this->entityManager->flush();

        // Construire le chemin de la vidéo
        $videoPath = $this->getParameter('kernel.project_dir') . '/public/video/gala/' . $videoName . '.mp4';

        // Vérifier si le fichier existe
        if (!file_exists($videoPath)) {
            throw $this->createNotFoundException('Le fichier vidéo n\'existe pas.');
        }

        // Créer une réponse de fichier binaire
        $response = new BinaryFileResponse($videoPath);
        $response->headers->set('Content-Type', 'video/mp4');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $videoName . '.mp4');

        // Permettre le cache côté client
        $response->setCache([
            'public' => true,
            'max_age' => 3600,
            's_maxage' => 3600,
        ]);

        // Si c'est une requête pour la page, rendre le template
        if (!$request->headers->has('Range')) {
            return $this->render('utilisateur/video/editVideo.html.twig', [
                'video' => $video,
                'video_path' => '/video/gala/' . $videoName . '.mp4', // Chemin relatif pour le template
            ]);
        }

        // Sinon, retourner la réponse de fichier binaire pour le streaming
        return $response;
    }
    /**
     * @Route("/{videoName}/editphoto", name="utilisateur_evenement_photo_edit", methods={"GET","POST"})
     */
    public function editPhoto(string $videoName, VideoRepository $videoRepository): Response
    {
        $video = $videoRepository->findOneBy(['file' => $videoName]);

        if (!$video) {
            throw $this->createNotFoundException('Le répertoire n\'hesite pas!!');
        }
        $photosDir = 'video/gala/' . $videoName;
        $photos = array_diff(scandir($photosDir), array('..', '.'));

        return $this->render('utilisateur/video/editPhoto.html.twig', [
            'video' => $video,
            'photos' => $photos,
            'photosDir' => $photosDir,
        ]);
    }
    /**
     * @Route("/add-user-to-video", name="gestion_evenement_add_user_to_video", methods={"GET","POST"})
     */
    public function addUserToVideo(Request $request): Response
    {
        $form = $this->createForm(VideoUtilisateurType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $video = $data['video'];
            $utilisateur = $data['utilisateur'];

            // Ajouter l'utilisateur à la vidéo
            if (!$video->getUtilisateurs()->contains($utilisateur)) {
                $video->addUtilisateur($utilisateur);
                $this->entityManager->flush();
            }

            return $this->redirectToRoute('gestion');
        }

        return $this->render('gestion/video/add_user_to_video.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    private function streamVideo(string $filePath): Response
    {
        $fileSize = filesize($filePath);
        $start = 0;
        $end = $fileSize - 1;
        $status = 200;

        if (isset($_SERVER['HTTP_RANGE'])) {
            $status = 206;
            list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
            if (strpos($range, ',') !== false) {
                return new Response('416 Requested Range Not Satisfiable', 416);
            }
            if ($range == '-') {
                $start = $fileSize - substr($range, 1);
            } else {
                $range = explode('-', $range);
                $start = $range[0];
                $end = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $fileSize - 1;
            }
            if ($start > $end || $start > $fileSize - 1 || $end >= $fileSize) {
                return new Response('416 Requested Range Not Satisfiable', 416);
            }
        }

        $length = $end - $start + 1;

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'video/mp4');
        $response->headers->set('Accept-Ranges', 'bytes');
        $response->headers->set('Content-Length', $length);
        if ($status == 206) {
            $response->headers->set('Content-Range', sprintf('bytes %s-%s/%s', $start, $end, $fileSize));
        }
        $response->setStatusCode($status);

        $response->setCallback(function () use ($filePath, $start, $length) {
            $handle = fopen($filePath, 'rb');
            fseek($handle, $start);
            $remaining = $length;
            while (!feof($handle) && $remaining > 0) {
                $readLength = min(1024 * 8, $remaining);
                echo fread($handle, $readLength);
                $remaining -= $readLength;
                flush();
            }
            fclose($handle);
        });

        return $response;
    }
}
