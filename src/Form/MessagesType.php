<?php

namespace App\Form;

use App\Entity\Sujet;
use App\Entity\Messages;
use App\Entity\Utilisateur;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;

use Eckinox\TinymceBundle\Form\Type\TinymceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MessagesType extends AbstractType
{
    public $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->security->isGranted('ROLE_GESTION')) {
            $builder->add('titre', TextType::class)
                ->add('destinataire', EntityType::class, [
                    'class' => Utilisateur::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.nom', 'ASC');
                    },
                    'placeholder' => 'selectionner un destinaire',
                ]);
        } elseif ($this->security->isGranted('ROLE_INTERVENANT')) {
            $builder->add('sujet', EntityType::class, [
                'class' => Sujet::class,
                'placeholder' => 'selectionner un sujet',
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.categorie = ?1')
                        ->setParameter(1, 'INTERVENANT');
                },
            ]);
        } else {
            $builder->add('sujet', EntityType::class, [
                'class' => Sujet::class,
                'placeholder' => 'Sélectionner un sujet',
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.categorie = ?1')
                        ->orderBy('s.sujet', 'asc')
                        ->setParameter(1, 'UTILISATEUR');
                }
            ]);
        }

        $builder->add("message", TinymceType::class, [])
            ->add('envoyer', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);
        $formModifier = function (FormInterface $form, Sujet $sujet = null) {
            if ($sujet->getSujet() === $sujet::SUJET_MESSAGE_INTERVENANT) {
                $form->add('intervenant', EntityType::class, [
                    'label' => 'Intervenant',
                    'class' => Utilisateur::class,
                    'placeholder' => 'Sélectionner un intervenant',
                    'mapped' => false,
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->where('u.roles LIKE :role')
                            ->orderBy('u.nom', 'ASC')
                            ->setParameter('role', '%' . 'ROLE_INTERVENANT' . '%');
                    },
                ]);
            }
        };
        if ($builder->has('sujet')) {
            $builder->get('sujet')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier) {
                    $sujet = $event->getForm()->getData();
                    $formModifier($event->getForm()->getParent(), $sujet);
                }
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Messages::class,
        ]);
    }
}
