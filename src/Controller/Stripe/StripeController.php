<?php

namespace App\Controller\Stripe;

use App\Entity\Messages;
use App\Entity\Paiement;
use App\Service\EnCours;
use App\Service\SendEmail;
use App\Stripe\StripeService;
use App\Message\SendEmailMessage;
use App\Repository\SujetRepository;
use App\Repository\BanqueRepository;
use App\Service\InfosDossierEncours;
use App\Repository\DossierRepository;
use App\Repository\InformationRepository;
use App\Repository\TypeDePaiementRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StripeController extends AbstractController
{
    /**
     *  @Route("/dossier/create-checkout-session", name="dossier_checkout", methods={"POST"})
     */
    public function checkout(InfosDossierEncours $infosDossierEncours, InformationRepository $informationRepository, StripeService $stripeService, EnCours $enCours, DossierRepository $dossierRepository)
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getStripe() == false) {
            $this->addFlash('danger', 'Le paiement par Stripe n\'est pas autorisé!');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        \Stripe\Stripe::setApiKey($stripeService->ObtenirSecretCle());
        $id_dossier = $enCours->dossierAdherentConnecte();
        $dossier = $dossierRepository->find($id_dossier);
        $montant = $infosDossierEncours->solde($dossier);
        $checkout_session = \Stripe\Checkout\Session::create([

            'payment_method_types' => ['card', 'klarna'],
            //'automatic_payment_methods' => [
            //    'enabled' => true,
            //],
            'line_items' => [[
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $montant * 100,
                    'product_data' => [
                        'name' => 'Cotisations',
                    ],
                ],
                'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $this->generateUrl('dossier_stripe_succes', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('dossier_stripe_erreur', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        return  new JsonResponse(['id' => $checkout_session->id]);
    }

    /**
     *  @Route("/stripe/succes", name="dossier_stripe_succes")
     */
    public function succes(InfosDossierEncours $infosDossierEncours, SujetRepository $sujetRepository, SendEmail $sendEmail, MessageBusInterface $messageBus, EnCours $enCours, DossierRepository $dossierRepository, TypeDePaiementRepository $typeDePaiementRepository, BanqueRepository $banqueRepository)
    {
        $id_dossier = $enCours->dossierAdherentConnecte();
        $dossier = $dossierRepository->find($id_dossier);
        $montant = $infosDossierEncours->solde($dossier);
        if (!$dossier || ($dossier && $dossier->getAdherent() !== $this->getUser())) {
            /*envoyer un mail à l'adminitrateur */

            return;
        }
        $typeDePaiement = $typeDePaiementRepository->findOneBy(['designation' => 'Carte bancaire']);
        $banque = $banqueRepository->findOneBy(['nom' => 'Stripe']);
        $paiement = new Paiement();
        $paiement->setDossier($dossier)
            ->setBanque($banque)
            ->setMontant($montant)
            ->setTypeDePaiement($typeDePaiement)
            ->setEncaisse(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($paiement);
        $entityManager->flush();
        /** @var Sujet $sujet */
        $sujet = $sujetRepository->findOneBy(['sujet' => 'Paiement en ligne']);
        if ($sujet) {
            $envoi = new Messages();
            $envoi->setExpediteur($sujet->getDestinataire())
                ->setDestinataire($dossier->getAdherent())
                ->setTitre($sujet->getSujet())
                ->setMessage('Votre paiement s\'est correctement déroulé. Vous trouverez votre facture dans votre espace, dossier, récapitulatif');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($envoi);
            $entityManager->flush();
        }

        $sendEmail->send([
            'recipient_email' => $dossier->getAdherent()->getEmail(),
            'sujet' => 'Paiement de votre dossier en ligne',
            'htmlTemplate' => 'email/paiement.html.twig',
            'destinataire' => $dossier->getAdherent(),
            'contenu' => [
                'utilisateur' => $dossier->getAdherent(),
                'montant' => $montant,
            ],
        ]);

        $this->addFlash('success', 'le dossier a été payé!');

        return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'paiement_none_none']);
    }

    /**
     *  @Route("/stripe/erreur", name="dossier_stripe_erreur")
     */
    public function erreur()
    {
        return $this->render('stripe/erreur.html.twig');
    }
}
