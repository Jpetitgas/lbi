<?php

namespace App\Repository;

use App\Entity\Saison;
use App\Entity\Evenement;
use App\Entity\Utilisateur;
use App\SearchData\UtilisateurData;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
 * @method Utilisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utilisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utilisateur[]    findAll()
 * @method Utilisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateurRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Utilisateur::class);
    }
    public function findAll()
    {
        return $this->findBy(array(), array('nom' => 'ASC'));
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof Utilisateur) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findUsersByRole($role)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');

        return $qb->getQuery()->getResult();
    }

    public function findUsersByEvenement(Evenement $evenement)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('u', 'p', 'c')
            ->leftJoin('u.places', 'p')
            ->innerJoin('p.container', 'c', 'WITH', 'c.id=p.container')
            ->orderBy('u.nom', 'ASC')
            ->andWhere('c.evenement = :evenement')
            ->setParameter('evenement', $evenement);

        $sql = $qb->getDQL();
        return $qb;
    }

    public function utilisateurs(UtilisateurData $data)
    {
        $query = $this
            ->createQueryBuilder('u')
            ->select('u', 't')
            ->leftJoin('u.town', 't')
            ->orderBy('u.nom', 'ASC')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%' . 'ROLE_USER' . '%');

        if (!empty($data->q)) {
            $qWithOUtSpace = str_replace(' ', '', $data->q);
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or u.portable LIKE :q or u.portable LIKE :qWithOUtSpace or u.email LIKE :q')
                ->setParameter('qWithOUtSpace', "%{$qWithOUtSpace}%")
                ->setParameter('q', "%{$data->q}%");
        }


        return $query->getQuery();
    }
    public function intervenants(UtilisateurData $data, Saison $saison)
    {
        $query = $this
            ->createQueryBuilder('u')
            ->select('DISTINCT u', 't')
            ->leftJoin('u.town', 't')
            ->innerJoin('u.cours', 'c')
            ->andWhere('u.roles LIKE :role')
            ->andWhere('c.saison = :saison')
            ->setParameter('role', '%' . 'ROLE_INTERVENANT' . '%')
            ->setParameter('saison', $saison)
            ->orderBy('u.nom', 'ASC');

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or u.portable LIKE :q or u.email LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }

        return $query->getQuery();
    }
    public function gestionnaires(UtilisateurData $data)
    {
        $query = $this
            ->createQueryBuilder('u')
            ->select('u', 't')
            ->leftJoin('u.town', 't')
            ->orderBy('u.nom', 'ASC')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%' . 'ROLE_GESTION' . '%');

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or u.portable LIKE :q or u.email LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }


        return $query->getQuery();
    }

    // /**
    //  * @returnUtilisateur[] Returns an array ofUtilisateur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Utilisateur
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
