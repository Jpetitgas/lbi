<?php

namespace App\Service;

use App\Repository\InformationRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Security;

class SendEmail
{
    private MailerInterface $mailer;
    private string $pasDeReponse;
    private string $pasDeReponse_name;

    private $informationRepository;

    public function __construct(MailerInterface $mailer, InformationRepository $informationRepository, Security $security, string $pasDeReponse, string $pasDeReponse_name)
    {
        $this->security = $security;
        $this->mailer = $mailer;
        $this->pasDeReponse = $pasDeReponse;
        $this->pasDeReponse_name = $pasDeReponse_name;
        $this->informationRepository = $informationRepository;
    }

    public function send(array $arguments): void
    {
        [
            'recipient_email' => $recipientEmail,
            'sujet' => $sujet,
            'htmlTemplate' => $htmlTemplate,
            'destinataire' => $destinataire,
            'contenu' => $contenu
        ] = $arguments;

        $information = $this->informationRepository->findOneBy([]);
        $context['nomStructure'] = $information->getNomStructure();
        $context['espace'] = $information->getEspace();
        $context['signatureMail'] = $information->getSignature();
        $context['contenu'] = $contenu;
        $context['destinataire'] = $destinataire;

        // Calculer le temps de lecture estimé
        $readingTime = $this->estimateReadingTime($contenu);

        // Ajouter le temps de lecture au sujet
        $sujetWithReadingTime = $sujet . ' ' . $readingTime;

        $email = new TemplatedEmail();
        $email->from(new Address($this->pasDeReponse, $this->pasDeReponse_name))
            ->to($recipientEmail)
            ->subject($sujetWithReadingTime)
            ->htmlTemplate($htmlTemplate)
            ->context($context);


        try {
            $this->mailer->send($email);
            // sleep(20);
        } catch (TransportExceptionInterface $mailerException) {
            throw $mailerException;
        }
    }
    private function estimateReadingTime($content): string
    {
        $wordsPerMinute = 200; // Vitesse de lecture moyenne

        if (is_array($content)) {
            // Si $content est un tableau, joignez tous les éléments en une seule chaîne
            $contentString = implode(' ', array_map('strval', $content));
        } else {
            $contentString = $content;
        }

        $wordCount = str_word_count(strip_tags($contentString));
        $minutes = $wordCount / $wordsPerMinute;

        if ($minutes < 0.5) {
            return "(<1 min de lecture)";
        } elseif ($minutes < 1) {
            return "(1 min de lecture)";
        } else {
            return "(" . ceil($minutes) . " mins de lecture)";
        }
    }
}
