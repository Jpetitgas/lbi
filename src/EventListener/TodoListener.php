<?php

namespace App\EventListener;

use App\Entity\Todolist;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;

class TodoListener
{
    protected $security;

    public function __construct(Security $security)
    {
        $this->security=$security;
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $todo=$event->getObject();

        if ($todo instanceof Todolist) {
            $todo->setUtilisateur($this->security->getUser());
        }
    }
}
