<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Activite;
use App\Service\EnCours;
use App\Entity\Pratiquant;
use App\Entity\Inscription;
use Symfony\Component\Form\Form;
use App\Entity\CategorieActivite;
use Doctrine\ORM\EntityRepository;
use App\Service\VerificationActivite;
use Symfony\Component\Form\FormEvent;
use App\Repository\ActiviteRepository;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class _InscriptionGestionType extends AbstractType
{
    protected $enCours;
    protected $cat;
    protected $verificationActivite;
    protected $activite;
    protected $pratiquant;

    public function __construct(EnCours $enCours, VerificationActivite $verificationActivite)
    {
        $this->enCours = $enCours;
        $this->verificationActivite = $verificationActivite;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('statut', ChoiceType::class, [
                'choices' => [
                    'LISTE PRINCIPALE' => 'LISTE PRINCIPALE',
                    'LISTE D\'ATTENTE' => 'LISTE D\'ATTENTE',
                    'EN ATTENTE' => 'EN ATTENTE'
                ],
            ])

            ->add('categorie', EntityType::class, [
                'class' => CategorieActivite::class,
                'placeholder' => 'selectionner une categorie',
                'mapped' => false,
                'required' => false,
            ]);

        $builder->get('categorie')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $this->ajoutChampActivite($form->getParent(), $form->getData());
            }
        );
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();
                /* @var $activite Activite */
                $activite = $data->getActivite();
                $this->activite = $activite;
                $pratiquant = $data->getPratiquant();
                $this->pratiquant = $pratiquant;
                $cours = $data->getCours();

                if ($activite) {
                    $categorie = $activite->getCategorie();
                    $form->get('categorie')->setData($categorie);
                    $this->ajoutChampActivite($form, $categorie);
                    $this->ajoutChampCours($form, $activite);

                    $form->get('activite')->setData($activite);
                    $form->get('cours')->setData($cours);
                    $this->ajoutDemandeParticuliere($form);
                    $this->ajoutBoutonValide($form);
                } else {
                    $this->ajoutChampActivite($form, null);
                    $this->ajoutChampCours($form, null);
                    $this->ajoutDemandeParticuliere($form);
                    $this->ajoutBoutonValide($form);
                }
            }
        );
    }

    private function ajoutChampActivite(FormInterface $form, ?CategorieActivite $categorie)
    {
        $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
            'activite',
            EntityType::class,
            null,
            [
                'class' => Activite::class,
                'placeholder' => $categorie ? 'Selectionner une activité' : 'Selectionner d\'abord une catégorie',
                'auto_initialize' => false,
                'query_builder' => function (ActiviteRepository $activiteRepository) use ($categorie) {
                    $saison = $this->enCours->saison();

                    return $activiteRepository->activiteParCategorie($categorie, $saison);
                },
            ]
        );

        $form->add($builder->getForm());
    }

    private function ajoutChampCours(FormInterface $form, ?Activite $activite)
    {
        $form->add('cours', EntityType::class, [
            'class' => Cours::class,
            'label' => 'Cours',
            'required' => false,
            'placeholder' => $activite ? 'Selectionner un cours' : 'Selectionner d\'abord une activité',
            'auto_initialize' => false,
            'query_builder' => function (EntityRepository $er) use ($activite) {
                return $er->createQueryBuilder('c')
                    ->where('c.activite = ?1')
                    ->setParameter(1, $activite);
            },
        ]);
    }

    private function ajoutDemandeParticuliere(Form $form)
    {
        //$this->pratiquant = $form->getData()->getPratiquant()->getId();
        $form->add('demandeParticuliere', TextType::class, [
            'required' => false,
            'label' => 'Demande Particuliere',
        ]);
    }

    private function ajoutBoutonValide(Form $form)
    {
        //$this->pratiquant = $form->getData()->getPratiquant()->getId();
        $form->add('envoyer', SubmitType::class, [
            'label' => 'Enregistrer',
            'attr' => [
                'class' => 'btn btn-primary',
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Inscription::class,
        ]);
    }
}
