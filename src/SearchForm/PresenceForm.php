<?php

namespace App\SearchForm;

use App\Entity\Cours;
use App\SearchData\PresenceData;
use App\Service\EnCours;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class PresenceForm extends AbstractType
{
    protected $security;
    protected $enCours;

    public function __construct(Security $security, EnCours $enCours)
    {
        $this->security = $security;
        $this->enCours = $enCours;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $intervenant = $this->security->getUser();
        $saison = $this->enCours->saison();
        $builder

            ->add('cours', EntityType::class, [
               'required' => false,
               'class' => Cours::class,
               'multiple' => false,
                'query_builder' => function (EntityRepository $er) use ($intervenant, $saison) {
                    return $er->createQueryBuilder('c')
                                    ->andWhere('c.intervenant= :intervenant')
                                    ->andWhere('c.saison=:saison')
                                    ->orderBy('c.nom', 'ASC')
                                    ->setParameter('intervenant', $intervenant)
                                    ->setParameter('saison', $saison);
                },
            ])

            ->add('nb', IntegerType::class, [
                'required' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PresenceData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
