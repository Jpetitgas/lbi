<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Pratiquant;
use App\Entity\Utilisateur;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Utilisateurs extends Fixture
{
    private $faker;
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 4; ++$i) {
            $utilisateur = new Utilisateur();
            $hash = $this->userPasswordEncoder->encodePassword($utilisateur, 'Intervenant' . $i . '_2021');

            $utilisateur->setEmail('intervenant' . $i . '@demo.fr')
                ->setPassword($hash)
                ->setNom($this->faker->lastName())
                ->setPrenom($this->faker->firstName())
                ->Setroles(['ROLE_INTERVENANT'])
                ->setPortable($this->faker->PhoneNumber())
                ->setAdresse($this->faker->address())
                ->setCP($this->getReference('cp'))
                ->setTown($this->getReference('ville'))
                ->setIsVerified(true);
            $manager->persist($utilisateur);
            $this->addReference('intervenant' . $i, $utilisateur);
        }
        for ($i = 1; $i <= 50; ++$i) {
            $utilisateur = new Utilisateur();
            $hash = $this->userPasswordEncoder->encodePassword($utilisateur, 'Utilisateur' . $i . '_2021');

            $utilisateur->setEmail('utilisateur' . $i . '@demo.fr')
                ->setPassword($hash)
                ->setNom($this->faker->lastName())
                ->setPrenom($this->faker->firstName())
                ->Setroles(['ROLE_USER'])
                ->setPortable($this->faker->PhoneNumber())
                ->setAdresse($this->faker->address())
                ->setCP($this->getReference('cp'))
                ->setTown($this->getReference('ville'))
                ->setIsVerified(true);
            $manager->persist($utilisateur);
            $this->addReference('utilisateur_' . $i, $utilisateur);
            for ($p = 1; $p <= 3; ++$p) {
                $pratiquant = new Pratiquant();
                $pratiquant->setNom($this->faker->lastName())
                    ->setPrenom($this->faker->firstName())
                    ->setNaissance($this->faker->dateTimeThisCentury())
                    ->setSexe('F')
                    ->setUtilisateur($utilisateur);
                $manager->persist($pratiquant);
                $this->addReference('utilisateur_' . $i . '_pratiquant_' . $p, $pratiquant);
            }
        }
        $manager->flush();
    }
}
