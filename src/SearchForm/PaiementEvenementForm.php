<?php

namespace App\SearchForm;

use App\Entity\Banque;
use App\Entity\TypeDePaiement;
use Doctrine\ORM\EntityRepository;
use App\SearchData\PaiementDossierData;
use Symfony\Component\Form\AbstractType;
use App\SearchData\PaiementEvenementData;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class PaiementEvenementForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('q', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Rechercher sur les noms, prénoms et remarque',
                ],
            ])

            ->add('typeDePaiement', EntityType::class, [
               'required' => false,
               'class' => TypeDePaiement::class,
               'multiple' => false,
               'query_builder' => function (EntityRepository $er) {
                   return $er->createQueryBuilder('u')
                        ->orderBy('u.designation', 'ASC');
               },
            ])

           ->add('encaissement', TextType::class, [
                'required' => false,

                'attr' => [
                    'placeholder' => 'Rechercher',
                ],
            ])
            ->add('banque', EntityType::class, [
               'required' => false,
               'class' => Banque::class,
               'multiple' => false,
               'query_builder' => function (EntityRepository $er) {
                   return $er->createQueryBuilder('u')
                        ->orderBy('u.nom', 'ASC');
               },
            ])
            ->add('encaisse', ChoiceType::class, [
                'required' => false,
                'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
            ])

            ->add('nb', IntegerType::class, [
                'required' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PaiementEvenementData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
