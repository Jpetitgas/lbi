<?php

namespace App\Controller\Gestion;

use App\Entity\Utilisateur;
use App\Form\UtilisateurType;
use App\SearchData\UtilisateurData;
use App\SearchForm\UtilisateurForm;

use App\Repository\UtilisateurRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/gestionnaire", name="gestion_gestionnaire")
 */
class GestionnaireController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, UtilisateurRepository $utilisateurRepository): Response
    {
        $data = new UtilisateurData();
        $form = $this->createForm(UtilisateurForm::class, $data);
        $form->handleRequest($request);

        $gestionnaires = $paginator->paginate(
            $utilisateurRepository->gestionnaires($data),
            $request->query->getInt('page', 1),
            $data->nb
        );


        return $this->render('gestion/gestionnaire/index.html.twig', [
            'gestionnaires' => $gestionnaires,
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Utilisateur $utilisateur): Response
    {
        $formUtilisateur = $this->createForm(UtilisateurType::class, $utilisateur);
        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_gestionnaire_index');
        }

        return $this->render('gestion/gestionnaire/edit.html.twig', [
            'gestionnaire' => $utilisateur,
            'formUtilisateur' => $formUtilisateur->createView(),
        ]);
    }
}
