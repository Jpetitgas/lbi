<?php

namespace App\SearchData;

class InscriptionData
{
    public $q = '';

    public $section;

    public $refAssureur;

    public $activite;
    public $cours;
    public $intervenant;
    public $certificatObligatoire;
    public $certificatPresent;
    public $certificatValide;
    public $transmisAssureur;
    public $dossierComplet;
    public $statut;
    public $Nbpresence;
    public $canceled = false;
    public $nb = 20;
}
