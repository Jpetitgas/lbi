

var $table = $('#paiement');
function getRowSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
        return row;
    })
}


$('#encaisseGroupe').click(function () {
    var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each(selectedRows, function (index, value) {
        ids.push(value.id);
    });
    if (ids.length) {
        var r = confirm("Etes-vous sur de vouloir changer l\'etat de l\'encaissement de(s) ligne(s) séléctionnée(s)?");
        if (r == true) {
            window.location.href = "/gestion/paiement/encaisserGroupe/" + ids.toString();
        }

    } else {
        alert('Vous devez d\'abord selectionner une ligne');
    }
});

$('#message').click(function () {
    var selectedRows = getRowSelections();
    var selectedItems = '\n';
    let ids = [];
    $.each(selectedRows, function (index, value) {
        ids.push(value.utilisateur);
    });
    if (ids.length) {
        window.location.href = "/gestion/correspondance/message/" + ids.toString();
    } else {
        alert('Vous devez d\'abord selectionner une ligne');
    }
});
