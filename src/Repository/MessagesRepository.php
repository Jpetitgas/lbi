<?php

namespace App\Repository;

use App\Entity\Messages;
use App\Entity\Utilisateur;
use App\SearchData\MessageData;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Messages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messages[]    findAll()
 * @method Messages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Messages::class);
    }

    public function nonLu($utilisateur)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.destinataire = :val')
            ->setParameter('val', $utilisateur)
            ->andWhere('m.is_read= 0')
            ->select('count(m.is_read) as non_lu')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function nonOuvert($utilisateur)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.expediteur = :val')
            ->setParameter('val', $utilisateur)
            ->andWhere('m.is_read= 0')
            ->select('count(m.is_read) as non_lu')
            ->getQuery()
            ->getSingleScalarResult();
    }
    public function nonOuvertParSaison($saison)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.saison = :val')
            ->setParameter('val', $saison)
            ->andWhere('m.is_read = 0')
            ->join('m.destinataire', 'u')
            ->select('count(m.is_read) as non_lu, u.email')
            ->groupBy('m.destinataire')
            ->getQuery()
            ->getResult();
    }
    public function messagesReceptions(Utilisateur $destinataire, MessageData $data)
    {
        $query = $this
            ->createQueryBuilder('m')
            ->select('m', 'u')
            ->join('m.expediteur', 'u')
            ->andWhere('m.destinataire = :destinataire')
            ->setParameter('destinataire', $destinataire)
            ->orderBy('m.id', 'DESC');


        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or m.titre LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }
        if ($data->lu === true) {
            $query = $query->andWhere('m.is_read =1');
        }
        if ($data->lu === false) {
            $query = $query->andWhere('m.is_read =0');
        }
       

        return $query->getQuery();
    }
    public function messagesEnvoye(Utilisateur $expediteur, MessageData $data)
    {
        $query = $this
            ->createQueryBuilder('m')
            ->select('m', 'u')
            ->join('m.destinataire', 'u')
            ->andWhere('m.expediteur = :expediteur')
            ->setParameter('expediteur', $expediteur)
            ->orderBy('m.id', 'DESC');


        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or m.titre LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }
        if ($data->lu === true) {
            $query = $query->andWhere('m.is_read =1');
        }
        if ($data->lu === false) {
            $query = $query->andWhere('m.is_read =0');
        }

        return $query->getQuery();
    }

    // /**
    //  * @return Messages[] Returns an array of Messages objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Messages
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
