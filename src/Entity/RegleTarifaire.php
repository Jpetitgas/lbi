<?php

namespace App\Entity;

use App\Repository\RegleTarifaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RegleTarifaireRepository::class)
 */
class RegleTarifaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=NiveauTarifaire::class, inversedBy="regleTarifaires", fetch="LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $niveau;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity=Operateur::class, inversedBy="regleTarifaires", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $operateur;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class, inversedBy="regleTarifaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $saison;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parametre;

    /**
     * @ORM\ManyToMany(targetEntity=Ville::class, inversedBy="regleTarifaires")
     */
    private $villes;

    public function __construct()
    {
        $this->villes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMontant(): ?int
    {
        return $this->montant;
    }

    public function setMontant(?int $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getNiveau(): ?NiveauTarifaire
    {
        return $this->niveau;
    }

    public function setNiveau(?NiveauTarifaire $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getOperateur(): ?Operateur
    {
        return $this->operateur;
    }

    public function setOperateur(?Operateur $operateur): self
    {
        $this->operateur = $operateur;

        return $this;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    public function getParametre(): ?int
    {
        return $this->parametre;
    }

    public function setParametre(?int $parametre): self
    {
        $this->parametre = $parametre;

        return $this;
    }

    /**
     * @return Collection|Ville[]
     */
    public function getVilles(): Collection
    {
        return $this->villes;
    }

    public function addVille(Ville $ville): self
    {
        if (!$this->villes->contains($ville)) {
            $this->villes[] = $ville;
        }

        return $this;
    }

    public function removeVille(Ville $ville): self
    {
        $this->villes->removeElement($ville);

        return $this;
    }

    public function __clone()
    {
        $this->id = null;
    }
}
