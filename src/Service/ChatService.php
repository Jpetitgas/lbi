<?php

namespace App\Service;

use App\Entity\ConsultationChat;
use App\Entity\Cours;
use App\Entity\Utilisateur;
use App\Repository\ChatRepository;
use App\Repository\ConsultationChatRepository;
use App\Repository\CoursRepository;
use App\Repository\DossierRepository;
use App\Repository\InscriptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class ChatService
{
    private $chatRepository;
    private $enCours;
    private $inscriptionRepository;
    private $dossierRepository;
    private $security;
    private $consultationChatRepository;
    private $coursRepository;
    private $em;

    public function __construct(ChatRepository $chatRepository, EntityManagerInterface $em, CoursRepository $coursRepository, ConsultationChatRepository $consultationChatRepository, Security $security, EnCours $enCours, InscriptionRepository $inscriptionRepository, DossierRepository $dossierRepository)
    {
        $this->chatRepository = $chatRepository;
        $this->enCours = $enCours;
        $this->inscriptionRepository = $inscriptionRepository;
        $this->dossierRepository = $dossierRepository;
        $this->security = $security;
        $this->consultationChatRepository = $consultationChatRepository;
        $this->coursRepository = $coursRepository;
        $this->em = $em;
    }

    public function nbPostNonLuParUtilisateurParCours(Cours $cours, Utilisateur $utilisateur)
    {
        $nb = 0;
        $posts = $this->chatRepository->findBy(['cours' => $cours]);
        $consultation = $this->consultationChatRepository->findOneBy(['cours' => $cours, 'utilisateur' => $utilisateur]);
        if ($consultation) {
            foreach ($posts as $post) {
                if ($post->getExpediteur() != $utilisateur && $post->getCreatedAt() > $consultation->getDateConsultation()) {
                    ++$nb;
                }
            }
        } else {
            foreach ($posts as $post) {
                if ($post->getExpediteur() != $utilisateur) {
                    ++$nb;
                }
            }
        }

        return $nb;
    }

    public function coursChatParUtilisateurConnected()
    {
        $utilisateur = $this->security->getUser();
        $coursChat = [];
        if ($this->security->isGranted('ROLE_INTERVENANT')) {
            $coursChat = $this->coursRepository->coursParSaisonParIntervenant($this->enCours->saison(), $utilisateur);
        }
        if ($this->security->isGranted('ROLE_USER')) {
            /** @var Dossier $dossier */
            $dossier = $this->dossierRepository->dossierCourant($utilisateur, $this->enCours->saison());
            if ($dossier) {
                $inscriptions = $dossier->getInscriptions();
                if ($inscriptions) {
                    foreach ($inscriptions as $inscription) {
                        if ($inscription->getCours()) {
                            if (!in_array($inscription->getCours(), $coursChat)) {
                                array_push($coursChat, $inscription->getCours());
                            }
                        }
                    }
                }
            }
        }

        return $coursChat;
    }

    public function nbPostNonLuParUtilisateur(?Utilisateur $utilisateur)
    {
        $nb = 0;
        if ($utilisateur) {
            $saison = $this->enCours->saison();
            $inscriptions = $this->inscriptionRepository->coursParSaisonUtilisateur($this->dossierRepository->dossierCourant($utilisateur, $saison));

            foreach ($inscriptions as $inscription) {
                if ($inscription->getCours()) {
                    $nb = $nb + $this->nbPostNonLuParUtilisateurParCours($inscription->getCours(), $utilisateur);
                }
            }
            $cours = $this->coursRepository->coursParSaisonParIntervenant($saison, $utilisateur);
            foreach ($cours as $cour) {
                if ($cour) {
                    $nb = $nb + $this->nbPostNonLuParUtilisateurParCours($cour, $utilisateur);
                }
            }
        }

        return $nb;
    }

    public function nbPostNonLuGestionnaire()
    {
        $saison = $this->enCours->saison();
        $cours = $this->coursRepository->findBy(['saison' => $saison]);
        $nb = 0;

        foreach ($cours as $cour) {
            if ($cour) {
                $nb = $nb + $this->nbPostNonLuParUtilisateurParCours($cour, $this->security->getUser());
            }
        }

        return $nb;
    }

    public function consultation(Cours $cours)
    {
        $consultation = $this->consultationChatRepository->findOneBy(['utilisateur' => $this->security->getUser(), 'cours' => $cours]);
        if ($consultation) {
            $consultation->setDateConsultation(new \DateTimeImmutable('now'));
            $this->em->flush();
        } else {
            $consultation = new ConsultationChat();
            $consultation->setCours($cours)
                ->setUtilisateur($this->security->getUser())
                ->setDateConsultation(new \DateTimeImmutable('now'));
            $this->em->persist($consultation);
            $this->em->flush();
        }
    }

    public function autorisation(Cours $cours)
    {
        $cours = $this->coursRepository->find($cours);
        $dossier = $this->enCours->dossierAdherentConnecte();
        $inscription = $this->inscriptionRepository->findBy(['cours' => $cours, 'dossier' => $dossier]);
        $autorisation = false;
        if ($inscription || $cours->getIntervenant() === $this->security->getUser() || $this->security->isGranted('ROLE_GESTION')) {
            $autorisation = true;
        }

        return $autorisation;
    }
}
