<?php

namespace App\Entity;

use DateInterval;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SaisonRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=SaisonRepository::class)
 */
class Saison
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $saison;

    /**
     * @ORM\OneToMany(targetEntity=Activite::class,cascade={"persist"}, mappedBy="saison")
     */
    private $activites;

    /**
     * @ORM\OneToMany(targetEntity=Dossier::class, mappedBy="saison")
     */
    private $dossiers;

    /**
     * @ORM\OneToMany(targetEntity=RegleTarifaire::class,cascade={"persist"}, mappedBy="saison")
     */
    private $regleTarifaires;

    /**
     * @ORM\OneToMany(targetEntity=Presence::class, mappedBy="saison")
     */
    private $presences;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class,cascade={"persist"}, mappedBy="saison")
     */
    private $cours;

    /**
     * @ORM\OneToMany(targetEntity=Paiement::class, mappedBy="saison")
     */
    private $paiements;

    /**
     * @ORM\OneToMany(targetEntity=Evenement::class, mappedBy="saison")
     */
    private $evenements;

    /**
     * @ORM\OneToMany(targetEntity=Chat::class, mappedBy="saison")
     */
    private $chats;

    /**
     * @ORM\OneToMany(targetEntity=Purchase::class, mappedBy="saison")
     */
    private $purchases;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private $debut;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private $fin;

    /**
     * @ORM\OneToMany(targetEntity=Messages::class, mappedBy="saison")
     */
    private $messages;

    public function __construct()
    {
        $this->activites = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
        $this->dossiers = new ArrayCollection();
        $this->regleTarifaires = new ArrayCollection();
        $this->presences = new ArrayCollection();
        $this->cours = new ArrayCollection();
        $this->paiements = new ArrayCollection();
        $this->evenements = new ArrayCollection();
        $this->chats = new ArrayCollection();
        $this->purchases = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getSaison(): ?string
    {
        return $this->saison;
    }

    public function setSaison(string $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    /**
     * @return Collection|Activite[]
     */
    public function getActivites(): Collection
    {
        return $this->activites;
    }

    public function addActivite(Activite $activite): self
    {
        if (!$this->activites->contains($activite)) {
            $this->activites[] = $activite;
            $activite->setSaison($this);
        }

        return $this;
    }

    public function removeActivite(Activite $activite): self
    {
        if ($this->activites->removeElement($activite)) {
            // set the owning side to null (unless already changed)
            if ($activite->getSaison() === $this) {
                $activite->setSaison(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getSaison();
    }

    /**
     * @return Collection|Dossier[]
     */
    public function getDossiers(): Collection
    {
        return $this->dossiers;
    }

    public function addDossier(Dossier $dossier): self
    {
        if (!$this->dossiers->contains($dossier)) {
            $this->dossiers[] = $dossier;
            $dossier->setSaison($this);
        }

        return $this;
    }

    public function removeDossier(Dossier $dossier): self
    {
        if ($this->dossiers->removeElement($dossier)) {
            // set the owning side to null (unless already changed)
            if ($dossier->getSaison() === $this) {
                $dossier->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RegleTarifaire[]
     */
    public function getRegleTarifaires(): Collection
    {
        return $this->regleTarifaires;
    }

    public function addRegleTarifaire(RegleTarifaire $regleTarifaire): self
    {
        if (!$this->regleTarifaires->contains($regleTarifaire)) {
            $this->regleTarifaires[] = $regleTarifaire;
            $regleTarifaire->setSaison($this);
        }

        return $this;
    }

    public function removeRegleTarifaire(RegleTarifaire $regleTarifaire): self
    {
        if ($this->regleTarifaires->removeElement($regleTarifaire)) {
            // set the owning side to null (unless already changed)
            if ($regleTarifaire->getSaison() === $this) {
                $regleTarifaire->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Presence[]
     */
    public function getPresences(): Collection
    {
        return $this->presences;
    }

    public function addPresence(Presence $presence): self
    {
        if (!$this->presences->contains($presence)) {
            $this->presences[] = $presence;
            $presence->setSaison($this);
        }

        return $this;
    }

    public function removePresence(Presence $presence): self
    {
        if ($this->presences->removeElement($presence)) {
            // set the owning side to null (unless already changed)
            if ($presence->getSaison() === $this) {
                $presence->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setSaison($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->removeElement($cour)) {
            // set the owning side to null (unless already changed)
            if ($cour->getSaison() === $this) {
                $cour->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Paiement[]
     */
    public function getPaiements(): Collection
    {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): self
    {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements[] = $paiement;
            $paiement->setSaison($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): self
    {
        if ($this->paiements->removeElement($paiement)) {
            // set the owning side to null (unless already changed)
            if ($paiement->getSaison() === $this) {
                $paiement->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Evenement[]
     */
    public function getEvenements(): Collection
    {
        return $this->evenements;
    }

    public function addEvenement(Evenement $evenement): self
    {
        if (!$this->evenements->contains($evenement)) {
            $this->evenements[] = $evenement;
            $evenement->setSaison($this);
        }

        return $this;
    }

    public function removeEvenement(Evenement $evenement): self
    {
        if ($this->evenements->removeElement($evenement)) {
            // set the owning side to null (unless already changed)
            if ($evenement->getSaison() === $this) {
                $evenement->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Chat[]
     */
    public function getChats(): Collection
    {
        return $this->chats;
    }

    public function addChat(Chat $chat): self
    {
        if (!$this->chats->contains($chat)) {
            $this->chats[] = $chat;
            $chat->setSaison($this);
        }

        return $this;
    }

    public function removeChat(Chat $chat): self
    {
        if ($this->chats->removeElement($chat)) {
            // set the owning side to null (unless already changed)
            if ($chat->getSaison() === $this) {
                $chat->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Purchase[]
     */
    public function getPurchases(): Collection
    {
        return $this->purchases;
    }

    public function addPurchase(Purchase $purchase): self
    {
        if (!$this->purchases->contains($purchase)) {
            $this->purchases[] = $purchase;
            $purchase->setSaison($this);
        }

        return $this;
    }

    public function removePurchase(Purchase $purchase): self
    {
        if ($this->purchases->removeElement($purchase)) {
            // set the owning side to null (unless already changed)
            if ($purchase->getSaison() === $this) {
                $purchase->setSaison(null);
            }
        }

        return $this;
    }

    public function getDebut(): ?\DateTimeImmutable
    {
        return $this->debut;
    }

    public function setDebut(\DateTimeImmutable $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeImmutable
    {
        return $this->fin;
    }

    public function setFin(\DateTimeImmutable $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * @return Collection<int, Messages>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Messages $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setSaison($this);
        }

        return $this;
    }

    public function removeMessage(Messages $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getSaison() === $this) {
                $message->setSaison(null);
            }
        }

        return $this;
    }
}
