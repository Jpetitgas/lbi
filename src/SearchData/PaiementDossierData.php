<?php

namespace App\SearchData;

class PaiementDossierData
{
    public $q='';

    public $typeDePaiement;

    public $banque;

    public $encaissement;
    public $encaisse;

    public $nb=20;
}
