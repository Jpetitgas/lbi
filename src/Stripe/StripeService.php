<?php

namespace App\Stripe;

class StripeService
{
    protected $secretCle;
    protected $publicCle;

    public function __construct(string $secretCle, string $publicCle)
    {
        $this->publicCle = $publicCle;
        $this->secretCle = $secretCle;
    }

    public function ObtenirPublicCle()
    {
        return $this->publicCle;
    }

    public function ObtenirSecretCle()
    {
        return $this->secretCle;
    }
}
