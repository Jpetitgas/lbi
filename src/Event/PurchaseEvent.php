<?php

namespace App\Event;

use App\Entity\Purchase;
use Symfony\Contracts\EventDispatcher\Event;

class PurchaseEvent extends Event
{
    private $purchase;

    public function __construct(Purchase $purchase)
    {
        $this->purchase= $purchase;
    }

    public function getInscription(): Purchase
    {
        return $this->purchase;
    }
}
