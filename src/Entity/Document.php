<?php

namespace App\Entity;

use App\Repository\DocumentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DocumentRepository::class)
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Pratiquant::class, inversedBy="documents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pratiquant;

    /**
     * @ORM\ManyToOne(targetEntity=TypeDeDocument::class, inversedBy="documents")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Inscription::class, mappedBy="document",fetch="EAGER")
     */
    private $inscription;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateCertificat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;

    public function __construct()
    {
        $this->inscritption = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPratiquant(): ?Pratiquant
    {
        return $this->pratiquant;
    }

    public function setPratiquant(?Pratiquant $pratiquant): self
    {
        $this->pratiquant = $pratiquant;

        return $this;
    }

    public function getType(): ?TypeDeDocument
    {
        return $this->type;
    }

    public function setType(?TypeDeDocument $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscription(): Collection
    {
        return $this->inscription;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscription->contains($inscription)) {
            $this->inscription[] = $inscription;
            $inscription->setDocument($this);
        }

        return $this;
    }

    public function removeInscritption(Inscription $inscritption): self
    {
        if ($this->inscritption->removeElement($inscritption)) {
            // set the owning side to null (unless already changed)
            if ($inscritption->getDocument() === $this) {
                $inscritption->setDocument(null);
            }
        }

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateCertificat(): ?\DateTimeInterface
    {
        return $this->dateCertificat;
    }

    public function setDateCertificat(?\DateTimeInterface $dateCertificat): self
    {
        $this->dateCertificat = $dateCertificat;

        return $this;
    }
    public function __toString(): string
    {
        return $this->getDesignation();
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }
}
