<?php

namespace App\Controller\Gestion;

use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/gestion/sms", name="gestion_sms")
 */
class SmsController extends AbstractController
{
    /**
     * @Route("/envoi/{sms}/{message}", name="_envoi", methods={"GET", "POST"})
     */
    public function sms($sms, $message, UtilisateurRepository $utilisateurRepository, Security $security)
    {
        $destinaires = explode(',', $sms);
        $gestionnaire = $utilisateurRepository->findOneBy(['id' => $security->getUser()]);
        if ($gestionnaire->getQuotaSms() < 1 || $gestionnaire->getQuotaSms()==null) {
            $this->addFlash('danger', 'Aucun quota sms n\'a été indiqué pour ce gestionnaire');

            return $this->redirectToRoute('gestion_gestionnaire_index');
        }

        return $this->render('gestion/sms/sms.html.twig', [
            'quota' => $gestionnaire->getQuotaSms(),
            'destinataires' => $destinaires,
            'message' => $message,
        ]);
    }
}
