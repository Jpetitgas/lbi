<?php

namespace App\Controller\Intervenant;

use App\Entity\Chat;
use App\Entity\ConsultationChat;
use App\Event\NouveauMessageChatEvent;
use App\Form\ChatType;
use App\Repository\ChatRepository;
use App\Repository\ConsultationChatRepository;
use App\Repository\CoursRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/intervenant/chat", name="intervenant_chat")
 */
class _ChatController extends AbstractController
{
    /**
     * @Route("/{id_cours}", name="_show", methods={"GET","POST"})
     */
    public function show($id_cours, ChatRepository $chatRepository, EventDispatcherInterface $dispatcher, Security $security, Request $request, CoursRepository $coursRepository, ConsultationChatRepository $consultationChatRepository): Response
    {
        $cours = $coursRepository->find($id_cours);
        if ($cours->getIntervenant() === $security->getUser()) {
            $consultation = $consultationChatRepository->findOneBy(['utilisateur' => $security->getUser(), 'cours' => $cours]);
            if ($consultation) {
                $consultation->setDateConsultation(new \DateTimeImmutable('now'));
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->flush();
            } else {
                $consultation = new ConsultationChat();
                $consultation->setCours($cours)
                ->setUtilisateur($security->getUser())
                ->setDateConsultation(new \DateTimeImmutable('now'));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($consultation);
                $entityManager->flush();
            }
            $chats = $chatRepository->findBy(['cours' => $id_cours], ['created_at' => 'DESC']);
            $chat = new Chat();
            $formChat = $this->createForm(ChatType::class, $chat);
            $formChat->handleRequest($request);

            if ($formChat->isSubmitted() && $formChat->isValid()) {
                $chat->setExpediteur($this->getUser())
                ->setCours($cours);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($chat);
                $entityManager->flush();

                $chatEvent = new NouveauMessageChatEvent($chat);
                $dispatcher->dispatch($chatEvent, 'ajout.message.chat');

                return $this->redirectToRoute('intervenant_chat_show', ['id_cours' => $id_cours]);
            }
            $saison = $cours->getSaison();

            return $this->render('intervenant/chat/show.html.twig', [
            'saison' => $saison,
            'cours' => $cours,
            'chats' => $chats,
            'formChat' => $formChat->createView(),
        ]);
        }

        return $this->render('security/accesRefuse.html.twig');
    }

    /**
     * @Route("/{id}/{id_cours}", name="_delete", methods={"POST"})
     */
    public function delete($id_cours, Request $request, Chat $chat): Response
    {
        if ($this->isCsrfTokenValid('delete'.$chat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($chat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('intervenant_chat_show', ['id_cours' => $id_cours]);
    }
}
