<?php

namespace App\Entity;

use App\Repository\NiveauTarifaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NiveauTarifaireRepository::class)
 */
class NiveauTarifaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=RegleTarifaire::class, mappedBy="niveau",fetch="EAGER")
     */
    private $regleTarifaires;

    /**
     * @ORM\ManyToMany(targetEntity=Operateur::class, mappedBy="niveauTarifaire",fetch="EAGER")
     */
    private $operateurs;

    public function __construct()
    {
        $this->regleTarifaires = new ArrayCollection();
        $this->operateurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|RegleTarifaire[]
     */
    public function getRegleTarifaires(): Collection
    {
        return $this->regleTarifaires;
    }

    public function addRegleTarifaire(RegleTarifaire $regleTarifaire): self
    {
        if (!$this->regleTarifaires->contains($regleTarifaire)) {
            $this->regleTarifaires[] = $regleTarifaire;
            $regleTarifaire->setNiveau($this);
        }

        return $this;
    }

    public function removeRegleTarifaire(RegleTarifaire $regleTarifaire): self
    {
        if ($this->regleTarifaires->removeElement($regleTarifaire)) {
            // set the owning side to null (unless already changed)
            if ($regleTarifaire->getNiveau() === $this) {
                $regleTarifaire->setNiveau(null);
            }
        }

        return $this;
    }
    public function __toString(): string
    {
        return $this->getNom();
    }

    /**
     * @return Collection|Operateur[]
     */
    public function getOperateurs(): Collection
    {
        return $this->operateurs;
    }

    public function addOperateur(Operateur $operateur): self
    {
        if (!$this->operateurs->contains($operateur)) {
            $this->operateurs[] = $operateur;
            $operateur->addNiveauTarifaire($this);
        }

        return $this;
    }

    public function removeOperateur(Operateur $operateur): self
    {
        if ($this->operateurs->removeElement($operateur)) {
            $operateur->removeNiveauTarifaire($this);
        }

        return $this;
    }
}
