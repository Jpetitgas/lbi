<?php

namespace App\Controller\Gestion;

use App\Entity\CategorieActivite;
use App\Form\CategorieActiviteType;
use App\Form\CategorieActivite1Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\CategorieActiviteRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/categorie", name="gestion_categorie")
 */
class CategorieActiviteController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(CategorieActiviteRepository $categorieActiviteRepository): Response
    {
        return $this->render('gestion/categorie/index.html.twig', [
            'categorie_activites' => $categorieActiviteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $categorieActivite = new CategorieActivite();
        $formCategorie = $this->createForm(CategorieActiviteType::class, $categorieActivite);
        $formCategorie->handleRequest($request);

        if ($formCategorie->isSubmitted() && $formCategorie->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categorieActivite);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_categorie_index');
        }

        return $this->render('gestion/categorie/new.html.twig', [
            'categorie_activite' => $categorieActivite,
            'formCategorie' => $formCategorie->createView(),
        ]);
    }



    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CategorieActivite $categorieActivite): Response
    {
        $formCategorie = $this->createForm(CategorieActiviteType::class, $categorieActivite);
        $formCategorie->handleRequest($request);

        if ($formCategorie->isSubmitted() && $formCategorie->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_categorie_index');
        }

        return $this->render('gestion/categorie/edit.html.twig', [
            'categorie_activite' => $categorieActivite,
            'formCategorie' => $formCategorie->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, CategorieActivite $categorieActivite): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categorieActivite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categorieActivite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_categorie_index');
    }
}
