<?php

namespace App\Controller\Gestion;

use App\Entity\Section;
use App\Form\SectionType;
use App\Repository\SectionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/section", name="gestion_section")
 */
class SectionController extends AbstractController
{
    /**
     * @Route("/", name="_index", methods={"GET"})
     */
    public function index(SectionRepository $sectionRepository): Response
    {
        return $this->render('gestion/section/index.html.twig', [
            'sections' => $sectionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $section = new Section();
        $formSection = $this->createForm(SectionType::class, $section);
        $formSection->handleRequest($request);

        if ($formSection->isSubmitted() && $formSection->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($section);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_section_index');
        }

        return $this->render('gestion/section/new.html.twig', [
            'section' => $section,
            'formSection' => $formSection->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Section $section): Response
    {
        $formSection = $this->createForm(SectionType::class, $section);
        $formSection->handleRequest($request);

        if ($formSection->isSubmitted() && $formSection->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_section_index');
        }

        return $this->render('gestion/section/edit.html.twig', [
            'section' => $section,
            'formSection' => $formSection->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Section $section): Response
    {
        if ($this->isCsrfTokenValid('delete'.$section->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($section);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_section_index');
    }
}
