
window.addEventListener('DOMContentLoaded', responsivePlan)
window.addEventListener('resize', responsivePlan);
function responsivePlan() {
    let contenu = document.querySelector('.paquet-contenu');
    let plan = document.getElementById('plan');
    let pied = document.getElementById('pied');
    let largeurEcran = contenu.clientWidth - 20;
    let hauteurReglage = document.getElementById('reglage').offsetHeight;
    let hauteurPied = pied.offsetHeight;
    let largeur = plan.dataset['width']
    let hauteur = plan.dataset['height']
    //reglage.style.setProperty('--width-plan', largeur + "px")
    plan.style.setProperty('--height-plan', hauteur + "px")
    plan.style.setProperty('--width-plan', largeur + "px")
    if ((largeurEcran) < largeur) {
        let echelle = largeurEcran / largeur;
        let deplacementPlan = -(hauteur - (hauteur * echelle)) / 2;
        plan.style.setProperty('--translate-plan', deplacementPlan + "px")
        plan.style.setProperty('--scale', echelle);
        let deplacementPied = -(hauteur - (hauteur * echelle));
        pied.style.setProperty('--translate-plan', (deplacementPied) + "px")
        let newHauteurContenu = hauteurReglage + hauteurPied + (hauteur * echelle) + 40;
        
        contenu.style.setProperty('height', newHauteurContenu + "px")
    }
}
/* indication du container */
let balls = document.querySelectorAll(".ball")

for (let ball of balls) {
    let c = document.getElementById('nom')
    ball.addEventListener('mouseover', function () {
        
        c.innerHTML = this.dataset['nom']
    })
    ball.addEventListener('mouseout', function () {
        c.innerHTML = ""
    })
}
/* localition */
var macarte = null;
// Fonction d'initialisation de la carte
function initMap() { // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
    macarte = L.map('localisation').setView([
        lat, lon
    ], 15);
    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', { // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20,

    }).addTo(macarte);

}

// Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
initMap();

var marker = L.marker([
    lat,
    lon
]).addTo(macarte);
// Nous ajoutons la popup. A noter que son contenu (ici la variable ville) peut être du HTML
marker.bindPopup('Lieu');



/* Paiment stripe */
var evenement = document.getElementById('evenement');
var id_evenement = evenement.dataset.id;
var utilisateur = document.getElementById('utilisateur');
var id_utilisateur = utilisateur.dataset.id;
const stripe = Stripe(strikePublicCle);
var checkoutButton = document.getElementById('checkout-button');
if (checkoutButton != undefined) {
    checkoutButton.addEventListener('click', function () {
        // Create a new Checkout Session using the server-side endpoint you
        // created in step 3.
        fetch('/evenement/create-checkout-session', {
            method: 'POST',
            body: JSON.stringify({
                id_evenement: id_evenement,
                id_utilisateur: id_utilisateur
            }),
            headers: {
                "Content-type": "application/json"
            }
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (session) {
                return stripe.redirectToCheckout({ sessionId: session.id });
            })
            .then(function (result) {
                // If `redirectToCheckout` fails due to a browser or network
                // error, you should display the localized error message to your
                // customer using `error.message`.
                if (result.error) {
                    alert(result.error.message);
                }
            })
            .catch(function (error) {
                console.error('Error:', error);
            });
    });
}
