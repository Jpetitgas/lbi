<?php

namespace App\Repository;

use App\Entity\CorpFacture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CorpFacture|null find($id, $lockMode = null, $lockVersion = null)
 * @method CorpFacture|null findOneBy(array $criteria, array $orderBy = null)
 * @method CorpFacture[]    findAll()
 * @method CorpFacture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CorpFactureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CorpFacture::class);
    }

    // /**
    //  * @return CorpFacture[] Returns an array of CorpFacture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CorpFacture
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
