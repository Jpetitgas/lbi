<?php

namespace App\Controller;

use App\Service\SendEmail;
use App\Entity\Utilisateur;
use App\Message\SendEmailMessage;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register", methods={"GET", "POST"})    */


    public function register(
        Request $request,
        SendEmail $sendEmail,
        FormFactoryInterface $formFactory,
        MessageBusInterface $messageBus,
        TokenGeneratorInterface $tokenGenerator,
        UserPasswordEncoderInterface $passwordEncoder,
        UrlGeneratorInterface $urlGenerator
    ): Response {
        $user = new Utilisateur();
        $formUtilisateur = $this->createForm(RegistrationFormType::class, $user);
        $formUtilisateur = $formFactory->createNamed('utilisateur', RegistrationFormType::class, $user);
        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $registrationToken = $tokenGenerator->generateToken();
            $user->setRegistrationToken($registrationToken)
                ->setPassword($passwordEncoder->encodePassword($user, $formUtilisateur->get('password')->getData()));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // Générer l'URL de confirmation
            $confirmationUrl = $request->getSchemeAndHttpHost() . '/verify/' . $user->getId() . '/' . $registrationToken;

            $emailContent = "
            <div class='paquet reduireL'>
                <div class='paquet-header'>
                    <h1 class='text-center'>Veuillez confirmer votre email!</h1>
                </div>
                <div class='paquet-contenu'>
                    <p>
                        Veuillez confirmer votre adresse e-mail en cliquant sur le lien suivant:
                        <br><br>
                        <a href='{$confirmationUrl}'>Confirmer mon e-mail</a>.
                        
                        Ce lien expirera le {$user->getAccountMustBeVerifiedBefore()->format('d/m/Y à H:i')}.
                    </p>
                </div>
            </div>
        ";

            $sendEmail->send([
                'recipient_email' => $user->getEmail(),
                'sujet' => 'Vérification de votre adresse e-mail pour activer votre compte',
                'htmlTemplate' => 'email/email.html.twig',
                'destinataire' => $user,
                'contenu' => $emailContent,
            ]);

            $this->addFlash('success', 'Un mail de validation vient de vous être adressé');

            return $this->redirectToRoute('security_login');
        }

        return $this->render('registration/register.html.twig', [
            'formUtilisateur' => $formUtilisateur->createView(),
        ]);
    }

    private function isNotRequestedInTime(\DateTimeImmutable $getAccountMustBeVerifiedBefore): bool
    {
        return new \DateTimeImmutable('now') > $getAccountMustBeVerifiedBefore;
    }
    /**
     * @Route("/verify/{id<\d+>}/{token}", name="app_verify_account", methods={"GET"})
     */
    public function verifyUserAccount(Utilisateur $utilisateur, EntityManagerInterface $entityManagerInterface, string $token)
    {
        if (($utilisateur->getRegistrationToken() === null) || ($utilisateur->getRegistrationToken() !== $token) || ($this->isNotRequestedInTime($utilisateur->getAccountMustBeVerifiedBefore()))) {
            throw new AccessDeniedException();
        }
        $utilisateur->setIsVerified(true);
        $utilisateur->setAccountVerifiedAt(new \DateTimeImmutable('now'));
        $utilisateur->setRegistrationToken(null);
        $entityManagerInterface->flush();

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Votre adresse e-mail a été vérifiée. Vous pouvez vous connecter');

        return $this->redirectToRoute('security_login');
    }
    /**
     * @Route("/essai", name="essai", methods={"GET"})
     */
    public function essai(Security $security)
    {
        $user = $security->getUser();
        /**@var Utilisateur $user */

        return $this->render('registration/confirmation_email.html.twig', [
            'utilisateurID' => $user->getId(),
            'registrationToken' => $user->getRegistrationToken(),
            'tokenLifeTime' => $user->getAccountMustBeVerifiedBefore()->format('d/m/Y à H:i'),
        ]);
    }
}
