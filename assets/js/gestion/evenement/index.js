
let actif = document.querySelectorAll(".actif")
for (let bouton of actif) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/actif/' + this.dataset.id)
        xmlhttp.send()
    })
}
let payant = document.querySelectorAll(".payant")
for (let bouton of payant) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/payant/' + this.dataset.id)
        xmlhttp.send()
    })
}
let plan = document.querySelectorAll(".plan")
for (let bouton of plan) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/evenement/plan/' + this.dataset.id)
        xmlhttp.send()
    })
}

