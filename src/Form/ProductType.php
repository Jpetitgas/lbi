<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Category;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FileUploadError;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Eckinox\TinymceBundle\Form\Type\TinymceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'required' => true,
            ])
            ->add('shortDescription', TinymceType::class, [
                'required' => true,
                'label' => 'Description du produit',
            ])
            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'required' => true,
                'class' => Category::class,
                'placeholder' => 'Selectionner une categorie',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
            ])
            ->add('enAvant', CheckboxType::class, [
                'label' => 'Mis en avant',
                'required' => false,
            ])
            ->add('actif', CheckboxType::class, [
                'label' => 'Mis en boutique',
                'required' => false,
            ])
            ->add('imageFile', VichFileType::class, [
                'label' => 'Choisir une photo pour ce produit',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
