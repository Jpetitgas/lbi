<?php

namespace App\Controller\Gestion\Boutique;

use App\Entity\Taille;
use App\Entity\Product;
use App\Entity\Category;
use App\Form\TailleType;
use App\Form\ProductType;
use App\Form\BoutiqueType;
use App\Form\CategoryType;

use App\Repository\TailleRepository;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TailleController extends AbstractController
{
    /**
     * @Route("gestion/boutique/taille/{id}", name="gestion_boutique_taille_index")
     */
    public function indexTaille($id, PaginatorInterface $paginator, Request $request, TailleRepository $tailleRepository, ProductRepository $productRepository): Response
    {
        $product = $productRepository->find($id);
        $tailles =$paginator->paginate(
            $tailleRepository->findBy(['product' => $id]),
            $request->query->getInt('page', 1),
            20
        ) ;
        return $this->render(
            'gestion/boutique/taille/index.html.twig',
            [
                'product' => $product,
                'tailles' => $tailles
            ]
        );
    }

    /**
     * @Route("gestion/boutique/taille/edit/{id}", name="gestion_boutique_taille_edit")
     */
    public function editTaille($id, Request $request, TailleRepository $tailleRepository): Response
    {
        $taille = $tailleRepository->find($id);
        $formTaille = $this->createForm(TailleType::class, $taille);
        $formTaille->handleRequest($request);

        if ($formTaille->isSubmitted() && $formTaille->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_boutique_taille_index', ['id' => $taille->getProduct()->getId()]);
        }

        return $this->render('gestion/boutique/taille/edit.html.twig', [
            'taille' => $taille,
            'formTaille' => $formTaille->createView(),
        ]);
    }

    /**
     * @Route("gestion/boutique/taille/new/{id}", name="gestion_boutique_taille_new")
     */
    public function newTaille($id, Request $request, ProductRepository $productRepository): Response
    {
        $product = $productRepository->find($id);
        $taille = new Taille();
        $formTaille = $this->createForm(TailleType::class, $taille);
        $formTaille->handleRequest($request);

        if ($formTaille->isSubmitted() && $formTaille->isValid()) {
            $taille->setProduct($product);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($taille);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_boutique_taille_index', ['id' => $taille->getProduct()->getId()]);
        }

        return $this->render('gestion/boutique/taille/new.html.twig', [
            'product' => $product,
            'taille' => $taille,
            'formTaille' => $formTaille->createView(),
        ]);
    }
    /**
        * @Route("gestion/boutique/taille/delete/{id}", name="gestion_boutique_taille_delete")
        */
    public function delete($id, Request $request, TailleRepository $tailleRepository): Response
    {
        $taille = $tailleRepository->find($id);
        if ($taille) {
            if ($this->isCsrfTokenValid('delete'.$taille->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($taille);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('gestion_boutique_taille_index', ['id' => $taille->getProduct()->getId()]);
    }
}
