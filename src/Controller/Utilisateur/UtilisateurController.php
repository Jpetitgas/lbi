<?php

namespace App\Controller\Utilisateur;

use App\Entity\Utilisateur;
use App\Form\UtilisateurType;
use App\Form\UtilisateurUtilisateurType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/utilisateur/profile", name="utilisateur_profile")
 */
class UtilisateurController extends AbstractController
{
    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $utilisateur = new Utilisateur();
        $formUtilisateur = $this->createForm(UtilisateurType::class, $utilisateur);

        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($utilisateur);
            $entityManager->flush();

            return $this->redirectToRoute('espace');
        }

        return $this->render('utilisateur/utilisateur/new.html.twig', [
            'utilisateur' => $utilisateur,
            'formUtilisateur' => $formUtilisateur->createView(),
        ]);
    }

    /**
     * @Route("/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FormFactoryInterface $formFactory, EntityManagerInterface $em): Response
    {
        $utilisateur = $this->getUser();

        $formUtilisateur = $this->createForm(UtilisateurUtilisateurType::class, $utilisateur);
        $formUtilisateur = $formFactory->createNamed('utilisateur', UtilisateurUtilisateurType::class, $utilisateur);
        $formUtilisateur->handleRequest($request);

        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $em->flush();

            return $this->redirectToRoute('espace');
        }

        return $this->render('utilisateur/utilisateur/edit.html.twig', [
            'utilisateur' => $utilisateur,
            'formUtilisateur' => $formUtilisateur->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Utilisateur $utilisateur): Response
    {
        if ($this->isCsrfTokenValid('delete' . $utilisateur->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($utilisateur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('utilisateur_index');
    }
}
