<?php

namespace App\EventListener;

use App\Entity\Chat;

use App\Service\EnCours;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class ChatListener
{
    protected $saison;

    public function __construct(EnCours $encours)
    {
        $this->saison=$encours->saison();
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $chat=$event->getObject();

        if ($chat instanceof Chat) {
            $chat->setSaison($this->saison);
        }
    }
}
