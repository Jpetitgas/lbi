
let plan = document.querySelector('#evenement_plan')
plan.addEventListener("change", function () {
    let form = this.closest("form");
    
    let data = this.name + "=" + this.checked

    fetch(form.action, {
        method: form.getAttribute("method"),
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset:utf-8"
        }
    })
        .then(response => response.text())
        .then(html => {
            let content = document.createElement("html");
            content.innerHTML = html;

            console.log(content);
            let nouvelImage = content.querySelector("#evenement_imageFile_file");
            let nouveauLargeur = content.querySelector("#evenement_largeur");
            let nouveauHauteur = content.querySelector("#evenement_hauteur");
            let fileField="";
            if (document.querySelector('#evenement_imageFile')) {
                fileField = document.querySelector('#evenement_imageFile');
            } else {
                fileField = document.querySelector('#evenement_imageFile_file');
            }
            if (fileField.parentElement.localName === "form") {
                /** Ajout du champ image */
                var newDiv = document.createElement("div");
                newDiv.classList.add('form-group');
                var label = document.createElement('label');
                label.classList.add('required');
                label.setAttribute("for", 'evenement_imageFile_file');
                label.innerHTML = "Choisir un dessin de fond pour le plan de salle";
                newDiv.appendChild(label);
                newDiv.appendChild(nouvelImage);
                document.querySelector('#evenement_imageFile').replaceWith(newDiv);
            } else {
                fileField.replaceWith(nouvelImage);

            }
            /** Ajout du champ largeur */
            if (document.querySelector('#evenement_largeur').parentElement.localName === "form") {
                var newDiv = document.createElement("div");
                newDiv.classList.add('form-group');
                var label = document.createElement('label');
                label.classList.add('required');
                label.setAttribute("for", 'evenement_largeur');
                label.innerHTML = "Largeur du lieu";
                newDiv.appendChild(label);
                newDiv.appendChild(nouveauLargeur);
                document.querySelector('#evenement_largeur').replaceWith(newDiv);
            } else {
                document.querySelector('#evenement_largeur').replaceWith(nouveauLargeur);

            }
            /** Ajout du champ hauteur */
            if (document.querySelector('#evenement_hauteur').parentElement.localName === "form") {
                var newDiv = document.createElement("div");
                newDiv.classList.add('form-group');
                var label = document.createElement('label');
                label.classList.add('required');
                label.setAttribute("for", 'evenement_hauteur');
                label.innerHTML = "Hauteur lieu";
                newDiv.appendChild(label);
                newDiv.appendChild(nouveauHauteur);
                document.querySelector('#evenement_hauteur').replaceWith(newDiv);
            } else {
                document.querySelector('#evenement_hauteur').replaceWith(nouveauHauteur);
            }


        })
})
