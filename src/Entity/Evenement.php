<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EvenementRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=EvenementRepository::class)
 * @Vich\Uploadable
 */
class Evenement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $actif = false;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class, inversedBy="evenements")
     */
    private $saison;

    /**
     * @ORM\OneToMany(targetEntity=Container::class, mappedBy="evenement", cascade={"persist"}, orphanRemoval=true,fetch="EAGER")
     */
    private $containers;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageB;

    /**
     * @Vich\UploadableField(mapping="evenement_images", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @Vich\UploadableField(mapping="evenement_images", fileNameProperty="imageB")
     *
     * @var File
     */
    private $imageBillet;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $localisation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $payant = false;

    /**
     * @ORM\OneToMany(targetEntity=PaiementEvenement::class, mappedBy="evenement")
     */
    private $paiementEvenements;

    /**
     * @ORM\Column(type="boolean")
     */
    private $plan = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $quota;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $largeur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hauteur;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieSection::class, inversedBy="evenements")
     */
    private $section;

    /**
     * @ORM\OneToMany(targetEntity=Option::class, mappedBy="evenement")
     */
    private $options;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionBillet;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reduction;

    public function __construct()
    {
        $this->containers = new ArrayCollection();
        $this->information = new ArrayCollection();
        $this->paiementEvenements = new ArrayCollection();
        $this->options = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getNom();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    /**
     * @return Collection|Container[]
     */
    public function getContainers(): Collection
    {
        return $this->containers;
    }

    public function addContainer(Container $container): self
    {
        if (!$this->containers->contains($container)) {
            $this->containers[] = $container;
            $container->setEvenement($this);
        }

        return $this;
    }

    public function removeContainer(Container $container): self
    {
        if ($this->containers->removeElement($container)) {
            // set the owning side to null (unless already changed)
            if ($container->getEvenement() === $this) {
                $container->setEvenement(null);
            }
        }

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }



    public function getLocalisation()
    {
        return $this->localisation;
    }

    public function setLocalisation($localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
    public function setImageBillet(File $image = null)
    {
        $this->imageBillet = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageBillet()
    {
        return $this->imageBillet;
    }

    public function setImageB($image)
    {
        $this->imageB = $image;
    }

    public function getImageB()
    {
        return $this->imageB;
    }

    public function getPayant(): ?bool
    {
        return $this->payant;
    }

    public function setPayant(bool $payant): self
    {
        $this->payant = $payant;

        return $this;
    }

    /**
     * @return Collection|PaiementEvenement[]
     */
    public function getPaiementEvenements(): Collection
    {
        return $this->paiementEvenements;
    }

    public function addPaiementEvenement(PaiementEvenement $paiementEvenement): self
    {
        if (!$this->paiementEvenements->contains($paiementEvenement)) {
            $this->paiementEvenements[] = $paiementEvenement;
            $paiementEvenement->setEvenement($this);
        }

        return $this;
    }

    public function removePaiementEvenement(PaiementEvenement $paiementEvenement): self
    {
        if ($this->paiementEvenements->removeElement($paiementEvenement)) {
            // set the owning side to null (unless already changed)
            if ($paiementEvenement->getEvenement() === $this) {
                $paiementEvenement->setEvenement(null);
            }
        }

        return $this;
    }

    public function getPlan(): ?bool
    {
        return $this->plan;
    }

    public function setPlan(bool $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    public function getQuota()
    {
        return $this->quota;
    }

    public function setQuota($quota): self
    {
        $this->quota = $quota;

        return $this;
    }

    public function getLargeur()
    {
        return $this->largeur;
    }

    public function setLargeur($largeur): self
    {
        $this->largeur = $largeur;

        return $this;
    }

    public function getHauteur()
    {
        return $this->hauteur;
    }

    public function setHauteur($hauteur): self
    {
        $this->hauteur = $hauteur;

        return $this;
    }

    public function getSection(): ?CategorieSection
    {
        return $this->section;
    }

    public function setSection(?CategorieSection $section): self
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return Collection|Option[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(Option $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
            $option->setEvenement($this);
        }

        return $this;
    }

    public function removeOption(Option $option): self
    {
        if ($this->options->removeElement($option)) {
            // set the owning side to null (unless already changed)
            if ($option->getEvenement() === $this) {
                $option->setEvenement(null);
            }
        }

        return $this;
    }

    public function getDescriptionBillet(): ?string
    {
        return $this->descriptionBillet;
    }

    public function setDescriptionBillet(?string $descriptionBillet): self
    {
        $this->descriptionBillet = $descriptionBillet;

        return $this;
    }

    public function __clone()
    {
        
        $this->setNom($this->getNom(). " cloné");
        foreach ($this->containers as $container) {
            $containerClone=clone $container;
            $this->addContainer($containerClone);
            $places = new ArrayCollection();
        foreach ($container->getPlaces() as $place) {
            /* @var Address $address */
            $place = clone $place;
            $place->setUtilisateur(null)
                ->setVerrouiller(false)
                ->setContainer($containerClone);
            $places->add($place);
        }
    }
        
    }

    public function getReduction(): ?int
    {
        return $this->reduction;
    }

    public function setReduction(?int $reduction): self
    {
        $this->reduction = $reduction;

        return $this;
    }
}
