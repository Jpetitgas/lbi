<?php

namespace App\Controller\Gestion;

use App\Entity\Chat;
use App\Form\ChatType;
use App\Entity\ConsultationChat;
use App\Repository\ChatRepository;
use App\Repository\CoursRepository;
use App\Repository\SaisonRepository;
use App\Event\NouveauMessageChatEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use App\Repository\ConsultationChatRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/chat", name="gestion_chat")
 */
class ChatController extends AbstractController
{
    /**
     * @Route("/index/{id_saison}", name="_index", methods={"GET","POST"})
     */
    public function index($id_saison, SaisonRepository $saisonRepository, CoursRepository $coursRepository): Response
    {
        $saison = $saisonRepository->find($id_saison);
        $cours = $coursRepository->findBy(['saison' => $saison], ['nom' => 'ASC']);

        return $this->render('gestion/chat/index.html.twig', [
            'saison' => $saison,
            'cours' => $cours,
        ]);
    }


    /**
     * @Route("/{id_cours}", name="_show", methods={"GET","POST"})
     */
    /* public function show($id_cours, ChatRepository $chatRepository, EventDispatcherInterface $dispatcher, Security $security, Request $request, CoursRepository $coursRepository, ConsultationChatRepository $consultationChatRepository): Response
    {
      $cours = $coursRepository->find($id_cours);
      $consultation = $consultationChatRepository->findOneBy(['utilisateur' => $security->getUser(), 'cours' => $cours]);
      if ($consultation) {
          $consultation->setDateConsultation(new \DateTimeImmutable('now'));
          $entityManager = $this->getDoctrine()->getManager();

          $entityManager->flush();
      } else {
          $consultation = new ConsultationChat();
          $consultation->setCours($cours)
              ->setUtilisateur($security->getUser())
              ->setDateConsultation(new \DateTimeImmutable('now'));
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($consultation);
          $entityManager->flush();
      }
      $chats = $chatRepository->findBy(['cours' => $id_cours], ['created_at' => 'DESC']);
      $chat = new Chat();
      $formChat = $this->createForm(ChatType::class, $chat);
      $formChat->handleRequest($request);

      if ($formChat->isSubmitted() && $formChat->isValid()) {
          $chat->setExpediteur($this->getUser())
              ->setCours($cours);
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($chat);
          $entityManager->flush();

          $chatEvent = new NouveauMessageChatEvent($chat);
          $dispatcher->dispatch($chatEvent, 'ajout.message.chat');

          return $this->redirectToRoute('gestion_chat_show', ['id_cours' => $id_cours]);
      }
      $saison = $cours->getSaison();

      return $this->render('gestion/chat/show.html.twig', [
          'saison' => $saison,
          'cours' => $cours,
          'chats' => $chats,
          'formChat' => $formChat->createView(),
      ]);
    }*/

    /**
     * @Route("/{id}/{id_cours}", name="_delete", methods={"POST"})
     */
    public function delete($id_cours, Request $request, Chat $chat): Response
    {
        if ($this->isCsrfTokenValid('delete'.$chat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($chat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('utilisateur_chat_show', ['id_cours' => $id_cours]);
    }
}
