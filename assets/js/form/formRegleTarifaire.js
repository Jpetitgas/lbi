import 'jquery'
const $ = require('jquery');
global.$ = $;

import 'select2/dist/css/select2.css'
import 'select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.rtl.min.css'
import 'select2/dist/js/select2'

$(document).ready(function () {
    function formSecondaryConstruct() {
        let form = this.closest("form");
        let data = this.name + "=" + this.value
        fetch(form.action, {
            method: form.getAttribute("method"),
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset:utf-8"
            }
        })
            .then(response => response.text())
            .then(html => {
                let content = document.createElement("html");
                content.innerHTML = html;
                modifElement('regle_tarifaire_parametre', 'Paramètre', content);
                modifElement('regle_tarifaire_villes', 'Villes', content);
                modifElement('regle_tarifaire_montant', 'Montant', content);

                var communes = $('#regle_tarifaire_villes');
                if (!(communes.attr('type') == 'hidden')) {
                    communes.select2({
                        theme: "bootstrap-5",
                    });
                }
            })
    }
    function formPrimaryConstruct() {
        let form = this.closest("form");
        let data = this.name + "=" + this.value

        fetch(form.action, {
            method: form.getAttribute("method"),
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset:utf-8"
            }
        })
            .then(response => response.text())
            .then(html => {
                let content = document.createElement("html");
                content.innerHTML = html;
                modifElement('regle_tarifaire_operateur', 'Opérateur', content)
                $('#regle_tarifaire_operateur').on('change', formSecondaryConstruct)
                modifElement('regle_tarifaire_parametre', 'Paramètre', content);
                modifElement('regle_tarifaire_villes', 'Villes', content);
                modifElement('regle_tarifaire_montant', 'Montant', content);
            })
    }
    $('#regle_tarifaire_niveau').on('change', formPrimaryConstruct);
    $('#regle_tarifaire_operateur').on('change', formSecondaryConstruct);

    var communes = $('#regle_tarifaire_villes');
    if (!(communes.attr('type') == 'hidden')) {
        communes.select2({
            theme: "bootstrap-5",
        });
    }
    function modifElement(champ, titre, content) {
        let element = document.getElementById(champ);
        if (typeof (element) != 'undefined' && element != null) {
            let nouveau = content.querySelector('#' + champ);
            let ancienChamp = document.querySelector('#' + champ);
            let parent = ancienChamp.parentElement;
            if (parent.localName === "form") {
                if (nouveau.type !== "hidden") {
                    let nouveau = content.querySelector('#' + champ);
                    var newDiv = document.createElement("div");
                    newDiv.classList.add('mb-3');
                    var label = document.createElement('label');
                    label.classList.add('form-label');
                    label.classList.add('required');
                    label.setAttribute("for", champ);
                    label.innerHTML = titre;
                    newDiv.appendChild(label);
                    newDiv.appendChild(nouveau);
                    document.querySelector('#' + champ).replaceWith(newDiv);
                }
            } else {
                if (parent.localName === "div" && nouveau.type === "hidden") {
                    //parent.removeChild(document.querySelector('#' + champ))
                    ancienChamp.closest(".mb-3").replaceWith(nouveau)
                }

                if (parent.localName === "div" && nouveau.type !== "hidden") {
                    ancienChamp.replaceWith(nouveau);
                }
            }

        }

    }
});

