<?php

namespace App\Controller\Utilisateur;

use App\Entity\Inscription;
use App\Event\InscriptionEvent;
use App\Form\InscriptionType;
use App\Repository\DocumentRepository;
use App\Repository\DossierRepository;
use App\Repository\InformationRepository;
use App\Repository\InscriptionRepository;
use App\Repository\PratiquantRepository;
use App\Service\StatutInscription;
use App\Service\VerificationActivite;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur/inscription", name="utilisateur_inscription")
 */
class InscriptionController extends AbstractController
{
    /**
     * @Route("/{id}/edit/{id_dossier}", name="_edit", methods={"GET","POST"})
     */
    public function edit($id_dossier, Request $request, VerificationActivite $verificationActivite, Inscription $inscription, DossierRepository $dossierRepository, InformationRepository $informationRepository)
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        $dossier = $dossierRepository->find($id_dossier);
        $formInscription = $this->createForm(InscriptionType::class, $inscription);
        $formInscription->handleRequest($request);

        if ($formInscription->isSubmitted() && $formInscription->isValid()) {
            if ($verificationActivite->certificatMedical($inscription->getActivite())) {
                $inscription->setCertificatObligatoire(true);
            } else {
                $inscription->setCertificatObligatoire(false);
            }
            $this->getDoctrine()->getManager()->flush();
            if ($formInscription->offsetExists('ajouter')) {
                if ($formInscription->get('ajouter')->isClicked()) {
                    return $this->redirectToRoute('utilisateur_document_new', ['id_pratiquant' => $inscription->getPratiquant()->getId(), 'id_dossier' => $dossier->getId()]);
                }
            }
            $ancre = 'pratiquant_' . $inscription->getPratiquant()->getId() . '_inscription';

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
        }

        return $this->render('utilisateur/inscription/edit.html.twig', [
            'inscription' => $inscription,
            'formInscription' => $formInscription->createView(),
        ]);
    }

    /**
     * @Route("/new/{id_pratiquant}/{id_dossier}", name="_new", methods={"GET","POST"})
     */
    public function new($id_pratiquant, EventDispatcherInterface $dispatcher, $id_dossier, StatutInscription $statutInscription, InformationRepository $informationRepository, VerificationActivite $verificationActivite, DossierRepository $dossierRepository, PratiquantRepository $pratiquantRepository, Request $request)
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }
        $inscription = new Inscription();
        $pratiquant = $pratiquantRepository->find($id_pratiquant);
        $inscription->setPratiquant($pratiquant);
        $formInscription = $this->createForm(InscriptionType::class, $inscription);
        $formInscription->handleRequest($request);

        if ($formInscription->isSubmitted() && $formInscription->isValid()) {
            $dossier = $dossierRepository->find($id_dossier);
            $inscription
                ->setDossier($dossier)
                ->setStatut($statutInscription->statut($inscription->getActivite()))
                ->setDateChangementStatut(new DateTime())

                ->setCertificatValide(false)
                ->setTransmisAssureur((false))
                ->setCanceled(0);

            if ($verificationActivite->certificatMedical($inscription->getActivite())) {
                $inscription->setCertificatObligatoire(true);
            } else {
                $inscription->setCertificatObligatoire(false);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($inscription);
            $entityManager->flush();

            $inscriptionEvent = new InscriptionEvent($inscription);
            $dispatcher->dispatch($inscriptionEvent, 'ajout.inscription');
            if ($formInscription->offsetExists('ajouter')) {
                if ($formInscription->get('ajouter')->isClicked()) {
                    return $this->redirectToRoute('utilisateur_document_new', ['id_pratiquant' => $pratiquant->getId(), 'id_dossier' => $dossier->getId()]);
                }
            }

            $ancre = 'pratiquant_' . $pratiquant->getId() . '_inscription';

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
        }

        return $this->render('utilisateur/inscription/new.html.twig', [
            'inscription' => $inscription,
            'formInscription' => $formInscription->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, EventDispatcherInterface $dispatcher, StatutInscription $StatutInscription, Inscription $inscription, InformationRepository $informationRepository): Response
    {
        $information = $informationRepository->findOneBy([]);
        if ($information->getModificationDossier() == false) {
            $this->addFlash('danger', 'Les modifications du dossier ne sont pas autorisées');

            return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => 'information_none_none']);
        }

        $dossier = $inscription->getDossier();
        if ($this->isCsrfTokenValid('delete' . $inscription->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $inscriptionEvent = new InscriptionEvent($inscription);
            $dispatcher->dispatch($inscriptionEvent, 'suppression.inscription');
            $entityManager->remove($inscription);
            $entityManager->flush();
        }
        $ancre = 'pratiquant_' . $inscription->getPratiquant()->getId() . '_inscription';

        /*$StatutInscription->deleteInscription(($inscription));*/

        return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
    }

    /**
     * @Route("/document/{id_inscription}/{id_document}", name="_document", methods={"GET","POST"})
     */
    public function document($id_inscription, $id_document, InscriptionRepository $inscriptionRepository, DocumentRepository $documentRepository)
    {
        $inscription = $inscriptionRepository->find($id_inscription);
        $document = $documentRepository->find($id_document);
        $inscription->setDocument($document);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        $ancre = 'pratiquant_' . $inscription->getPratiquant()->getId() . '_inscription';

        return $this->redirectToRoute('utilisateur_dossier_edit', ['ancre' => $ancre]);
    }
}
