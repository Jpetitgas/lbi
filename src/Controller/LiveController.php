<?php

namespace App\Controller;

use App\Entity\Information;
use App\Repository\InformationRepository;
use ContainerPAvtA55\getLiveControllerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LiveController extends AbstractController
{

    /**
     * @Route("/live", name="live", methods={"GET","POST"})
     */
    public function index(InformationRepository $informationRepository): Response

    {
        $information = $informationRepository->findOneBy([]);

        if (!$information->getLive()) {
            $this->addFlash('danger', 'Vous n\'avez pas accés au direct!!');

            return $this->redirectToRoute('espace');
        }
        return $this->render('live/index.html.twig', [
            'information' => $information,
        ]);
    }
}
