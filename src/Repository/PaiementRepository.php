<?php

namespace App\Repository;

use App\Entity\Saison;
use App\Entity\Paiement;
use App\SearchData\PaiementDossierData;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\AST\Functions\SumFunction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Paiement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Paiement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Paiement[]    findAll()
 * @method Paiement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaiementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Paiement::class);
    }

    /**
     * @return Paiement[] Returns an array of Paiement objects
     */
    public function paiementsByDossier($dossier)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.dossier = :val')
            ->setParameter('val', $dossier)
            ->getQuery()
            ->getResult()
        ;
    }

    public function paiementsParSaison($saison)
    {
        return $this->createQueryBuilder('p')
            ->join('p.dossier', 'd', 'WITH', 'p.dossier=d.id')
            ->andWhere('d.saison = :saison')
            ->setParameter('saison', $saison)
            ->getQuery()
            ->getResult()
        ;
    }

    public function totalPaiementParSaison($saison)
    {
        return $this->createQueryBuilder('p')
            ->join('p.dossier', 'd', 'WITH', 'p.dossier=d.id')
            ->andWhere('d.saison = :saison')
            ->setParameter('saison', $saison)
            ->select('SUM(p.montant) as totalPaiement')
            ->getQuery()
            ->getResult()
        ;
    }
    public function paiements(Saison $saison, PaiementDossierData $data)
    {
        $query = $this
            ->createQueryBuilder('p')
            ->select('p', 'd', 'u', 't', 'b')
            ->leftJoin('p.typeDePaiement', 't')
            ->leftJoin('p.banque', 'b')
            ->leftJoin('p.dossier', 'd')
            ->leftJoin('d.adherent', 'u')
            ->where('d.saison = :saison')
            ->setParameter('saison', $saison);

        if (!empty($data->q)) {
            $query = $query
                ->andWhere('u.nom LIKE :q or u.prenom LIKE :q or p.remarque LIKE :q')
                ->setParameter('q', "%{$data->q}%");
        }
        if (!empty($data->typeDePaiement)) {
            $query = $query
                ->andWhere('p.typeDePaiement =:type')
                ->setParameter('type', $data->typeDePaiement);
        }
        if (!empty($data->banque)) {
            $query = $query
                ->andWhere('p.banque =:banque')
                ->setParameter('banque', $data->banque);
        }
        if (!empty($data->encaissement)) {
            $query = $query
                ->andWhere('p.encaissement LIKE :q')
                ->setParameter('q', "%{$data->encaissement}%");
        }
        if ($data->encaisse === true) {
            $query = $query->andWhere('p.encaisse =1');
        }
        if ($data->encaisse === false) {
            $query = $query->andWhere('p.encaisse =0');
        }

        return $query->getQuery();
    }
    /*
    public function findOneBySomeField($value): ?Paiement
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
