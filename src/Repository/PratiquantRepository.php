<?php

namespace App\Repository;

use App\Entity\Dossier;
use App\Entity\Pratiquant;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Pratiquant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pratiquant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pratiquant[]    findAll()
 * @method Pratiquant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PratiquantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pratiquant::class);
    }


    public function pratiquantAdherent(Dossier $dossier)
    {
        $adherent=$dossier->getAdherent();
        return $this->createQueryBuilder('p')
            ->andWhere('p.utilisateur = :adherent')
            ->setParameter('adherent', $adherent)
            ->innerJoin('p.inscriptions', 'i', 'WITH', 'p.id=i.pratiquant')
            ->andWhere('i.dossier =:dossier')
            ->setParameter('dossier', $dossier)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Pratiquant
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
