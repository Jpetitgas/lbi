<?php

namespace App\SearchForm;

use App\SearchData\DossierData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class DossierForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('q', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Rechercher',
                ],
            ])
            ->add('bulletinPresent', ChoiceType::class, [
                'required' => false,
                'choices' => [
                        'Oui' => "Oui",
                        'Non' => "Non",
                    ],
            ])
            ->add('nbCertificatManquant', IntegerType::class, [
                'required' => false,
            ])
             ->add('cotisationAJour', ChoiceType::class, [
                'required' => false,
                'choices' => [
                        'Oui' => "Oui",
                        'Non' => "Non",
                    ],
            ])

            ->add('bulletinValide', ChoiceType::class, [
                'required' => false,
                'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
            ])

            ->add('dossierVerrouille', ChoiceType::class, [
                'required' => false,
                'label'=>'Dossier complet',
                'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
            ])
             ->add('locked', ChoiceType::class, [
                'required' => false,
                'label'=>'Dossier supendu',
                'choices' => [
                        'Oui' => true,
                        'Non' => false,
                    ],
            ])
            ->add('nb', IntegerType::class, [
                'required' => false,
            ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DossierData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
