
let categorie = document.querySelector('#inscription_categorie')
categorie.addEventListener("change", function () {
    let form = this.closest("form");
    let data = this.name + "=" + this.value

    fetch(form.action, {
        method: form.getAttribute("method"),
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset:utf-8"
        }
    })
        .then(response => response.text())
        .then(html => {
            let content = document.createElement("html");
            content.innerHTML = html;
            let nouvelActivite = content.querySelector("#inscription_activite");
            let nouveauCours = content.querySelector("#inscription_cours");
            let nouveauDocument = content.querySelector("#inscription_document");
            if (document.querySelector('#inscription_activite').parentElement.localName === "form") {
                /** Ajout du champ activité */
                var newDiv = document.createElement("div");
                newDiv.classList.add('form-group');
                var label = document.createElement('label');
                label.classList.add('required');
                label.setAttribute("for", 'inscription_activite');
                label.innerHTML = "Activité";
                newDiv.appendChild(label);
                newDiv.appendChild(nouvelActivite);
                document.querySelector('#inscription_activite').replaceWith(newDiv);

            } else {
                document.querySelector('#inscription_activite').replaceWith(nouvelActivite);
                let documentDiv = document.querySelector('#inscription_document').parentElement;
                if (documentDiv.localName !== "form") {
                    documentDiv.remove();
                    let boutonAjouter = document.querySelector('#inscription_ajouter').parentElement;
                    boutonAjouter.remove();
                    let demandeDiv = document.querySelector('#inscription_demandeParticuliere').parentElement
                    document.forms.inscription.insertBefore(nouveauDocument, demandeDiv);
                }
                var element = document.getElementById('inscription_cours');
                if (typeof (element) != 'undefined' && element != null) {
                    let coursDiv = document.querySelector('#inscription_cours').parentElement;
                    if (coursDiv.localName === "div") {
                        coursDiv.remove();
                        let doc = document.querySelector('#inscription_document');

                        document.forms.inscription.insertBefore(nouveauCours, doc);
                    }
                }
            }
            let act = document.querySelector('#inscription_activite')
            act.addEventListener("change", activite)
        })
})
let act = document.querySelector('#inscription_activite')
act.addEventListener("change", activite)
function activite() {
    let act = document.querySelector('#inscription_activite')
    let form = act.closest("form");
    let data = act.name + "=" + act.value

    fetch(form.action, {
        method: form.getAttribute("method"),
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset:utf-8"
        }
    })
        .then(response => response.text())
        .then(html => {
            let content = document.createElement("html");
            content.innerHTML = html;

            var element = document.getElementById('inscription_cours');
            if (typeof (element) != 'undefined' && element != null) {
                let nouveauCours = content.querySelector("#inscription_cours");
                if (document.querySelector('#inscription_cours').parentElement.localName === "form") {
                    /** Ajout du Champ cours */
                    if (nouveauCours.localName !== "input") {
                        var newDiv = document.createElement("div");
                        newDiv.classList.add('form-group');
                        var label = document.createElement('label');
                        label.classList.add('required');
                        label.setAttribute("for", 'inscription_cours');
                        label.innerHTML = "Cours";
                        newDiv.appendChild(label);
                        newDiv.appendChild(nouveauCours);
                        document.querySelector('#inscription_cours').replaceWith(newDiv);
                    }
                } else {
                    let coursDiv = document.querySelector('#inscription_cours').parentElement;
                    if (coursDiv.localName === "div") {
                        document.querySelector('#inscription_cours').replaceWith(nouveauCours);
                    }
                }
            }

            let nouveauDocument = content.querySelector("#inscription_document");
            if (document.querySelector('#inscription_document').parentElement.localName === "form") {
                /** Ajout du Champ document */
                if (nouveauDocument.localName !== "input") {
                    var newDiv = document.createElement("div");
                    newDiv.classList.add('form-group');
                    var label = document.createElement('label');
                    label.setAttribute("for", 'inscription_document');
                    label.innerHTML = "Document";
                    newDiv.appendChild(label);
                    newDiv.appendChild(nouveauDocument);
                    document.querySelector('#inscription_document').replaceWith(newDiv);
                    let boutonAjouter = content.querySelector('#inscription_ajouter').parentElement;
                    let demandeDiv = document.querySelector('#inscription_demandeParticuliere').parentElement
                    document.forms.inscription.insertBefore(boutonAjouter, demandeDiv);
                }
            } else {
                let documentDiv = document.querySelector('#inscription_document').parentElement;
                if (documentDiv.localName !== "form") {
                    documentDiv.remove();
                    let boutonAjouter = document.querySelector('#inscription_ajouter').parentElement;
                    boutonAjouter.remove();
                    let demandeDiv = document.querySelector('#inscription_demandeParticuliere').parentElement
                    document.forms.inscription.insertBefore(nouveauDocument, demandeDiv);
                }
            }
        })
}