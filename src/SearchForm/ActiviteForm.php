<?php

namespace App\SearchForm;

use App\Entity\Section;
use App\Entity\CategorieSection;
use App\SearchData\ActiviteData;
use App\Entity\CategorieActivite;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class ActiviteForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('q', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Rechercher',
                ],
            ])
            ->add('section', EntityType::class, [
                'required' => false,
                'class' => CategorieSection::class,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.nom', 'ASC');
                },
            ])
            ->add('categorieAge', EntityType::class, [
                'required' => false,
                'class' => CategorieActivite::class,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.designation', 'ASC');
                },
            ])

            ->add('condition', ChoiceType::class, [
                'required' => false,
                'label' => 'Sous condition',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('certificat', ChoiceType::class, [
                'required' => false,
                'label' => 'Certificat obligatoire',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])


            ->add('nb', IntegerType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ActiviteData::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
