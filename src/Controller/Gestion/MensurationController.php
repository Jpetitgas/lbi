<?php

namespace App\Controller\Gestion;

use App\Entity\Mensuration;
use App\Form\MensurationType;
use App\Repository\MensurationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/mensuration")
 */
class MensurationController extends AbstractController
{
    /**
     * @Route("/", name="gestion_mensuration_index", methods={"GET"})
     */
    public function index(MensurationRepository $mensurationRepository): Response
    {
        return $this->render('gestion/mensuration/index.html.twig', [
            'mensurations' => $mensurationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="gestion_mensuration_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $mensuration = new Mensuration();
        $form = $this->createForm(MensurationType::class, $mensuration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($mensuration);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_mensuration_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gestion/mensuration/new.html.twig', [
            'mensuration' => $mensuration,
            'form' => $form,
        ]);
    }



    /**
     * @Route("/{id}/edit", name="gestion_mensuration_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Mensuration $mensuration, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(MensurationType::class, $mensuration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('gestion_mensuration_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gestion/mensuration/edit.html.twig', [
            'mensuration' => $mensuration,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="gestion_mensuration_delete", methods={"POST"})
     */
    public function delete(Request $request, Mensuration $mensuration, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$mensuration->getId(), $request->request->get('_token'))) {
            $entityManager->remove($mensuration);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_mensuration_index', [], Response::HTTP_SEE_OTHER);
    }
}
