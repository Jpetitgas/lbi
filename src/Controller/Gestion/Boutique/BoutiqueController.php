<?php

namespace App\Controller\Gestion\Boutique;

use App\Repository\PaiementBoutiqueRepository;
use App\Repository\PurchaseRepository;
use App\Service\EnCours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class BoutiqueController extends AbstractController
{
    /**
     * @Route("gestion/boutique", name="gestion_boutique")
     */
    public function boutique(Request $request, EnCours $enCours, PaiementBoutiqueRepository $paiementBoutiqueRepository, PurchaseRepository $purchaseRepository, ChartBuilderInterface $chartBuilder): Response
    {
        // graphiquecommande

        $produit = [];
        $total=0;
        $saison = $enCours->saison();
        $purchases = $purchaseRepository->findBy(['saison' => $saison]);
        foreach ($purchases as $purchase) {
            foreach ($purchase->getPurchaseItems() as $item) {
                if (array_key_exists($item->getProductName(), $produit)) {
                    $produit[$item->getProductName()] += $item->getQuantity();
                } else {
                    $produit[$item->getProductName()] = $item->getQuantity();
                }
                $total+=$item->getTotal();
            }
        }
        $chartProduit = $chartBuilder->createChart(Chart::TYPE_BAR);
        $chartProduit->setData([
            'labels' => array_keys($produit),
            'datasets' => [
                [
                    'label' => 'Produit',
                    'backgroundColor' => 'rgb(209, 205, 206)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($produit),
                ],
            ],
        ]);
        $chartProduit->setOptions([
            'scales' => [
                            'yAxes' => [
                                ['ticks' => ['min' => 0]],
                            ],
                        ],
        ]);

        $paiements=$paiementBoutiqueRepository->findBy(['saison'=>$saison]);
        $totalPaiement=0;
        foreach ($paiements as $paiement) {
            $totalPaiement+=$paiement->getMontant();
        }

        return $this->render('gestion/boutique/boutique.html.twig', [
            'purchases'=>$purchases,
            'totalPaiement'=>$totalPaiement,
            'total'=>$total,

            'chartProduit' => $chartProduit,
        ]);
    }
}
