<?php

namespace App\Controller\Gestion;

use App\Form\ReglagesType;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReglagesController extends AbstractController
{
    /**
    * @Route("/gestion/reglages", name="gestion_reglages")
    */
    public function index(Request $request): Response
    {
        $value = Yaml::parseFile('../config/app/app.yaml');
        $data=$value['parameters'];
        $form = $this->createForm(ReglagesType::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data=$form->getData();
            $value['parameters'] = $data;
            $yaml = Yaml::dump($value);
            file_put_contents('../config/app/app.yaml', $yaml);
            return $this->redirectToRoute('gestion');
        }

        return $this->render('gestion/reglages/reglages.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
