let valider = document.querySelectorAll(".validationBulletin")
for (let bouton of valider) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/dossier/validationBulletin/' + this.dataset.id)
        xmlhttp.send()
    })
}
let verrouille = document.querySelectorAll(".verrouille")
for (let bouton of verrouille) {
    bouton.addEventListener("click", function () {
        let xmlhttp = new XMLHttpRequest;
        xmlhttp.open("get", '/gestion/dossier/verrouille/' + this.dataset.id)
        xmlhttp.send()
    })
}

/*let mail = document.getElementById('mail')

mail.addEventListener("click", function () {
    let selecteds = document.querySelectorAll(".selected")
    let ids = [];
    for (let selected of selecteds) {
        ids.push(selected.querySelector('.sorting_1').textContent);
    }
    if (ids.length) {
        window.location.href = "/gestion/correspondance/message/" + ids.toString();
    } else {
        alert('Vous devez d\'abord selectionner une ligne');
    }
});*/
