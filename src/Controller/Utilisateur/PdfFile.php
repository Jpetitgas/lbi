<?php

namespace App\Controller\Utilisateur;

use App\Entity\Evenement;
use App\Repository\DossierRepository;
use App\Repository\EvenementRepository;
use App\Repository\InformationRepository;
use App\Repository\OptionPurchaseRepository;
use App\Repository\PurchaseRepository;
use App\Service\EvenementService;
use App\Service\InfosDossierEncours;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 *  @Route("/utilisateur/pdf", name="utilisateur_pdf")
 */
class PdfFile extends AbstractController
{
    /**
     *  @Route("/bulletin/{id_dossier}", name="_bulletin")
     */
    public function bulletin($id_dossier, InformationRepository $informationRepository, InfosDossierEncours $infosDossierEncours, DossierRepository $dossierRepository)
    {
        $dossier = $dossierRepository->find($id_dossier);
        $information = $informationRepository->findOneBy([]);
        $cotisation = $infosDossierEncours->cotisation($dossier);
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        /*return $this->render('pdf/bulletin.html.twig',[
             'dossier'=>$dossier,
             'information'=>$information,
             'cotisation'=>$cotisation
         ]);*/
        $html = $this->renderView('pdf/bulletin.html.twig', [
            'dossier' => $dossier,
            'information' => $information,
            'cotisation' => $cotisation,
        ]);

        $options = new Options();
        $options->setIsHtml5ParserEnabled(true);

        $dompdf = new Dompdf($options);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream('bulletin d\'inscription', ['Attachment' => 1]);

        return  new Response("Votre bulletin d'inscription a été généré");
    }

    /**
     *  @Route("/facture/{id_dossier}", name="_facture")
     */
    public function facture($id_dossier, InformationRepository $informationRepository, InfosDossierEncours $infosDossierEncours, DossierRepository $dossierRepository)
    {
        $dossier = $dossierRepository->find($id_dossier);
        $information = $informationRepository->findOneBy([]);
        $cotisation = $infosDossierEncours->cotisation($dossier);
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        $html = $this->renderView('pdf/facture.html.twig', [
            'dossier' => $dossier,
            'information' => $information,
            'cotisation' => $cotisation,
        ]);

        $options = new Options();
        $options->setIsHtml5ParserEnabled(true);

        $dompdf = new Dompdf($options);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream('Facture', ['Attachment' => 1]);

        return  new Response('Votre facture a été généré');
    }

    /**
     *  @Route("/boutique/facture/{id_purchase}", name="_boutique_facture")
     */
    public function factureBoutique($id_purchase, InformationRepository $informationRepository, PurchaseRepository $purchaseRepository)
    {
        $purchase = $purchaseRepository->find($id_purchase);
        $information = $informationRepository->findOneBy([]);

        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        $html = $this->renderView('pdf/factureBoutique.html.twig', [
            'purchase' => $purchase,
            'information' => $information,
        ]);

        $options = new Options();
        $options->setIsHtml5ParserEnabled(true);

        $dompdf = new Dompdf($options);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream('Facture', ['Attachment' => 1]);

        return  new Response('Votre facture a été généré');
    }

    /**
     *  @Route("/billet/{id_evenement}", name="_billet")
     */
    public function billet($id_evenement, EvenementRepository $evenementRepository, Security $security, EvenementService $evenementService, OptionPurchaseRepository $optionPurchaseRepository)
    {
        $evenement = $evenementRepository->find($id_evenement);
        $utilisateur = $security->getUser();

        $totalReservation = $evenementService->montantTotalReservation($utilisateur, $evenement);
        $totalPaiement = $evenementService->montantTotalPaiement($security->getUser(), $evenement);
        $options = $optionPurchaseRepository->findByEvenementAndUtilisateur($evenement, $utilisateur);
        $totalOptions = 0;
        if ($options) {
            foreach ($options as $option) {
                $totalOptions += $option['price'] * $option['qty'];
            }
        }

        $solded = ($totalPaiement - ($totalReservation['totalReservation'] + $totalOptions)) >= 0 ? true : false;
        if ($solded == false) {
            return $this->render('security/accesRefuse.html.twig');
        }
        $reservations = $evenementService->reservation($utilisateur, $evenement);

        // return $this->render('pdf/billet.html.twig', [
        //     'evenement' => $evenement,
        //     'reservations' => $reservations,
        // ]);

        $html = $this->renderView('pdf/billet.html.twig', [
            'evenement' => $evenement,
            'reservations' => $reservations,
        ]);

        $options = new Options();
        $options->setIsRemoteEnabled(true);
        $options->setIsHtml5ParserEnabled(true);

        $pdf = new Dompdf($options);
        $pdf->setPaper('A4');

        $pdf->loadHtml($html);
        $pdf->render();

        // Output the generated PDF to Browser
        $pdf->stream('Billets', ['Attachment' => 1]);

        return  new Response('Vos billets ont été généré');
    }
}
