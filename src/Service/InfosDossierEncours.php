<?php

namespace App\Service;

use DateTime;
use App\Entity\Saison;
use App\Entity\Dossier;
use App\Entity\Pratiquant;
use App\Entity\Utilisateur;
use App\Entity\RegleTarifaire;
use App\Repository\SaisonRepository;
use App\Repository\ActiviteRepository;
use App\Repository\DossierRepository;
use App\Repository\PaiementRepository;
use App\Repository\ReductionRepository;
use App\Repository\PratiquantRepository;
use App\Repository\InscriptionRepository;
use App\Repository\RegleTarifaireRepository;
use Symfony\Component\Security\Core\Security;

class InfosDossierEncours
{
    protected $encours;
    protected $activiteRepository;
    protected $inscriptionRepository;
    protected $regleTarifaireRepository;
    protected $pratiquantRepository;
    protected $reductionRepository;
    protected $paiementRepository;
    protected $saisonRepository;
    protected $dossierRepository;
    

    public function __construct(EnCours $encours,DossierRepository $dossierRepository, SaisonRepository $saisonRepository, PaiementRepository $paiementRepository, ReductionRepository $reductionRepository, PratiquantRepository $pratiquantRepository, ActiviteRepository $activiteRepository, RegleTarifaireRepository $regleTarifaireRepository, InscriptionRepository $inscriptionRepository)
    {
        $this->encours = $encours;
        $this->dossierRepository= $dossierRepository;        
        $this->activiteRepository = $activiteRepository;
        $this->inscriptionRepository = $inscriptionRepository;
        $this->regleTarifaireRepository = $regleTarifaireRepository;
        $this->pratiquantRepository = $pratiquantRepository;
        $this->reductionRepository = $reductionRepository;
        $this->paiementRepository = $paiementRepository;
        $this->saisonRepository = $saisonRepository;
    }

    public function dossierEncoursExitant(Utilisateur $utilisateur)
    {
        return $this->dossierRepository->dossierCourant($utilisateur, $this->encours->saison());
    }

    public function aCompleterUtilisateur(Dossier $dossier)
    {
        $acompleter = [];
        $acompleter['bulletin'] = $this->bulletinPresent($dossier);
        $acompleter['certificat'] = $this->nbCertificatManquant($dossier);
        $acompleter['cotisation'] = $this->cotisationAJour($dossier);
        $acompleter['nbInscription'] = $this->nbInscription($dossier);

        return $acompleter;
    }

    public function nbInscription(Dossier $dossier)
    {
        $inscriptions = $this->inscriptionRepository->findBy(['dossier' => $dossier]);
        $nb = count($inscriptions);

        return $nb;
    }

    public function bulletinPresent(Dossier $dossier)
    {
        if (empty($dossier->getBulletinAdhesion())) {
            return 'Non';
        }

        return 'Oui';
    }

    public function nbCertificatManquant(Dossier $dossier)
    {
        $inscriptions = $this->inscriptionRepository->findBy(['dossier' => $dossier]);
        $nb = 0;
        foreach ($inscriptions as $inscription) {
            if ($inscription->getCertificatObligatoire() == 'true') {
                if (empty($inscription->getDocument())) {
                    ++$nb;
                }
            }
        }

        return $nb;
    }

    public function cotisationAJour(Dossier $dossier)
    {
        if ($this->solde($dossier) === 0.00) {
            return 'Oui';
        }

        return 'Non';
    }

    public function aFaireGestion(Dossier $dossier)
    {
        $afaire['bulletin'] = $this->bulletinValide($dossier);
        $afaire['certificat'] = $this->nbCertificatAValider($dossier);
        $afaire['assureur'] = $this->nbTransmissionAssureur($dossier);
        $afaire['complet'] = $this->dossierVerrouille($dossier);

        return $afaire;
    }

    public function bulletinValide(Dossier $dossier)
    {
        return $dossier->getBulletinValide() ? 'Oui' : 'Non';
    }

    public function nbCertificatAValider(Dossier $dossier)
    {
        $inscriptions = $this->inscriptionRepository->findBy(['dossier' => $dossier]);
        $nb = 0;
        foreach ($inscriptions as $inscription) {
            if ($inscription->getCertificatObligatoire() == true) {
                if ($inscription->getCertificatValide() == false && $inscription->getDocument()) {
                    ++$nb;
                }
            }
        }

        return $nb;
    }

    public function nbTransmissionAssureur(Dossier $dossier)
    {
        $inscriptions = $this->inscriptionRepository->findBy(['dossier' => $dossier]);
        $nb = 0;
        foreach ($inscriptions as $inscription) {
            if ($inscription->getTransmisAssureur() == false) {
                ++$nb;
            }
        }

        return $nb;
    }

    public function dossierVerrouille(Dossier $dossier)
    {
        return $dossier->getDossierVerrouille() ? 'Oui' : 'Non';
    }

    public function paiement(Dossier $dossier)
    {
        $totalDesPaiements['nonencaisse'] = 0;
        $totalDesPaiements['encaisse'] = 0;
        $totalDesPaiements['total'] = 0;
        foreach ($dossier->getPaiements() as $paiement) {
            if ($paiement->getEncaisse()) {
                $totalDesPaiements['encaisse'] += $paiement->getMontant();
            } else {
                $totalDesPaiements['nonencaisse'] += $paiement->getMontant();
            }
            $totalDesPaiements['total'] = $totalDesPaiements['encaisse'] + $totalDesPaiements['nonencaisse'];
        }

        return $totalDesPaiements;
    }

    public function solde(Dossier $dossier)
    {
        $cotisation = $this->cotisation($dossier)['total'];
        $paiement = $this->paiement($dossier)['total'];
        $solde = $cotisation - $paiement;

        return $solde;
    }

    public function cotisation(Dossier $dossier)
    {
        $cotisationActivite = $this->cotisationActivite($dossier);
        $ajout = $this->regleTarifaire($dossier);
        $aDeduire = $this->reductionDossier($dossier);
        $cotisation = [];
        $cotisation['totalReduction'] = $aDeduire['totalReduction'];
        $cotisation['totalActivite'] = $cotisationActivite;
        $cotisation['total'] = $cotisationActivite + $ajout['totalAjout'] + $aDeduire['totalReduction'];

        empty($ajout['detail']) ? $cotisation['detailTarif'] = 0 : $cotisation['detailTarif'] = $ajout['detail'];

        empty($aDeduire['detail']) ? $cotisation['detailReduction'] = 0 : $cotisation['detailReduction'] = $aDeduire['detail'];

        return $cotisation;
    }

    public function cotisationActivite(Dossier $dossier)
    {
        $inscriptions = $this->tousLesCours($dossier);
        $cotisationActivite = 0;
        foreach ($inscriptions as $inscription) {
            $cotisationActivite += $inscription->getActivite()->getTarif() + $inscription->getActivite()->getCotisation();
        }
        $cotisationActivite;

        return $cotisationActivite;
    }

    public function regleTarifaire(Dossier $dossier)
    {
        $ajout = [];
        $ajoutUtilisateur = 0;
        $ajoutPratiquant = 0;
        $ajoutActivite = 0;
        if (count($dossier->getInscriptions()) > 0) {
            $saison = $dossier->getSaison();

            // niveau UTILISATEUR
            $reglesUtilisateur = $this->regleTarifaireRepository->findByniveau($saison, 'UTILISATEUR');

            foreach ($reglesUtilisateur as $regleUtilisateur) {
                switch ($regleUtilisateur->getOperateur()->getNom()) {
                case 'UNIQUE':
                    $ajoutUtilisateur += $regleUtilisateur->getMontant();
                    $ajout['detail'][] = [$regleUtilisateur->getNom() => $regleUtilisateur->getMontant()];
                    break;
                case 'INTRA-COMMUNES':
                    if ($this->intra($regleUtilisateur, $dossier->getAdherent())) {
                        $ajoutUtilisateur += $regleUtilisateur->getMontant();
                        $ajout['detail'][] = [$regleUtilisateur->getNom() => $regleUtilisateur->getMontant()];
                    }
                break;
                case 'EXTRA-COMMUNES':
                    if ($this->extra($regleUtilisateur, $dossier->getAdherent())) {
                        $ajoutUtilisateur += $regleUtilisateur->getMontant();
                        $ajout['detail'][] = [$regleUtilisateur->getNom() => $regleUtilisateur->getMontant()];
                    }
                break;
            }
            }
            // niveau PRATIQUANT
            $reglesPratiquant = $this->regleTarifaireRepository->findByniveau($saison, 'PRATIQUANT');
            $inscriptions = $this->inscriptionRepository->findBy(['dossier' => $dossier]);
            $pratiquants = [];
            foreach ($inscriptions as $inscription) {
                $id_pratiquant = $inscription->getPratiquant()->getId();
                in_array($id_pratiquant, $pratiquants) ?: array_push($pratiquants, $id_pratiquant);
            }
            $nombreDePratiquant = count($pratiquants);

            foreach ($reglesPratiquant as $reglePratiquant) {
                switch ($reglePratiquant->getOperateur()->getNom()) {
                case 'UNIQUE':
                    if ($nombreDePratiquant > 0) {
                        $ajoutPratiquant += ($reglePratiquant->getMontant()) * $nombreDePratiquant;
                        $ajout['detail'][] = [$reglePratiquant->getNom() => $reglePratiquant->getMontant() * $nombreDePratiquant];
                    }
                    break;
                case 'NOMBRE>=':
                    if ($nombreDePratiquant >= $reglePratiquant->getParametre()) {
                        $ajoutPratiquant += $reglePratiquant->getMontant();
                        $ajout['detail'][] = [$reglePratiquant->getNom() => $reglePratiquant->getMontant()];
                    }

                    break;
                case 'INTRA-COMMUNES':
                    if ($this->intra($reglePratiquant, $dossier->getAdherent())) {
                        $ajoutUtilisateur += $reglePratiquant->getMontant() * $nombreDePratiquant;
                        $ajout['detail'][] = [$reglePratiquant->getNom() => $reglePratiquant->getMontant() * $nombreDePratiquant];
                    }
                break;
                case 'EXTRA-COMMUNES':
                    if ($this->extra($reglePratiquant, $dossier->getAdherent())) {
                        $ajoutPratiquant += $reglePratiquant->getMontant() * $nombreDePratiquant;
                        $ajout['detail'][] = [$reglePratiquant->getNom() => $reglePratiquant->getMontant() * $nombreDePratiquant];
                    }
                break;
                 case 'AGE<=':
                     foreach ($pratiquants as $id_pratiquant) {
                         /** @var Pratiquant $pratiquant */
                         $pratiquant = $this->pratiquantRepository->find($id_pratiquant);
                         if ($reglePratiquant->getParametre() >= $this->age($pratiquant->getNaissance())) {
                             $ajoutPratiquant += $reglePratiquant->getMontant();
                             $ajout['detail'][] = [$reglePratiquant->getNom() => $reglePratiquant->getMontant()];
                         }
                     }
                break;
                 case 'AGE>=':
                    foreach ($pratiquants as $id_pratiquant) {
                        /** @var Pratiquant $pratiquant */
                        $pratiquant = $this->pratiquantRepository->find($id_pratiquant);
                        $dif = $reglePratiquant->getParametre();
                        if ($reglePratiquant->getParametre() <= $this->age($pratiquant->getNaissance())) {
                            $ajoutPratiquant += $reglePratiquant->getMontant();
                            $ajout['detail'][] = [$reglePratiquant->getNom() => $reglePratiquant->getMontant()];
                        }
                    }
                break;
            }
            }

            // niveau Activité
            $reglesActivite = $this->regleTarifaireRepository->findByniveau($saison, 'ACTIVITE');

            foreach ($reglesActivite as $regleActivite) {
                switch ($regleActivite->getOperateur()->getNom()) {
                case 'UNIQUE':
                    $nombreActivite = count($dossier->getInscriptions());
                    if ($nombreActivite > 0) {
                        $ajoutActivite += ($regleActivite->getMontant()) * $nombreActivite;
                        $ajout['detail'][] = [$regleActivite->getNom() => $regleActivite->getMontant() * $nombreActivite];
                    }
                    break;
                case 'NOMBRE>=':
                    foreach ($pratiquants as $id_pratiquant) {
                        $nbDeCours = count($this->tousLesCoursPratiquant($id_pratiquant, $dossier));
                        if ($nbDeCours >= $regleActivite->getParametre()) {
                            $ajoutActivite += $regleActivite->getMontant();
                            $ajout['detail'][] = [$regleActivite->getNom() => $regleActivite->getMontant()];
                        }
                    }
            }
            }
        }
        $totalAjout = $ajoutUtilisateur + $ajoutPratiquant + $ajoutActivite;
        $ajout['totalAjout'] = $totalAjout;

        return $ajout;
    }

    public function reductionDossier($dossier)
    {
        $reductions = $this->reductionRepository->findBy(['dossier' => $dossier]);
        $aDeduire = [];
        $totalReduction = 0;
        foreach ($reductions as $reduction) {
            $totalReduction += $reduction->getMontant();
            $aDeduire['detail'][] = [$reduction->getDescription() => $reduction->getMontant()];
        }
        $aDeduire['totalReduction'] = $totalReduction;

        return $aDeduire;
    }

    public function tousLesCours(Dossier $dossier)
    {
        $inscriptions = $this->inscriptionRepository->findBy(['dossier' => $dossier]);

        return $inscriptions;
    }

    public function tousLesCoursPratiquant($id_pratiquant, Dossier $dossier)
    {
        $pratiquant = $this->pratiquantRepository->find($id_pratiquant);
        $inscriptions = $this->inscriptionRepository->findBy(['pratiquant' => $pratiquant, 'dossier' => $dossier]);

        return $inscriptions;
    }

    private function intra(RegleTarifaire $regle, Utilisateur $utilisateur)
    {
        $villes = $regle->getVilles();
        $villeUtilisateur = $utilisateur->getTown();
        foreach ($villes as $ville) {
            if ($ville === $villeUtilisateur) {
                return true;
            }
        }

        return false;
    }

    private function extra(RegleTarifaire $regle, Utilisateur $utilisateur)
    {
        $villes = $regle->getVilles();
        $villeUtilisateur = $utilisateur->getTown();
        foreach ($villes as $ville) {
            if ($ville !== $villeUtilisateur) {
                return true;
            }
        }

        return false;
    }

    private function age(DateTime $date)
    {
        /** @var Saison $saison */
        $saison = $this->encours->saison();
        $debutSaison = $saison->getDebut()->format('Y');
        $dateSaison = new DateTime("$debutSaison/12/31");

        $difference = $dateSaison->diff($date);

        $age = $difference->y;

        return  $age;
    }
}
