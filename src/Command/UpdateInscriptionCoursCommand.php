<?php

namespace App\Command;

use App\Entity\Inscription;
use App\Entity\Cours;
use App\Service\EnCours;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class UpdateInscriptionCoursCommand extends Command
{
    protected static $defaultName = 'app:update-inscription-cours';
    protected static $defaultDescription = 'Mettre à jour le cours des inscriptions pour les activités n\'ayant qu\'un seul cours';

    private $entityManager;
    private $enCours;

    public function __construct(EntityManagerInterface $entityManager, EnCours $enCours)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->enCours = $enCours;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $saisonCourante = $this->enCours->saison();

        // Demande de confirmation
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(
            sprintf("Nous allons mettre à jour les inscriptions de la saison: '%s'. Voulez-vous continuer ? (oui/non) [oui] ", $saisonCourante->getSaison()),
            true
        );

        if (!$helper->ask($input, $output, $question)) {
            $io->note('Opération annulée.');
            return Command::SUCCESS;
        }

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('i')
            ->from(Inscription::class, 'i')
            ->join('i.dossier', 'd')
            ->join('i.activite', 'a')
            ->leftJoin(Cours::class, 'c', 'WITH', 'c.activite = a AND c.saison = :saison')
            ->where('d.saison = :saison')
            ->andWhere('i.cours IS NULL')
            ->groupBy('i.id')
            ->having('COUNT(c.id) = 1')
            ->setParameter('saison', $saisonCourante);

        $inscriptions = $qb->getQuery()->getResult();

        $count = 0;
        foreach ($inscriptions as $inscription) {
            $cours = $this->entityManager->getRepository(Cours::class)->findOneBy([
                'activite' => $inscription->getActivite(),
                'saison' => $saisonCourante
            ]);

            if ($cours) {
                $inscription->setCours($cours);
                $count++;
            }
        }

        $this->entityManager->flush();

        $io->success(sprintf('%d inscriptions ont été mises à jour pour la saison %s.', $count, $saisonCourante->getSaison()));

        return Command::SUCCESS;
    }
}
