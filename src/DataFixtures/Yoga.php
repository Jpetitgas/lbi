<?php

namespace App\DataFixtures;

use App\Entity\Activite;
use App\DataFixtures\Parametres;
use App\DataFixtures\Utilisateurs;
use App\Entity\Cours;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class Yoga extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $activite = new Activite();
        $activite->setSaison($this->getReference('saison'))
            ->setSection($this->getReference('section_yoga'))
            ->setCategorie($this->getReference('categorie_adulte'))
            ->setCotisation(0)
            ->setDuree('1h30')
            ->setNbDePlace(12)
            ->setCertificatObligatoire(true)
            ->setNom("yoga-lun-adulte")
            ->setSousCondition(false)
            ->setTarif(140);
        $manager->persist($activite);
        $this->addReference('activite7', $activite);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant3'))
            ->setNom("yoga-lun-adulte-9h30-11h");
        $manager->persist($cours);
        $this->addReference('cours_7_1', $cours);
        $cours = new Cours();
        $cours->setSaison($this->getReference('saison'))
            ->setActivite($activite)
            ->setIntervenant($this->getReference('intervenant3'))
            ->setNom("yoga-lun-adulte-11h-12h30");
        $manager->persist($cours);
        $this->addReference('cours_7_2', $cours);


        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            Parametres::class,
            Utilisateurs::class,

        ];
    }
}
