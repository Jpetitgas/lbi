<?php

namespace App\Form;

use App\Entity\Section;
use App\Entity\Activite;
use App\Entity\CategorieSection;
use App\Entity\CategorieActivite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ActiviteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categorie', EntityType::class, [
                'class' => CategorieActivite::class,
                'label'=>'Catégorie d\'âge',
                'placeholder' => 'selectionner une catégorie d\'âge',
                'required' => true,
            ])
            ->add('section', EntityType::class, [
                'class' => Section::class,
                'label'=>'Libellé assureur',
                'placeholder' => 'selectionner un libellé',
                'required' => true,
            ])
            ->add('nom', TextType::class, [
                'label' => 'Désignation de l\'activité',
                'required'=>true
            ])
            ->add('duree', TextType::class, [
                'label' => 'Durée de l\'activité',
                'required'=>true
            ])
            ->add('nbDePlace')
            ->add('tarif', MoneyType::class, [
                'required'=>true
            ])
            ->add('sousCondition')
            ->add('certificatObligatoire')

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Activite::class,
        ]);
    }
}
