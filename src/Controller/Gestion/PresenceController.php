<?php

namespace App\Controller\Gestion;

use App\Entity\Cours;
use App\Entity\Presence;
use App\Service\EnCours;
use App\Form\PresenceType;
use Doctrine\ORM\EntityRepository;
use Symfony\UX\Chartjs\Model\Chart;
use App\SearchData\PresenceTotalData;
use App\SearchForm\PresenceTotalForm;
use App\Repository\PresenceRepository;
use App\Repository\InscriptionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/presence", name="gestion_presence")
 */
class PresenceController extends AbstractController
{
    /**
     * @Route("/{id_saison}/index", name="_index", methods={"GET","POST"})
     */
    public function index(PresenceRepository $presenceRepository, PaginatorInterface $paginator, InscriptionRepository $inscriptionRepository, Request $request, EnCours $enCours, ChartBuilderInterface $chartBuilder): Response
    {
        $saison = $enCours->saison();
        $presencesGeneral = $presenceRepository->findBy(['saison' => $saison], ['create_at' => 'DESC']);
        $data = new PresenceTotalData();
        $form = $this->createForm(PresenceTotalForm::class, $data);
        $form->handleRequest($request);

        $presencesGeneral = $paginator->paginate(
            $presenceRepository->presences($saison, $data),
            $request->query->getInt('page', 1),
            $data->nb
        );
        $id_saison = $saison->getId();
        $defaultData = [];
        $formSelection = $this->createFormBuilder($defaultData)
            ->add('cours', EntityType::class, [
                'class' => Cours::class,
                'placeholder' => 'selectionner un cours',
                'required' => true,
                'query_builder' => function (EntityRepository $er) use ($id_saison) {
                    return $er->createQueryBuilder('p')
                                    ->where('p.saison = :saison')
                                    ->orderBy('p.nom', "ASC")
                                    ->setParameter('saison', $id_saison);
                },
            ])
            ->add('envoyer', SubmitType::class, [
                'label' => 'Afficher le graphique',
                'attr' => [
                    'class' => 'btn btn-primary',
                ], ])
            ->getForm();

        $formSelection->handleRequest($request);

        if ($formSelection->isSubmitted() && $formSelection->isValid()) {
            $cours = $formSelection->getData()['cours'];
            $labels = [];
            $nb = [];
            $inscrits = $inscriptionRepository->findBy(['cours' => $cours]);
            $presences = $presenceRepository->presenceParCours($cours, $saison);
            foreach ($presences as $presence) {
                array_push($labels, $presence['nom'].' '.$presence['prenom']);
                array_push($nb, $presence['nb']);
            }
            $inscrits = $inscriptionRepository->findBy(['cours' => $cours]);
            foreach ($inscrits as $inscrit) {
                if (!in_array($inscrit->getPratiquant()->getNom().' '.$inscrit->getPratiquant()->getPrenom(), $labels)) {
                    array_push($labels, $inscrit->getPratiquant()->getNom().' '.$inscrit->getPratiquant()->getPrenom());
                    array_push($nb, 0);
                }
            }
            $chart = $chartBuilder->createChart(Chart::TYPE_BAR);

            $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Nb de cours pris',
                    'backgroundColor' => 'rgb(209, 205, 206)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $nb,
                ],
            ],
        ])
            ->setOptions([
                'scales' => [
                    'yAxes' => [
                    ['ticks' => ['min' => 0]],
                ],
                ],
            ]);

            return  $this->render('gestion/presence/index.html.twig', [
                'saison' => $saison,
                'presences' => $presencesGeneral,
                'formSelection' => $formSelection->createView(),
                'chart' => $chart,
                'form'=>$form->createView()
            ]);
        }

        return $this->render('gestion/presence/index.html.twig', [
            'saison' => $saison,
            'presences' => $presencesGeneral,
            'formSelection' => $formSelection->createView(),
            'form'=>$form->createView()
            ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $presence = new Presence();
        $form = $this->createForm(PresenceType::class, $presence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($presence);
            $entityManager->flush();

            return $this->redirectToRoute('gestion_presence_index');
        }

        return $this->render('gestion/presence/new.html.twig', [
            'presence' => $presence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Presence $presence): Response
    {
        if ($this->isCsrfTokenValid('delete'.$presence->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($presence);
            $entityManager->flush();
        }
        $id_saison = $presence->getSaison()->getId();

        return $this->redirectToRoute('gestion_presence_index', ['id_saison' => $id_saison]);
    }
}
