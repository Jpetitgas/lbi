<?php

namespace App\Controller\Gestion;

use App\Entity\Messages;
use App\Service\EnCours;
use App\Form\RepondreType;
use App\Service\SendEmail;
use App\Form\MessagesGestionType;
use App\Message\SendEmailMessage;
use Symfony\Component\Mime\Message;
use App\Repository\DossierRepository;
use App\Form\RepondreGestionnaireType;
use App\Repository\MessagesRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/gestion/message", name="gestion_message")
 */
class _MessageController extends AbstractController
{
    /**
     * @Route("/reception", name="_reception")
     */
    public function reception(MessagesRepository $messagesRepository): Response
    {
        $nbNonLu = $messagesRepository->nonLu($this->getUser());
        $messages = $messagesRepository->findBy(['destinataire' => $this->getUser()], ['id' => 'DESC']);

        return $this->render('gestion/message/reception.html.twig', [
            'messages' => $messages,
        ]);
    }

    /**
     * @Route("/envoi", name="_envoi")
     */
    public function envoi(MessagesRepository $messagesRepository): Response
    {
        $messages = $messagesRepository->findBy(['expediteur' => $this->getUser()], ['id' => 'DESC']);

        return $this->render('gestion/message/envoi.html.twig', [
            'messages' => $messages,
        ]);
    }

    /**
     * @Route("/show/reception/{{id}}", name="_reception_show")
     */
    public function showReception(Messages $message, Request $request, EnCours $enCours, DossierRepository $dossierRepository): Response
    {
        $saison = $enCours->saison();
        $dossier = $dossierRepository->findOneby(['saison' => $saison, 'adherent' => $message->getExpediteur()]);

        $message->setIsRead(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        return $this->render('gestion/message/receptionShow.html.twig', [
            'message' => $message,
            'dossier' => $dossier
        ]);
    }

    /**
     * @Route("/show/envoi/{{id}}", name="_envoi_show")
     */
    public function showEnvoi(Messages $message, Request $request): Response
    {
        return $this->render('gestion/message/envoiShow.html.twig', compact('message'));
    }

    /**
     * @Route("/new", name="_new")
     */
    public function new(Request $request, SendEmail $sendEmail, MessageBusInterface $messageBus): Response
    {
        $message = new Messages();
        $formMessage = $this->createForm(MessagesGestionType::class, $message);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $message->setExpediteur($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();


            $sendEmail->send([
                'recipient_email' => $formMessage->get('destinataire')->getData()->getEmail(),
                'sujet' => 'Vous avez reçu un nouveau message',
                'htmlTemplate' => 'email/infoMessage.html.twig',
                'destinataire' => $formMessage->get('destinataire'),
                'context' => [],
            ]);

            $this->addFlash('success', 'Votre message a été envoyé');

            return $this->redirectToRoute('gestion_message_reception');
        }

        return $this->render('gestion/message/new.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }

    /**
     * @Route("/repondre/{id_destinaire}/{titre}", name="_repondre")
     */
    public function repondre($id_destinaire, $titre, Request $request, SendEmail $sendEmail, MessageBusInterface $messageBus, UtilisateurRepository $utilisateurRepository): Response
    {
        $destinataire = $utilisateurRepository->find($id_destinaire);
        $message = new Messages();
        $formMessage = $this->createForm(RepondreGestionnaireType::class, $message);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $message->setExpediteur($this->getUser())
                ->setDestinataire($destinataire)
                ->setTitre($titre);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();


            $sendEmail->send([
                'recipient_email' => $destinataire->getEmail(),
                'sujet' => 'Vous avez reçu un nouveau message',
                'htmlTemplate' => 'email/infoMessage.html.twig',
                'destinataire' => $destinataire,
                'context' => [],
            ]);
            $this->addFlash('success', 'Votre message a été envoyé');

            return $this->redirectToRoute('gestion_message_reception');
        }

        return $this->render('gestion/message/new.html.twig', [
            'formMessage' => $formMessage->createView(),
        ]);
    }

    /**
     * @Route("/marque/{id}", name="_marque", methods={"GET"})
     */
    public function marque($id, MessagesRepository $messagesRepository): Response
    {
        $message = $messagesRepository->find($id);
        $message->setMarque(($message->getMarque() ? '0' : '1'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        return $this->redirectToRoute('gestion_message_reception_show', ['id' => $message->getId()]);
    }
}
