<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ActiviteRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=ActiviteRepository::class)
 *
 */
class Activite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity=Section::class, inversedBy="activites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $section;

    /**
     * @ORM\ManyToOne(targetEntity=Saison::class, inversedBy="activites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $saison;

    /**
     * @ORM\OneToMany(targetEntity=Inscription::class, mappedBy="activite",fetch="EAGER")
     */
    private $inscriptions;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sousCondition;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieActivite::class, inversedBy="activites")
     */
    private $categorie;

    /**
     * @ORM\Column(type="boolean")
     */
    private $certificatObligatoire;

    /**
     * @ORM\Column(type="integer")
     */
    private $tarif;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbDePlace;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cotisation;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class, mappedBy="activite",fetch="EAGER")
     */
    private $cours;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $duree;

    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
        $this->cours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSection(): ?Section
    {
        return $this->section;
    }

    public function setSection(?Section $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNom();
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->setActivite($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->removeElement($inscription)) {
            // set the owning side to null (unless already changed)
            if ($inscription->getActivite() === $this) {
                $inscription->setActivite(null);
            }
        }

        return $this;
    }

    public function getSousCondition(): ?bool
    {
        return $this->sousCondition;
    }

    public function setSousCondition(bool $sousCondition): self
    {
        $this->sousCondition = $sousCondition;

        return $this;
    }

    public function getCategorie(): ?CategorieActivite
    {
        return $this->categorie;
    }

    public function setCategorie(?CategorieActivite $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getCertificatObligatoire(): ?bool
    {
        return $this->certificatObligatoire;
    }

    public function setCertificatObligatoire(bool $certificatObligatoire): self
    {
        $this->certificatObligatoire = $certificatObligatoire;

        return $this;
    }

    public function getTarif(): ?int
    {
        return $this->tarif;
    }

    public function setTarif(int $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getNbDePlace(): ?int
    {
        return $this->nbDePlace;
    }

    public function setNbDePlace(int $nbDePlace): self
    {
        $this->nbDePlace = $nbDePlace;

        return $this;
    }

    public function getCotisation(): ?int
    {
        return $this->cotisation;
    }

    public function setCotisation(?int $cotisation): self
    {
        $this->cotisation = $cotisation;

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setActivite($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->removeElement($cour)) {
            // set the owning side to null (unless already changed)
            if ($cour->getActivite() === $this) {
                $cour->setActivite(null);
            }
        }

        return $this;
    }

    public function getDuree(): ?string
    {
        return $this->duree;
    }

    public function setDuree(string $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function __clone()
    {
        $this->id = null;
    }
}
