<?php

namespace App\Repository;

use App\Entity\ConsultationChat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConsultationChat|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConsultationChat|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConsultationChat[]    findAll()
 * @method ConsultationChat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsultationChatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsultationChat::class);
    }

    // /**
    //  * @return ConsultationChat[] Returns an array of ConsultationChat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConsultationChat
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
