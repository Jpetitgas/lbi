<?php

namespace App\Controller\Gestion;

use App\Entity\Reduction;
use App\Form\ReductionType;
use App\Repository\DossierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/reduction", name="gestion_reduction")
 */
class ReductionController extends AbstractController
{
    /**
     * @Route("/{id}/edit", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Reduction $reduction): Response
    {
        $formReduction = $this->createForm(ReductionType::class, $reduction);
        $formReduction->handleRequest($request);
        $dossier=$reduction->getDossier();
        if ($formReduction->isSubmitted() && $formReduction->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $ancre = 'reduction_none_none';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $reduction->getDossier()->getId(), 'ancre' => $ancre]);
        }

        return $this->render('gestion/reduction/edit.html.twig', [
            'dossier'=>$dossier,
            'reduction' => $reduction,
            'formReduction' => $formReduction->createView(),
        ]);
    }

    /**
     * @Route("/new/{id_dossier}", name="_new", methods={"GET","POST"})
     */
    public function new($id_dossier, Request $request, DossierRepository $dossierRepository): Response
    {
        $reduction = new Reduction();
        $formReduction = $this->createForm(ReductionType::class, $reduction);
        $formReduction->handleRequest($request);
        $dossier = $dossierRepository->find($id_dossier);
        if ($formReduction->isSubmitted() && $formReduction->isValid()) {
            $reduction->setDossier($dossier);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reduction);
            $entityManager->flush();

            $ancre = 'reduction_none_none';

            return $this->redirectToRoute('gestion_dossier_edit', ['id' => $id_dossier, 'ancre' => $ancre]);
        }

        return $this->render('gestion/reduction/new.html.twig', [
            'dossier'=>$dossier,
            'reduction' => $reduction,
            'formReduction' => $formReduction->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Reduction $reduction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reduction->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($reduction);
            $entityManager->flush();
        }
        $this->addFlash('success', 'La réduction a été supprimée');
        $ancre = 'reduction_none_none';

        return $this->redirectToRoute('gestion_dossier_edit', ['id' => $reduction->getDossier()->getId(), 'ancre' => $ancre]);
    }
}
