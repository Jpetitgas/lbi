<?php

namespace App\Form;

use App\Entity\Pratiquant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PratiquantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sexe', ChoiceType::class, [
                'choices' => [
                    'F' => 'F',
                    'M' => 'M',
                ], ])
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('naissance', BirthdayType::class, [
                 'widget'=> 'single_text',
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pratiquant::class,
        ]);
    }
}
