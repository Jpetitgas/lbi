<?php

namespace App\Controller\Gestion\Evenement;

use App\Entity\Container;
use App\Entity\Evenement;
use App\Entity\Place;
use App\Form\BilletType;
use App\Form\EvenementType;
use App\Repository\EvenementRepository;
use App\Repository\OptionRepository;
use App\Repository\PlaceRepository;
use App\Repository\UtilisateurRepository;
use App\SearchData\PlaceData;
use App\SearchForm\PlaceForm;
use App\Service\EnCours;
use App\Service\EvenementService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("gestion/evenement", name="gestion_evenement")
 */
class EvenementController extends AbstractController
{
    /**
     * @Route("/index", name="_index", methods={"GET"})
     */
    public function index(EvenementRepository $evenementRepository, EnCours $enCours): Response
    {
        $saison = $enCours->saison();
        $evenements = $evenementRepository->findBy([],['id' => 'DESC']);

        return $this->render('gestion/evenement/index.html.twig', [
            'evenements' => $evenements,
        ]);
    }

    /**
     * @Route("/relance/{id}", name="_relance", methods={"GET"})
     */
    public function relance(Evenement $evenement, EvenementService $evenementService): Response
    {
        if ($evenement->getSection()) {
            $ids = $evenementService->utilisateurSansReservation($evenement);
            if ($ids != '') {
                return $this->redirectToRoute('gestion_correspondance_message', ['ids' => $ids], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->redirectToRoute('gestion_evenement_index');
    }

    /**
     * @Route("/liste/{id}", name="_liste", methods={"GET"})
     */
    public function liste(Evenement $evenement, EvenementService $evenementService, Request $request, PlaceRepository $placeRepository, PaginatorInterface $paginator): Response
    {
        $data = new PlaceData();
        $form = $this->createForm(PlaceForm::class, $data);
        $form->handleRequest($request);
        $liste = $placeRepository->findPlaceByEvenementReserved($evenement, $data);
        $newList = $liste->getResult();
        /** @var Place $list */
        foreach ($newList as $list) {
            $list->setSolde($evenementService->solded($list->getUtilisateur(), $list->getContainer()->getEvenement()));
            if ($data->reglementAJour !== null && !($data->reglementAJour === $list->getSolde())) {
                unset($newList[array_search($list, $newList)]);
            }
        }

        $reservations = $paginator->paginate(
            $newList,
            $request->query->getInt('page', 1),
            $data->nb
        );

        return $this->render('gestion/evenement/liste.html.twig', [
            'reservations' => $reservations,
            'evenement' => $evenement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $evenement = new Evenement();
        $formEvenement = $this->createForm(EvenementType::class, $evenement);

        $formEvenement->handleRequest($request);

        if ($formEvenement->isSubmitted() && $formEvenement->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($evenement);
            $entityManager->flush();
            $saison = $evenement->getSaison();

            return $this->redirectToRoute('gestion_evenement_index', ['id_saison' => $saison->getid()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('gestion/evenement/new.html.twig', [
            'evenement' => $evenement,
            'formEvenement' => $formEvenement->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/{ancre}", name="_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Evenement $evenement, $ancre, UtilisateurRepository $utilisateurRepository, OptionRepository $optionRepository): Response
    {
        $options = $optionRepository->findBy(['evenement' => $evenement]);
        $formEvenement = $this->createForm(EvenementType::class, $evenement);

        $formEvenement->handleRequest($request);

        if ($formEvenement->isSubmitted() && $formEvenement->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $saison = $evenement->getSaison();

            return $this->redirectToRoute('gestion_evenement_edit', ['id' => $evenement->getid(), 'ancre' => 'info'], Response::HTTP_SEE_OTHER);
        }
        $formBillet = $this->createForm(BilletType::class, $evenement);

        $formBillet->handleRequest($request);

        if ($formBillet->isSubmitted() && $formBillet->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $saison = $evenement->getSaison();

            return $this->redirectToRoute('gestion_evenement_edit', ['id' => $evenement->getid(), 'ancre' => 'billet'], Response::HTTP_SEE_OTHER);
        }

        return $this->render('gestion/evenement/edit.html.twig', [
            'ancre' => $ancre,
            'utilisateurs' => $utilisateurRepository->findAll(),
            'evenement' => $evenement,
            'options' => $options,
            'formEvenement' => $formEvenement->createView(),
            'formBillet' => $formBillet->createView(),
        ]);
    }

    /**
     * @Route("/plan/{id}", name="_plan", methods={"GET","POST"})
     */
    public function planEdit($id, EnCours $enCours, EvenementRepository $evenementRepository): Response
    {
        $evenement = $evenementRepository->find($id);
        if (!$evenement) {
            $saison = $enCours->saison();

            return $this->redirectToRoute('gestion_evenement_index', ['id_saison' => $saison->getid()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('gestion/evenement/plan/edit.html.twig', [
            'evenement' => $evenement,
        ]);
    }

    /**
     * @Route("/plan/container/position/{id}", name="_plan_container_position", methods={"GET"})
     */
    public function containerPosition($id, EvenementRepository $evenementRepository): Response
    {
        $evenement = $evenementRepository->find($id);
        if ($evenement) {
            $evenement->setPlan(($evenement->getPlan() ? '0' : '1'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($evenement);
            $em->flush();
        }

        return new Response('Le plan a été enregistré', Response::HTTP_OK);
    }

    /**
     * @Route("/plan/data/{id}", name="_plan_data", methods={"GET","POST"})
     */
    public function evenementData(Evenement $evenement): Response
    {
        $data = [];

        foreach ($evenement->getContainers() as $c) {
            $container = [
                'id' => $c->getId(),
                'nom' => $c->getNom(),
                'x' => $c->getX(),
                'y' => $c->getY(),
                'angle' => $c->getAngle(),
                'largeur' => $c->getLargeur(),
                'hauteur' => $c->getHauteur(),
                'nbLigne' => $c->getNbLigne(),
            ];
            $places = [];
            foreach ($c->getPlaces() as $p) {
                array_push($places, $p->getNom());
            }
            array_push($container, $places);
            array_push($data, $container);
        }

        $response = new JsonResponse($data);

        return $response;
    }

    /**
     * @Route("/{id}", name="_delete", methods={"POST"})
     */
    public function delete(Request $request, Evenement $evenement): Response
    {
        $saison = $evenement->getSaison();
        if ($this->isCsrfTokenValid('delete'.$evenement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($evenement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_evenement_index', ['id_saison' => $saison->getid()], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/dimension/", name="_dimension", methods={"GET", "POST"})
     */
    public function position(Request $request, EvenementRepository $evenementRepository): Response
    {
        $params = json_decode($request->getContent(), true);
        extract($params);

        $evenement = $evenementRepository->find($id);
        if ($evenementRepository) {
            $evenement->setLargeur($largeur)
                ->setHauteur($hauteur);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        }

        return $this->redirectToRoute('gestion_evenement_plan_edit', ['id' => $id]);
    }

     /**
     * @Route("/dupliquer/{id}", name="_dupliquer", methods={"GET", "POST"})
     */
    public function dupliquer($id, EvenementRepository $evenementRepository)
    {
        $evenement = $evenementRepository->find($id);
        $evenementClone = clone $evenement;
        $em = $this->getDoctrine()->getManager();
        $em->persist($evenement);
        $em->flush();

        return $this->redirectToRoute('gestion_evenement_index');
    }
}
